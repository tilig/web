/// <reference types="vite-svg-loader" />
/// <reference types="vitest" />
/// <reference types="vite/client" />
/// <reference types="vue-router" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare const APP_VERSION: string
declare const COMMIT_REF: string
