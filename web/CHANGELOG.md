# Tilig Web Application Changelog

## Released

## 3.2.0

- [Allow 2FA edit on web](https://gitlab.com/subshq/clients/webapp/-/merge_requests/849)
- [Add more logging for the onboarding process](https://gitlab.com/subshq/clients/webapp/-/merge_requests/848)

## 3.1.1

- [Move item to folder](https://gitlab.com/subshq/clients/webapp/-/merge_requests/777)
- [Handle navigation failures due to updates](https://gitlab.com/subshq/clients/webapp/-/merge_requests/828)
- [Reset api session after user deletion / refresh token failure](https://gitlab.com/subshq/clients/webapp/-/merge_requests/825)
- [Fix intercom and import profile picture](https://gitlab.com/subshq/clients/webapp/-/merge_requests/819)

## 3.1.0

- [Add a feature to only import personal 1Password vaults](https://gitlab.com/subshq/clients/webapp/-/merge_requests/811)
- [Hide getting started for existing users](https://gitlab.com/subshq/clients/webapp/-/commit/9e032ec48f0c42701e6910a50b15e202f339c678)

## 3.0.0

- [Implement folders](https://gitlab.com/subshq/clients/webapp/-/merge_requests/777)
- [Create a team for personal users](https://gitlab.com/subshq/clients/webapp/-/merge_requests/793)
- [Hide squirrel campaign on the sidebar](https://gitlab.com/subshq/clients/webapp/-/merge_requests/791)

## 2.11

- [Remove duplicate empty items state image](https://gitlab.com/subshq/clients/webapp/-/merge_requests/795)
- [Disable the referral plushie until there's a better solution](https://gitlab.com/subshq/clients/webapp/-/merge_requests/802)
- [New landing page empty items state](https://gitlab.com/subshq/clients/webapp/-/merge_requests/778)
- [Some small CSS fixes from the extension item types MR also apply to web](https://gitlab.com/subshq/clients/webapp/-/merge_requests/800)
- [Getting started changes](https://gitlab.com/subshq/clients/webapp/-/merge_requests/773)

## 2.10.2

- [Add 1PUX imports](https://gitlab.com/subshq/clients/webapp/-/merge_requests/757)

## 2.10.1

- [Add videos for autofill and autosave](https://gitlab.com/subshq/clients/webapp/-/merge_requests/774)
- [Hide getting started link on safari and firefox](https://gitlab.com/subshq/clients/webapp/-/merge_requests/771)

## 2.10.0

- [Getting Started](https://gitlab.com/subshq/clients/webapp/-/merge_requests/729)

## 2.9.1

- [Remove leaderboard mask](https://gitlab.com/subshq/clients/webapp/-/merge_requests/765)

## 2.9.0

- [Improve leaderboard email mask](https://gitlab.com/subshq/clients/webapp/-/merge_requests/763)
- [Tilig referral program](https://gitlab.com/subshq/clients/webapp/-/merge_requests/732)

## 2.8.0

- [Show the tooltip when item is copied](https://gitlab.com/subshq/clients/webapp/-/merge_requests/744)
- [Support accepting invites without invite link](https://gitlab.com/subshq/clients/webapp/-/merge_requests/727)
- [Share items with contacts](https://gitlab.com/subshq/clients/webapp/-/merge_requests/724)
- [Confirm pending folder membership](https://gitlab.com/subshq/clients/webapp/-/merge_requests/731)

## 2.7.1

- [Store the landing page in Mixpanel](https://gitlab.com/subshq/clients/webapp/-/merge_requests/739)
- [Fix sidebar item info and title cut off](https://gitlab.com/subshq/clients/webapp/-/merge_requests/738)
- [Only show "Other" items when the user has custom items](https://gitlab.com/subshq/clients/webapp/-/merge_requests/736)
- [Change wording to team/company](https://gitlab.com/subshq/clients/webapp/-/merge_requests/734)

## 2.7.0

- [LastPass import improvements](https://gitlab.com/subshq/clients/webapp/-/merge_requests/722)
- [Fix and cleanup item icons](https://gitlab.com/subshq/clients/webapp/-/merge_requests/719)

### 2.6.0

- UI and auth improvements
- [Improve CSS](https://gitlab.com/subshq/clients/webapp/-/merge_requests/716)
- [Add additional context to auth and items stores](https://gitlab.com/subshq/clients/webapp/-/merge_requests/714)
- [Onboarding tweaks to increase activation](https://gitlab.com/subshq/clients/webapp/-/merge_requests/711)
- [Team UI fixes](https://gitlab.com/subshq/clients/webapp/-/merge_requests/708)
- [Improve item creation modals](https://gitlab.com/subshq/clients/webapp/-/merge_requests/648)
- [Make entire area of the card clickable](https://gitlab.com/subshq/clients/webapp/-/merge_requests/681)

### 2.5.0

- The first team/organization features!
- [Signup as an organization, including inviting team members](https://gitlab.com/subshq/clients/webapp/-/merge_requests/682)
- [Mixpanel events for that too](https://gitlab.com/subshq/clients/webapp/-/merge_requests/706)
- [UI Fixes for organization setup](https://gitlab.com/subshq/clients/webapp/-/merge_requests/707)
- [Encrypt folder share-link with folder keypair](https://gitlab.com/subshq/clients/webapp/-/merge_requests/691)

### 2.4.0

- [Fix copy button value (again)](https://gitlab.com/subshq/clients/webapp/-/merge_requests/685)
- [Use browser crypto API for legacy encryption](https://gitlab.com/subshq/clients/webapp/-/merge_requests/645)
- [Simplify Auth watcher logic](https://gitlab.com/subshq/clients/webapp/-/merge_requests/654)
- [Ensure firefox & safari users skip extension installation page](https://gitlab.com/subshq/clients/webapp/-/merge_requests/677)
- [Fix wrong route link](https://gitlab.com/subshq/clients/webapp/-/merge_requests/676)
- [Add the correct notes field class](https://gitlab.com/subshq/clients/webapp/-/merge_requests/666)
- [Truncate sidebar item name](https://gitlab.com/subshq/clients/webapp/-/merge_requests/656)
- [Encrypt folder share-link with folder keypair](https://gitlab.com/subshq/clients/webapp/-/merge_requests/662)

### 2.3.1

- [Fix copy button](https://gitlab.com/subshq/clients/webapp/-/merge_requests/661)
- [Try to convert to JSON, but handle anything](https://gitlab.com/subshq/clients/webapp/-/merge_requests/657)

### 2.3.0

- Some dependency updates
- [Single share feature](https://gitlab.com/subshq/clients/webapp/-/merge_requests/640)
- [Track audience (personal vs business)](https://gitlab.com/subshq/clients/webapp/-/merge_requests/651)
- [Refactor and rename form fields](https://gitlab.com/subshq/clients/webapp/-/merge_requests/633)
- [Add empty field component for consistency](https://gitlab.com/subshq/clients/webapp/-/merge_requests/635)

### 2.2.2

- [Improve imports](https://gitlab.com/subshq/clients/webapp/-/merge_requests/625)
- [Reset all stores on sign out](https://gitlab.com/subshq/clients/webapp/-/merge_requests/631)
- [Disable button when new item is being created](https://gitlab.com/subshq/clients/webapp/-/merge_requests/600)
- [Set firebase log level to debug](https://gitlab.com/subshq/clients/webapp/-/merge_requests/629)
- [Always reencrypt overview and details](https://gitlab.com/subshq/clients/webapp/-/merge_requests/623)
- [Fix onboarding always shows after signing in on Web](https://gitlab.com/subshq/clients/webapp/-/merge_requests/628)

### 2.2.1

- [Fix export file download](https://gitlab.com/subshq/clients/webapp/-/merge_requests/622)
- [Resolve "Allow deleting user from API and firebase"](https://gitlab.com/subshq/clients/webapp/-/merge_requests/624)
- [Improve error login handling](https://gitlab.com/subshq/clients/webapp/-/merge_requests/626)
- [Simplify import assistant chat](https://gitlab.com/subshq/clients/webapp/-/merge_requests/620)

### 2.2.0

- [WiFi and Credit Card style cleanups](https://gitlab.com/subshq/clients/webapp/-/merge_requests/602)
- [Remove unnecessary fields during secure note creation](https://gitlab.com/subshq/clients/webapp/-/merge_requests/544)
- [Add spinner to app install prompt when button is clicked](https://gitlab.com/subshq/clients/webapp/-/merge_requests/550)
- [Support for Credit Cards](https://gitlab.com/subshq/clients/webapp/-/merge_requests/560)
- [Resolve "Microsoft login button does appear on 1st page loading."](https://gitlab.com/subshq/clients/webapp/-/merge_requests/596)
- [Don't remove source maps on netlify](https://gitlab.com/subshq/clients/webapp/-/merge_requests/598)

### 2.1.2

- [Add better polling that supports updated and deleted items](https://gitlab.com/subshq/clients/webapp/-/merge_requests/593)
- [Actually copy the notes on the shared page](https://gitlab.com/subshq/clients/webapp/-/merge_requests/595)
- [Fix tracing for production](https://gitlab.com/subshq/clients/webapp/-/merge_requests/594)
- [Resolve "Disable legacy encryption for everything but login/1](https://gitlab.com/subshq/clients/webapp/-/merge_requests/592)

### 2.1.1

- [Remove 190 character limit](https://gitlab.com/subshq/clients/webapp/-/merge_requests/579)
- [Add support for WiFi items](https://gitlab.com/subshq/clients/webapp/-/merge_requests/534)
- [Mark share events as anonymous](https://gitlab.com/subshq/clients/webapp/-/merge_requests/585)
- [Add migration script to encrypt DEK with Libsodium keypair](https://gitlab.com/subshq/clients/webapp/-/merge_requests/565)

### 2.1.0

- [Add Microsoft login](https://gitlab.com/subshq/clients/webapp/-/merge_requests/527)

### 2.0.4

- [Fix a regression of copying not working in firefox](https://gitlab.com/subshq/clients/webapp/-/merge_requests/548)
- [Fix the copy button click error](https://gitlab.com/subshq/clients/webapp/-/merge_requests/172)
- [Resolve "Follow-up from "Create secret sharing landing page""](https://gitlab.com/subshq/clients/webapp/-/merge_requests/518)
- [Reduce concurrency during import](https://gitlab.com/subshq/clients/webapp/-/merge_requests/508)
- [Fix active state on sidebar tab](https://gitlab.com/subshq/clients/webapp/-/merge_requests/558)

### 2.0.3

- [Pick first item to redirect to from the filteredItems](https://gitlab.com/subshq/clients/webapp/-/merge_requests/554)
- [Skip onboarding extension install on firefox](https://gitlab.com/subshq/clients/webapp/-/merge_requests/542)
- [Add app version to account menu](https://gitlab.com/subshq/clients/webapp/-/merge_requests/549)
- [Fix verbose brand logging](https://gitlab.com/subshq/clients/webapp/-/merge_requests/543)

### 2.0.2

- [Web: Account Screen Improvements](https://gitlab.com/subshq/clients/webapp/-/merge_requests/399)
- [Use ranking and is_fetched in the brand search](https://gitlab.com/subshq/clients/webapp/-/merge_requests/539)
- [Show correct share link url based on environment](https://gitlab.com/subshq/clients/webapp/-/merge_requests/530)
- [Refactor AddItem](https://gitlab.com/subshq/clients/webapp/-/merge_requests/499)
- [Make item screen full on <desktop screens](https://gitlab.com/subshq/clients/webapp/-/merge_requests/516)
- [Web: Secure Notes Improvements](https://gitlab.com/subshq/clients/webapp/-/merge_requests/482)
- [Redirect to the next item in the list after deletion](https://gitlab.com/subshq/clients/webapp/-/merge_requests/501)
- [Validate the item type uris](https://gitlab.com/subshq/clients/webapp/-/merge_requests/519)
- [Web: Add secure notes events](https://gitlab.com/subshq/clients/webapp/-/merge_requests/483)

### 2.0.1

- [Web: Hotfix for textarea XSS vulnerability](https://gitlab.com/subshq/clients/webapp/-/merge_requests/520)

### 2.0.0

- [Create secret sharing landing page](https://gitlab.com/subshq/clients/webapp/-/merge_requests/515)
- [Go back to view page when back button is tapped](https://gitlab.com/subshq/clients/webapp/-/merge_requests/502)
- [Close menu when the menu item is clicked](https://gitlab.com/subshq/clients/webapp/-/merge_requests/505)
- [Build sharelink component](https://gitlab.com/subshq/clients/webapp/-/merge_requests/510)
- [Fix horizontal scroll on account page](https://gitlab.com/subshq/clients/webapp/-/merge_requests/503)
- [Resolve "Core: Use libsodium for asymmetrical encryption"](https://gitlab.com/subshq/clients/webapp/-/merge_requests/481)
- [Cache invalidation of ItemsStore in localstorage](https://gitlab.com/subshq/clients/webapp/-/merge_requests/492)
- [Web: Refactor item type routing](https://gitlab.com/subshq/clients/webapp/-/merge_requests/488)
- [Bug: Core: Unable to update website URL](https://gitlab.com/subshq/clients/webapp/-/merge_requests/497)
- [Use breakpoints instead of window size](https://gitlab.com/subshq/clients/webapp/-/merge_requests/496)
- [Fix issue with randomly failing OTP tests](https://gitlab.com/subshq/clients/webapp/-/merge_requests/484)
- [Web: Fix previous item showing up briefly after adding a new one](https://gitlab.com/subshq/clients/webapp/-/merge_requests/487)
- [Hotfix: Fix caching bug with template changes](https://gitlab.com/subshq/clients/webapp/-/merge_requests/491)
- [Clear items on sign out](https://gitlab.com/subshq/clients/webapp/-/merge_requests/489)
- [Web: Fix loading items on root path](https://gitlab.com/subshq/clients/webapp/-/merge_requests/486)

### 1.10

- [Web: Don't show loading screen if item id does not change](https://gitlab.com/subshq/clients/webapp/-/merge_requests/479)
- [Web: Support for other item types](https://gitlab.com/subshq/clients/webapp/-/merge_requests/424)
- [Migrate Legacy Items](https://gitlab.com/subshq/clients/webapp/-/merge_requests/459)
- [Web: Fix import user photo](https://gitlab.com/subshq/clients/webapp/-/merge_requests/423)
- [Update brands to get details after first search](https://gitlab.com/subshq/clients/webapp/-/merge_requests/409)
- [Resolve "Fix or cleanup SCP issues in Sentry"](https://gitlab.com/subshq/clients/webapp/-/merge_requests/470)
- [Fix error where buffer is externalized by vite](https://gitlab.com/subshq/clients/webapp/-/merge_requests/468)
- [Web: Modify Netlify CSP header](https://gitlab.com/subshq/clients/webapp/-/merge_requests/403)
- [Resolve "Handle user closing firebase popup"](https://gitlab.com/subshq/clients/webapp/-/merge_requests/467)
- Some dependency updated
- [Organize and cleanup message constants](https://gitlab.com/subshq/clients/webapp/-/merge_requests/451)
- [Resolve "Popup blocked when coming from www.tilig.com](https://gitlab.com/subshq/clients/webapp/-/merge_requests/465)

### 1.9.7

- [Re-enabled Firebase redirect when possible](https://gitlab.com/subshq/clients/webapp/-/merge_requests/465)
- [Fix 2FA codes on web](https://gitlab.com/subshq/clients/webapp/-/merge_requests/462)
- [Remove stuff from the webapp for the old handoff](https://gitlab.com/subshq/clients/webapp/-/merge_requests/460/)

### 1.9.6

- [Fix login flow for safari on macOS Ventura and firefox](https://gitlab.com/subshq/clients/webapp/-/merge_requests/456)
- [Opera browser detecetion on onboarding extension install](https://gitlab.com/subshq/clients/webapp/-/merge_requests/430)
- [Changes to core needed for Pinia migration on Ext](https://gitlab.com/subshq/clients/webapp/-/merge_requests/425)
- [Strip schema before filtering items](https://gitlab.com/subshq/clients/webapp/-/merge_requests/422)
- [Add x-tilig-version to Mixpanel for Web and WebMobile](https://gitlab.com/subshq/clients/webapp/-/merge_requests/432)

### 1.9.5

- [Add events to extension uninstall page](https://gitlab.com/subshq/clients/webapp/-/merge_requests/413)
- [Log browser detection errors in onboarding](https://gitlab.com/subshq/clients/webapp/-/merge_requests/420)
- [Update CSP settings](https://gitlab.com/subshq/clients/webapp/-/merge_requests/421)
- [Fix export layout](https://gitlab.com/subshq/clients/webapp/-/merge_requests/412)
- [Add import button to sidebar](https://gitlab.com/subshq/clients/webapp/-/merge_requests/402)
- [Add feature flags](https://gitlab.com/subshq/clients/webapp/-/merge_requests/405)
- [Fix exports not working](https://gitlab.com/subshq/clients/webapp/-/merge_requests/410)
- [Fix decrypting base64Url strings w/ newlines](https://gitlab.com/subshq/clients/webapp/-/merge_requests/416)

### 1.9.4

- [Fix extension install success route](https://gitlab.com/subshq/clients/webapp/-/merge_requests/404)

### 1.9.3

- [Hybrid encryption](https://gitlab.com/subshq/clients/webapp/-/merge_requests/387)

### 1.9.2

- [Enforce extension install](https://gitlab.com/subshq/clients/webapp/-/merge_requests/394)

### 1.9.1

- [Refactor Account Show and Edit pages](https://gitlab.com/subshq/clients/webapp/-/merge_requests/380)
- [Fix routing issue in Accounts page](https://gitlab.com/subshq/clients/webapp/-/merge_requests/352)
- [Refactor stores and services](https://gitlab.com/subshq/clients/webapp/-/merge_requests/378)

### 1.9.0

- [New add and edit account flows](https://gitlab.com/subshq/clients/webapp/-/merge_requests/329)

### Older

- [Remove the onboarding step to explain disabling Chrome autofill](https://gitlab.com/subshq/clients/webapp/-/merge_requests/305). The extension disables this step automatically.
- [UTM param tracking](https://gitlab.com/subshq/clients/webapp/-/merge_requests/346). Update Mixpanel profile with UTM tags after signup.
