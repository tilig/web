const tailwindConfig = require('../core/tailwind.config.cjs')

module.exports = {
  ...tailwindConfig,
  content: ['index.html', './src/**/*.{vue,js,ts,jsx,tsx}', '../core/src/**/*.{vue,js,ts,jsx,tsx}'],
}
