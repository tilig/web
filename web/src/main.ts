import { createApp } from 'vue'
import { createPinia } from 'pinia'
import * as Sentry from '@sentry/vue'
import { BrowserTracing } from '@sentry/tracing'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

import '@/index.css'
import App from '@/App.vue'
import router from '@/router'

const app = createApp(App)
const apiHostname = new URL(import.meta.env.VITE_APIROOT).host
const pinia = createPinia()

// add a pinia persistence plugin
pinia.use(piniaPluginPersistedstate)

// clear old vuex state
localStorage.removeItem('vuex')
localStorage.removeItem('decrypted_cache')

// Sentry is only enabled when the VITE_SENTRY_DSN env var is configured
if (import.meta.env.VITE_SENTRY_DSN) {
  Sentry.init({
    app,
    dsn: import.meta.env.VITE_SENTRY_DSN,
    environment: import.meta.env.VITE_SENTRY_ENVIRONMENT || import.meta.env.NODE_ENV,
    release: import.meta.env.VITE_SENTRY_RELEASE,
    tracesSampleRate: 0.02,
    integrations: [
      new BrowserTracing({
        tracingOrigins: [apiHostname],
      }),
    ],
  })
}

app.use(router).use(pinia).mount('#app')
