import { useRouter, useRoute } from 'vue-router'

import { useOnboardingStore } from '@core/stores/onboarding'
import { useOrganizationStore } from '@core/stores/organization'
import { useTrackingStore } from '@core/stores/tracking'

import { AccountType } from '@core/clients/tilig'
import { createLogger } from '@core/utils/logger'
import { TrackingEvent } from '@core/utils/trackingConstants'

const logger = createLogger('login:redirect-handler')

const useLoginRedirectToNextPage = () => {
  const onboardingStore = useOnboardingStore()
  const organizationStore = useOrganizationStore()
  const router = useRouter()
  const route = useRoute()
  const trackingStore = useTrackingStore()

  async function loginRedirectToNextPage() {
    // login redirect started
    await organizationStore.initialize()
    await onboardingStore.initialize()
    await organizationStore.fetchPendingInvitations()

    if (organizationStore.hasAcceptedInvitation) {
      logger.debug('routing to invitation accepted page')
      router.push({ name: 'invitation-accepted' })
      return
    }

    if (organizationStore.hasPendingInvitations) {
      logger.debug('routing to pending invitations page')
      router.push({ name: 'pending-invitations' })
      return
    }

    if (onboardingStore.isOnboarding) {
      trackingStore.trackEvent({
        event: TrackingEvent.USER_ONBOARDING_STARTED,
        properties: { accountType: onboardingStore.accountType },
      })

      switch (onboardingStore.accountType) {
        case AccountType.Business:
          logger.debug('business user')
          if (!organizationStore.organization) {
            router.push({ name: 'onboarding-team-name' })
          } else {
            router.push({ name: 'onboarding-step-install' })
          }
          break
        case AccountType.Personal:
          logger.debug('personal user')
          router.push({ name: 'onboarding-step-install' })
          break
        case undefined:
          logger.debug('No account type yet')
          router.push({ name: 'onboarding-account-type' })
          break
      }
      return
    }

    trackingStore.trackEvent({
      event: TrackingEvent.CONTINUE_TO_NEXT_PAGE,
      properties: {
        completed: onboardingStore.completed,
        dismissed: onboardingStore.dismissed,
        webStore: onboardingStore.browserSpecificWebstore,
        route: {
          current: route.name,
          next: 'items',
        },
      },
    })

    logger.debug(
      'User has either completed onboarding, dismissed it or does not have a specific webstore',
      {
        completed: onboardingStore.completed,
        dismissed: onboardingStore.dismissed,
        webStore: onboardingStore.browserSpecificWebstore,
      }
    )

    router.push({ name: 'items' })
  }

  return loginRedirectToNextPage
}

export default useLoginRedirectToNextPage
