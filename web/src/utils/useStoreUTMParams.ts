import { LocationQuery } from 'vue-router'
import { useTrackingStore } from '@core/stores/tracking'
import { useOnboardingStore } from '@core/stores/onboarding'
import { useUserSettingsStore } from '@core/stores/userSettings'
import { AccountType } from '@core/clients/tilig'

const useStoreUTMParams = () => {
  const trackingStore = useTrackingStore()
  const onboardingStore = useOnboardingStore()
  const userSettingsStore = useUserSettingsStore()

  /**
   * Parse UTM params from the URL params (route query), and update the user's Mixpanel profile with it.
   * This needs to happen right after authentication, but before redirecting to a different page,
   * otherwise the URL params are cleared out.
   */
  async function storeUTMParams(query: LocationQuery) {
    const allowed = [
      'utm_source',
      'utm_medium',
      'utm_campaign',
      'utm_content',
      'utm_term',
      'network',
      'audience',
      'landing',
      'fbclid',
    ]
    const utmParams = Object.keys(query)
      .filter((key) => allowed.includes(key))
      .reduce((obj: { [key: string]: string }, key) => {
        obj[key] = query[key] as string
        return obj
      }, {})
    if (Object.keys(utmParams).length) {
      await trackingStore.trackPerson(utmParams)

      // Set the audience on the onboardingstore too
      if (utmParams['audience'] == 'business') {
        onboardingStore.setAccountType(AccountType.Business)
      } else if (utmParams['audience'] == 'personal') {
        onboardingStore.setAccountType(AccountType.Personal)
      }

      if (utmParams['landing']?.includes('plushie')) {
        userSettingsStore.updateSettings({
          web: {
            squirrelCampaignEnabled: true,
          },
        })
      }
    }
  }

  return storeUTMParams
}

export default useStoreUTMParams
