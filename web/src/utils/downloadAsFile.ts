/**
 * Trigger a file download
 * @param filename Filename to save as
 * @param contents String contents of the file
 * @param parent Optional parent element to place link
 */
export const downloadAsFile = (filename: string, contents: string, parent = document.body) => {
  const file = new File([contents], filename)
  const url = URL.createObjectURL(file)

  const link = document.createElement('a')
  link.style.display = 'none'
  link.href = url
  link.download = file.name
  parent.appendChild(link)
  link.click()

  setTimeout(() => {
    link.remove()
    URL.revokeObjectURL(url)
  })
}
