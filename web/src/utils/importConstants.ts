import {
  IMPORT_CHOOSE_CURRENT_BROWSER,
  IMPORT_CHOOSE_CURRENT_PASSWORD_MANAGER,
  IMPORT_DOWNLOAD_TEMPLATE,
  IMPORT_PASSWORD_STORAGE_OPTION_SELECTED,
  IMPORT_STARTED,
  IMPORT_1PASSWORD_VAULTS,
  IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
  IMPORT_PASSWORD_FILE_UPLOADED,
  IMPORT_PASSWORD_FILE_UPLOAD_ERROR,
  IMPORT_PASSWORD_SPECIFY_ACCOUNT_COLUMNS,
  IMPORT_PASSWORDS_STARTED,
  IMPORT_PASSWORDS_COMPLETED_FULLY,
  IMPORT_PASSWORDS_COMPLETED_PARTIALLY,
  IMPORT_FEATURE_RATING,
  IMPORT_SHOW_IMPORTED_ACCOUNTS_BUTTON_CLICKED,
} from '@core/utils/trackingConstants'

export const RADIO_OPTIONS = 'RADIO_OPTIONS'
export const STATIC_MESSAGE = 'STATIC_MESSAGE'
export const YES_OR_NO = 'YES_OR_NO'
export const DECISION = {
  YES: 'YES',
  NO: 'NO',
}
export const PASSWORD_STORAGE = {
  MANUAL: 'MANUAL',
  BROWSER: 'BROWSER',
  PASSWORD_MANAGER: 'PASSWORD_MANAGER',
}

export const CURRENT_METHOD_ID = 'password-storage-selection'

export const CHROME_BROWSER_ID = 'chrome-browser'
export const EDGE_BROWSER_ID = 'edge-browser'
export const FIREFOX_BROWSER_ID = 'firefox-browser'
export const SAFARI_BROWSER_ID = 'safari-browser'
export const OTHER_BROWSER_ID = 'other-browser'

export const ONE_PASSWORD_PWM_ID = 'one-password-pwm'
export const BITWARDEN_PWM_ID = 'bitwarden-pwm'
export const DASHLANE_PWM_ID = 'dashlane-pwm'
export const LASTPASS_PWM_ID = 'lastpass-pwm'
export const KEEPER_PWM_ID = 'keeper-pwm'
export const OTHER_PWM_ID = 'other-pwm'

export const PASSWORD_MANAGER_ID = 'password-manager'
export const WEB_BROWSER_ID = 'web-browser'
export const MANUAL_ENTRY_ID = 'manual-entry'
export const CURRENT_BROWSER_ID = 'current-browser'
export const CURRENT_PASSWORD_MANAGER_ID = 'current-password-manager'
export const TEMPLATE_DOWNLOAD_ID = 'template-download'
export const TEMPLATE_EDIT_ID = 'template-edit'

export const ONE_PASSWORD_VAULT_SELECION_ID = '1password-personal-or-all-vaults'
export const ONE_PASSWORD_ONLY_PERSONAL_VAULTS_CHOICE = '1password-only-personal-vaults'

export const SUCCESSFUL_IMPORT_FINAL_STEP = 'successful-import-final-step'
export const IMPORT_PROGRESS_SOLID_FOUNDATION = 'import-progress-solid-foundation'
export const IMPORT_PROGRESS_NO_RUSH = 'import-progress-no-rush'
export const IMPORT_PROGRESS_ENCRYPT_WELL = 'import-progress-encrypt-well'
export const FILE_UPLOAD_ERROR_INIT = 'file-upload-error-init'
export const FILE_UPLOAD = 'file-upload'
export const SPECIFY_FIELDS_AND_COLUMNS = 'specify-columns-init'
export const PRE_IMPORT_ANIMATION = 'pre-import-animation'
export const PARTIAL_IMPORT_COMPLETED = 'partial-import-completed'
export const PARTIAL_IMPORT_COUNT = 'partial-import-count'
export const DELETE_EXPORTED_FILE = 'delete-exported-file'

// we are using this to determine when the mixpanel event is fired within the various components
export const MIXPANEL_EVENT_TRIGGER = {
  CLICK: 'CLICK',
  LOAD: 'LOAD',
}

export const MIXPANEL_EVENT_PREFIX = 'Import-Tili'

type ImportingMixpanelEvent = {
  name: string
  when: string
  what: string
  trigger: string // TODO: enum
}

export type MessageType = {
  id: string
  type: string // TODO: enum
  component: string
  // TODO: can be of different types -> Option type
  content?: any
  nextMessageId?: string
  mixpanelEvent?: ImportingMixpanelEvent
  ms?: number
  eventType?: string
}

// NOTE: even if you set a type of STATIC_MESSAGE, you need to define `component: 'StaticMessage',`
// otherwise it will SILENTLY fail and drive you crazy.
export const importMessages: MessageType[] = [
  {
    id: 'init-greeting',
    type: STATIC_MESSAGE,
    content: "Hi, I'm the import assistant! I'm here to help you import your passwords.",
    nextMessageId: CURRENT_METHOD_ID,
    component: 'StaticMessage',
    mixpanelEvent: {
      name: IMPORT_STARTED,
      when: 'When import page is loaded or first message of Tili is provided',
      what: '',
      trigger: MIXPANEL_EVENT_TRIGGER.LOAD,
    },
  },
  {
    id: CURRENT_METHOD_ID,
    type: RADIO_OPTIONS,
    content: {
      label: 'Please tell me, where have you been storing your passwords up until now?',
      options: [
        {
          id: 1,
          text: 'I’ve been using another password manager',
          nextMessageId: PASSWORD_MANAGER_ID,
        },
        {
          id: 2,
          text: 'I’ve been using my web browser to store them',
          nextMessageId: WEB_BROWSER_ID,
        },
        {
          id: 3,
          text: 'I’ve stored them in a file on my computer',
          nextMessageId: MANUAL_ENTRY_ID,
        },
        {
          id: 4,
          text: 'I’ve just been remembering them',
          nextMessageId: MANUAL_ENTRY_ID,
        },
        {
          id: 5,
          text: 'I’ve written them down in a notebook or on paper',
          nextMessageId: MANUAL_ENTRY_ID,
        },
      ],
    },
    component: 'MultipleChoiceMessage',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_STORAGE_OPTION_SELECTED,
      when: 'When user answers the question',
      what: 'Store selected answer',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: PASSWORD_MANAGER_ID,
    type: STATIC_MESSAGE,
    content:
      'Awesome! I can import your passwords all in one go. All we need is a file that contains your passwords.',
    nextMessageId: CURRENT_PASSWORD_MANAGER_ID,
    component: 'StaticMessage',
  },
  {
    id: WEB_BROWSER_ID,
    type: STATIC_MESSAGE,
    content:
      'Awesome! I can import your passwords all in one go. All we need is a file that contains your passwords.',
    nextMessageId: CURRENT_BROWSER_ID,
    component: 'StaticMessage',
  },
  {
    id: MANUAL_ENTRY_ID,
    type: STATIC_MESSAGE,
    content: 'Importing your passwords requires adding some data to a file and uploading it',
    nextMessageId: 'manual-entry-init',
    component: 'StaticMessage',
  },
  {
    id: 'manual-entry-init',
    type: STATIC_MESSAGE,
    content: 'Don’t start sweating 😰 straight away',
    nextMessageId: 'manual-entry-relax',
    component: 'StaticMessage',
  },
  {
    id: 'manual-entry-relax',
    type: STATIC_MESSAGE,
    content: 'I’ve streamlined this process',
    nextMessageId: 'manual-entry-relax-2',
    component: 'StaticMessage',
  },
  {
    id: 'manual-entry-relax-2',
    type: STATIC_MESSAGE,
    content: [
      'This way, importing may be more suited than:',
      '- Adding passwords one by one',
      '- Saving passwords when you log in to websites',
    ],
    nextMessageId: 'password-import-fields',
    component: 'StaticMessageList',
  },
  {
    id: 'password-import-fields',
    type: STATIC_MESSAGE,
    content: 'You only need a name, the website (URL), the email (username), and the password',
    nextMessageId: TEMPLATE_DOWNLOAD_ID,
    component: 'StaticMessage',
  },
  {
    id: TEMPLATE_DOWNLOAD_ID,
    type: STATIC_MESSAGE,
    content: 'Please add the information to this template',
    component: 'DownloadTemplate',
    mixpanelEvent: {
      name: IMPORT_DOWNLOAD_TEMPLATE,
      when: 'When user downloads the template',
      what: '',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: 'template-edit',
    type: STATIC_MESSAGE,
    content: 'You can open and edit this file in any spreadsheet program or text editor',
    nextMessageId: 'template-enter-password',
    component: 'StaticMessage',
  },
  {
    id: 'template-enter-password',
    type: STATIC_MESSAGE,
    content: 'Enter as many passwords as you like',
    nextMessageId: 'template-save',
    component: 'StaticMessage',
  },
  {
    id: 'template-save',
    type: STATIC_MESSAGE,
    content: 'Once you are done, save the file',
    nextMessageId: 'template-save-csv',
    component: 'StaticMessage',
  },
  {
    id: 'template-save-csv',
    type: STATIC_MESSAGE,
    content: 'It should automatically save as a csv file',
    nextMessageId: 'file-upload',
    component: 'StaticMessage',
  },
  {
    id: CURRENT_BROWSER_ID,
    type: RADIO_OPTIONS,
    content: {
      label: 'Please indicate which browser holds your passwords.',
      options: [
        { id: CHROME_BROWSER_ID, text: 'Chrome', nextMessageId: CHROME_BROWSER_ID },
        { id: EDGE_BROWSER_ID, text: 'Edge', nextMessageId: EDGE_BROWSER_ID },
        { id: FIREFOX_BROWSER_ID, text: 'Firefox', nextMessageId: FIREFOX_BROWSER_ID },
        {
          id: SAFARI_BROWSER_ID,
          text: 'Safari / iCloud / Keychain',
          nextMessageId: SAFARI_BROWSER_ID,
        },
        { id: OTHER_BROWSER_ID, text: 'Other', nextMessageId: OTHER_BROWSER_ID },
      ],
    },
    component: 'MultipleChoiceMessage',
    mixpanelEvent: {
      name: IMPORT_CHOOSE_CURRENT_BROWSER,
      when: 'When user answers the question',
      what: 'Store selected answer',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: CURRENT_PASSWORD_MANAGER_ID,
    type: RADIO_OPTIONS,
    content: {
      label: 'Please indicate which password manager holds your passwords.',
      options: [
        { id: LASTPASS_PWM_ID, text: 'Lastpass', nextMessageId: 'lastpass-selected' },
        { id: BITWARDEN_PWM_ID, text: 'Bitwarden', nextMessageId: 'bitwarden-selected' },
        { id: ONE_PASSWORD_PWM_ID, text: '1Password', nextMessageId: '1password-selected' },
        { id: KEEPER_PWM_ID, text: 'Keeper', nextMessageId: 'keeper-selected' },
        { id: DASHLANE_PWM_ID, text: 'Dashlane', nextMessageId: 'dashlane-selected' },
        { id: OTHER_PWM_ID, text: 'Other', nextMessageId: 'other-pwm-selected' },
      ],
    },
    component: 'MultipleChoiceMessage',
    mixpanelEvent: {
      name: IMPORT_CHOOSE_CURRENT_PASSWORD_MANAGER,
      when: 'When user answers the question',
      what: 'Store selected answer',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: 'lastpass-selected',
    type: STATIC_MESSAGE,
    content: "Making the switch from LastPass to Tilig? No worries, I've got you covered!",
    nextMessageId: LASTPASS_PWM_ID,
    component: 'StaticMessage',
  },
  {
    id: 'bitwarden-selected',
    type: STATIC_MESSAGE,
    content: "Making the switch from Bitwarden to Tilig? No worries, I've got you covered!",
    nextMessageId: BITWARDEN_PWM_ID,
    component: 'StaticMessage',
  },
  {
    id: '1password-selected',
    type: STATIC_MESSAGE,
    content: "Making the switch from 1Password to Tilig? No worries, I've got you covered!",
    nextMessageId: ONE_PASSWORD_VAULT_SELECION_ID,
    component: 'StaticMessage',
  },
  {
    id: 'keeper-selected',
    type: STATIC_MESSAGE,
    content: "Making the switch from Keeper to Tilig? No worries, I've got you covered!",
    nextMessageId: KEEPER_PWM_ID,
    component: 'StaticMessage',
  },
  {
    id: 'dashlane-selected',
    type: STATIC_MESSAGE,
    content: "Making the switch from Dashlane to Tilig? No worries, I've got you covered!",
    nextMessageId: DASHLANE_PWM_ID,
    component: 'StaticMessage',
  },
  {
    id: 'other-pwm-selected',
    type: STATIC_MESSAGE,
    content:
      "Making the switch from another password manager to Tilig? No worries, I've got you covered!",
    nextMessageId: OTHER_PWM_ID,
    component: 'StaticMessage',
  },
  {
    id: ONE_PASSWORD_VAULT_SELECION_ID,
    type: RADIO_OPTIONS,
    content: {
      label: 'Do you want to import all vaults, or only your personal vaults?',
      options: [
        {
          id: '1password-all-vaults',
          text: 'All vaults (including shared vaults)',
          nextMessageId: ONE_PASSWORD_PWM_ID,
        },
        {
          id: ONE_PASSWORD_ONLY_PERSONAL_VAULTS_CHOICE,
          text: 'Only personal vaults (no shared vaults)',
          nextMessageId: ONE_PASSWORD_PWM_ID,
        },
      ],
    },
    component: 'MultipleChoiceMessage',
    mixpanelEvent: {
      name: IMPORT_1PASSWORD_VAULTS,
      when: 'When user answers the question',
      what: 'Store selected answer',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: CHROME_BROWSER_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: [
        'In Chrome, click on the menu icon',
        'Choose Settings > Autofill > Passwords',
        'In the “Saved Passwords” area click on menu icon',
        'Choose “Export passwords...” and follow the steps',
      ],
      screenshots: [{ url: 'browsers/chrome.png', orientation: 'landscape' }],
    },
    nextMessageId: 'file-upload',
    component: 'ChromeExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: EDGE_BROWSER_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: [
        'In Edge click on the menu icon',
        'Choose Settings > Autofill > Passwords',
        'In the “Saved Passwords” area click on the icon',
        'Choose “Export passwords ...” and follow the steps',
      ],
      screenshots: [{ url: 'browsers/edge.png', orientation: 'landscape' }],
    },
    nextMessageId: 'file-upload',
    component: 'EdgeExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: FIREFOX_BROWSER_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: [
        'In Firefox click on the menu icon',
        'Choose Passwords',
        'Click on the settings icon',
        'Choose “Export passwords” and follow the steps',
      ],
      supportLink: 'https://support.mozilla.org/en-US/kb/export-login-data-firefox',
      screenshots: [{ url: 'browsers/firefox.png', orientation: 'landscape' }],
    },
    nextMessageId: 'file-upload',
    component: 'FirefoxExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: SAFARI_BROWSER_ID,
    type: STATIC_MESSAGE,
    content: {
      // Disabled this for simplicity, assuming most users will do this on desktop at the moment.
      // preInstruction:
      //   'Unfortunately, exporting from Safari is only possible on a MacBook, not on iPhones or iPads',
      otherInstructions: [
        'On your MacBook navigate in Safari to Safari > Preferences',
        'Select Passwords',
        'Choose “Export passwords” and follow the steps',
      ],
      supportLink:
        'https://support.apple.com/guide/safari/import-bookmarks-and-passwords-ibrw1015/mac',
      screenshots: [{ url: 'browsers/safari.png', orientation: 'portrait' }],
    },
    nextMessageId: 'file-upload',
    component: 'BrowserAndPasswordManagerExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: OTHER_BROWSER_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: [
        'Navigate in your browser to the Settings',
        'Search for Passwords or Autofill',
        'Follow the Export steps and export a CSV file',
      ],
      screenshots: [],
    },
    nextMessageId: 'file-upload',
    component: 'OtherBrowserAndPasswordManagerExportSteps',
  },
  {
    id: ONE_PASSWORD_PWM_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: [
        'Open the 1Password (version 8) desktop App',
        'Click File > Export and select the account to export',
        'Follow the steps and export a 1PUX file',
        '(Using 1Password version 7 or earlier? Choose the CSV option)',
      ],
      supportLink: 'https://support.1password.com/export',
      screenshots: [{ url: 'pwm/1password.png', orientation: 'portrait' }],
    },
    nextMessageId: 'file-upload',
    component: 'BrowserAndPasswordManagerExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: BITWARDEN_PWM_ID,
    type: STATIC_MESSAGE,
    content: {
      instructionWithLink: `Open in your browser the<a href="https://vault.bitwarden.com" target="_blank" class="link support-link">Bitwarden Vault</a>`,
      otherInstructions: ['Click Tools  > Export Vault', 'Follow the steps and export a CSV file'],
      supportLink: 'https://bitwarden.com/help/export-your-data',
      screenshots: [{ url: 'pwm/bitwarden.png', orientation: 'landscape' }],
    },
    nextMessageId: 'file-upload',
    component: 'BrowserAndPasswordManagerExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: DASHLANE_PWM_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: [
        'Click Settings > Export',
        'Follow the steps and export a CSV file',
        'Extract the Zip files',
        'Upload the file "credentials.csv"',
      ],
      instructionWithLink: `Open in your browser the<a href="https://app.dashlane.com" target="_blank" class="link support-link">Dashlane Web App</a>`,
      supportLink:
        'https://support.dashlane.com/hc/en-us/articles/202625092-Export-your-passwords-from-Dashlane',
      screenshots: [{ url: 'pwm/dashlane.gif', orientation: 'landscape' }],
    },
    nextMessageId: 'file-upload',
    component: 'BrowserAndPasswordManagerExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: KEEPER_PWM_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: ['Go to Acccount > Settings', 'Follow the steps and export a CSV file'],
      instructionWithLink: `Open in your browser the<a href="https://keepersecurity.com/vault" target="_blank" class="link support-link">Keeper Security Web Vault</a>`,
      supportLink: 'https://docs.keeper.io/user-guides/export-and-reports/vault-export',
      screenshots: [
        { url: 'pwm/keeper-1.png', orientation: 'landscape' },
        { url: 'pwm/keeper-2.png', orientation: 'landscape' },
      ],
    },
    nextMessageId: 'file-upload',
    component: 'BrowserAndPasswordManagerExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: LASTPASS_PWM_ID,
    type: STATIC_MESSAGE,
    content: {
      instructionWithLink: `Open in your browser the<a href="https://lastpass.com/?ac=1&lpnorefresh=1" target="_blank" class="link support-link">LastPass Vault</a>`,
      otherInstructions: [
        'Choose Advanced Options and select Export',
        'Follow the steps and export a CSV file',
      ],
      supportLink:
        'https://support.lastpass.com/help/how-do-i-nbsp-export-stored-data-from-lastpass-using-a-generic-csv-file',
      screenshots: [{ url: 'pwm/lastpass-csv.png', orientation: 'landscape' }],
    },
    nextMessageId: 'file-upload',
    component: 'BrowserAndPasswordManagerExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: OTHER_PWM_ID,
    type: STATIC_MESSAGE,
    content: {
      otherInstructions: [
        'Open the password manager you are using',
        'Search in the settings for “Export”',
        'Follow the steps and export a CSV file',
      ],
      screenshots: [],
    },
    nextMessageId: 'file-upload',
    component: 'OtherBrowserAndPasswordManagerExportSteps',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED,
      when: 'When user clicks on a link in this entire section',
      what: 'Store opened URL',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: 'file-upload-init',
    type: STATIC_MESSAGE,
    content: 'Once you have this file, please drop it in the area below',
    nextMessageId: FILE_UPLOAD,
    component: 'StaticMessage',
  },
  {
    id: FILE_UPLOAD,
    type: FILE_UPLOAD,
    nextMessageId: 'import-init',
    component: 'FileUploader',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_FILE_UPLOADED,
      when: 'When user uploads the file',
      what: '',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: FILE_UPLOAD_ERROR_INIT,
    type: STATIC_MESSAGE,
    content: 'Something is wrong',
    nextMessageId: 'file-upload-try-again',
    component: 'StaticMessage',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_FILE_UPLOAD_ERROR,
      when: 'When the file upload error is show to the user',
      what: '',
      trigger: MIXPANEL_EVENT_TRIGGER.LOAD,
    },
  },
  {
    id: 'file-upload-try-again',
    type: STATIC_MESSAGE,
    content: 'Try uploading the file again',
    nextMessageId: 'file-upload-ensure-right-file',
    component: 'StaticMessage',
  },
  {
    id: 'file-upload-ensure-right-file',
    type: STATIC_MESSAGE,
    content: 'Make sure it is the right file format',
    nextMessageId: 'file-upload-error-contact',
    component: 'StaticMessage',
  },
  {
    id: 'file-upload-error-contact',
    type: STATIC_MESSAGE,
    content: 'If the problem persists, contact my bosses',
    nextMessageId: 'file-upload-error-chat',
    component: 'StaticMessage',
  },
  {
    id: 'file-upload-error-chat',
    type: STATIC_MESSAGE,
    // requires custom content that needs to be composed
    // we will supply the content inside the component
    content: '',
    // FIXME: I've removed the nextMessageId, because it was always
    // triggering a transition to the file-upload. Now this is handled
    // in the FileUploadError component.
    nextMessageId: '', // 'file-upload',
    component: 'FileUploadError',
  },
  {
    id: 'import-init',
    type: STATIC_MESSAGE,
    content: 'Alright, it’s time to move your passwords to Tilig.',
    ms: 1000,
    nextMessageId: 'import-file-confirmed',
    component: 'StaticMessage',
  },
  {
    id: 'import-file-confirmed',
    type: STATIC_MESSAGE,
    content: 'Great, this file has the correct format.',
    ms: 1300,
    nextMessageId: 'import-file-detail-check',
    component: 'StaticMessage',
  },
  {
    id: 'import-file-detail-check',
    type: STATIC_MESSAGE,
    content: 'Let me check it in more detail.',
    ms: 1300,
    nextMessageId: 'import-file-count',
    component: 'StaticMessage',
  },
  {
    id: 'import-file-count',
    type: STATIC_MESSAGE,
    // requires custom content that needs to be composed
    // we will supply the content inside the component
    content: '',
    ms: 1300,
    component: 'AccountsCount',
  },
  {
    id: SPECIFY_FIELDS_AND_COLUMNS,
    type: STATIC_MESSAGE,
    content: 'Almost there, I need some help',
    ms: 1000,
    nextMessageId: 'specify-columns-indicate',
    component: 'StaticMessage',
  },
  {
    id: 'specify-columns-indicate',
    type: STATIC_MESSAGE,
    content: 'Can you indicate what kind of information is in each column?',
    ms: 1000,
    nextMessageId: 'specify-columns-easy',
    component: 'StaticMessage',
    mixpanelEvent: {
      name: IMPORT_PASSWORD_SPECIFY_ACCOUNT_COLUMNS,
      when: 'When the first message gets shown to user',
      what: '',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: 'specify-columns-easy',
    type: STATIC_MESSAGE,
    content: 'Let’s make this as easy as possible for you',
    ms: 1000,
    nextMessageId: 'specify-columns',
    component: 'StaticMessage',
  },
  {
    id: 'specify-columns',
    type: STATIC_MESSAGE,
    // requires custom content that needs to be composed
    // we will supply the content inside the component
    content: '',
    ms: 1000,
    nextMessageId: PRE_IMPORT_ANIMATION,
    component: 'SpecifyFieldsAndColumns',
  },
  {
    id: PRE_IMPORT_ANIMATION,
    type: STATIC_MESSAGE,
    // requires custom content that needs to be composed
    // we will supply the content inside the component
    content: '',
    ms: 1000,
    nextMessageId: 'start-import',
    component: 'StartImport',
    mixpanelEvent: {
      name: IMPORT_PASSWORDS_STARTED,
      when: 'When the import actually starts',
      what: '',
      trigger: MIXPANEL_EVENT_TRIGGER.LOAD,
    },
  },
  {
    id: 'start-import',
    type: STATIC_MESSAGE,
    content: 'Ok, I will start importing',
    ms: 1000,
    nextMessageId: IMPORT_PROGRESS_NO_RUSH,
    component: 'StaticMessage',
  },
  {
    id: IMPORT_PROGRESS_NO_RUSH,
    type: STATIC_MESSAGE,
    content: 'I don’t want to rush this. After all, these are essential passwords',
    ms: 2000,
    nextMessageId: IMPORT_PROGRESS_ENCRYPT_WELL,
    component: 'StaticMessage',
  },
  {
    id: IMPORT_PROGRESS_ENCRYPT_WELL,
    type: STATIC_MESSAGE,
    content: 'I’m making sure they are securely encrypted',
    ms: 2000,
    component: 'StaticMessage',
  },
  {
    id: SUCCESSFUL_IMPORT_FINAL_STEP,
    type: STATIC_MESSAGE,
    // requires custom content that needs to be composed
    // we will supply the content inside the component
    content: '',
    component: 'AccountsImportedSuccessfully',
    nextMessageId: DELETE_EXPORTED_FILE,
    ms: 2000,
    mixpanelEvent: {
      name: IMPORT_PASSWORDS_COMPLETED_FULLY,
      when: 'When the import is succesful',
      what: 'Number of imported accounts',
      trigger: MIXPANEL_EVENT_TRIGGER.LOAD,
    },
  },
  {
    id: PARTIAL_IMPORT_COMPLETED,
    type: STATIC_MESSAGE,
    // requires custom content that needs to be composed
    // we will supply the content inside the component
    content: '',
    component: 'AccountsImportedPartially',
    nextMessageId: PARTIAL_IMPORT_COUNT,
    ms: 2000,
    mixpanelEvent: {
      name: IMPORT_PASSWORDS_COMPLETED_PARTIALLY,
      when: 'When the import is done partially',
      what: 'Y succesful, Z unsuccessful, Total X',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
  {
    id: PARTIAL_IMPORT_COUNT,
    type: STATIC_MESSAGE,
    // requires custom content that needs to be composed
    // we will supply the content inside the component
    content: '',
    nextMessageId: 'partial-import-notes',
    component: 'AccountsImportedPartially',
    ms: 2000,
  },
  {
    id: 'partial-import-notes',
    type: STATIC_MESSAGE,
    content: 'At this moment I can only handle items without long notes',
    component: 'StaticMessage',
    nextMessageId: 'partial-import-restriction',
    ms: 2000,
  },
  {
    id: 'partial-import-restriction',
    type: STATIC_MESSAGE,
    content: 'We are working on removing this restriction',
    component: 'StaticMessage',
    nextMessageId: 'partial-import-files',
    ms: 2000,
  },
  {
    id: 'partial-import-files',
    type: STATIC_MESSAGE,
    content:
      'You can find the non-imported items by opening the CSV file in Microsoft Excel or Google Sheets',
    component: 'StaticMessage',
    nextMessageId: 'partial-import-items',
    ms: 2000,
  },
  {
    id: 'partial-import-items',
    type: STATIC_MESSAGE,
    content: 'Look for items that hold a lot of information.',
    component: 'StaticMessage',
    nextMessageId: 'one-last-question',
    ms: 2000,
  },
  {
    id: DELETE_EXPORTED_FILE,
    type: STATIC_MESSAGE,
    content: "Great! All that's left to do now is a bit of cleanup. 🧹",
    component: 'StaticMessage',
    nextMessageId: 'delete-file-request',
    ms: 1000,
  },
  {
    id: 'delete-file-request',
    type: STATIC_MESSAGE,
    content:
      'Let’s remove the export file with passwords from your computer if you no longer need it.',
    component: 'StaticMessage',
    ms: 2000,
    nextMessageId: 'delete-file-hackers',
  },
  {
    id: 'delete-file-hackers',
    type: STATIC_MESSAGE,
    content: 'It’s a file that hackers would love to get hold of.',
    component: 'StaticMessage',
    ms: 2000,
    nextMessageId: 'file-deleted-question',
  },
  {
    id: 'file-deleted-question',
    type: YES_OR_NO,
    content: {
      label: 'Did you delete the file from your computer?',
      options: [
        {
          id: 'file-deleted-answer-yes',
          text: 'Yes, deleted',
          nextMessageId: 'empty-recycle-bin',
          decision: DECISION.YES,
        },
        {
          id: 'file-deleted-answer-no',
          text: 'No, I want to keep it',
          nextMessageId: 'one-last-question',
          decision: DECISION.NO,
        },
      ],
    },
    eventType: 'show-next-message',
    component: 'YesOrNoResponse',
  },
  {
    id: 'empty-recycle-bin',
    type: STATIC_MESSAGE,
    content: 'Almost there, let’s also empty your Recycle Bin.',
    component: 'StaticMessage',
    ms: 1000,
    nextMessageId: 'empty-recycle-bin-question',
  },
  {
    id: 'empty-recycle-bin-question',
    type: YES_OR_NO,
    content: {
      label: 'Did you empty your Recycle Bin?',
      options: [
        {
          id: 'empty-recycle-bin-answer',
          text: 'Also done',
          nextMessageId: 'delete-exported-file-complete',
          decision: DECISION.YES,
        },
        {
          id: 'empty-recycle-bin-answer',
          text: 'No, I will do it later',
          nextMessageId: 'one-last-question',
          decision: DECISION.NO,
        },
      ],
    },
    eventType: 'show-next-message',
    component: 'YesOrNoResponse',
  },
  {
    id: 'delete-exported-file-complete',
    type: STATIC_MESSAGE,
    content: 'Perfect, this gives me a secure feeling. I hope that’s mutual.',
    component: 'StaticMessage',
    ms: 2000,
    nextMessageId: 'delete-original-passwords',
  },
  {
    id: 'delete-original-passwords',
    type: STATIC_MESSAGE,
    content: 'You might even consider deleting all the passwords where you originally stored them.',
    component: 'StaticMessage',
    ms: 2000,
    nextMessageId: 'one-last-question',
  },
  {
    id: 'one-last-question',
    type: STATIC_MESSAGE,
    content: 'Ok, one last question.',
    component: 'StaticMessage',
    ms: 2000,
    nextMessageId: 'rating',
  },
  {
    id: 'rating',
    type: RADIO_OPTIONS,
    content: {
      label: 'Please, can you let me know how helpful I was?',
      options: [
        { id: 'not-helpful', text: 'Not helpful', nextMessageId: 'rating-appreciation' },
        { id: 'helpful', text: 'Helpful', nextMessageId: 'rating-appreciation' },
        { id: 'very-helpful', text: 'Very helpful', nextMessageId: 'rating-appreciation' },
      ],
    },
    component: 'MultipleChoiceMessage',
    mixpanelEvent: {
      name: IMPORT_FEATURE_RATING,
      when: 'When the import is succesful',
      what: 'Store the answer',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
    ms: 1000,
  },
  {
    id: 'rating-appreciation',
    type: STATIC_MESSAGE,
    content: 'Thanks!',
    ms: 1000,
    component: 'StaticMessage',
    nextMessageId: 'show-accounts-init',
  },
  {
    id: 'show-accounts-init',
    type: STATIC_MESSAGE,
    content: 'This was it',
    ms: 1000,
    component: 'StaticMessage',
    nextMessageId: 'show-accounts-countdown',
  },
  {
    id: 'show-accounts-countdown',
    type: STATIC_MESSAGE,
    content: 'Ok, I’m saying goodbye in 3, 2, 1...',
    ms: 1000,
    component: 'StaticMessage',
    nextMessageId: 'show-accounts',
  },
  {
    id: 'show-accounts',
    type: STATIC_MESSAGE,
    ms: 3000,
    component: 'ShowImportedAccounts',
    nextMessageId: 'show-accounts',
    mixpanelEvent: {
      name: IMPORT_SHOW_IMPORTED_ACCOUNTS_BUTTON_CLICKED,
      when: 'When this button is clicked by the user',
      what: 'Number of accounts imported',
      trigger: MIXPANEL_EVENT_TRIGGER.CLICK,
    },
  },
]
