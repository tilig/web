const app_id = import.meta.env.VITE_INTERCOM_APP_ID

/**
 * Intercom settings
 */
window.intercomSettings = {
  app_id,
  api_base: 'https://api-iam.intercom.io',
  hide_default_launcher: true,
}

/**
 * Install the intercom widget through the official Intercom script
 *
 * See {@link https://developers.intercom.com/installing-intercom/docs/basic-javascript#single-page-app}
 */
export default function installIntercomScript() {
  const w = window
  const ic = w.Intercom
  if (typeof ic === 'function') {
    ic('reattach_activator')
    ic('update', w.intercomSettings)
  } else {
    const d = document
    const i = function (...args) {
      i.c(...args)
    }
    i.q = []
    i.c = function (args) {
      i.q.push(args)
    }
    w.Intercom = i
    const l = function () {
      const s = d.createElement('script')
      s.type = 'text/javascript'
      s.async = true
      s.src = 'https://widget.intercom.io/widget/' + app_id
      const x = d.getElementsByTagName('script')[0]
      x.parentNode.insertBefore(s, x)
    }
    if (document.readyState === 'complete') {
      l()
    } else if (w.attachEvent) {
      w.attachEvent('onload', l)
    } else {
      w.addEventListener('load', l, false)
    }
  }
}

export function boot(user) {
  window.Intercom('boot', {
    app_id: import.meta.env.VITE_INTERCOM_APP_ID,
    user_id: user.id,
    name: user.name,
    email: user.email,
    created_at: new Date(),
  })
  window.Intercom('show')
}
