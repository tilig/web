import acorn from '@/assets/acorn.svg?url'
import { EncryptedItem } from '@core/clients/tilig'

/**
 * Tumbnail for the given website
 * @param item
 * @returns The URL as a string
 */
const itemIconURL = (item: EncryptedItem | null) => {
  if (item?.brand?.logo_icon_source) {
    return item.brand.logo_icon_source
  }

  return acorn
}

export default itemIconURL
