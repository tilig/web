import { describe, expect, it, vi } from 'vitest'
import { fireEvent } from '@testing-library/vue'
import { createRouter, createWebHistory } from 'vue-router'

import { renderWithPinia } from '@core/utils/tests'
import { useAuthStore } from '@core/stores/auth'
import { useFlagsStore } from '@core/stores/flags'

import LoginCard from '../LoginCard.vue'
import { nextTick, ref } from 'vue'
import SignupCard from '../SignupCard.vue'

vi.mock('@core/stores/flags')

const renderLoginCard = () => {
  const router = createRouter({
    history: createWebHistory(),
    routes: [
      { path: '/', component: LoginCard },
      { path: '/signup', name: 'signup', component: SignupCard },
    ],
  })

  return renderWithPinia(LoginCard, {
    global: {
      plugins: [router],
    },
  })
}

describe('LoginCard.vue', () => {
  it('renders correctly', () => {
    renderLoginCard()
  })

  it('signs in with google if the google button is clicked', async () => {
    const { getByText } = renderLoginCard()

    const authStore = useAuthStore()
    authStore.initialized = true

    const signInMock = vi.mocked(authStore.signInWith)
    signInMock.mockResolvedValue(true)

    await nextTick()

    const button = getByText('Continue with Google')
    await fireEvent.click(button)
    expect(signInMock).toHaveBeenCalledWith('google')
  })

  it('signs in with apple if the apple button is clicked', async () => {
    const { getByText } = renderLoginCard()

    const authStore = useAuthStore()
    authStore.initialized = true

    const signInMock = vi.mocked(authStore.signInWith)
    signInMock.mockResolvedValue(true)

    await nextTick()

    const button = getByText('Continue with Apple')
    await fireEvent.click(button)
    expect(signInMock).toHaveBeenCalledWith('apple')
  })

  it('signs in with microsoft if the microsoft flag is enabled', async () => {
    const flagsStore = useFlagsStore()
    const useFlagMock = vi.mocked(flagsStore.useFlag)
    useFlagMock.mockImplementation(() => ref(true))

    const { getByText } = renderLoginCard()

    const authStore = useAuthStore()
    authStore.initialized = true
    const signInMock = vi.mocked(authStore.signInWith)
    signInMock.mockResolvedValue(true)

    await nextTick()

    const button = getByText('Continue with Microsoft')
    await fireEvent.click(button)
    expect(signInMock).toHaveBeenCalledWith('microsoft')
  })
})
