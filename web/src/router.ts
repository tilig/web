import { createRouter, createWebHistory } from 'vue-router'
import * as Sentry from '@sentry/vue'

import { useAuthStore } from '@core/stores/auth'
import { ItemType } from '@core/stores/items'

declare module 'vue-router' {
  interface RouteMeta {
    /**
     * Do not redirect to login if on this route
     */
    unauthenticated?: boolean
    /**
     * Keep the Sidebar menu open on this route
     */
    sidebarOpen?: boolean
  }
}

const supportedItemTemplateRegex = [
  ItemType.Logins,
  ItemType.SecureNotes,
  ItemType.WiFi,
  ItemType.CreditCards,
  ItemType.Custom,
].join('|')

const idRegex = '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/s',
      name: 'private-link',
      component: () => import('@/pages/PrivateLink.vue'),
      meta: {
        unauthenticated: true,
      },
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/pages/Login.vue'),
      meta: {
        unauthenticated: true,
      },
    },
    {
      path: '/signup',
      name: 'signup',
      component: () => import('@/pages/SignUp.vue'),
      meta: {
        unauthenticated: true,
      },
    },
    {
      path: '/accept-invite/:id',
      name: 'accept-invitation',
      component: () => import('@/pages/organization/AcceptInvitation.vue'),
      meta: {
        unauthenticated: true,
      },
    },
    {
      path: '/invitation-accepted',
      name: 'invitation-accepted',
      component: () => import('@/pages/organization/InvitationAccepted.vue'),
    },
    {
      path: '/welcome/pending-invitations',
      name: 'pending-invitations',
      component: () => import('@/pages/organization/PendingInvitations.vue'),
    },
    {
      path: '/welcome/account-type',
      component: () => import('@/pages/onboarding/SelectAccountType.vue'),
      name: 'onboarding-account-type',
    },
    {
      path: '/welcome/team-name',
      component: () => import('@/pages/onboarding/SetTeamName.vue'),
      name: 'onboarding-team-name',
    },
    {
      path: '/welcome/team-invitation',
      component: () => import('@/pages/onboarding/InviteTeamMembers.vue'),
      name: 'onboarding-team-invitation',
    },
    {
      path: '/welcome/invitation-accepted',
      component: () => import('@/pages/organization/InvitationAccepted.vue'),
      name: 'onboarding-invitation-accepted',
    },
    {
      path: '/welcome/install-extension',
      component: () => import('@/pages/onboarding/InstallExtension.vue'),
      name: 'onboarding-step-install',
    },
    {
      path: '/welcome/install-extension/success',
      component: () => import('@/pages/onboarding/InstallExtensionSuccess.vue'),
      name: 'onboarding-step-install-success',
    },
    {
      path: '/import',
      component: () => import('@/layouts/BaseLayout.vue'),
      children: [
        {
          path: '',
          name: 'import',
          component: () => import('@/pages/Import.vue'),
        },
      ],
    },
    {
      path: '/export',
      component: () => import('@/layouts/SidebarLayout.vue'),
      children: [
        {
          path: '',
          name: 'export',
          component: () => import('@/pages/Export.vue'),
        },
        {
          path: '/export/1password',
          name: 'export-1password',
          component: () => import('@/pages/export/Export1Password.vue'),
        },
        {
          path: '/export/keeper',
          name: 'export-keeper',
          component: () => import('@/pages/export/ExportKeeper.vue'),
        },
        {
          path: '/export/dashlane',
          name: 'export-dashlane',
          component: () => import('@/pages/export/ExportDashlane.vue'),
        },
        {
          path: '/export/bitwarden',
          name: 'export-bitwarden',
          component: () => import('@/pages/export/ExportBitwarden.vue'),
        },
        {
          path: '/export/lastpass',
          name: 'export-lastpass',
          component: () => import('@/pages/export/ExportLastPass.vue'),
        },
      ],
    },
    {
      path: '/goodbye',
      component: () => import('@/layouts/BaseLayout.vue'),
      children: [
        {
          path: '',
          name: 'goodbye',
          component: () => import('@/pages/Goodbye.vue'),
          meta: {
            unauthenticated: true,
          },
        },
      ],
    },
    {
      path: '',
      component: () => import('@/layouts/SidebarLayout.vue'),
      children: [
        {
          path: '/organization',
          component: () => import('@/pages/organization/ViewOrganization.vue'),
          name: 'organization',
        },
        {
          path: '/getting-started',
          name: 'getting-started',
          component: () => import('@/pages/GettingStarted.vue'),
        },
        {
          path: '',
          component: () => import('@/layouts/ItemsLayout.vue'),
          meta: {
            sidebarOpen: true,
          },
          children: [
            {
              path: '',
              name: 'root',
              component: () => import('@/pages/Accounts.vue'),
              meta: { sidebarOpen: true },
            },
            {
              path: `/:type(${supportedItemTemplateRegex})?`,
              name: 'items',
              component: () => import('@/pages/Accounts.vue'),
              meta: { sidebarOpen: true },
            },
            {
              // Match routes with a UUID, and an optional type param.
              // /:id/ - UUID only
              // /logins/:id
              path: `/:type(${supportedItemTemplateRegex})?/:id(${idRegex})/`,
              component: () => import('@/pages/Account.vue'),
              meta: { sidebarOpen: false },
              children: [
                {
                  name: 'item',
                  path: '',
                  component: () => import('@/pages/account/AccountShow.vue'),
                },
                {
                  name: 'item-edit',
                  path: 'edit',
                  component: () => import('@/pages/account/AccountEdit.vue'),
                },
              ],
            },
            {
              path: `/folders/:folderId(${idRegex})`,
              name: 'folder-items',
              component: () => import('@/pages/Accounts.vue'),
              meta: { sidebarOpen: true },
            },
            {
              // Match routes with a UUID, and an optional type param.
              // /:id/ - UUID only
              // /logins/:id
              path: `/folders/:folderId(${idRegex})/:id(${idRegex})/`,
              component: () => import('@/pages/Account.vue'),
              meta: { sidebarOpen: false },
              children: [
                {
                  name: 'folder-item',
                  path: '',
                  component: () => import('@/pages/account/AccountShow.vue'),
                },
                {
                  name: 'folder-item-edit',
                  path: 'edit',
                  component: () => import('@/pages/account/AccountEdit.vue'),
                },
              ],
            },
          ],
        },
      ],
    },
    {
      path: '//getting-started',
      redirect: '/getting-started',
      strict: true,
      meta: {
        unauthenticated: true,
      },
    },
    {
      path: '/testdrive-return',
      redirect: '/',
      meta: {
        unauthenticated: true,
      },
    },
    {
      path: '/testdrive-return-success',
      redirect: '/',
      meta: {
        unauthenticated: true,
      },
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'not-found',
      component: () => import('@/pages/PageNotFound.vue'),
      meta: {
        unauthenticated: true,
      },
    },
  ],
})

router.beforeEach(async (to) => {
  if (to.meta.unauthenticated) {
    return
  }

  const authStore = useAuthStore()
  await authStore.ready

  if (!authStore.isAuthenticated) {
    return { name: 'login' }
  }
})

const isError = (err: unknown): err is Error => {
  return 'name' in (err as { name?: string }) && 'message' in (err as { message?: string })
}

const UPDATE_ERROR_FINDERS = [
  /Failed to fetch dynamically/,
  /Importing a module script failed/,
  /Unable to preload CSS/,
]

const isSiteUpdateError = (err: Error) => UPDATE_ERROR_FINDERS.some((r) => r.test(err.message))

router.onError((error, to) => {
  console.error('Error during navigation', error, to)
  if (isError(error) && isSiteUpdateError(error)) {
    // Site changed in background
    console.info('Site changed in background, reloading')
    window.location.href = to.fullPath
  } else {
    console.debug('Not a navigation failure due to site update')
    Sentry.captureException(error)
  }
})

export default router
