import { ref } from 'vue'

// import {
//   bitwardenCsv,
//   dashlaneCsv,
//   genericCsv,
//   keeperCsv,
//   lastPassCsv,
//   onePasswordCsv,
// } from '@/services/exports/csvExporter'
import { ExportItem, useExportStore } from '@/stores/exports'
import { downloadAsFile } from '@/utils/downloadAsFile'
import { toLastPassCsv } from '@/services/exports/lastPassExport'

export enum CSVExportFormat {
  OnePassword = '1password',
  LastPass = 'lastpass',
  DashLane = 'dashlane',
  Keeper = 'keeper',
  Bitwarden = 'bitwarden',
  Generic = 'generic',
}

export function useGenerateCSV() {
  const exportStore = useExportStore()
  const generatingCSV = ref(false)

  const getCSVData = (_exportFormat: CSVExportFormat, items: ExportItem[]): string => {
    return toLastPassCsv(items)
    // switch (exportFormat) {
    //   case CSVExportFormat.OnePassword:
    //     return onePasswordCsv(items)
    //   case CSVExportFormat.LastPass:
    //     return lastPassCsv(items)
    //   case CSVExportFormat.DashLane:
    //     return dashlaneCsv(items)
    //   case CSVExportFormat.Keeper:
    //     return keeperCsv(items)
    //   case CSVExportFormat.Bitwarden:
    //     return bitwardenCsv(items)
    //   default:
    //     return genericCsv(items)
    // }
  }

  const getCSVName = (exportFormat: CSVExportFormat): string => {
    return `tilig-${exportFormat}-export.csv`
  }

  const downloadCSV = async (exportType: CSVExportFormat) => {
    generatingCSV.value = true

    await exportStore.getItems()
    const name = getCSVName(exportType)
    const data = getCSVData(exportType, exportStore.items)

    downloadAsFile(name, data)

    generatingCSV.value = false
  }

  return {
    downloadCSV,
    generatingCSV,
  }
}
