import { useRoute, RouteLocationRaw, RouteParamsRaw } from 'vue-router'
import { createLogger } from '@core/utils/logger'
import { EncryptedItem } from '@core/clients/tilig'

const logger = createLogger('composables:useItemRoutes')

type ItemRouteParam = {
  id: EncryptedItem['id']
  folder?: EncryptedItem['folder']
}

export const useItemRoutes = () => {
  const currentRoute = useRoute()

  const routeWithFolder = (route: string, params?: RouteParamsRaw): RouteLocationRaw => {
    logger.debug('Current folder id', currentRoute.params.folderId)
    // Allow overriding folderId
    const { folderId, ...rest } = (params ?? {}) as { folderId?: string }

    if (currentRoute?.params.folderId) {
      return {
        name: `folder-${route}`,
        params: { folderId: folderId || currentRoute.params.folderId, ...rest },
      }
    }
    return {
      name: route,
      params: { type: currentRoute?.params.type ?? null, ...rest },
    }
  }

  const routeWithItem = (route: string, item: ItemRouteParam) => {
    const params: RouteParamsRaw = { id: item.id }
    if (item.folder !== undefined) params.folderId = item.folder?.id
    return routeWithFolder(route, params)
  }

  const toItems = () => routeWithFolder('items')
  const toItem = (item: ItemRouteParam) => routeWithItem('item', item)
  const toItemEdit = (item: ItemRouteParam) => routeWithItem('item-edit', item)

  return {
    toItems,
    toItem,
    toItemEdit,
  }
}
