import { storeToRefs } from 'pinia'
import { useCurrentItemStore } from '@core/stores/currentItem'
import { watch, ref } from 'vue'

const useScrollCurrentItemIntoView = (id: string) => {
  const { currentItem } = storeToRefs(useCurrentItemStore())
  const element = ref<HTMLElement | null>(null)

  watch(currentItem, () => {
    if (element.value && currentItem.value?.item.id === id) {
      element.value.scrollIntoView({ block: 'center', behavior: 'smooth' })
    }
  })

  return element
}

export default useScrollCurrentItemIntoView
