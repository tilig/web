import { useRouter } from 'vue-router'

export function useNavigateBack() {
  const router = useRouter()

  const navigateBack = () => {
    router.go(-1)
  }

  return {
    navigateBack,
  }
}
