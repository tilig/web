import { acceptHMRUpdate, defineStore } from 'pinia'

import { createLogger } from '@core/utils/logger'
import { chunkedForEach } from '@core/utils/rateLimit'
import { Folder, initializeItem, Template } from '@core/clients/tilig'
import {
  CURRENT_METHOD_ID,
  CURRENT_PASSWORD_MANAGER_ID,
  KEEPER_PWM_ID,
  LASTPASS_PWM_ID,
  ONE_PASSWORD_ONLY_PERSONAL_VAULTS_CHOICE,
  ONE_PASSWORD_PWM_ID,
  ONE_PASSWORD_VAULT_SELECION_ID,
  WEB_BROWSER_ID,
} from '@/utils/importConstants'
import { stripSchema } from '@core/utils/url'
import { useItemsStore } from '@core/stores/items'

import {
  CSVData,
  CSVHeader,
  CSVParseResult,
  HeaderMapping,
  CSVItemResult,
  parseCsv,
  parseData,
} from '@/services/import/csvParser'
import { parseKeeperCsv } from '@/services/import/keeperParser'
import { parseLastPassCsv } from '@/services/import/lastPassParser'
import { parseOnePwPux } from '@/services/import/onePwParser'
import { useFoldersStore } from '@core/stores/folders'

const logger = createLogger('stores:imports')

export interface ImportState {
  selectedOptions: SelectedOptions
  parseResult: CSVParseResult | null
  importResults: ('success' | 'fail' | undefined)[]
  onePasswordFileCount: number
  createdFolderMap: Map<string, Folder>
}

// The key is the id of the message,
// the value is the id of the selected option.
type SelectedOptions = Record<string, string>

const REQUIRED_HEADERS: CSVHeader[] = ['website', 'username', 'password']

export const useImportsStore = defineStore('imports', {
  state: () => {
    return {
      selectedOptions: {},
      parseResult: null,
      importResults: [],
      onePasswordFileCount: 0,
      createdFolderMap: new Map(),
    } as ImportState
  },
  getters: {
    successfullyParsed: (state) => {
      if (!state.parseResult?.success) {
        return false
      }

      const headers = state.parseResult.headers
      return REQUIRED_HEADERS.every((h) => headers.includes(h))
    },
    numberOfFailedImports: (state) => {
      return state.importResults.filter((v) => v === 'fail').length
    },
    numberOfTotalImports: (state) => {
      return state.importResults.length
    },
    didSelectKeeper: (state) => {
      return state.selectedOptions[CURRENT_PASSWORD_MANAGER_ID] === KEEPER_PWM_ID
    },
    didSelectLastPass: (state) => {
      return state.selectedOptions[CURRENT_PASSWORD_MANAGER_ID] === LASTPASS_PWM_ID
    },
    didSelectOnePw: (state) => {
      return state.selectedOptions[CURRENT_PASSWORD_MANAGER_ID] === ONE_PASSWORD_PWM_ID
    },
    didSelectOnePwOnlyPersonalVaults: (state) => {
      return (
        state.selectedOptions[ONE_PASSWORD_VAULT_SELECION_ID] ===
        ONE_PASSWORD_ONLY_PERSONAL_VAULTS_CHOICE
      )
    },
    didSelectBrowser: (state) => {
      return state.selectedOptions[CURRENT_METHOD_ID] === WEB_BROWSER_ID
    },
  },
  actions: {
    async parseFile(array: ArrayBuffer) {
      const contents = new Uint8Array(array)

      logger.debug('First bytes: %d %d', contents.at(0), contents.at(1))

      // 1pux files are simply zip archives, which starts with the bytes `PK`
      if (this.didSelectOnePw && contents.at(0) === 0x50 && contents.at(1) == 0x4b) {
        const { parseResult, numberOfFiles } = await parseOnePwPux(
          contents,
          this.didSelectOnePwOnlyPersonalVaults
        )
        this.onePasswordFileCount = numberOfFiles
        this.parseResult = parseResult
        return
      }

      const decoder = new TextDecoder('utf-8')
      const decoded = decoder.decode(contents)
      this.parseCsv(decoded)
    },
    /**
     * Automatically parse CSV into items
     *
     * @param contents CSV string
     */
    parseCsv(contents: string) {
      if (this.didSelectKeeper) {
        this.parseResult = parseKeeperCsv(contents)
      } else if (this.didSelectLastPass) {
        this.parseResult = parseLastPassCsv(contents)
      } else {
        this.parseResult = parseCsv(contents)
      }
    },
    /**
     * Parse CSV data into items
     *
     * Used if we can't parse automatically
     * @param headers Headers mapped to indices
     * @param data CSV data
     */
    parseData(headers: HeaderMapping, data: CSVData) {
      this.parseResult = parseData(headers, data)
    },
    async createFolders() {
      if (!this.parseResult?.success || !this.parseResult.folders) return

      const foldersStore = useFoldersStore()

      for (const [key, folder] of this.parseResult.folders.entries()) {
        const createdFolder = await foldersStore.createFolder(folder)
        if (!createdFolder) {
          logger.error("Couldn't create folder")
          throw new Error('Bleh')
          continue
        }
        this.createdFolderMap.set(key, createdFolder)
      }
    },
    async createItems() {
      if (!this.parseResult?.success) return

      // Create potential folders
      await this.createFolders()

      this.importResults = new Array(this.parseResult.items.length)

      await chunkedForEach(
        ({ item, index }) =>
          this.createItem(item, index).catch((err) => logger.error('Error during import %O', err)),
        this.parseResult.items.map((item, index) => ({
          item,
          index,
        })),
        { wait: 100 }
      )
    },
    async createItem(
      { template, overview, details, temporaryFolderId }: CSVItemResult,
      index: number
    ) {
      const itemsStore = useItemsStore()

      // Currently cannot import accounts with long notes
      if (
        (template === Template.Login || !template) &&
        itemsStore.maxLoginValueLength &&
        details.notes &&
        details.notes.length > itemsStore.maxLoginValueLength
      ) {
        logger.error('Could not import accounts with long notes')
        this.importResults[index] = 'fail'
        return
      }

      const newItem = initializeItem({ overview, details })
      newItem.item.template = template || Template.Login
      newItem.item.website = newItem.overview.urls[0]?.url
        ? stripSchema(newItem.overview.urls[0].url)
        : null

      if (temporaryFolderId) {
        logger.debug('Adding item to folder %s', temporaryFolderId)
        newItem.item.folder = this.createdFolderMap.get(temporaryFolderId) || null
        if (!newItem.item.folder) logger.error("Can't find folder with id %s", temporaryFolderId)
      }

      try {
        const result = await itemsStore.createItem(newItem)
        this.importResults[index] = result ? 'success' : 'fail'
      } catch (e) {
        logger.error('Error during import %O', e)
        this.importResults[index] = 'fail'
      }
    },
  },
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useImportsStore, import.meta.hot))
}
