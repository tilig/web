import { acceptHMRUpdate, defineStore } from 'pinia'
import { ref } from 'vue'

import { createLogger } from '@core/utils/logger'

import { DecryptedItem, deferredDecryptItem } from '@core/clients/tilig'
import { useItemsStore } from '@core/stores/items'
import { useAuthStore } from '@core/stores/auth'

const logger = createLogger('stores:exports')

export type ExportItem = Omit<DecryptedItem, 'share'>

export const useExportStore = defineStore('export', () => {
  const isLoaded = ref(false)
  const items = ref<ExportItem[]>([])

  const itemsStore = useItemsStore()
  const authStore = useAuthStore()

  const $reset = () => {
    isLoaded.value = false
    items.value = []
  }

  authStore.onSignOut(() => {
    logger.debug('Resetting store')
    $reset()
  })

  const getItems = async () => {
    if (!authStore.keyPair || !authStore.legacyKeyPair) return

    isLoaded.value = false
    items.value = []

    if (!itemsStore.isLoaded) {
      logger.debug('Fetching items')
      await itemsStore.fetchItems()
      logger.debug('Items fetched')
    }

    logger.debug('Decrypting items')
    await Promise.all(
      itemsStore.itemStates.map(async ({ item }) => {
        if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) return

        try {
          const { overview, details } = await deferredDecryptItem(
            item,
            authStore.keyPair,
            authStore.rsaCryptoKeyPair,
            true
          )

          items.value.push({ item, overview, details })
        } catch (e) {
          logger.error('Error decrypting items %O', e)
        }
      })
    )

    logger.debug('Items decrypted')
    isLoaded.value = true
  }

  return {
    isLoaded,
    items,
    getItems,
    $reset,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useExportStore, import.meta.hot))
}
