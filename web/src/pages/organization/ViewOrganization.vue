<script setup lang="ts">
import { computed, ref } from 'vue'
import { storeToRefs } from 'pinia'

import Button from '@core/components/Button.vue'
import OrganizationMember from '@/components/organization/OrganizationMember.vue'
import Spinner from '@core/components/icons/Spinner.vue'
import Modal from '@core/components/Modal.vue'
import Alert from '@/shared-components/Alert.vue'
import InviteTeamMembers from '@/components/organization/InviteTeamMembers.vue'
import RevokeAccess from '@/components/organization/RevokeAccess.vue'
import NoOrganization from '@/pages/organization/NoOrganization.vue'
import CreateTeamForm from '@/components/organization/CreateTeamForm.vue'

import { useOrganizationStore } from '@core/stores/organization'
import { useTrackingStore } from '@core/stores/tracking'
import { TrackingEvent } from '@core/utils/trackingConstants'

const organizationStore = useOrganizationStore()
const { trackEvent } = useTrackingStore()

const {
  organization,
  isFetchingOrganization,
  isSendingOrganizationInvitation,
  invitationErrors,
  isRevokingInvitation,
  invitations,
} = storeToRefs(organizationStore)
organizationStore.initialize()

const invitationFormIsVisibile = ref(false)
const notificationIsVisible = ref(false)
const revokeAccessModalIsVisible = ref(false)
const createTeamFormIsVisible = ref(false)
const invitationIdToBeRevoked = ref<string | null>(null)
const notification = ref<string | null>(null)

const organizationName = computed(() => organization.value?.name)
const members = computed(() => organization.value?.users)
const invitationToRevoke = computed(() =>
  invitations.value?.find((invitation) => invitation.id === invitationIdToBeRevoked.value)
)

const showInvitationForm = () => {
  invitationFormIsVisibile.value = true
}

const closeInvitationForm = () => {
  // we want to clear the server validation error here too
  organizationStore.clearInvitationErrors()
  invitationFormIsVisibile.value = false
}

const closeInvitationSuccessNotification = () => {
  notificationIsVisible.value = false
  // clear the notification message too
  notification.value = null
}

const showNotification = (text: string) => {
  notification.value = text
  notificationIsVisible.value = true
}

const openRevokeAccessModal = (id: string) => {
  invitationIdToBeRevoked.value = id
  revokeAccessModalIsVisible.value = true
}

const closeRevokeAccessModal = () => {
  revokeAccessModalIsVisible.value = false
  invitationIdToBeRevoked.value = null
}

const sendInvitation = async (emails: string[]) => {
  const invitationSent = await organizationStore.sendOrganizationInvitations(emails)

  if (invitationSent) {
    closeInvitationForm()
    const inviteText = emails.length > 1 ? 'Invites' : 'Invite'
    showNotification(`${inviteText} successfully sent!`)
  }
}

const revokeAccess = async () => {
  if (!invitationIdToBeRevoked.value) return

  const invitationRevoked = await organizationStore.revokeInvitation(invitationIdToBeRevoked.value)

  if (invitationRevoked) {
    closeRevokeAccessModal()
    showNotification('Invitation revoked successfully!')
  }
}

const createOrganization = async (teamName: string) => {
  const organizationCreated = await organizationStore.createNewOrganization(teamName)

  if (organizationCreated) {
    await organizationStore.fetchOrganization()
    createTeamFormIsVisible.value = false
  }
}

trackEvent({
  event: TrackingEvent.TEAM_OVERVIEW_APPEARED,
})
</script>

<template>
  <Spinner v-if="isFetchingOrganization" class="loader" />
  <div v-else-if="organization" class="organization">
    <header class="header">
      <h2 class="organization-name">{{ organizationName }}</h2>
      <Button
        button-style="outline-primary"
        class="add-members"
        label-class="add-members-label"
        @click="showInvitationForm"
        >+ Add members</Button
      >
    </header>

    <section class="members-section">
      <div class="members-section-header">
        <div>Your team</div>
        <div>Status</div>
      </div>

      <div class="members">
        <OrganizationMember
          v-for="invitation in invitations"
          :id="invitation.id"
          :key="invitation.id"
          :email="invitation.email"
          :organization="organizationName"
          :invitation-expired="invitation.expired"
          :invitation-pending="invitation.pending"
          @revoke-access="openRevokeAccessModal(invitation.id)"
        />

        <OrganizationMember
          v-for="member in members"
          :id="member.id"
          :key="member.id"
          :email="member.email"
          :name="member.display_name"
          :organization="organizationName"
          :picture="member.picture"
        />
      </div>
    </section>

    <Modal
      v-if="invitationFormIsVisibile"
      :open="invitationFormIsVisibile"
      @close-modal="closeInvitationForm"
    >
      <template #default>
        <InviteTeamMembers
          :invitation-errors="invitationErrors"
          :loading="isSendingOrganizationInvitation"
          @close="closeInvitationForm"
          @send-invitation="sendInvitation"
        />
      </template>
    </Modal>

    <Modal
      v-if="revokeAccessModalIsVisible"
      :open="revokeAccessModalIsVisible"
      @close-modal="closeRevokeAccessModal"
    >
      <RevokeAccess
        :loading="isRevokingInvitation"
        :email="invitationToRevoke?.email"
        :organization="organization.name"
        @revoke="revokeAccess"
        @close="closeRevokeAccessModal"
      />
    </Modal>

    <Alert v-if="notificationIsVisible" @hide="closeInvitationSuccessNotification">{{
      notification
    }}</Alert>
  </div>

  <div v-else>
    <NoOrganization class="organization" @show-create-form="createTeamFormIsVisible = true" />

    <Modal
      v-if="createTeamFormIsVisible"
      :open="createTeamFormIsVisible"
      @close-modal="createTeamFormIsVisible = false"
    >
      <div class="create-team">
        <CreateTeamForm
          :loading="organizationStore.isCreatingOrganization"
          @create-team="createOrganization"
        />
      </div>
    </Modal>
  </div>
</template>

<style scoped>
.organization {
  @apply max-w-4xl px-3 py-5 xl:pl-10;
}

.header {
  @apply mb-10 flex items-center justify-between;
}

.organization-name {
  @apply font-spoof text-2xl font-bold lg:text-4xl;
}

.members-section {
  @apply flex flex-col gap-4;
}

.members-section-header {
  @apply grid grid-cols-[8fr_6rem_1fr] text-sm text-blue-darkest;
}

.members {
  @apply flex flex-col gap-4;
}

.loader {
  @apply m-3;
}

.button.add-members {
  @apply h-10 border-indigo hover:bg-indigo-lighter;
}

:deep(.add-members-label) {
  @apply text-indigo;
}

.create-team {
  @apply w-[calc(100vw-2rem)] p-6 text-center md:max-w-[28rem];
}
</style>
