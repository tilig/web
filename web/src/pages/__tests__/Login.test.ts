import { beforeEach, describe, expect, it, vi } from 'vitest'
import { render } from '@testing-library/vue'
import { createTestingPinia } from '@pinia/testing'
import { useRoute } from 'vue-router'

import { flushPromises } from '@core/utils/tests'
import { useTrackingStore } from '@core/stores/tracking'
import { useAuthStore } from '@core/stores/auth'

import Login from '../Login.vue'

const patchWithAuthenticatedUser = () => {
  const store = useAuthStore()
  store.$patch({
    accessToken: { accessToken: 'test ' },
    keyPair: { privateKey: '1235' },
    legacyKeyPair: { privateKey: '1234' },
    hasFirebaseUser: true,
    tiligUser: {
      displayName: 'Fred Durst',
    },
  })
}

// We use the router inside the Login component
vi.mock('vue-router', () => ({
  useRouter: vi.fn(() => ({
    push: vi.fn(),
  })),
  useRoute: vi.fn(() => ({ query: { my: 'query' } })),
}))

const pinia = createTestingPinia({ createSpy: vi.fn })

const renderLogin = (options?: { authenticated: boolean }) => {
  const { authenticated = false } = options ?? {}

  if (authenticated) {
    patchWithAuthenticatedUser()
  }

  return render(Login, {
    global: {
      plugins: [pinia],
    },
  })
}

const mockQuery = (query: Record<string, string>) => {
  vi.mocked(useRoute).mockImplementation(() => {
    return {
      query,
    } as ReturnType<typeof useRoute>
  })
}

describe('Login.vue', () => {
  beforeEach(() => {
    vi.mocked(useRoute).mockClear()
    vi.mocked(useTrackingStore().trackPerson).mockClear()
  })

  it('renders correctly when unauthenticated', () => {
    renderLogin()
  })

  it('renders correctly when authenticated', () => {
    renderLogin({
      authenticated: true,
    })
  })

  it('tracks UTM params when authenticated and UTM params present', async () => {
    mockQuery({
      utm_source: 'google',
    })

    renderLogin({
      authenticated: true,
    })

    await flushPromises()

    expect(vi.mocked(useRoute)).toHaveBeenCalled()

    const trackingStore = useTrackingStore()

    expect(vi.mocked(trackingStore.trackPerson)).toHaveBeenCalledOnce()
    expect(vi.mocked(trackingStore.trackPerson)).toHaveBeenCalledWith({
      utm_source: 'google',
    })
  })

  it('does not tracks UTM params when authenticated without UTM params present', async () => {
    mockQuery({
      other_param: 'google',
    })

    renderLogin({
      authenticated: true,
    })

    await flushPromises()

    expect(vi.mocked(useRoute)).toHaveBeenCalled()
    const trackingStore = useTrackingStore()
    expect(vi.mocked(trackingStore.trackPerson)).not.toHaveBeenCalled()
  })
})
