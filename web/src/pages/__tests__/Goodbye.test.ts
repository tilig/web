import { render } from '@testing-library/vue'
import { describe, test, expect, vi, beforeEach } from 'vitest'
import { createTestingPinia } from '@pinia/testing'

import GoodbyePage from '../Goodbye.vue'

import { useTrackingStore } from '@core/stores/tracking'
import { TrackingEvent } from '@core/utils/trackingConstants'

const pinia = createTestingPinia({ createSpy: vi.fn })

const renderGoodbyePage = () => {
  return render(GoodbyePage, {
    global: {
      plugins: [pinia],
    },
  })
}
describe('Goodbye Page', () => {
  beforeEach(() => {
    vi.mocked(useTrackingStore().trackEvent).mockClear()
    vi.mocked(useTrackingStore().trackAnonymousEvent).mockClear()
  })
  test('Should render properly ', () => {
    const { getByText } = renderGoodbyePage()

    const pageTitle = getByText(`We'll miss you!`)

    expect(pageTitle).toBeTruthy()
  })

  test('Triggers the BROWSER_EXTENSION_UNINSTALLED tracking event when the page loads', () => {
    renderGoodbyePage()

    const trackingStore = useTrackingStore()

    expect(vi.mocked(trackingStore.trackEvent)).toHaveBeenCalledOnce()
    expect(vi.mocked(trackingStore.trackEvent)).toHaveBeenCalledWith({
      event: TrackingEvent.BROWSER_EXTENSION_UNINSTALLED,
    })
  })
})
