import * as z from 'zod'
import { unzip } from 'unzipit'

import {
  CustomField,
  FieldKind,
  HistoryField,
  initializeDetails,
  initializeOverview,
  setInMain,
  Template,
} from '@core/clients/tilig'

import { CSVItemResult, CSVParseResult, FolderResultMap } from './csvParser'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('services:import:onePassword')

const OnePwOverview = z.object({
  title: z.string().nullable(),
  subtitle: z.string().nullable(),
  url: z.string().nullable(),
})
type OnePwOverview = z.infer<typeof OnePwOverview>

const OptionalNullableString = z.string().nullable().optional()
const OptionalNullableNumber = z.number().nullable().optional()

const OnePwValue = z
  .object({
    address: z
      .object({
        street: OptionalNullableString,
        city: OptionalNullableString,
        country: OptionalNullableString,
        zip: OptionalNullableString,
        state: OptionalNullableString,
      })
      .nullable()
      .optional(),
    concealed: OptionalNullableString,
    creditCardNumber: OptionalNullableString,
    creditCardType: OptionalNullableString,
    date: OptionalNullableNumber,
    email: z
      .object({
        email_address: OptionalNullableString,
      })
      .nullable()
      .optional(),
    file: z
      .object({
        fileName: OptionalNullableString,
      })
      .nullable()
      .optional(),
    menu: OptionalNullableString,
    monthYear: OptionalNullableNumber,
    phone: OptionalNullableString,
    string: OptionalNullableString,
    totp: OptionalNullableString,
    url: OptionalNullableString,
  })
  .passthrough()
type OnePwValue = z.infer<typeof OnePwValue>

const OnePWLoginField = z.object({
  value: OptionalNullableString,
  designation: OptionalNullableString,
})
type OnePWLoginField = z.infer<typeof OnePWLoginField>

const OnePwSectionField = z.object({
  id: z.string(),
  title: z.string(),
  value: OnePwValue,
})
type OnePwSectionField = z.infer<typeof OnePwSectionField>

const OnePwSection = z.object({
  title: OptionalNullableString,
  fields: OnePwSectionField.array(),
})
type OnePwSection = z.infer<typeof OnePwSection>

const OnePwPasswordHistory = z.object({
  value: z.string(),
  time: z.number(),
})
type OnePwPasswordHistory = z.infer<typeof OnePwPasswordHistory>

const OnePwDetails = z.object({
  loginFields: OnePWLoginField.array().optional(),
  sections: OnePwSection.array(),
  documentAttributes: z
    .object({
      fileName: OptionalNullableString,
    })
    .nullable()
    .optional(),
  password: OptionalNullableString,
  passwordHistory: OnePwPasswordHistory.array().optional(),
  notesPlain: OptionalNullableString,
})
type OnePwDetails = z.infer<typeof OnePwDetails>

const OnePwItem = z.object({
  categoryUuid: z.string(),
  overview: OnePwOverview,
  details: OnePwDetails,
})
type OnePwItem = z.infer<typeof OnePwItem>

const OnePwVault = z.object({
  attrs: z.object({
    name: OptionalNullableString,
    desc: OptionalNullableString,
    uuid: z.string(),
    type: OptionalNullableString,
  }),
  items: OnePwItem.array(),
})
type OnePwVault = z.infer<typeof OnePwVault>

export const OnePwPuxContents = z.object({
  accounts: z
    .object({
      vaults: OnePwVault.array(),
    })
    .array(),
})
export type OnePwPuxContents = z.infer<typeof OnePwPuxContents>

const timestampToISOString = (timestamp: number | null) => {
  if (!timestamp) return null
  return new Date(timestamp * 1000).toISOString()
}

const monthYearToString = (monthYear: number | null) => {
  if (!monthYear) return null
  const month = monthYear % 100
  const year = Math.floor(monthYear / 100) % 1000
  return `${String(month).padStart(2, '0')}/${String(year).padStart(2, '0')}`
}

type CompoundValueType = string | number | boolean | null | undefined
type CompoundValue = CompoundValueType | Record<string, CompoundValueType>
const compoundValueToString = (value: CompoundValue): string | null => {
  if (value === null || value === undefined) return null

  if (typeof value === 'string' || typeof value === 'number' || typeof value === 'boolean') {
    return String(value)
  }

  return (
    Object.entries(value)
      .map(([key, val]) => [key, compoundValueToString(val)])
      .filter(([_, value]) => !!value)
      .map(([key, value]) => `${key}: ${value}`)
      .join(', ') || null
  )
}

const parseValue = (value: OnePwValue): [FieldKind, string | null] | undefined => {
  if ('string' in value) {
    return ['text', value.string || null]
  } else if ('concealed' in value) {
    return ['secret', value.concealed || null]
  } else if ('totp' in value) {
    return ['totp', value.totp || null]
  } else if ('address' in value) {
    return ['text', compoundValueToString(value.address)]
  } else if ('creditCardNumber' in value) {
    return ['text', value.creditCardNumber || null]
  } else if ('creditCardType' in value) {
    let type = value.creditCardType || null
    if (type === 'visa') type = 'Visa'
    if (type === 'mc') type = 'Mastercard'
    return ['text', type]
  } else if ('date' in value) {
    return ['date', timestampToISOString(value.date || null)]
  } else if ('email' in value) {
    return ['text', value.email?.email_address || null]
  } else if ('file' in value) {
    return ['text', value.file?.fileName || null]
  } else if ('menu' in value) {
    return ['text', value.menu || null]
  } else if ('monthYear' in value) {
    return ['text', monthYearToString(value.monthYear || null)]
  } else if ('phone' in value) {
    return ['text', value.phone || null]
  } else if ('url' in value) {
    return ['text', value.url || null]
  } else {
    return ['text', compoundValueToString(value as CompoundValue)]
  }
}

type ParsedLoginValues = {
  username: string | null
  password: string | null
}

const parseLoginFields = (loginFields: OnePWLoginField[]) => {
  const result: ParsedLoginValues = {
    username: null,
    password: null,
  }

  loginFields.forEach(({ designation, value }) => {
    if (designation === 'password') {
      result.password = value || null
    } else if (designation === 'username') {
      result.username = value || null
    }
  })

  return result
}

type MainFields<T extends FieldKind = FieldKind> = {
  [k in T]: string | null
}
type ItemDetailsParser = (item: OnePwItem) => {
  template: Template
  info?: string | null
  mainFields?: MainFields
  history?: HistoryField[]
  customFields?: CustomField[]
}

/**
 * Parse a 1pw item into a Tilig item
 * @param item 1pw item
 * @param itemDetailsParser Parser that extract details from 1pw item
 */
const parseItem = (item: OnePwItem, itemDetailsParser: ItemDetailsParser): CSVItemResult => {
  const overview = initializeOverview()
  const details = initializeDetails()

  // Set common fields
  overview.urls = item.overview.url ? [{ url: item.overview.url }] : []
  overview.name = item.overview.title || null
  details.notes = item.details.notesPlain || null

  // Get details from parser
  const { template, info, mainFields, history, customFields } = itemDetailsParser(item)

  // Set details
  if (info) overview.info = info
  if (mainFields) {
    Object.entries(mainFields).forEach(([field, value]) => setInMain(details, field, value))
  }
  if (history) details.history = history
  if (customFields) details.custom_fields = customFields

  // Some items have a password field in details
  if (item.details.password) {
    details.custom_fields.push({ name: 'Password', kind: 'password', value: item.details.password })
  }

  // Some items only store files
  if (item.details.documentAttributes) {
    details.custom_fields.push({
      name: 'Original filename',
      kind: 'text',
      value: item.details.documentAttributes.fileName || null,
    })
  }

  return { template, overview, details }
}

const addCustomField = (customFields: CustomField[], title: string, value: OnePwValue) => {
  const parsedValue = parseValue(value)
  if (!parsedValue) return
  let name: string = title

  if (!name) {
    if ('file' in value) {
      name = 'Original filename'
    } else if ('address' in value) {
      name = 'Address'
    } else {
      name = 'Field'
    }
  }
  customFields.push({ name, kind: parsedValue[0], value: parsedValue[1] })
}

const parseLoginItemDetails = (item: OnePwItem) => {
  const { password, username } = parseLoginFields(item.details.loginFields || [])

  const mainFields: MainFields<'username' | 'password' | 'totp'> = {
    username,
    password,
    totp: null,
  }
  const history: HistoryField[] = []
  const customFields: CustomField[] = []

  const sectionFields = item.details.sections.flatMap((section) => section.fields)
  sectionFields.forEach(({ title, value }) => {
    if ('totp' in value && !mainFields.totp) {
      // Grab first TOTP field
      mainFields.totp = value.totp || null
    } else {
      addCustomField(customFields, title, value)
    }
  })
  item.details.passwordHistory?.forEach(({ value, time }) => {
    history.push({
      kind: 'password',
      value,
      replaced_at: timestampToISOString(time) as string,
    })
  })

  return {
    template: Template.Login,
    info: username,
    mainFields,
    history,
    customFields,
  }
}
const parseCreditCardItemDetails = (item: OnePwItem) => {
  const mainFields: MainFields<'ccnumber' | 'ccholder' | 'ccexp' | 'cvv' | 'pin' | 'zipcode'> = {
    ccnumber: null,
    ccholder: null,
    ccexp: null,
    cvv: null,
    pin: null,
    zipcode: null,
  }
  const customFields: CustomField[] = []

  const sectionFields = item.details.sections.flatMap((section) => section.fields)
  sectionFields.forEach(({ title, id, value }) => {
    switch (id) {
      case 'cardholder':
        mainFields.ccholder = value.string || null
        break
      case 'ccnum':
        mainFields.ccnumber = value.creditCardNumber || null
        break
      case 'cvv':
        mainFields.cvv = value.concealed || null
        break
      case 'expiry':
        mainFields.ccexp = monthYearToString(value.monthYear || null)
        break
      case 'pin':
        mainFields.pin = value.concealed || null
        break
      default:
        addCustomField(customFields, title, value)
    }
  })

  return {
    template: Template.CreditCard,
    info: mainFields.ccnumber?.slice(-4),
    mainFields,
    customFields,
  }
}
const parseSecureNoteDetails = (item: OnePwItem) => {
  const customFields: CustomField[] = []

  const sectionFields = item.details.sections.flatMap((section) => section.fields)
  sectionFields.forEach(({ title, value }) => addCustomField(customFields, title, value))

  return {
    template: Template.SecureNote,
    mainFields: {},
    customFields,
  }
}
const parseWifiItemDetails = (item: OnePwItem) => {
  const mainFields: MainFields<'ssid' | 'password'> = {
    ssid: null,
    password: null,
  }
  const customFields: CustomField[] = []

  const sectionFields = item.details.sections.flatMap((section) => section.fields)
  sectionFields.forEach(({ title, id, value }) => {
    switch (id) {
      case 'network_name':
        mainFields.ssid = value.string || null
        break
      case 'wireless_password':
        mainFields.password = value.concealed || null
        break
      default:
        addCustomField(customFields, title, value)
    }
  })

  return {
    template: Template.WiFi,
    mainFields,
    customFields,
  }
}

const parseCustomItemDetails = (item: OnePwItem) => {
  const customFields: CustomField[] = []

  const sectionFields = item.details.sections.flatMap((section) => section.fields)
  sectionFields.forEach(({ title, value }) => addCustomField(customFields, title, value))

  return {
    template: Template.Custom,
    mainFields: {},
    customFields,
  }
}

/**
 * Parse a 1pw item into a Tilig item
 * @param item 1pw item
 */
const parseOnePwItem = (item: OnePwItem): CSVItemResult => {
  let detailsParser: ItemDetailsParser

  switch (item.categoryUuid) {
    case '001':
      detailsParser = parseLoginItemDetails
      break
    case '002':
      detailsParser = parseCreditCardItemDetails
      break
    case '003':
      detailsParser = parseSecureNoteDetails
      break
    case '109':
      detailsParser = parseWifiItemDetails
      break
    default:
      detailsParser = parseCustomItemDetails
  }

  return parseItem(item, detailsParser)
}

const PERSONAL_VAULT_TYPE = 'P'

export const parseOnePwPuxContents = (puxContents: unknown, onlyPersonalVaults?: boolean) => {
  const contents = OnePwPuxContents.parse(puxContents)
  const folders: FolderResultMap = new Map()
  const items: CSVItemResult[] = []

  contents.accounts
    .flatMap((account) => account.vaults)
    .forEach((vault) => {
      if (onlyPersonalVaults && vault.attrs.type !== PERSONAL_VAULT_TYPE) {
        return
      }

      folders.set(vault.attrs.uuid, {
        name: vault.attrs.name || 'New folder',
        description: vault.attrs.desc || null,
      })

      vault.items.map(parseOnePwItem).forEach((item) => {
        items.push({
          ...item,
          temporaryFolderId: vault.attrs.uuid,
        })
      })
    })

  return { folders, items }
}

type PuxBuffer = Parameters<typeof unzip>[0]

export const extractDataFromPux = async (puxBuffer: PuxBuffer) => {
  const unzipped = await unzip(puxBuffer)
  const dataEntry = unzipped.entries['export.data']
  if (!dataEntry) throw new Error('No export.data file in zip')

  let numberOfFiles = 0
  for (const filename in unzipped.entries) {
    if (/^files\/.+$/.test(filename)) numberOfFiles++
  }

  return {
    data: await dataEntry.json(),
    numberOfFiles,
  }
}

export const parseOnePwPux = async (
  puxBuffer: PuxBuffer,
  onlyPersonalVaults?: boolean
): Promise<{ parseResult: CSVParseResult; numberOfFiles: number }> => {
  try {
    const { data, numberOfFiles } = await extractDataFromPux(puxBuffer)
    const { folders, items } = parseOnePwPuxContents(data, onlyPersonalVaults)

    const parseResult: CSVParseResult = {
      headers: ['name', 'notes', 'password', 'totp', 'username', 'website', 'folder'],
      items,
      folders,
      success: true,
      originalData: [],
    }

    return {
      parseResult,
      numberOfFiles,
    }
  } catch (e) {
    logger.error('Error parsing pux', e)
    return {
      parseResult: {
        success: false,
        headers: [],
        errors: [e as Error],
      },
      numberOfFiles: 0,
    }
  }
}
