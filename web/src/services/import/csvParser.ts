import {
  Template,
  DecryptedOverview,
  DecryptedDetails,
  initializeOverview,
  initializeDetails,
  setInMain,
} from '@core/clients/tilig'
import { convertToUrl } from '@core/utils/url'
import { parse, ParseError } from 'papaparse'

const HEADER_ALIASES = {
  name: [/(\b|[_-])name(\b|[_-])/i, /(\b|[_-])title(\b|[_-])/i],
  notes: [
    /(\b|[_-])(other)?notes?(\b|[_-])/i,
    /(\b|[_-])description(\b|[_-])/i,
    /(\b|[_-])extra(\b|[_-])/i,
  ],
  totp: [
    /(\b|[_-])t?otp(\b|[_-])/i,
    /(\b|[_-])t?otpauth(\b|[_-])/i,
    /(\b|[_-])t?otpsecret(\b|[_-])/i,
  ],
  password: [/(\b|[_-])password(\b|[_-])/i, /(\b|[_-])secret(\b|[_-])/i],
  username: [
    /(\b|[_-])username(\b|[_-])/i,
    /(\b|[_-])email(\b|[_-])/i,
    /(\b|[_-])emailaddress(\b|[_-])/i,
  ],
  website: [/(\b|[_-])website(\b|[_-])/i, /(\b|[_-])ur(i|l)(\b|[_-])/i],
  folder: [/(\b|[_-])folder(\b|[_-])/i, /(\b|[_-])group(ing)?(\b|[_-])/i],
}

const IGNORED_FOLDER_NAMES = ['(none)']

export type CSVHeader = keyof typeof HEADER_ALIASES

export type HeaderMapping = Partial<Record<CSVHeader, number | null>>
type CSVRow = string[]
export type CSVData = CSVRow[]

export type CSVRowParseResult = Partial<Record<CSVHeader, string | null>>

export interface FolderResult {
  name: string
  description?: string | null
}
export type FolderResultMap = Map<string, FolderResult>

/**
 * A single result item
 */
export interface CSVItemResult {
  template?: Template
  temporaryFolderId?: string
  overview: DecryptedOverview
  details: DecryptedDetails
}

interface CSVParseResultSuccess {
  success: true
  headers: CSVHeader[]
  items: CSVItemResult[]
  folders?: Map<string, FolderResult>
  originalData: CSVData
}

interface CSVParseResultError {
  success: false
  headers: []
  errors: (ParseError | Error)[]
}

/**
 * Result from parsing a CSV file
 */
export type CSVParseResult = CSVParseResultSuccess | CSVParseResultError

const filledHeaders = (headerMapping: HeaderMapping) => {
  return Object.keys(headerMapping).filter(
    (k) => headerMapping[k as CSVHeader] !== null
  ) as CSVHeader[]
}

export const parseData = (
  headerMapping: HeaderMapping,
  data: CSVData,
  originalData?: CSVData | null,
  dataMapper?: (result: CSVRowParseResult) => CSVItemResult
): CSVParseResult => {
  const mappedData = mapData(headerMapping, data)
  const items = mappedData.map(dataMapper || loginItemFromResult)
  const folders: FolderResultMap = new Map()

  items.forEach(({ temporaryFolderId }) => {
    if (temporaryFolderId) {
      folders.set(temporaryFolderId, { name: temporaryFolderId })
    }
  })

  return {
    success: true,
    headers: filledHeaders(headerMapping),
    items,
    folders,
    originalData: originalData || data,
  }
}

/**
 * Automatically parse a CSV file with headers into a result
 * @param contents string contents of csv file
 */
export const parseCsv = (contents: string): CSVParseResult => {
  const { data, errors } = extractData(contents)

  if (errors.length) {
    return {
      success: false,
      headers: [],
      errors,
    }
  }

  // Save a copy of the original data in case headers are missing.
  const originalData = [...data]
  const headers = data.shift()

  if (!headers) {
    return {
      success: false,
      headers: [],
      errors: [],
    }
  }

  const headerMapping = mapHeaders(headers)
  return parseData(headerMapping, data, originalData)
}

/**
 * Extract rows from a CSV file
 *
 * Skips empty rows, headers (if present) are the first row
 * @param contents string contents of a csv file
 */
export const extractData = (contents: string) => {
  return parse<string[]>(contents, { header: false, skipEmptyLines: true })
}

/**
 * Automatically map headers to a list of columns
 * @param headers list of headers
 */
export const mapHeaders = (headers: string[]) => {
  const mapping = {} as HeaderMapping

  Object.entries(HEADER_ALIASES).forEach(([header, aliases]) => {
    const index = headers.findIndex((h) => aliases.some((a) => a.test(h)))
    mapping[header as CSVHeader] = index === -1 ? null : index
  })

  return mapping
}

/**
 * Apply a headerMapping to csv rows
 * @param headerMapping column to header mapping
 * @param data parsed csv rows
 */
const mapData = (headerMapping: HeaderMapping, data: CSVData) => {
  return data.map((row) => {
    const mapped: CSVRowParseResult = {}

    Object.entries(headerMapping).forEach(([header, index]) => {
      // Empty values are mapped to `null`
      mapped[header as CSVHeader] = index === null ? null : row[index] || null
    })

    return mapped
  })
}

export const loginItemFromResult = (result: CSVRowParseResult): CSVItemResult => {
  const overview = initializeOverview()
  const details = initializeDetails()

  overview.name = result.name ?? null
  overview.info = result.username ?? null

  if (result.website) {
    overview.urls = [{ url: convertToUrl(result.website) }]
  }

  details.notes = result.notes ?? null
  setInMain(details, 'username', result.username ?? null)
  setInMain(details, 'password', result.password ?? null)
  setInMain(details, 'totp', result.totp ?? null)

  const temporaryFolderId =
    result.folder && !IGNORED_FOLDER_NAMES.includes(result.folder) ? result.folder : undefined

  return { template: Template.Login, temporaryFolderId, overview, details }
}
