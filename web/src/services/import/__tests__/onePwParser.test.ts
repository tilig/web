import { describe, expect, it } from 'vitest'
import { readFile } from 'fs/promises'
import * as path from 'path'

import { OnePwPuxContents, extractDataFromPux, parseOnePwPuxContents } from '../onePwParser'
import exportData from './__fixtures__/1pux_export.data.json'

describe('1pwParser', () => {
  describe('OnePwPuxContents', () => {
    it('parses', () => {
      expect(OnePwPuxContents.parse(exportData)).toMatchSnapshot()
    })
  })

  describe('item generation', () => {
    it('generates the expected items', () => {
      expect(parseOnePwPuxContents(exportData)).toMatchSnapshot()
    })
  })

  describe('data extraction', () => {
    // Helper needed since unzipit doesn't support Buffer
    const toArrayBuffer = (buffer: Buffer) => {
      const arrayBuffer = new ArrayBuffer(buffer.length)
      const view = new Uint8Array(arrayBuffer)
      for (let i = 0; i < buffer.length; ++i) {
        view[i] = buffer[i]
      }
      return arrayBuffer
    }

    it('extract the json data from export.data', async () => {
      const contents = await readFile(path.resolve(__dirname, '__fixtures__', '1password.1pux'))
      const arrayBuffer = toArrayBuffer(contents)
      const { data, numberOfFiles } = await extractDataFromPux(arrayBuffer)
      expect(data).toEqual(exportData)
      expect(numberOfFiles).toEqual(0)
    })
  })
})
