import { readFileSync } from 'fs'
import { describe, expect, it } from 'vitest'
import * as path from 'path'

import { parseCsv, CSVHeader } from '../csvParser'
import { parseKeeperCsv } from '../keeperParser'
import { parseLastPassCsv } from '../lastPassParser'

interface TestCase {
  filename: string
  headers: CSVHeader[]
  items: number
}

const TEST_CASES: TestCase[] = [
  {
    filename: '1password_nonotes_10items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: '1password_notes_10items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'bitwarden_nonotes_10items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'bitwarden_notes_10items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'dashlane_nonotes_10items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'dashlane_notes_10items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'lastpass_nonotes_8items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 8,
  },
  {
    filename: 'lastpass_notes_copiedfromweb_10items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'lastpass_notes_downloaded_8items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 8,
  },
  {
    filename: 'other_customheaders_nonotes_28items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 28,
  },
  {
    filename: 'other_nonotes_41items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 41,
  },
  {
    filename: 'remembear_nonotes_10items',
    headers: ['name', 'notes', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'remembear_notes_10items',
    headers: ['name', 'notes', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'chrome_nonotes_6items',
    headers: ['name', 'password', 'username', 'website'],
    items: 6,
  },
  {
    filename: 'edge_nonotes_6items',
    headers: ['name', 'password', 'username', 'website'],
    items: 6,
  },
  {
    filename: 'firefox_nonotes_7items',
    headers: ['password', 'username', 'website'],
    items: 7,
  },
  {
    filename: 'safari_nonotes_6items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 6,
  },
  {
    filename: 'tilig_nonotes_41items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 41,
  },
]

const KEEPER_TEST_CASES: TestCase[] = [
  {
    filename: 'keeper_nonotes_10items',
    headers: ['name', 'notes', 'password', 'username', 'website'],
    items: 10,
  },
  {
    filename: 'keeper_notes_10items',
    headers: ['name', 'notes', 'password', 'username', 'website'],
    items: 10,
  },
]

const LASTPASS_TEST_CASES: TestCase[] = [
  {
    filename: 'lastpass_custom_22items',
    headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
    items: 22,
  },
]

const CASES = [
  { title: 'CSV files with headers', parser: parseCsv, cases: TEST_CASES },
  { title: 'Keeper headerless CSV files', parser: parseKeeperCsv, cases: KEEPER_TEST_CASES },
  { title: 'Lastpass custom item CSV files', parser: parseLastPassCsv, cases: LASTPASS_TEST_CASES },
]

// FIXME: Finish this one
// const HEADERLESS_TEST_CASES = [
//   {
//     filename: './other_noheaders_nonotes_28items',
//     headers: ['name', 'notes', 'totp', 'password', 'username', 'website'],
//     items: 10,
//   },
// ]

const readCsvAsText = (filename: string) =>
  readFileSync(path.resolve(__dirname, '__fixtures__', `${filename}.csv`), {
    encoding: 'utf8',
    flag: 'r',
  })

describe('CSV parsing', () => {
  describe.each(CASES)('$title', ({ parser, cases }) => {
    describe.each(cases)('parses $filename into $items items', ({ filename, headers, items }) => {
      it(`parses the csv succesfully`, () => {
        const contents = readCsvAsText(filename)
        const result = parser(contents)

        expect(result.success).toBe(true)
        expect(result.headers).toEqual(expect.arrayContaining(headers))

        expect('items' in result).toBe(true)

        if ('items' in result) {
          expect(result.items.length).toBe(items)
          expect(result.items).toMatchSnapshot()
        }
      })
    })
  })
})
