import {
  CustomField,
  initializeOverview,
  initializeDetails,
  setInMain,
  Template,
} from '@core/clients/tilig'
import { isEqual } from 'lodash'
import {
  CSVParseResult,
  extractData,
  HeaderMapping,
  loginItemFromResult,
  parseData,
  CSVItemResult,
  CSVRowParseResult,
  parseCsv,
} from './csvParser'

const LASTPASS_EXPECTED_HEADERS = [
  'url',
  'username',
  'password',
  'totp',
  'extra',
  'name',
  'grouping',
  'fav',
]

/**
 * Parse a Keeper CSV file into a result
 * @param contents string contents of csv file
 */
export const parseLastPassCsv = (contents: string): CSVParseResult => {
  const { data, errors } = extractData(contents)

  if (errors.length) {
    return {
      success: false,
      headers: [],
      errors,
    }
  }

  const headerMapping: HeaderMapping = {
    website: 0,
    username: 1,
    password: 2,
    totp: 3,
    notes: 4,
    name: 5,
    folder: 6,
  }

  // Remove header row
  const headers = data.shift()
  if (!isEqual(LASTPASS_EXPECTED_HEADERS, headers)) {
    // If this isn't a lastpass CSV, parse like any other CSV.
    return parseCsv(contents)
  }

  return parseData(headerMapping, data, null, parseLastPassResult)
}

type LastPassField = {
  field: string
  value: string
}

const splitLastPassLine = (line: string): [string, string] => {
  const i = line.indexOf(':')
  if (i < 0) return ['', '']
  return [line.slice(0, i), line.slice(i + 1)]
}

const lastPassNotesToFields = (lines: string[]) => {
  const fields: LastPassField[] = []

  while (lines.length > 0) {
    const line = lines.shift()
    if (!line) break

    const [field, value] = splitLastPassLine(line)

    if (field === 'Notes') {
      fields.push({ field, value: [value, ...lines].join('\n') })
      break
    } else if (field) {
      fields.push({ field, value })
    }
  }

  return fields
}

const getLastPassFieldValue = (field: string, fields: LastPassField[]) =>
  fields.find(({ field: f }) => field === f)?.value || null

const lastPassMonths = {
  January: '01',
  February: '02',
  March: '03',
  April: '04',
  May: '05',
  June: '06',
  July: '07',
  August: '08',
  September: '09',
  October: '10',
  November: '11',
  December: '12',
}

const parseLastPassMonthYear = (monthYear: string | null) => {
  if (!monthYear) return null
  const [month, year] = monthYear.split(',', 2)
  if (!month || !year) return null
  return `${lastPassMonths[month as keyof typeof lastPassMonths]}/${year.substring(2)}`
}

const lastPassFieldToKind = (field: string) => {
  if (/passw/i.test(field)) {
    return 'password'
  } else if (/(routing|pin)/i.test(field)) {
    return 'secret'
  } else {
    return 'text'
  }
}

const parseFolderName = (name: string | null | undefined) => {
  if (!name) return undefined
  if (name === '(none)') return undefined
  return name
}

/**
 * Parses a lastpass custom item value into a string
 *
 * Some values are actually JSON encoded objects
 * @param value Row value
 */
const parseLastPassCustomFieldValue = (value: string) => {
  if (/^{.*}$/.test(value)) {
    try {
      return Object.entries(JSON.parse(value))
        .map((arr) => arr.join(': '))
        .join(', ')
    } catch {
      return value
    }
  } else {
    return value
  }
}
const lastPassFieldsToCustomFields = (
  fields: LastPassField[],
  without: string[] = []
): CustomField[] => {
  return fields
    .filter(({ field }) => !without.includes(field))
    .map(({ field, value }) => ({
      kind: lastPassFieldToKind(field),
      name: field,
      value: parseLastPassCustomFieldValue(value) || null,
    }))
}

const parseLastPassCreditCard = (result: CSVRowParseResult, fields: LastPassField[]) => {
  const overview = initializeOverview()
  const details = initializeDetails()

  overview.name = result.name ?? null

  setInMain(details, 'ccnumber', getLastPassFieldValue('Number', fields))
  setInMain(details, 'ccholder', getLastPassFieldValue('Name on Card', fields))
  setInMain(
    details,
    'ccexp',
    parseLastPassMonthYear(getLastPassFieldValue('Expiration Date', fields))
  )
  setInMain(details, 'cvv', getLastPassFieldValue('Security Code', fields))
  setInMain(details, 'pin', getLastPassFieldValue('Pin', fields))
  setInMain(details, 'zipcode', null)

  details.notes = getLastPassFieldValue('Notes', fields)

  details.custom_fields = lastPassFieldsToCustomFields(fields, [
    'Number',
    'Name on Card',
    'Expiration Date',
    'Security Code',
    'Pin',
    'Notes',
  ])

  return {
    template: Template.CreditCard,
    temporaryFolderId: parseFolderName(result.folder),
    overview,
    details,
  } as CSVItemResult
}
const parseLastPassWifi = (result: CSVRowParseResult, fields: LastPassField[]) => {
  const overview = initializeOverview()
  const details = initializeDetails()

  overview.name = result.name ?? null

  setInMain(details, 'ssid', getLastPassFieldValue('SSID', fields))
  setInMain(details, 'password', getLastPassFieldValue('Password', fields))

  details.notes = getLastPassFieldValue('Notes', fields)
  details.custom_fields = lastPassFieldsToCustomFields(fields, ['SSID', 'Password', 'Notes'])

  return {
    template: Template.WiFi,
    temporaryFolderId: parseFolderName(result.folder),
    overview,
    details,
  } as CSVItemResult
}
const parseLastPassCustomItem = (result: CSVRowParseResult, fields: LastPassField[]) => {
  const overview = initializeOverview()
  const details = initializeDetails()

  overview.name = result.name ?? null
  overview.urls = []
  details.notes = getLastPassFieldValue('Notes', fields)
  details.custom_fields = lastPassFieldsToCustomFields(fields, ['Notes'])

  return {
    template: Template.Custom,
    temporaryFolderId: parseFolderName(result.folder),
    overview,
    details,
  } as CSVItemResult
}

const parseLastPassCustomNotes = (result: CSVRowParseResult): CSVItemResult => {
  if (!result.notes) throw new Error('Cannot parse lastpass item without notes')

  const lines = result.notes.split(/\r?\n/)
  const itemLine = lines.shift()

  if (!itemLine) throw new Error('This lastpass item is empty')

  const itemType = itemLine.split(':', 2)[1]
  const lastPassFields = lastPassNotesToFields(lines)

  switch (itemType) {
    case 'Credit Card':
      return parseLastPassCreditCard(result, lastPassFields)
    case 'Wi-Fi Password':
      return parseLastPassWifi(result, lastPassFields)
    default:
      return parseLastPassCustomItem(result, lastPassFields)
  }
}

const parseLastPassResult = (result: CSVRowParseResult) => {
  if (result.website !== 'http://sn') {
    return loginItemFromResult(result)
  }

  if (!result.notes?.startsWith('NoteType:')) {
    const overview = initializeOverview()
    const details = initializeDetails()
    overview.name = result.name ?? null
    details.notes = result.notes ?? null

    return {
      template: Template.SecureNote,
      temporaryFolderId: parseFolderName(result.folder),
      overview,
      details,
    }
  }

  return parseLastPassCustomNotes(result)
}
