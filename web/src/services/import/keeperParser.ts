import { CSVParseResult, extractData, HeaderMapping, parseData } from './csvParser'

/**
 * Parse a Keeper CSV file into a result
 * @param contents string contents of csv file
 */
export const parseKeeperCsv = (contents: string): CSVParseResult => {
  const { data, errors } = extractData(contents)

  if (errors.length) {
    return {
      success: false,
      headers: [],
      errors,
    }
  }

  const headerMapping: HeaderMapping = {
    name: 1,
    notes: 5,
    totp: null,
    password: 3,
    username: 2,
    website: 4,
  }

  return parseData(headerMapping, data)
}
