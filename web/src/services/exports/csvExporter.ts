import { unparse } from 'papaparse'
import { DecryptedDetails, findInMain } from '@core/clients/tilig'
import { ExportItem } from '@/stores/exports'

export const toCsv = (fields: string[], data: string[][]) => {
  return unparse({ fields, data })
}

type RowData = Omit<ExportItem, 'item'>

const onePasswordFieldsForAccount = ({ overview, details }: RowData) => {
  return [
    overview.name ?? '',
    overview.urls[0]?.url ?? '',
    findInMain(details, 'username') ?? '',
    findInMain(details, 'password') ?? '',
    findInMain(details, 'totp') ?? '',
    details.notes ?? '',
  ]
}

/**
 * Export to 1Password compatible CSV
 * @see https://support.1password.com/import-1password-com/
 * @param items Items to export
 */
export const onePasswordCsv = (items: ExportItem[]) => {
  return toCsv(
    ['Title', 'URL', 'Username', 'Password', 'One-Time Password', 'Notes'],
    items.map(onePasswordFieldsForAccount)
  )
}

/**
 * Export to Generic CSV
 *
 * Re-uses 1Password export
 * @param items Items to export
 */
export const genericCsv = onePasswordCsv

const notesWithTotp = (details: DecryptedDetails) => {
  let notes = `${details.notes}`

  const totp = findInMain(details, 'totp')
  if (totp) notes += `\n\nOTP Secret: ${totp}`

  return notes
}

const lastPassFieldsForAccount = ({ overview, details }: RowData) => {
  return [
    overview.urls[0]?.url ?? '',
    findInMain(details, 'username') ?? '',
    findInMain(details, 'password') ?? '',
    details.notes ?? '',
    overview.name ?? '',
    'Tilig Import',
    '0',
    findInMain(details, 'totp') ?? '',
  ]
}

/**
 * Export to LastPass compatible CSV
 * @see https://support.lastpass.com/help/how-do-i-import-stored-data-into-lastpass-using-a-generic-csv-file
 * @param items Items to export
 */
export const lastPassCsv = (items: ExportItem[]) => {
  return toCsv(
    ['url', 'username', 'password', 'extra', 'name', 'grouping', 'fav', 'totp'],
    items.map(lastPassFieldsForAccount)
  )
}

const dashlaneFieldsForAccount = ({ overview, details }: RowData) => {
  return [
    'Login',
    overview.name ?? '',
    overview.urls[0]?.url ?? '',
    findInMain(details, 'username') ?? '',
    findInMain(details, 'password') ?? '',
    details.notes ?? '',
    findInMain(details, 'totp') ?? '',
  ]
}

/**
 * Export to Dashlane compatible CSV
 * @see https://support.dashlane.com/hc/en-us/articles/360004101920-Import-your-data-into-Dashlane#csv
 * @param items Items to export
 */
export const dashlaneCsv = (items: ExportItem[]) => {
  return toCsv(
    ['Type', 'name', 'url', 'username', 'password', 'note', 'totp'],
    items.map(dashlaneFieldsForAccount)
  )
}

const bitwardenFieldsForAccount = ({ overview, details }: RowData) => {
  return [
    'Tilig Import',
    '0',
    'login',
    overview.name ?? '',
    details.notes ?? '',
    '',
    overview.urls[0]?.url ?? '',
    findInMain(details, 'username') ?? '',
    findInMain(details, 'password') ?? '',
    findInMain(details, 'totp') ?? '',
  ]
}

/**
 * Export to Bitwarden compatible CSV
 * @see https://bitwarden.com/help/condition-bitwarden-import/#for-your-individual-vault
 * @param items Items to export
 */
export const bitwardenCsv = (items: ExportItem[]) => {
  return toCsv(
    [
      'folder',
      'favorite',
      'type',
      'name',
      'notes',
      'fields',
      'login_uri',
      'login_username',
      'login_password',
      'login_totp',
    ],
    items.map(bitwardenFieldsForAccount)
  )
}

const keeperFieldsForAccount = ({ overview, details }: RowData) => {
  return [
    'Tilig Import',
    overview.name ?? '',
    findInMain(details, 'username') ?? '',
    findInMain(details, 'password') ?? '',
    overview.urls[0]?.url ?? '',
    notesWithTotp(details),
  ]
}

/**
 * Export to Keeper compatible CSV
 * @see https://docs.keeper.io/user-guides/import-records-1/import-a-.csv-file
 * @param items Items to export
 */
export const keeperCsv = (items: ExportItem[]) => {
  return toCsv(
    ['Folder', 'Title', 'Login', 'Password', 'URL', 'Notes'],
    items.map(keeperFieldsForAccount)
  )
}
