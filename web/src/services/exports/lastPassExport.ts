import { unparse } from 'papaparse'

import { ExportItem } from '@/stores/exports'
import { Template, findInMain } from '@core/clients/tilig'

export const toCsv = (data: string[][]) => {
  const fields = ['url', 'username', 'password', 'totp', 'extra', 'name', 'grouping', 'fav']
  return unparse({ fields, data })
}

export const toLastPassCsv = (items: ExportItem[]) => {
  const rows = items.map(toRow)
  return toCsv(rows)
}

const toRow = (item: ExportItem) => {
  switch (item.item.template) {
    case Template.Login:
      return loginToRow(item)
    case Template.WiFi:
      return wifiToRow(item)
    case Template.CreditCard:
      return creditCardToRow(item)
    default:
      return secureNoteToRow(item)
  }
}

//url,username,password,totp,extra,name,grouping,fav

const loginToRow = ({ item, overview, details }: ExportItem) => {
  const notes = [details.notes ?? '']

  details.custom_fields.forEach(({ name, value }) => {
    notes.push(`${name}:${value}`)
  })

  return [
    overview.urls[0]?.url ?? '',
    findInMain(details, 'username') ?? '',
    findInMain(details, 'password') ?? '',
    findInMain(details, 'totp') ?? '',
    notes.join('\n'),
    overview.name ?? '',
    item.folder?.name ?? '',
    '0',
  ]
}

const wifiToRow = ({ item, overview, details }: ExportItem) => {
  const notes = ['NoteType:Wi-Fi Password']
  notes.push(`SSID:${findInMain(details, 'ssid')}`)
  notes.push(`Password:${findInMain(details, 'password')}`)

  details.custom_fields.forEach(({ name, value }) => {
    notes.push(`${name}:${value}`)
  })

  notes.push(`Notes:${details.notes ?? ''}`)

  return [
    'http://sn',
    '',
    '',
    '',
    notes.join('\n'),
    overview.name ?? '',
    item.folder?.name ?? '',
    '0',
  ]
}

const secureNoteToRow = ({ item, overview, details }: ExportItem) => {
  const notes = [details.notes ?? '']

  details.custom_fields.forEach(({ name, value }) => {
    notes.push(`${name}:${value}`)
  })

  return [
    'http://sn',
    '',
    '',
    '',
    notes.join('\n'),
    overview.name ?? '',
    item.folder?.name ?? '',
    '0',
  ]
}

const lastPassMonths = {
  '01': 'January',
  '02': 'February',
  '03': 'March',
  '04': 'April',
  '05': 'May',
  '06': 'June',
  '07': 'July',
  '08': 'August',
  '09': 'September',
  '10': 'October',
  '11': 'November',
  '12': 'December',
}

const creditCardToRow = ({ item, overview, details }: ExportItem) => {
  const notes = ['NoteType:Credit Card']

  notes.push(`Number:${findInMain(details, 'ccnumber')}`)
  notes.push(`Name on Card:${findInMain(details, 'ccholder')}`)

  let lastpassExp = ''
  const exp = findInMain(details, 'ccexp')
  if (exp) {
    const [m, y] = exp.split('/')
    lastpassExp = `${lastPassMonths[m as keyof typeof lastPassMonths]},20${y}`
  }

  notes.push(`Expiration Date:${lastpassExp}`)
  notes.push(`Security Code:${findInMain(details, 'cvv')}`)
  notes.push(`Pin:${findInMain(details, 'pin')}`)

  details.custom_fields.forEach(({ name, value }) => {
    notes.push(`${name}:${value}`)
  })

  notes.push(`Notes:${details.notes ?? ''}`)

  return [
    'http://sn',
    '',
    '',
    '',
    notes.join('\n'),
    overview.name ?? '',
    item.folder?.name ?? '',
    '0',
  ]
}
