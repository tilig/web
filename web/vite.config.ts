/// <reference types="vitest" />
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import svgLoader from 'vite-svg-loader'

import * as packageJson from './package.json'

const APP_VERSION = JSON.stringify(packageJson.version)
const COMMIT_REF = JSON.stringify(process.env.COMMIT_REF)

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    svgLoader({
      defaultImport: 'url',
    }),
  ],
  server: {
    port: 3001,
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@core': fileURLToPath(new URL('../core/src', import.meta.url)),
    },
  },
  define: {
    APP_VERSION,
    COMMIT_REF,
  },
  build: {
    sourcemap: ['hidden', 'inline'].includes(process.env.SOURCEMAP)
      ? (process.env.SOURCEMAP as 'hidden' | 'inline')
      : Boolean(process.env.SOURCEMAP),
    target: 'es2020',
    rollupOptions: {
      output: {
        manualChunks: {
          vendor: [
            // '@firebase/auth/internal',
            '@sentry/tracing',
            '@sentry/vue',
            '@vueuse/core',
            'axios',
            'bowser',
            'debug',
            // 'firebase/app',
            // 'firebase/auth',
            // 'firebase/utils',
            'lodash',
            'totp-generator',
            'unleash-proxy-client',
            'vue-router',
            'vue',
            'zod',
          ],
          stores: ['pinia-plugin-persistedstate', 'pinia'],
          crypto: ['libsodium-wrappers'],
          imports: ['papaparse', 'unzipit'],
          // ui: ['@headlessui/vue', '@heroicons/vue'],
        },
      },
    },
  },
  // @ts-expect-error Vitest types still don't work
  test: {
    globals: true,
    environment: 'jsdom',
  },
  optimizeDeps: {
    esbuildOptions: {
      target: 'es2020',
    },
  },
})
