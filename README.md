# Tilig Web and Browser extension

This monorepo contains the code for both the SPA web application as well as the browser extension (soon), and all shared code.

Both the webapp and the browser extension are based on [Vue 3](https://vuejs.org) and are built using [Vite](https://vitejs.dev).

A lot of the code has been written in JavaScript, but we are slowly making the transition to TypeScript. In general, when adding new code, try to write it in TypeScript.

## IDE Setup

We recommend that you use the devcontainer that is bundled with this repository. These contain
all recommended extensions.

- [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
  Make sure you disable the built-in Typescript extension, since Volar has a bundled
  version that works better with Vue and Vite. Otherwise, install `Vue.vscode-typescript-vue-plugin`.

  To disable the built-in extension, search for `@builtin typescript` and disable `TypeScript and JavaScript Language Features`.

- Eslint and Prettier.
- GitLens, NPM/JS helpers.
- Tailwind

If you want to install the extensions manually, here is a list:

```javascript
const extensions = [
  'bradlc.vscode-tailwindcss',
  'christian-kohler.npm-intellisense',
  'dbaeumer.vscode-eslint',
  'eamodio.gitlens',
  'eg2.vscode-npm-script',
  'esbenp.prettier-vscode',
  'ms-azuretools.vscode-docker',
  'tamasfe.even-better-toml',
  'Vue.volar',
]
```

Using the devcontainer version of this is quite easy. Simply use the "Open the Workspace as a Container" using the [VSCode Remote Container extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers).

## Monorepo

This repository is a [monorepo](https://monorepo.tools), which means it contains the source code for multiple packages.

We chose a monorepo, because the webapp and the extension (should) share a lot of code, due to consitent branding, cryptography, and functionality.

We use [yarn workspaces](https://classic.yarnpkg.com/lang/en/docs/workspaces/) to manage the packages in this repository.

The repo consists of three packages:

- _web_

  This package contains everything needed to develop and build the SPA.

- _core (soon)_

  This package contains all code that is shared between the SPA and the extension.

- _ext (soon)_

  This package contains everything needed to develop and build the browser extension.

Yarn commands can be run in two ways:

- `cd` into the package directory and run `yarn <command>`

- Stay on the top level dir and run `yarn workspace <package> <command>`
  (e.g. `yarn workspace web dev`)

**NOTE:** Sometimes installing packages (`yarn install`, `yarn add`) will have a different effect when run from the top dir or a package dir. If you run into issues, run `yarn install` again from the top level directory.

## Development

### Web app

To run the web app in development mode, run

- `cd web && yarn dev`, or
- `yarn workspace web dev`

The vite dev server serves on port `3001` by default.

See more details in [web/README.md](web/README.md)

### Browser extension

Running the browser extension is similar:

- `cd ext && yarn dev`, or
- `yarn workspace ext dev`

Then open the resulting `/ext/dist` folder in Chrome or Firefox Canary as an unpacked extension.

See more details in [ext/README.md](ext/README.md)

## Testing

We are working on increasing the test coverage of the code base. There are currently three types of
tests we are writing:

- unit tests
- component tests
- end2end tests

### Unit and component tests

Unit and component tests are run using vitest. You can run all tests using

```bash
yarn test
```

You can run the individual package tests by running

```bash
yarn workspace <package> test
```

### E2E tests

We use cypress to run e2e tests, which are run in a real automated browser environment. E2E
tests are also called _integration tests_. They should test complete flows from a user's
perspective, and should test the behavior of the app as a whole.

There are three ways of runnig e2e tests:

1. Locally using the `tilig-dev` firebase environment. In this case, you'll need to download
   a set of serviceAccount.json credentials.

   ```bash
   yarn workspace web test:start # Wait for process to boot up
   CYPRESS_TEST_UID=<user you want to test> yarn workspace web test:open  # Opens cypress app
   ```

2. Locally using emulators for firebase.
   After starting the emulators, you'll need to create a user in the firebase auth emulator and
   grab their UID.

   ```bash
   yarn workspace web test:start
   yarn workspace web test:emulate:firebase
   CYPRESS_TEST_UID=<user you want to test> yarn workspace web test:emulate:open
   ```

3. In the CI, using a headless browser
   ```bash
   yarn workspace web test:start &
   ./bin/wait-for localhost:4173 -- yarn workspace web test:e2e
   ```

## Linting

We're using both ESLint and Prettier. The `eslint-config-prettier` package, along with the configuration in `eslintrc.js` makes sure those two don't clash. Using the `Prettier ESLint` VSCode is what you need. Explanation of hoe ESLint and Prettier work together [here](https://vueschool.io/articles/vuejs-tutorials/eslint-and-prettier-with-vite-and-vue-js-3/).

We're using `husky` to run `lint-staged` as a pre-commit hook.

## Vite env vars

Some useful reading in the [Vite docs](https://vitejs.dev/guide/env-and-mode.html).

## Sentry

Sentry is disabled by default in development. If you want to enable it, create a `.env.development.local` file in the proper package and copy over the `VITE_SENTRY_DSN` var from the `.env.production` file. Sentry is automatically scoped to the Node env, so that should not pollute Sentry data.

## Dependencies

We want to mimize the number of dependencies we use, not because of a NIH mindset, but
because dependencies need to be kept up-to-date, can have vulnerablities, can get abbandoned,
etc. Before adding a new dependency, check if it's really needed. We only allow (new)
dependencies that are actively maintained. If it's no longer maintained, ask yourself if
it would not be a better idea to vendor the code.

If you are adding a dependency, you'll need to add them to this section, to detail why
it was included.

## Shared 'core' library

The shared 'core' library is where we store code which can be shared between
'web' and 'ext'. It automatically compiles on `yarn install` as a postInstall
step, but if you make changes to 'core' you will want to manually rebuild the
library before testing. You can do this by simply running `yarn install` in the
root directory.

### App

- `@headlessui/vue`, `@heroicons/vue`, components and headless behavior
- `@sentry/vue`, error logging
- `axios`, `axios-retry`, making http requests
- `bowser`, user-agent detection
- `firebase`, user authentication
- `lodash`, js standard library
- `otplib`, generating one time passwords
- `papaparse`, csv parsing
- `psl`, domain parsing
- `vue`, `vue-router`, general vue dependencies
- `vuex`, `vuex-persistedstate`, state management

### Crypto

Currently, we use primitives from the node `crypto` library for our asymmetric encryption.
This module was not build for the browser. Until we change to the new security architecture,
we'll need a way to make the library behave correctly in the browser. Webpack used to shim
these modules automatically, but the available options for `vite` + `rollup` seem to break
pretty fast.

We use `patch-package` with a `postinstall` yarn hook to fix dependencies for our app.

- `crypto-browserify`, browser port of node's `crypto` library
- `events`, browser port of node's `events` library
- `stream-browserify`, browser port of node's `stream` library

### Building

- `netlify-cli`, production deployment
- `tailwindcss`, `autoprefixer`, `postcss`, `@tailwindcss/forms`, tailwind CSS
- `vite`, `@vitejs/plugin-vue`, building

### Development

- `cypress`, `cypress-*`, `cross-env`, `firebase-*`, e2e testing
- `eslint`, `eslint-*`, linting
- `prettier`, `prettier-*`, formatting

### Logging

We're using [debug-js](https://github.com/debug-js/debug) for logging.
If you want to enable all logs, type `localStorage.debug = '*'` in your browser console and refresh the page. Some other examples:

- `localStorage.debug = 'stores:profile'` for the profile store
- `localStorage.debug = 'stores:*'` for all stores
