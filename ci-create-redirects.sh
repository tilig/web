# Clean file
touch public/_redirects
> public/_redirects

# TODO: loop in reverse, because Gitlab only reads the first 1000 lines of a
# redirects file apparently.

# Loop over folders in reverse order
for d in public/dev/*/ ;
do
 # Cut /public part from path
 echo $d
 subdir=$(echo $d | awk '{print substr($0,8)}')
 echo "subdir: ${subdir}"
 LINE="/clients/webapp/${subdir}* /clients/webapp/${subdir}index.html 200"
 # For debugging
 echo $LINE;
 # Add line to redirects file
 echo $LINE >> public/_redirects
 # Also add the main version
done

# Fix for wildcard redirects, see https://gitlab.com/gitlab-org/gitlab-pages/-/issues/649
python3 -c "import string;print('\n'.join(f'/{x}* /index.html 200' for x in string.ascii_letters))" >> public/_redirects

echo "Final _redirects:"
cat public/_redirects