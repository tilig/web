const valueLiterals = ['self', 'none', 'unsafe-inline', 'unsafe-eval']

type CSVEntry = string | null
type CSVLine = CSVEntry | CSVEntry[]
type CSPMapping = Record<string, CSVLine>

const valueify = (val: CSVLine): string => {
  if (!val) return ''

  if (Array.isArray(val)) {
    return val
      .filter((x) => x)
      .map(valueify)
      .join(' ')
  }
  const trimmed = val.trim()
  return valueLiterals.includes(trimmed) || trimmed.startsWith('sha') ? `'${trimmed}'` : trimmed
}

const headerify = (obj: CSPMapping) => {
  return Object.entries(obj)
    .filter(([_k, v]) => v && v.length > 0)
    .map(([k, v]) => {
      return `${k} ${valueify(v)}`
    })
    .join('; ')
}

export class CSP {
  constructor(private cspObj: CSPMapping) {}

  toString() {
    return headerify(this.cspObj)
  }
}
