module.exports = {
  env: {
    webextensions: true,
    jest: true,
  },
  globals: {
    vi: true,
  },
}
