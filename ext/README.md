# Browser extension

The primary docs for this extension can be found it the [root project README.md](../README.md)

## Environment setup

This extension has several defaults that work well for both development and
production builds. It's unlikely you'll need to change anything but if you do
there are environment variables that can be set. See addendum.

The easiest way to get started is to use the devcontainer provided, but you can
run it on your own system provided you have the required development tools
installed:

- Node.js v16.x
- Yarn 1.22.x

## Getting Started

You may want to review the
[Chrome Extension Development Guide](https://developer.chrome.com/docs/extensions/mv3/devguide/)
before to getting started.

### Install dependencies and build the shared core library

Before working on the extenion, please run `yarn install` at the root directory
to install npm dependencies and compile the core library.

### Build an 'unpacked' the extension

At this point you should be able to build the extension and load it into your
browser in development mode. When the extension is built, it's files will be
placed in `ext/dist` where you can then install it into your browser.

You can find out more about loading a development extension [here](https://developer.chrome.com/docs/extensions/mv3/getstarted/#unpacked)

In the 'ext' directory run:

`yarn build` or `yarn build:dev` to build the respective versions

### Developing with HMR

In order to speed up development, it's possible to use Hot Module Reloading to
quickly view UI changes to the code. The alternative is to build after each
change and manually reload the extension, which can take > 20s.

In HMR mode, Vite builds the HTML files, but the assets are loaded via localhost.
For extension development, this means the `ext/dist` folder contains the HTML
files and manifest.json, but the assets are loaded via localhost:5143.

To start in HMR mode run `yarn dev`, then load the unpacked extension as usual.
You may notice an intial delay in the responsiveness of the popups/ui elements
while Vite compiles the application for the first time.

If you want to use production data, run `yarn dev:prod_api`

### Logger messages in console

This application uses [`debug-js`] for loggging. To enable all logging:

`localStorage.debug = '\*'

To only enable ui event logging (useful for debugging input types):

`localStorage.debug = 'ext:ui:\*'

To see runtime messaging:

`localStorage.debug = 'ext:api:\*'

Ensure your console log level is set to 'Debug'

**Note:** In order to view background.html generated log messages, please open the extension and click the "background.html" link. This will open the Chrome Devtools in the context of the background process.

#### Test

```
yarn test
```

#### Lints and fixes files

```
yarn lint
```

## Releasing a new version

New versions created when the release job is manually run.

You should increment the version appropriately before a new release

```
yarn version
```

Tags are protected on Gitlab, so deleting them is possible. In case you want to
delete them, you need to remove tag protection first. (And re-enable it
afterwards.)

#### Firefox

Firefox requires us to send along the unminified code. To do this, run:

```
git archive --format=zip [TAG] > code.zip
```

To test local builds, Firefox needs to accept [unsigned add-ons][ffsigning]. This is only possible in [Nightly][ffnightly] and [Developer][ffdev] channels. You also need to

- Go to `about:config`
- Set `xpinstall.signatures.required` to `false`

Install like so: open the `about:debugging` page > **This Firefox** > **Load Temporary Add-on** > select any file in the `/dist` directory.

#### Edge

- Visit `about:flags`
- Check the **Enable extension developer features** option
- Close and restart the browser
- **Menu** > **Extensions** > **Load extensions**

## Environment variables

By default, the extension points to our staging Rails server at
https://api-staging.tilig.com. It's possible to change it using an environment
variable VITE_APIROOT.

Because this environment variable is used by Vite's config file, it cannot be
placed in the .env

`VITE_APIROOT=https://localhost:3000 yarn run dev`

## For Firefox Reviewers

```
# Make sure your node version is the same as stated in .nvmrc
yarn install
yarn run build
```

The source code will be located in the `/dist` folder.

## Useful data

Chrome Key: `MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw2T0/MYCYbhiMb0P22Nj3js/1fU96Rs75K2r932YLSMqvCj5IYYBV7CREwcBpGyeq8Juk4txUaPlZs9zEqV+q4CFxhghC0/bTX23PjWJRNKGtrM8vxQLhi7tiyZvZECAarohv85XBzIG7n4vb/E5R45tlmAhHSBnwOQMk0b6xY/vf1ZG//2I6nb8pJmLruhJFylCmi9SpczV4KEFu1XWKsbA49oExzGn5+vwrju1aAnz49kqaA/HoiDOeU7WCuHw6updKcbs4Z3enCQbrZ8A/X/iWhrLC0qjaFw7bZ5Zz8++oiHTVXeWjuQoWqpKvVgnupDQ7jsW+gdflgD3A89hMwIDAQAB` (found [here][chromekey])

Chrome Webstore ID: `fnnhineegblcmjlhmmogickjhhobgpjo`

#### Dev / staging

OAuth Client ID (for Browser extension only): `765031685617-skt6aeq30nqj5m53rt2i7v0sj2d9nsjh.apps.googleusercontent.com`
Firebse App ID (for Browser extension only): `1:765031685617:web:c1991478eb5d1cd5b5b850`

#### Production

OAuth Client ID (for Browser extension only): `75227666521-9jjhr115b7u2uvsr422ibfojc2qddqo3.apps.googleusercontent.com`
Firebse App ID (for Browser extension only): `1:75227666521:web:733d70fdbf270215ccf948`

[ffsigning]: https://wiki.mozilla.org/Addons/Extension_Signing
[ffnightly]: https://www.mozilla.org/en-US/firefox/channel/desktop/#nightly
[ffdev]: https://www.mozilla.org/en-US/firefox/channel/desktop/#developer
[chromekey]: https://stackoverflow.com/a/32383820/609794
