/// <reference types="vite-svg-loader" />
/// <reference types="@types/chrome" />
/// <reference types="vite/client" />
/// <reference types="vue-router" />
/// <reference types="unplugin-icons/types/vue" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}

// Defined in vite.config.ts and is the version from package.json
declare const APP_VERSION: string
declare const COMMIT_REF: string
