/// <reference types="chrome" />
import { CSP } from './csp'
import * as pkg from './package.json'

type BuildManifestArgs = {
  mode: string
  apiEndpoint: string
  firebaseEndpoint: string
  devServer: string | null
}

export const buildManifest = ({
  mode,
  apiEndpoint,
  firebaseEndpoint,
  devServer,
}: BuildManifestArgs): chrome.runtime.ManifestV2 => {
  const isProd = mode === 'production'

  return {
    manifest_version: 2,
    version: pkg.version,

    author: 'Tilig',
    name: '__MSG_extName__',
    description: '__MSG_extDescription__',
    default_locale: 'en',

    key: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw2T0/MYCYbhiMb0P22Nj3js/1fU96Rs75K2r932YLSMqvCj5IYYBV7CREwcBpGyeq8Juk4txUaPlZs9zEqV+q4CFxhghC0/bTX23PjWJRNKGtrM8vxQLhi7tiyZvZECAarohv85XBzIG7n4vb/E5R45tlmAhHSBnwOQMk0b6xY/vf1ZG//2I6nb8pJmLruhJFylCmi9SpczV4KEFu1XWKsbA49oExzGn5+vwrju1aAnz49kqaA/HoiDOeU7WCuHw6updKcbs4Z3enCQbrZ8A/X/iWhrLC0qjaFw7bZ5Zz8++oiHTVXeWjuQoWqpKvVgnupDQ7jsW+gdflgD3A89hMwIDAQAB',

    background: {
      scripts: ['src/background.ts'],
      persistent: true,
    },

    browser_action: {
      default_popup: 'src/ui/popup/index.html',
      default_title: pkg.displayName || pkg.name,
      default_icon: {
        16: 'icons/16.png',
        24: 'icons/24.png',
        32: 'icons/32.png',
      },
    },

    commands: {
      _execute_browser_action: {
        suggested_key: {
          default: 'Ctrl+Shift+X',
          mac: 'Command+Shift+X',
        },
      },
    },

    content_scripts: [
      {
        all_frames: true,
        matches: ['https://*/*', 'http://*/*'],
        js: ['src/content-scripts/content-script.ts'],
        run_at: 'document_idle',
      },
    ],

    icons: {
      16: 'icons/16.png',
      32: 'icons/32.png',
      48: 'icons/48.png',
      96: 'icons/96.png',
      128: 'icons/128.png',
      256: 'icons/256.png',
    },

    permissions: ['*://*/*', '<all_urls>', 'privacy', 'tabs', 'storage'],
    optional_permissions: ['webRequest'],

    oauth2: {
      client_id: '75227666521-9jjhr115b7u2uvsr422ibfojc2qddqo3.apps.googleusercontent.com',
      scopes: ['https://www.googleapis.com/auth/userinfo.email'],
    },

    web_accessible_resources: [
      'src/assets/images',
      'src/ui/login/index.html',
      'src/ui/welcome/index.html',
      'src/ui/dropdown/index.html',
      'src/ui/autocapture/index.html',
      'src/ui/credentials/index.html',
    ],

    content_security_policy: new CSP({
      'default-src': ['self', devServer ? `http://${devServer}` : null],
      'img-src': [
        'self',
        'https://asset.brandfetch.io',
        'https://img.tilig.com',
        'https://logo.clearbit.com',
        'https://logo.uplead.com',
        'https://lh3.googleusercontent.com',
        'data:',
        devServer ? `http://${devServer}` : null,
      ],
      'connect-src': [
        apiEndpoint,
        'https://www.gstatic.com',
        'https://www.google-analytics.com',
        'https://*.firebaseio.com',
        'https://apis.google.com',
        'https://*.googleapis.com',
        'https://*.ingest.sentry.io',
        'https://us-central1-tilig-dev.cloudfunctions.net',
        'https://us-central1-tilig-prod.cloudfunctions.net',
        'https://flags-staging.tilig.com',
        'https://flags.tilig.com',
        'data:',
        devServer ? `ws://${devServer}` : null,
      ],
      'frame-src': firebaseEndpoint,
      'style-src': ['self', 'unsafe-inline'],
      'script-src': [
        'self',
        'unsafe-eval',
        'https://apis.google.com',
        'https://www.apple.com',
        'https://appleid.cdn-apple.com',
        'https://idmsa.apple.com',
        'https://gsa.apple.com',
        'https://idmsa.apple.com.cn',
        'https://signin.apple.com',
        'https://tilig-dev.firebaseapp.com',
        'https://tilig-prod.firebaseapp.com',
        devServer ? `http://${devServer}` : null,
      ],
      // script-src is patched during dev HMR, so we just copy it here
      'script-src-elem': [
        'self',
        'https://apis.google.com',
        'https://www.apple.com',
        'https://appleid.cdn-apple.com',
        'https://idmsa.apple.com',
        'https://gsa.apple.com',
        'https://idmsa.apple.com.cn',
        'https://signin.apple.com',
        'https://tilig-dev.firebaseapp.com',
        'https://tilig-prod.firebaseapp.com',
        devServer ? `http://${devServer}` : null,
      ],
      // Vite plugin modifies the manifest in HMR/Development mode
      // and add it's own object-src with causes a 'duplicate' key warning
      'object-src': isProd ? 'none' : null,
    }).toString(),

    externally_connectable: {
      matches: ['https://app.tilig.com/*', 'https://*.netlify.app/*'],
    },
  }
}
