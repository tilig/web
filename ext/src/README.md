# Tilig Browser Extension & Web App Shared Code Base

This repo holds the common code that is shared between
the browser extension and the web app.

# Status

### Goal

We aim to share as much code as is reasonable. Either by
using a git submodule, or by creating a (privately distributed)
`npm` package.

### Workflow

There are many small differences between the extension
and the webapp, even in parts where they share exactly the
same functionality. Therefore, we migrate per folder,
using Git's diff functionality to detect the small changes
in similar files and folders. When each part is successfully
merged and compatible with both, we move to the next.

To test the code, it is recommended to:

- move a folder from the browser extension into this repo.
- use a symbolic link so the existing repo can still reference the files in their new location, e.g. `ln -s /path/to/this/repo/{module} /path/to/old/repo/{module}`.
- commit or stage the folder in this repo.
- merge the matching folder from the web app project. This often contains some improvements or cleanups.
- use git to find al differences, and either accept them, revert them, or make sure they are only applied to the extension (see below) or now.
- commit and start from the top.

### Status

- [x] Move the busniess logic
- [x] Move the UI code

# Differences

Use the environment variable `VUE_APP_TILIG_APP_MODE` to determine if
the execution environment is the extension or not.

```javascript
if (process.env['VUE_APP_TILIG_APP_HOME'] == 'extension') {
  //.. do extension-specific things
}
```
