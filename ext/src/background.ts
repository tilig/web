import { createPinia } from 'pinia'
import { createApp, effectScope, watch } from 'vue'
import piniaPluginPersistedState from 'pinia-plugin-persistedstate'

import { createLogger } from '@core/utils/logger'
import { useTrackingStore } from '@core/stores/tracking'
import webappURL from '@core/utils/webappURL'
import {
  BROWSER_EXTENSION_CAPTURE_WEB_CREDENTIALS,
  BROWSER_EXTENSION_INSTALLED,
  BROWSER_EXTENSION_UPDATED,
  WEBREQUEST_API_STATUS,
} from '@core/utils/trackingConstants'
import { setGoogleAnalyticsUserId } from '@core/clients/analytics'
import { HandoffProperties, useAuthStore } from '@core/stores/auth'
import { useFlagsStore } from '@core/stores/flags'
import { useItemsStore } from '@core/stores/items'
import { backgroundMessageListener, externalMessageListener } from './background/messageApis'
import { getWebAuth } from './utils/importWebAuth'
import { addAutocaptureListener } from './background/addAutocaptureListener'
import { notifyWebapp } from './background/notifyWebapp'
import { useEngagementStore } from './background/stores/engagement'
import { sentrySetup } from './utils/captureException'

const logger = createLogger('ext:background')

logger.debug(`:::::::::::: Background init (mode: ${import.meta.env.MODE})  ::::::::::::`)

const app = createApp({})
const pinia = createPinia()
pinia.use(piniaPluginPersistedState)
app.use(pinia)
sentrySetup({ context: 'background' }, app)

const trackStore = useTrackingStore(pinia)
const authStore = useAuthStore(pinia)
authStore.isExtension = true

const itemsStore = useItemsStore(pinia)
const flagsStore = useFlagsStore(pinia)
flagsStore.start()

const engagementStore = useEngagementStore(pinia)

const webRequestApiStatus = addAutocaptureListener(pinia)

// Promisify onInstalled for later use after we attempt authentication
const onInstalled: Promise<chrome.runtime.InstalledDetails> = new Promise((resolve) => {
  chrome.runtime.onInstalled.addListener((details) => resolve(details))
})

/**
 * Show the 'goodbye' page when people uninstall
 */
chrome.runtime.setUninstallURL(`${webappURL()}goodbye`)

/**
 * Handle the messages sent from tabs in one place
 */
chrome.runtime.onMessage.addListener(backgroundMessageListener(pinia))

/**
 * Handle the messages sent from the webapp
 */
chrome.runtime.onMessageExternal.addListener(externalMessageListener(pinia))

/**
 * Attempt to disable Chrome's native autofill
 */
chrome.privacy.services.passwordSavingEnabled.set({ value: false }, () => {
  if (chrome.runtime.lastError) {
    logger.error(
      'Failure while trying to set passwordSavingEnabled to false',
      chrome.runtime.lastError
    )
  }
})
/**
 * Address autofill needs to be disabled too. This is weird Chrome behavior,
 * but not something we can change.
 */
chrome.privacy.services.autofillAddressEnabled.set({ value: false }, () => {
  if (chrome.runtime.lastError) {
    logger.error(
      'Failure while trying to set autofillAddressEnabled to false',
      chrome.runtime.lastError
    )
  }
})

// Returns a promise which resolves to boolean of the authenticated state
const authStateResolved = authStore.initialize().then(async () => {
  if (authStore.isAuthenticated) {
    logger.debug(`Auth initialized: isAuthenticated = '${authStore.isAuthenticated}'`)
    await itemsStore.fetchItems()
    return true
  } else {
    const result = await getWebAuth()
    if (result) {
      logger.debug('Imported webauth', result)
      await authStore.handoff(result as HandoffProperties)
      await trackStore.trackEvent({ event: BROWSER_EXTENSION_CAPTURE_WEB_CREDENTIALS })
      return true
    } else {
      return false
    }
  }
})

/**
 * An effect scope wherein Vue reactivity works as normal
 */
const backgroundScope = effectScope()
backgroundScope.run(() => {
  /**
   * Track firebase user session
   */
  const stopFirebaseWatch = watch(
    () => authStore.hasFirebaseUser,
    () => {
      const firebaseUser = authStore.getFirebaseUser()
      if (firebaseUser) {
        logger.debug('Tracking user sign in with Google Analytics')
        setGoogleAnalyticsUserId(firebaseUser.uid)
        engagementStore.trackLogin()
        stopFirebaseWatch()
      }
    },
    { immediate: true }
  )
})

// After authentication and if this was an update or install, handle the process from here.
Promise.all([authStateResolved, onInstalled]).then(([_authenticated, installDetails]) => {
  if (installDetails.reason === 'install') {
    trackStore.trackEvent({ event: BROWSER_EXTENSION_INSTALLED })
    notifyWebapp()
  } else if (installDetails.reason === 'update') {
    trackStore.trackEvent({
      event: BROWSER_EXTENSION_UPDATED,
      properties: { previousVersion: installDetails.previousVersion },
    })
  }
  trackStore.trackEvent({
    event: WEBREQUEST_API_STATUS,
    properties: {
      status: webRequestApiStatus,
      source: installDetails.reason,
    },
  })
})

logger.debug(':::::::::::: Background Ready ::::::::::::')
