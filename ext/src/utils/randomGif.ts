import dinosaur from '@/assets/images/90s/dinosaur.gif'
import squirrel from '@/assets/images/90s/squirrel.gif'
import flasingSkelleton from '@/assets/images/90s/flashing_skelleton.gif'
import heart from '@/assets/images/90s/heart.gif'
import spaceship from '@/assets/images/90s/spaceship.gif'
import purple_tear from '@/assets/images/90s/purple_tear.gif'
import welcome_fish from '@/assets/images/90s/welcome_fish.gif'

const allGifs = [dinosaur, squirrel, flasingSkelleton, heart, spaceship, purple_tear, welcome_fish]

export const getRandomGif = () => {
  return allGifs[Math.floor(Math.random() * allGifs.length)]
}
