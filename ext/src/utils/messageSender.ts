import captureException from './captureException'

/**
 * Emit an event to a tab by tab id
 * @param {Integer} tabId The tab number to send to
 * @param {String} event Event name that you are sending
 * @param {any} [payload = null] Payload you are sending to the event handler
 * @param {Object} [headers = {}] Metadata for the event handler
 * @returns {Promise}
 *
 */
export function sendToTab<T>(
  tabId: number,
  event: string,
  payload: any = null,
  headers: any = {}
): Promise<T> {
  return new Promise((resolve, reject) => {
    try {
      chrome.tabs.sendMessage(
        tabId,
        {
          msg: event,
          headers,
          data: payload,
        },
        (response) => resolve(response)
      )
    } catch (error) {
      captureException(error)
      reject(error)
    }
  })
}

/**
 * Emit an event to the active tab
 * @param {String} event Event name that you are sending
 * @param {any} [payload = null] Payload you are sending to the event handler
 * @param {Object} [headers = {}] Metadata for the event handler
 * @returns {Promise}
 *
 */
export function sendToActiveTab<T>(
  event: string,
  payload: any = null,
  headers: any = {}
): Promise<T> {
  return new Promise((resolve, reject) => {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
      if (tabs[0] && tabs[0].id) {
        sendToTab<T>(tabs[0].id, event, payload, headers).then(resolve).catch(reject)
      } else {
        reject(new Error('No active tab found'))
      }
    })
  })
}

/**
 * Emit an event to a tab and ensure that it is only triggered on the provided
 * domain. Useful for passing credentials and avoiding a race condition where a
 * subsequent page (after navigation) recieves the event and payload
 * See: https://gitlab.com/subshq/clients/browser/-/issues/154
 * @param {Integer} tabId The tab number to send to
 * @param {String} domain Domain you intended the message for
 * @param {String} event Event name that you are sending
 * @param {any} [payload = null] Payload you are sending to the event handler
 * @returns {Promise}
 *
 */
export function sendToTabAndDomain<T>(
  tabId: number,
  domain: string,
  event: string,
  payload: any = null
): Promise<T> {
  return sendToTab<T>(tabId, event, payload, { domain })
}

/**
 * Emit an event to a tab from the content script to iframes on the tab
 * @param {String} event Event name that you are sending
 * @param {any} [payload = null] Payload you are sending to the event handler
 * @returns {Promise}
 *
 */
export const sendToRuntime = <T>(event: string, payload: any = null): Promise<T> => {
  return new Promise((resolve, reject) => {
    try {
      chrome.runtime.sendMessage(
        {
          msg: event,
          data: payload,
        },
        (result) => resolve(result as T)
      )
    } catch (error) {
      captureException(error)
      reject(error)
    }
  })
}
