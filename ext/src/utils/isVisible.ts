// Logic from https://github.com/jquery/jquery/blob/master/src/css/hiddenVisibleSelectors.js
const isVisible = (element: HTMLElement): boolean => {
  const hasOffsets = !!(
    element.offsetWidth ||
    element.offsetHeight ||
    element.getClientRects().length
  )

  if (!hasOffsets) return false

  /** Catch elements that are hidden from assistive technology, as they are
   * usually not meant to be interacted with.
   * https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-hidden
   */
  if (element.ariaHidden === 'true') return false

  return true
}

export default isVisible
