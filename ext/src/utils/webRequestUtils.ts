import { createLogger } from '@core/utils/logger'

const logger = createLogger('utils:webRequestUtils')

export const findInObject = (obj: any, password: string): boolean => {
  if (typeof obj === 'string') {
    return obj === password
  } else if (Array.isArray(obj)) {
    for (const arr of obj) {
      if (findInObject(arr, password)) {
        return true
      }
    }
  } else if (typeof obj === 'object') {
    for (const key of Object.keys(obj)) {
      if (findInObject(obj[key], password)) {
        return true
      }
    }
  }
  return false
}
export const matchWebRequestBodyToPassword = (
  requestBody: chrome.webRequest.WebRequestBody,
  password: string
): boolean => {
  if (requestBody.formData) {
    return findInObject(requestBody.formData, password)
    // Iterante all keys and see if there's a match
  } else if (requestBody.raw && requestBody.raw.length > 0 && requestBody.raw[0].bytes) {
    try {
      const enc = new TextDecoder('utf-8')
      const rawJson = JSON.parse(enc.decode(requestBody.raw[0].bytes as ArrayBuffer))
      return findInObject(rawJson, password)
    } catch (err) {
      logger.debug(`Error parsing raw request body ${err}`)
      return false
    }
  }
  return false
}
