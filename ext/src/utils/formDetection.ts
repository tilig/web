import {
  guessInputType,
  InputTypes,
  isEmailInput,
  isPasswordInput,
  isTextInput,
  isUsernameInput,
} from './inputTyper'
import isVisible from './isVisible'
import { matchFormToAttributes, matchInputToAttributes } from './matchElementToAttributes'

export enum FormTypes {
  Signup = 'signup',
  Login = 'login',
  ChangePassword = 'change-password',
}

export type FormResultType = FormTypes | null

export type FormResult = {
  match: FormResultType
  name: string
  description?: string | null
}

function findMatchesFor<T>(
  element: T,
  matcherName: string,
  matcherFunction: ((el: T) => FormResult)[]
): FormResult {
  for (const test of matcherFunction) {
    const { match, name, description } = test(element)
    if (match) {
      return { match, name: `${matcherName}: ${name}`, description }
    }
  }
  return { match: null, name: `${matcherName}: No match` }
}

// Get the high possible parentElement of the passed in element
const getHighestParent = (
  element: HTMLElement,
  level: number,
  currentParentElement: HTMLElement | null = null
): HTMLElement | null => {
  if (element.parentElement && level === 0) {
    return element.parentElement
  } else if (element.parentElement) {
    return getHighestParent(element.parentElement, level - 1, element.parentElement)
  }
  return currentParentElement
}

/**
 * Look in a form for the submit button
 * @param {HTMLFormElement} parent From element to check
 * @returns {HTMLButtonElement | null} The button we think is submit
 */
const findSubmitButton = (parent: HTMLElement): HTMLButtonElement | null => {
  const submitButton =
    (parent.querySelector('button[type="submit"]') as HTMLButtonElement) ||
    (parent.querySelector('input[type="submit"]') as HTMLButtonElement)
  if (submitButton) {
    return submitButton
  }
  return null
}

/**
 * Look in a form element for any buttons that are visible
 * @param {HTMLFormElement} parent From element to check
 * @returns {HTMLButtonElement | null} The button we think is submit
 */
const findActionButtons = (parent: HTMLElement): HTMLButtonElement[] => {
  const buttons = (Array.from(parent.querySelectorAll('button')) as HTMLButtonElement[]).filter(
    (b) => isVisible(b)
  )
  return buttons
}

/**
 * Check an element to see if it looks like a signup element from the attributes and naming
 * @param {HTMLInputElement} input Input element to check
 * @returns {boolean} If this looks like a sign in input
 */
export const isSignupInput = (input: HTMLInputElement): boolean => {
  const inputType = guessInputType(input)?.match
  if (!inputType) return false

  if (inputType == InputTypes.Password && matchInputToAttributes(input, newPasswordMatchers))
    return true
  if (inputType == InputTypes.Email && matchInputToAttributes(input, newEmailMatchers)) return true

  return false
}

const newPasswordMatchers = [
  /(new.?Password)/i,
  /(confirm.?Password)/i,
  /(password.?Confirmation)/i,
]
const newEmailMatchers = [/(new.?Email)/i, /(confirm.?Email)/i]

const SignupFormInputsMin = 4

const buttonTextMatcher = (buttonText: string | null): FormTypes | null => {
  if (!buttonText) return null
  if (loginFormMatchers.find((r) => buttonText.match(r))) return FormTypes.Login
  else if (signupFormMatchers.find((r) => buttonText.match(r))) return FormTypes.Signup
  return null
}

const matchOnFormAttributes = (form: HTMLFormElement): FormResult => {
  const name = 'Form Attribute Matcher'
  let match = null
  if (matchFormToAttributes(form, signupFormAttributeMatchers)) {
    match = FormTypes.Signup
  } else if (matchFormToAttributes(form, loginFormAttributeMatchers)) {
    match = FormTypes.Login
  } else if (matchFormToAttributes(form, changePasswordFormAttributeMatchers)) {
    match = FormTypes.ChangePassword
  }
  if (match) return { match, name }
  return { match: null, name }
}
const signupFormAttributeMatchers = [/(sign.?up)/i, /(create)/i, /(register)/i]
const loginFormAttributeMatchers = [/(log.?in)/i, /(sign.?in)/i]
const changePasswordFormAttributeMatchers = [
  /(change.?password)/i,
  /(update.?password)/i,
  /(edit.?password)/i,
]

const matchOnSubmitButton = (parent: HTMLElement): FormResult => {
  const name = 'Submit Button Matcher'
  const submitButton = findSubmitButton(parent)
  if (submitButton) {
    const buttonText = submitButton.textContent
    const match = buttonTextMatcher(buttonText)
    if (match) return { match, name, description: buttonText }
  }
  return { match: null, name }
}

const matchOnActionButton = (parent: HTMLElement): FormResult => {
  const name = 'Action Button Matcher'
  const actionButtons = findActionButtons(parent)
  for (const actionButton of actionButtons) {
    if (actionButton) {
      const buttonText = actionButton.textContent
      const match = buttonTextMatcher(buttonText)
      if (match) return { match, name, description: buttonText }
    }
  }
  return { match: null, name }
}

const matchOnPasswordLink = (parent: HTMLElement): FormResult => {
  const name = 'Forgot Password Link'
  const anchors = parent.querySelectorAll('a')
  if (anchors) {
    const link = Array.from(anchors).find((a) =>
      a.textContent?.match(new RegExp('forgot password', 'i'))
    )
    if (link) {
      return { match: FormTypes.Login, name, description: link.textContent }
    }
  }
  return { match: null, name }
}

const matchOnChangePasswordButton = (parent: HTMLElement): FormResult => {
  const name = 'Change password style button'
  const passwordInputs = Array.from(parent.querySelectorAll('input')).filter((input) =>
    isPasswordInput(input)
  )
  if (passwordInputs.length) {
    const actionButtons = findActionButtons(parent)
    for (const actionButton of actionButtons) {
      if (actionButton) {
        const buttonText = actionButton.textContent
        if (changePasswordMatchers.find((r) => buttonText?.match(r))) {
          return {
            match: FormTypes.ChangePassword,
            name,
            description: `Match on button '${buttonText}'`,
          }
        }
      }
    }
  }
  return { match: null, name }
}

const matchOnMultipleInputs =
  (minInputs: number) =>
  (inputs: HTMLInputElement[]): FormResult => {
    const name = `Multiple inputs > ${minInputs}`
    if (inputs) {
      const inputsArr = Array.from(inputs)
      const inputLength = inputsArr.filter((i) => isTextInput(i) && isVisible(i)).length
      if (inputLength >= minInputs)
        return { match: FormTypes.Signup, name, description: `${inputLength} inputs found` }
    }
    return { match: null, name }
  }

const matchOnContainingSignupInputs = (inputs: HTMLInputElement[]): FormResult => {
  const name = 'Sign-in inputs'
  if (inputs) {
    const inputsArr = Array.from(inputs)
    const signupInput = inputsArr.find((i) => isSignupInput(i))
    if (signupInput)
      return { match: FormTypes.Signup, name, description: 'Found a signin input in the form' }
  }
  return { match: null, name }
}

const matchOnTwoPasswordInputs = (inputs: HTMLInputElement[]): FormResult => {
  const name = 'Two password inputs'
  if (inputs) {
    const inputsArr = Array.from(inputs)
    if (inputsArr.filter((i) => isTextInput(i) && i.type == 'password').length == 2) {
      return { match: FormTypes.Signup, name, description: 'Found two password inputs' }
    }
  }
  return { match: null, name }
}

// Assume three password inputs is a change password form
const matchOnThreePasswordInputs = (inputs: HTMLInputElement[]): FormResult => {
  const name = 'Three password inputs'
  if (inputs) {
    const inputsArr = Array.from(inputs)
    if (inputsArr.filter((i) => isTextInput(i) && i.type == 'password').length == 3) {
      return { match: FormTypes.ChangePassword, name, description: 'Found two password inputs' }
    }
  }
  return { match: null, name }
}

// Match any form containing a single password input and a username or email input
const matchOnSingleUsernameEmailPasswordInputs = (inputs: HTMLInputElement[]): FormResult => {
  const name = 'Basic Login'
  if (inputs) {
    const inputsArr = Array.from(inputs)
    const passwordCount = inputsArr.filter((i) => isTextInput(i) && i.type == 'password').length
    const emailCount = inputsArr.filter((i) => isEmailInput(i)).length
    const usernameCount = inputsArr.filter((i) => isUsernameInput(i)).length
    if (passwordCount + emailCount + usernameCount === 2)
      return {
        match: FormTypes.Login,
        name,
        description: 'Contains a single password input and an email or username input',
      }
  }
  return { match: null, name }
}

/**
 * Determine is a form is a Signup or Login Form
 */

export const profileForm = (form: HTMLFormElement): FormResult => {
  let result = findMatchesFor<HTMLFormElement>(form, 'Form element', [
    matchOnFormAttributes,
    matchOnSubmitButton,
    matchOnActionButton,
    matchOnPasswordLink,
    matchOnChangePasswordButton,
  ])

  if (result.match) return result

  const inputs = Array.from(form.querySelectorAll('input'))

  result = findMatchesFor<HTMLInputElement[]>(inputs, 'Form element', [
    matchOnThreePasswordInputs,
    matchOnTwoPasswordInputs,
    matchOnMultipleInputs(SignupFormInputsMin),
    matchOnContainingSignupInputs,
    matchOnSingleUsernameEmailPasswordInputs,
  ])
  return result
}

/**
 * Check an element to see if it looks like a signIn element.
 * Exclude elements with attribute implying that a new email or password
 * is being set.
 * Example: Changing your password where it asks for the existing password
 * then the new password twice.
 * @param {HTMLInputElement} input Input element to check parent form of
 * @returns [{string|null}, string] The best guess, along with the reason for this guess as a tuple
 */
export const guessFormType = (input: HTMLInputElement): FormResult => {
  // Look at the parent form:
  // - Submit button indicators (eg. "Signup")
  // - Forgot password link (login)
  // - Multiple text input field (signup)
  // - Other input fields suggest signup (id='confirm_email')
  // - Two password inputs (signup)

  const form = input.form
  if (form) {
    const result = profileForm(form)
    if (result.match) return result
  }

  /**
   * If we aren't in a proper form, look up a couple levels and see if we
   * can make a determination
   * */
  const parentElement = getHighestParent(input, 2)
  if (parentElement) {
    const result = findMatchesFor<HTMLElement>(parentElement, 'Parent element', [
      matchOnSubmitButton,
      matchOnActionButton,
      matchOnPasswordLink,
    ])
    if (result.match) return result
  }

  // Finally, just see if we can detect a signup for from a single element (eg. 'id:create-user')
  if (isSignupInput(input)) return { match: FormTypes.Signup, name: 'Signup input outside of form' }

  return { match: null, name: 'No Match' }
}

const signupFormMatchers = [/(sign.?up)/i, /(create)/i, /(register)/i, /(get started)/i]
const loginFormMatchers = [/(log.?in)/i, /(sign.?in)/i]
const changePasswordMatchers = [/(change.?password)/i, /(update)/i, /(edit.?password)/i]
