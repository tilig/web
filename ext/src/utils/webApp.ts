import { parseDomain } from '@core/utils/domainUtils'

export const executeScript = async (tabId: number, code: string) => {
  return new Promise((resolve) => {
    chrome.tabs.executeScript(tabId, { code }, (result: unknown[]) => {
      if (result && result[0]) {
        resolve(result[0])
      }
      resolve(null)
    })
  })
}

const isTiligDomain = (url?: string): boolean => {
  if (!url) return false
  if (import.meta.env.MODE !== 'production') {
    // We serve web on localhost:3001 locally
    if (url.match('http://localhost:3001/')) {
      return true
    }

    // We use netlify for staging, which parses to the full domain, e.g. preview-123-tilig-staging.netlify.app
    if (parseDomain(url)?.match(/tilig-staging\.netlify\.app$/)) {
      return true
    }
  }
  // Only match app.tilig.com, which has credentials, not tilig.com
  const hostname = new URL(url).hostname
  if (hostname === 'app.tilig.com') {
    return true
  }
  return false
}

interface TabWithId extends chrome.tabs.Tab {
  id: number
}

const tabUrlMatchers =
  import.meta.env.MODE === 'production'
    ? ['https://app.tilig.com/*']
    : ['http://localhost/*', 'https://*.netlify.app/*', 'https://app.tilig.com/*']

export const findTiligTab = async (): Promise<TabWithId | null> => {
  return new Promise((resolve, reject) => {
    try {
      chrome.tabs.query({ url: tabUrlMatchers }, (tabs) => {
        const tab = tabs.find((t) => isTiligDomain(t.url))
        if (tab?.id) {
          resolve(tab as TabWithId)
        } else {
          resolve(null)
        }
      })
    } catch (err) {
      reject(err)
    }
  })
}
