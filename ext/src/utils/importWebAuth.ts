import { createLogger } from '@core/utils/logger'
import { executeScript, findTiligTab } from './webApp'

const logger = createLogger('ext:background:importWebAuth')

export const getWebAuth = async () => {
  const tiligTab = await findTiligTab()

  if (tiligTab) {
    const authValue = await executeScript(tiligTab.id, `localStorage.getItem('auth')`)
    if (!authValue) {
      logger.debug("Webauth import failed, no 'auth' item found")
      return null
    }
    const auth = JSON.parse(authValue as string)

    if ('userJson' in auth && auth.userJson) {
      return { userJson: auth.userJson as string }
    }
  }

  return null
}
