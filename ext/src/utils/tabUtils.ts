/**
 * An async version of chrome.tabs.query
 * @returns {Promise} Promise that returns the active tab, or null.
 */
export const queryActiveTab = async (): Promise<chrome.tabs.Tab | null> => {
  return new Promise((resolve, reject) => {
    try {
      // Chrome API doesn't do promises in Manifest v2
      chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
        if (tabs) {
          resolve(tabs[0])
        } else {
          resolve(null)
        }
      })
    } catch (error) {
      reject(error)
    }
  })
}

type CreateTabProperties = Parameters<typeof chrome.tabs.create>[0]

export const openTab = (options: CreateTabProperties) => {
  return new Promise<chrome.tabs.Tab>((resolve) => {
    chrome.tabs.create(options, resolve)
  })
}

export const openExtensionTab = (src: string, options?: CreateTabProperties) => {
  return openTab({ ...options, url: chrome.runtime.getURL(src) })
}
