/** These message keys are intended for consumption by content-scripts
 *  message listener
 */
export const AUTOCAPTURE_MODAL_TAB_MESSAGE = 'autocapture-modal'
export const CREDENTIAL_MODAL_TAB_MESSAGE = 'credential-modal'
export const FILL_FOCUSSED_INPUT_TAB_MESSAGE = 'fill-focussed-input'
export const FILL_PASSWORD_INPUTS_TAB_MESSAGE = 'fill-password-inputs'
export const FILL_FOCUSSED_FORM_TAB_MESSAGE = 'fill-focussed-form'
export const FILL_FORM_TAB_MESSAGE = 'fill-form'
export const GET_CONTEXT_TAB_MESSAGE = 'get-context'

/** These message keys are intended for consumption by the
 *  background/worker message listener
 */
export const ACCEPT_SUGGESTION_WORKER_MESSAGE = 'accept-suggestion'
export const ACCOUNTS_STORE_WORKER_MESSAGE = 'accounts-store'
export const ATTEMPT_AUTOFILL_WORKER_MESSAGE = 'attempt-autofill'
export const AUTH_MESSAGE_WORKER_MESSAGE = 'auth-message'
export const AUTOCAPTURE_WORKER_MESSAGE = 'autocapture-store'
export const DRAFT_STORE_WORKER_MESSAGE = 'draft-store'
export const FETCH_DROPDOWN_STATE_WORKER_MESSAGE = 'fetch-dropdown-state'
export const FLAGS_API_WORKER_MESSAGE = 'flag-state'
export const GET_BRAND_WORKER_MESSAGE = 'get-brand'
export const ON_PAGE_LOAD_WORKER_MESSAGE = 'on-page-load'
export const SAVE_DRAFT_WORKER_MESSAGE = 'save-draft'
export const SETTINGS_API_WORKER_MESSAGE = 'settings'
export const TAB_API_WORKER_MESSAGE = 'tab-api'
export const TRACK_EVENT_WORKER_MESSAGE = 'track-event'
export const CONTACTS_STORE_WORKER_MESSAGE = 'contacts-store'
