// Take any possible url string and turn it into a valid URL
export const domainToHref = (url: string): string => {
  if (url.match(/^https?:/)) {
    return url
  }
  return 'https://' + url
}
