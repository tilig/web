export const closeWebstore = () => {
  chrome.tabs.query(
    {
      currentWindow: true,
      url: [
        'https://chrome.google.com/webstore/detail/tilig-password-manager/fnnhineegblcmjlhmmogickjhhobgpjo*',
        'https://microsoftedge.microsoft.com/addons/detail/tilig-password-manager/gidnnnbibkapnoapddmkepgbjcleekbc*',
        'https://addons.mozilla.org/en-US/firefox/addon/tilig-password-manager*',
      ],
    },
    (tabs) => {
      tabs.forEach(({ id: tabId }) => tabId && chrome.tabs.remove(tabId))
    }
  )
}
