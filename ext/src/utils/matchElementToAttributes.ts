import { createLogger } from '@core/utils/logger'

const logger = createLogger('formDetection:MatchElementToAttributes')

export const matchInputToAttributes = (element: HTMLInputElement, matchers: RegExp[]): boolean => {
  for (const matcher of matchers) {
    if (
      element.id.match(matcher) ||
      element.name.match(matcher) ||
      element.placeholder.match(matcher) ||
      element.autocomplete.match(matcher) ||
      element.className.match(matcher)
    )
      return true
    // eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
    for (const [_key, value] of Object.entries(element.dataset)) {
      // Maybe use data keys later
      if (value?.match(matcher)) return true
    }
  }
  return false
}

export const matchFormToAttributes = (form: HTMLFormElement, matchers: RegExp[]): boolean => {
  for (const matcher of matchers) {
    if (form.id.match(matcher)) return true

    // Form action, if unset, defaults to the window.location.href
    // We want to ignore that and focus on the location it will post to
    if (form.attributes.getNamedItem('action')) {
      try {
        const path = new URL(form.action).pathname
        if (path.match(matcher)) return true
      } catch (e) {
        logger.warn('Unable to parse form action for ', form.action)
      }
    }
  }
  return false
}
