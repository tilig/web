import { LocationQuery } from 'vue-router'

export const normalizeQuery = (query: LocationQuery): { [key: string]: string } => {
  const result: { [key: string]: string } = {}
  Object.keys(query).forEach((key) => {
    result[key] = query[key]?.toLocaleString() ?? ''
  })
  return result
}
