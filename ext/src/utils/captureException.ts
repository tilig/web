import { App } from 'vue'
import { Router } from 'vue-router'
import * as Sentry from '@sentry/vue'
import { createLogger } from '@core/utils/logger'
import { Integrations } from '@sentry/tracing'
import { Integration } from '@sentry/types'

const logger = createLogger('ext:captureException')

let sentryInitialized = false

const sentryInitialize = (
  tags: { [key: string]: string } = {},
  integrations: Integration[] = [],
  app?: App
) => {
  Sentry.init({
    app,
    dsn: import.meta.env.VITE_SENTRY_DSN,
    release: COMMIT_REF,
    initialScope: {
      tags,
    },
    integrations,
    environment: import.meta.env.MODE,
    tracesSampleRate: 0.01,
  })
  sentryInitialized = true
}

export const sentrySetupVue = (
  tags: { [key: string]: string } = {},
  vueApp: { app: App; router?: Router }
) => {
  const integrations = []
  if (vueApp.router) {
    integrations.push(
      new Integrations.BrowserTracing({
        routingInstrumentation: Sentry.vueRouterInstrumentation(vueApp.router),
      })
    )
  }
  sentryInitialize(tags, integrations, vueApp.app)
}

export const sentrySetup = (tags: { [key: string]: string } = {}, app?: App) => {
  if (!import.meta.env.VITE_SENTRY_DSN) return
  sentryInitialize(tags, [], app)
}

const captureException = (error: unknown, tags: { [key: string]: string } = {}) => {
  if (sentryInitialized) {
    logger.error(`Captured exception: `, error)
    Sentry.captureException(error, { tags })
  } else {
    logger.error(`Sentry not initialized, skipping captureException for: `, error)
  }
}

export default captureException
