/**
 * Always allow 'type' and 'inputmode' and 'autocomplete' to be a definitive match.
 * Then move in order of best guess based on attributes.
 * @param url URL used to guess signup state
 * @returns Returns true if the URL looks like a signup url
 */
export const isSignupUrl = (url: string): boolean => {
  // If URL looks like a signup, return true
  const path = new URL(url).pathname
  const match = signupUrlMatcherRegexList.find((regex) => {
    if (path.match(regex)) return true
  })
  if (match) return true
  return false
}

const signupUrlMatcherRegexList = [
  /(sign.?up)/i,
  /(join)/i,
  /(create.?account)/i,
  /(account.?create)/i,
  /(register|registration)/i,
]
