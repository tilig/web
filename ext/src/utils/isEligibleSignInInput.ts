import { isEmailInput, isPasswordInput, isUsernameInput } from './inputTyper'

export const isEligibleSignInInput = (input: HTMLInputElement) =>
  [isUsernameInput, isEmailInput, isPasswordInput].some((match) => match(input))
