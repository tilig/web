import { parseDomain } from '@core/utils/domainUtils'

/**
 * @param urlStr The url we are getting the name from
 * @param title The title of the page
 * @returns Our best guess at the credential title
 */
export const titleToName = (urlStr: string | null, title: string | null): string => {
  const domain = parseDomain(urlStr)
  if (!domain) {
    // This should never happen with a valid url, but just return the title if it does
    return title || ''
  }

  // Get the first part of the domain to match
  const sld = domain.split('.')[0]

  // Case insensitive match on the title text
  const matcher = new RegExp(sld, 'i')
  const matches = title?.match(matcher)

  // Return the first match, which will include case (eg. CNN from cnn.com)
  if (matches?.length && matches[0].length > 0) {
    return matches[0]
  }

  // Default to the domain as a title
  return domain
}
