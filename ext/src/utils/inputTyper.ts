import isVisible from '@/utils/isVisible'
import { FormResult, FormTypes, guessFormType } from './formDetection'
import { matchInputToAttributes } from './matchElementToAttributes'

export enum InputElementConfidence {
  Low,
  Medium,
  High,
}

export enum InputTypes {
  Email = 'email',
  Username = 'username',
  Password = 'password',
}

export enum InputOverallConfidence {
  Low,
  High,
}

export type InputResultType = {
  match: InputTypes
  /**
   * Allow us to override the form type if we see something in the input
   * that implies we should ignore the form guess
   * e.g. "Old Password" === FormType.Login
   * */
  formTypeOverride?: FormTypes | null
  confidence?: InputElementConfidence
}

const highConfidenceMatches = (input: HTMLInputElement): InputResultType | null => {
  const confidence = InputElementConfidence.High

  // Near certain guesses take precedence
  if (
    input.type === 'password' ||
    (input.type === 'text' && input.style.getPropertyValue('-webkit-text-security') === 'disc') ||
    (input.type === 'text' && input.style.getPropertyValue('text-security') === 'disc')
  ) {
    const formTypeOverride = changePasswordOverride(input)
    return { match: InputTypes.Password, confidence, formTypeOverride }
  }

  if (
    input.type === 'email' ||
    input.getAttribute('inputmode') == 'email' ||
    input.autocomplete == 'email'
  )
    return {
      match: InputTypes.Email,
      confidence,
    }

  return null
}

const mediumConfidenceMatches = (input: HTMLInputElement): InputResultType | null => {
  const confidence = InputElementConfidence.Medium

  if (matchInputToAttributes(input, emailAttributeMatcherRegex)) {
    return {
      match: InputTypes.Email,
      confidence,
    }
  }

  if (matchInputToAttributes(input, usernameAttributeMatcherRegex)) {
    return {
      match: InputTypes.Username,
      confidence,
    }
  }

  return null
}

const lowConfidenceMatches = (input: HTMLInputElement): InputResultType | null => {
  const confidence = InputElementConfidence.Low

  if (closestLabelMatch(input, [/username/i]))
    return {
      match: InputTypes.Username,
      confidence,
    }
  if (closestLabelMatch(input, [/email/i]))
    return {
      match: InputTypes.Email,
      confidence,
    }
  return null
}

/**
 * This tries to determine if an input is:
 * 1. An email, username or password input
 * 2. The confidence level of that guess
 *
 * When using this for determining autofill or dropdown, we should do additional tests
 * to see if it's actually part of a login/signup flow (is it part of a form with a button?)
 * @param {HTMLInputElement} input Input element to check
 * @returns {InputResultType|null} Our best guess along with a confidence level
 */
export const guessInputType = (input: HTMLInputElement): InputResultType | null => {
  if (!isValidInput(input)) return null

  for (const fn of [highConfidenceMatches, mediumConfidenceMatches, lowConfidenceMatches]) {
    const result = fn(input)
    if (result) return result
  }

  // Uncertain
  return null
}

const emailAttributeMatcherRegex = [/((e?)mail)/i]
const usernameAttributeMatcherRegex = [/(username|login|account)/i]

export const closestLabelMatch = (input: HTMLInputElement, matchers: RegExp[]): boolean => {
  const label = input.parentElement?.querySelector('label')
  if (label) {
    for (const matcher of matchers) {
      if (label.textContent?.match(matcher)) return true
    }
  }
  return false
}

const isInputTypeFunction = (inputType: InputTypes): ((input: HTMLInputElement) => boolean) => {
  return (input: HTMLInputElement): boolean => {
    return guessInputType(input)?.match == inputType
  }
}

export const isEmailInput = isInputTypeFunction(InputTypes.Email)
export const isUsernameInput = isInputTypeFunction(InputTypes.Username)
export const isPasswordInput = isInputTypeFunction(InputTypes.Password)

export const isValidInput = (input: HTMLInputElement): boolean => {
  if (input.tagName !== 'INPUT') return false

  // Exclusive list of types we care about.
  // Any input field where a user can type something
  if (!['email', 'text', 'password'].includes(input.type)) return false
  if (!isVisible(input)) return false
  return true
}

export const isTextInput = (input: HTMLInputElement): boolean => {
  if (input.tagName !== 'INPUT') return false

  // Exclusive list of types we care about.
  // Any input field where a user can type something
  if (!['email', 'text', 'password', 'search', 'tel'].includes(input.type)) return false
  return true
}

const changePasswordOldRegex = [/(current.?password|old.?password|previous.?password)/i]
const changePasswordOverride = (input: HTMLInputElement): FormTypes | null => {
  if (matchInputToAttributes(input, changePasswordOldRegex)) {
    return FormTypes.Login
  }
  return null
}

export const profileInput = (
  input: HTMLInputElement
): {
  inputResult: InputResultType | null
  formResult: FormResult
  confidence: InputOverallConfidence | null
} => {
  const inputResult = guessInputType(input)
  const formResult = guessFormType(input)

  // Unify this logic in one location for both autofill and dropdown logic
  // High confidence inputs should always be autofilled
  // Low confidence inputs should be filled if they have a form match
  let confidence = null
  if (
    (inputResult?.confidence && inputResult.confidence > InputElementConfidence.Medium) ||
    (inputResult && formResult.match)
  ) {
    confidence = InputOverallConfidence.High
  } else if (inputResult) {
    confidence = InputOverallConfidence.Low
  }

  return {
    inputResult,
    formResult,
    confidence,
  }
}
