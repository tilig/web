export const requestWebRequestPermissions = (): Promise<boolean> => {
  return new Promise((resolve) => {
    if (typeof chrome.webRequest !== 'undefined') return resolve(true)
    chrome.permissions.request(
      { permissions: ['webRequest'], origins: ['*://*/*', '<all_urls>'] },
      function (granted) {
        if (granted) {
          resolve(granted)
        }
      }
    )
  })
}
