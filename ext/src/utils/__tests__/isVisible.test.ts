import { afterEach, beforeEach, describe, expect, test, vi } from 'vitest'
import isVisible from '../isVisible'

beforeEach(() => {
  document.body.innerHTML = ''
})

afterEach(() => {
  vi.clearAllMocks()
})

describe('Testing an elements visibility', () => {
  test('check offsets to determine visibility', () => {
    const input = document.createElement('input')
    expect(isVisible(input)).toEqual(false)

    vi.spyOn(input, 'offsetWidth', 'get').mockImplementation(() => 200)
    expect(isVisible(input)).toEqual(true)
  })

  test('ignore aria hidden elements', () => {
    const input = document.createElement('input')
    vi.spyOn(input, 'offsetWidth', 'get').mockImplementation(() => 200)
    input.ariaHidden = 'true'

    expect(isVisible(input)).toEqual(false)
  })
})
