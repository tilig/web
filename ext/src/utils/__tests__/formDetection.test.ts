import { afterAll, beforeEach, describe, expect, test, vi } from 'vitest'

import { guessFormType, isSignupInput } from '../formDetection'
import { cases } from './__fixtures__/forms.fixtures'

// Required as we only fill in visible input elements
vi.mock('@/utils/isVisible', () => ({ default: vi.fn().mockReturnValue(true) }))
vi.mock('@/background/messageApis/trackEvent', () => {
  return {
    dispatchTrackFillSuccess: vi.fn(),
    dispatchTrackFillFailure: vi.fn(),
  }
})

beforeEach(() => {
  document.body.innerHTML = ''
})

afterAll(() => {
  vi.clearAllMocks()
})

describe('Guessing a Form type', () => {
  test.each(cases)(
    'Form: $name guessed as $expected',
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
    ({ name, html, expected, reason: expectedReason }) => {
      document.body.innerHTML = html
      const input = document.querySelector('input') as HTMLInputElement
      const result = guessFormType(input)
      expect(result.match).toEqual(expected)
      expect(result.name).toMatch(expectedReason)
    }
  )
})

describe('detecting sign in inputs', () => {
  test('returns true if classname matches', () => {
    const input = document.createElement('input')
    input.type = 'password'
    input.className = 'new-password'
    expect(isSignupInput(input)).toBe(true)
  })

  test('returns true if autocomplete matches', () => {
    const input = document.createElement('input')
    input.type = 'password'
    input.autocomplete = 'newPassword'
    expect(isSignupInput(input)).toBe(true)
  })

  test("returns false if it doesn't match", () => {
    const input = document.createElement('input')
    input.type = 'password'
    input.autocomplete = 'myPassword'
    expect(isSignupInput(input)).toBe(false)
  })
})

describe('detecting new email inputs', () => {
  test('returns true if classname matches', () => {
    const input = document.createElement('input')
    input.type = 'text'
    input.className = 'new-email'
    expect(isSignupInput(input)).toBe(true)
  })

  test('returns true if autocomplete matches', () => {
    const input = document.createElement('input')
    input.type = 'email'
    input.autocomplete = 'newEmail'
    expect(isSignupInput(input)).toBe(true)
  })

  test("returns false if it doesn't match", () => {
    const input = document.createElement('input')
    input.type = 'text'
    input.autocomplete = 'useremail'
    expect(isSignupInput(input)).toBe(false)
  })
})
