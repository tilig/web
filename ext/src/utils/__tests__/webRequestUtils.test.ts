import { describe, expect, test } from 'vitest'
import { findInObject, matchWebRequestBodyToPassword } from '../webRequestUtils'

const testFormData = {
  csrf: ['9e5f44751fe24d68180d93ca29cc1e8032c93c726c81f65e09dfc9c9e843885c'],
  password: ['mypassword'],
  recaptcha_type: ['web_invisible'],
  rrventerprise: [
    '03AEkXODBT4X-iA52D18I39zk9Vr0xLe4F6bz6uoZv_e1PWcTqsstdSYhXAVIxSpGoJftcSJsxnTGwir4YUtOePz_hchbdszenCYRAzhqCLCctyr9fAI6zC6RaJxqn4xnFF2YtLh3bdyuciqnHuk7fc6lOt0HlO02lxy06093B5qL5YQHN3zBjWEPUIbePiF1S74ScAamUWNMPd6ADu-NGEUtTeBCO7FUB6Naa8XSJFTkl2HJUZbP3ov9DJcCJdx1C1pZAAWAWXhSu68wsabvoTYRo8mXeCXmGvm4b30OI91lSY5Sow0_A0Vb1kd2MDZTbpMOh9O_wgtZq95bP7rE97Qpq7PH7GFkUEoeZqHoJrvXDdEUEiKv5aitvlUp8m64tC01gPoVpi4P_8vE36uICnzC_ltW-cSCjPIcMsOkWiNcmjmfYW0-M1hzPdEIg6T4bbNDoVKQI2XG3b-0yvfBxg9xH1KFZ9DparlEUJvkhPe02-V7mXKjHECcHVZdEfLW7FiPnGXy5gLDfD0oRH3PcEmWFxSWh9y06FkT1-GRBk0M3s4PnZla-tYmzlWbnVh_-CfzoEL4UnPKwYHXoYCExdhd77VzpFaqt23nSBMK1cP46t2hrdO9Z0J2tosGg63yUHDgM42OB-2F2MsSq3dagpp0G0w1n2CBM9E8X7leFHgWpEFmH426ngakJuExfSq1_Bn8zf2eEIMOAaGF1RfzMS5eNH6_1GDAZjZjLMOQAeMjoYgSAz2qUa3q-2ZTkFk7gA8IPNEP8tGzVYLQfP2fOiifeuV0HK_jAXHiwI9ZJt9sI2j0J8FTMI2EJAHKPsEf6VORExY8nTEXT8s9L4k5oKC-kammqmebL2BiSKfY1jMP7ONUPtCe3zByYHHpQ45wV6TttlJ-M5Q5T8vSveojYt35oxMw4WjkaiV0zPxCiNOFlFrN7Kn-H6EfyIZwnJwV3UMXVFOGpTliLtkd38lf3OfoVvve0jOMB2zdjdxgnr5sLYcs0qsWAPXi-21wMnwXPh-LweJMJq3NySe6ChPprxI-S7SSusAeJLrzGLSFPXA5VVmNHBsOFkoqO3_XFFu05vsI_eZSl6rKEJFwcy5OvxLwtykV1VVPP4gCVTxGHRZQA614isL42r15BQZ32pvUiDWOMMFwwo28Pv8dpE9NO1iaz3kx9sw7ne7z_CTUWY9NBug9V-R73Y0s6-iHqDKm9qJzggtpkVmnTAlTq1-surrO9YVURxpAE2NHjqpz7tGxncTwV_8BbdBwof7ZGwF1BQBkYrxt95IdqZhLvzkU1jYWuqIwtl2XCEwY_EF0jPChwdgKKk-F6x14',
  ],
  scenario: ['web_password_login'],
  user_email: ['mdp@test.com'],
}
const testArray = [['test', 'foo', 'mypassword']]
const testBasicObject = { username: 'mark', password: 'mypassword' }
const testMultiObject = {
  auth: {
    username: 'mark',
    password: 'mypassword',
  },
}
describe('Finding a password value in a given object', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const expectations: [any, string, boolean][] = [
    [testFormData, 'mypassword', true],
    [testArray, 'mypassword', true],
    [testBasicObject, 'mypassword', true],
    [testMultiObject, 'mypassword', true],
    [testFormData, 'mypassw0rd', false],
    [testArray, 'mypassw0rd', false],
    [testBasicObject, 'mypassw0rd', false],
    [testMultiObject, 'mypassw0rd', false],
  ]

  test.each(expectations)('Should correctly find the password', (testObj, password, expected) => {
    expect(
      findInObject(testObj, password),
      `Expected ${JSON.stringify(testObj)} test for ${password} to be ${expected}}`
    ).toBe(expected)
  })
})

describe('Finding a password value in a webrequest', () => {
  test('Handle requestBody with formData', () => {
    expect(
      matchWebRequestBodyToPassword(
        {
          formData: testFormData,
        },
        'mypassword'
      )
    ).toBe(true)
  })
  test('Handle requestBody with raw data', () => {
    const textEncoder = new TextEncoder()
    const encoded = textEncoder.encode(JSON.stringify([testBasicObject]))
    expect(
      matchWebRequestBodyToPassword(
        {
          raw: [{ bytes: encoded.buffer }],
        },
        'mypassword'
      )
    ).toBe(true)
  })
  test('Ignore requestBody with file data', () => {
    expect(
      matchWebRequestBodyToPassword(
        {
          raw: [{ file: 'mypassword' }],
        },
        'mypassword'
      )
    ).toBe(false)
  })
})
