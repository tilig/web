import { describe, expect, test } from 'vitest'
import { titleToName } from '../titleToName'

describe('detect basic signup based on URL', () => {
  const expectations = [
    ['https://www.netflix.com/', 'Netflix', 'Netflix'],
    ['https://cnn.com/signup', 'Create an account on CNN', 'CNN'],
    [
      'https://www.marktplaats.nl/account/createItem.html',
      '≥ Marktplaats - De plek om nieuwe en tweedehands spullen te kopen en verkopen',
      'Marktplaats',
    ],
    ['https://www.notitle.com/', '', 'notitle.com'],
    ['https://www.mysite.com/', 'This is a login form', 'mysite.com'],
    ['https://foo.github.io/', 'Welcome to fOO', 'fOO'],
  ]

  test.each(expectations)(
    'Extracting a name from the url and page title',
    (url, title, expected) => {
      expect(titleToName(url, title), `Expected ${url} to have the correct title`).toBe(expected)
    }
  )
})
