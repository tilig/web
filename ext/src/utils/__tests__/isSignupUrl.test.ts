import { describe, expect, test } from 'vitest'

import { isSignupUrl } from '../isSignupUrl'

describe('detect basic signup based on URL', () => {
  const signupUrls = [
    'https://foo.com/signup.php',
    'https://foo.com/sign-up/account.html',
    'https://foo.com/account/sign_up',
    'https://www.disneyplus.com/sign-up',
    'https://www.marktplaats.nl/account/createItem.html?target=https%3A%2F%2Fwww.marktplaats.nl%2F',
    'https://login.yahoo.com/account/create?intl=us&.lang=en-US&src=homepage&specId=yidregsimplified&activity=ybar-signin&pspid=2023538075&.done=https%3A%2F%2Fwww.yahoo.com%2F&done=https%3A%2F%2Fwww.yahoo.com%2F&context=reg',
  ]

  const loginUrls = [
    'https://signup.com/login.php',
    'https://foo.com/login/account.html',
    'https://foo.com/account/login.asp',
    // Ignore query params since for now
    'https://www.marktplaats.nl/account/login.html?returnTo=createItem',
  ]

  test.each(signupUrls)('Signup looking urls should return true', (testUrl) => {
    expect(isSignupUrl(testUrl), `Expected ${testUrl} to be a signup`).toBe(true)
  })

  test.each(loginUrls)('login urls should return false', (testUrl) => {
    expect(isSignupUrl(testUrl)).toBe(false)
  })
})
