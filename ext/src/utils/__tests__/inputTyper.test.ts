import isVisible from '@/utils/isVisible'
import { vi } from 'vitest'
import { guessInputType, InputTypes, isTextInput, isValidInput } from '../inputTyper'
import { cases } from './__fixtures__/inputTypes.fixtures'

vi.mock('@/utils/isVisible', () => ({ default: vi.fn().mockReturnValue(true) }))

beforeEach(() => {
  vi.clearAllMocks()
})

afterAll(() => {
  vi.clearAllMocks()
})

const inputTypeGuess = (element: HTMLElement) =>
  guessInputType(element as HTMLInputElement)?.match || null

describe('password input types', () => {
  test('returns password when type is "password"', () => {
    const input = document.createElement('input')
    input.type = 'password'
    expect(inputTypeGuess(input)).toBe(InputTypes.Password)
  })
})

describe('email input types', () => {
  const attributeNames = ['id', 'class', 'name']
  const validAttributeValues = ['email', 'EMAIL', 'gl-email-1', 'mail', 'MAIL', 'gl-mail-1']

  const table = [
    ...attributeNames
      .map((attributeName) =>
        validAttributeValues.map((attributeValue) => [attributeName, attributeValue])
      )
      .flat(),
    ['placeholder', 'Your email address'],
  ]

  test.each(table)("input[type=email][%s='%s'] === true", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.type = 'email'
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(InputTypes.Email)
  })

  test.each(table)("input[%s='%s'] === true", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(InputTypes.Email)
  })

  test.each(table)("input[type=hidden][%s='%s'] === false", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.type = 'hidden'
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(null)
  })

  test('returns null when the subject is not an input element', () => {
    const div = document.createElement('div')
    div.id = 'email'
    expect(inputTypeGuess(div)).toBe(null)
  })

  test('returns null when isVisible is false', () => {
    const input = document.createElement('input')
    input.setAttribute('type', 'email')

    // Make sure the test is not a null positive
    expect(inputTypeGuess(input)).toBe(InputTypes.Email)
    vi.mocked(isVisible).mockReturnValueOnce(false)

    expect(inputTypeGuess(input)).toBe(null)
  })

  // Exclude mailbox search like gmail
  test("returns null when the placeholder is 'search email'", () => {
    const input = document.createElement('input')
    input.setAttribute('placeholder', 'Search mail')

    vi.mocked(isVisible).mockReturnValueOnce(false)

    expect(inputTypeGuess(input)).toBe(null)
  })

  test('returns null when the input is a checkbox', () => {
    const input = document.createElement('input')
    input.id = 'email'
    input.type = 'checkbox'
    expect(inputTypeGuess(input)).toBe(null)
  })

  // https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/inputmode
  test("returns email when 'inputmode' is email ", () => {
    const input = document.createElement('input')
    input.inputMode = 'email'
    input.type = 'text'
    expect(inputTypeGuess(input)).toBe(InputTypes.Email)
  })

  // https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/inputmode
  test("returns email when 'autocomplete' is email ", () => {
    const input = document.createElement('input')
    input.autocomplete = 'email'
    input.type = 'text'
    expect(inputTypeGuess(input)).toBe(InputTypes.Email)
  })

  // Ambiguous inputs like this password reset prompt should still return email due to type="email"
  // <input id="new-password" type="email" name="email" class="mp-Input" tabindex="1">
  // From: https://www.marktplaats.nl/account/password-reset/initiate.html
  test("returns 'email' for ambiguously named inputs", () => {
    const input = document.createElement('input')
    input.id = 'new-password'
    input.type = 'email'
    expect(inputTypeGuess(input)).toBe(InputTypes.Email)
  })
})

describe('password input types', () => {
  const attributeNames: string[] = ['id', 'class', 'name']
  const validAttributeValues: string[] = ['password', 'PASSWORD', 'gl-password-1']

  const table = [
    ...attributeNames
      .map((attributeName) =>
        validAttributeValues.map((attributeValue) => [attributeName, attributeValue])
      )
      .flat(),
    ['placeholder', 'Your password'],
  ]

  test.each(table)("input[type!=password][%s='%s']", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(null)
  })

  test.each(table)("input[type=password][%s='%s'] === true", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.type = 'password'
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(InputTypes.Password)
  })

  test.each(table)("input[type=hidden][%s='%s'] === false", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.type = 'hidden'
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(null)
  })

  test('returns null when isVisible is false', () => {
    const input = document.createElement('input')
    input.setAttribute('type', 'password')

    // Make sure the test is not a null positive
    expect(inputTypeGuess(input)).toBe(InputTypes.Password)
    vi.mocked(isVisible).mockReturnValueOnce(false)
    expect(inputTypeGuess(input)).toBe(null)
  })

  test('returns null when the input is a checkbox', () => {
    const input = document.createElement('input')
    input.id = 'password'
    input.type = 'checkbox'
    expect(inputTypeGuess(input)).toBe(null)
  })
})

describe('username input types', () => {
  const attributeNames = ['id', 'class', 'name']
  const validAttributeValues = [
    'username',
    'USERNAME',
    'gl-username-1',
    'login',
    'LOGIN',
    'gl-login-1',
  ]

  const table = [
    ...attributeNames
      .map((attributeName) =>
        validAttributeValues.map((attributeValue) => [attributeName, attributeValue])
      )
      .flat(),
    ['placeholder', 'Your username'],
    ['placeholder', 'Your personal login'],
  ]

  test.each(table)("input[%s='%s'] === true", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(InputTypes.Username)
  })

  test.each(table)("input[type=hidden][%s='%s'] === false", (attributeName, attributeValue) => {
    const input = document.createElement('input')
    input.type = 'hidden'
    input.setAttribute(attributeName, attributeValue)
    expect(inputTypeGuess(input)).toBe(null)
  })

  test('returns null when the subject is not an input element', () => {
    const div = document.createElement('div') as HTMLInputElement
    expect(inputTypeGuess(div)).toBe(null)
  })

  test('returns null when the input is a checkbox', () => {
    const input = document.createElement('input')
    input.id = 'username'
    input.type = 'checkbox'
    expect(inputTypeGuess(input)).toBe(null)
  })

  test('returns null when the input type is hidden', () => {
    const input = document.createElement('input')
    input.id = 'username'
    input.type = 'hidden'
    expect(inputTypeGuess(input)).toBe(null)
  })

  test('returns null when isVisible is false', () => {
    const input = document.createElement('input')
    input.setAttribute('id', 'username')

    // Make sure the test is not a null positive
    expect(inputTypeGuess(input)).toBe(InputTypes.Username)
    vi.mocked(isVisible).mockReturnValueOnce(false)

    expect(inputTypeGuess(input)).toBe(null)
  })
})

// Test that we can detect inputs we care about for login/signup
describe('valid inputs', () => {
  const table: [string, boolean][] = [
    ['text', true],
    ['email', true],
    ['password', true],
    ['tel', false],
    ['checkbox', false],
    ['file', false],
    ['range', false],
  ]

  test.each(table)("isValidInput(input[type='%s']) == %s", (type, expected) => {
    const input = document.createElement('input')
    input.setAttribute('type', type)
    expect(isValidInput(input)).toBe(expected)
  })
})

describe('Determining if an input is a type of text input', () => {
  const table: [string, boolean][] = [
    ['text', true],
    ['email', true],
    ['password', true],
    ['tel', true],
    ['checkbox', false],
    ['file', false],
    ['range', false],
  ]

  test.each(table)("isTextInput(input[type='%s']) == %s", (type, expected) => {
    const input = document.createElement('input')
    input.setAttribute('type', type)
    expect(isTextInput(input)).toBe(expected)
  })
})

describe('Determining an inputs type based on HTML chunk', () => {
  test.each(cases)(
    'Input: $name guessed as $expected',
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
    ({ name, html, expectedGuess, expectedConfidence, expectedFormTypeOverride, selector }) => {
      document.body.innerHTML = html
      const input = document.querySelector(selector || 'input') as HTMLInputElement
      const result = guessInputType(input)
      expect(result?.match || null).toEqual(expectedGuess)
      if (expectedConfidence) expect(result?.confidence).toEqual(expectedGuess)
      if (expectedFormTypeOverride)
        expect(result?.formTypeOverride).toEqual(expectedFormTypeOverride)
    }
  )
})
