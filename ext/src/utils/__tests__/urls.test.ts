import { describe, expect, test } from 'vitest'
import { domainToHref } from '../urls'

describe('Handle domains and full schema urls', () => {
  const expectations = [
    ['https://www.netflix.com/', 'https://www.netflix.com/'],
    ['http://www.netflix.com', 'http://www.netflix.com'],
    ['www.netflix.com', 'https://www.netflix.com'],
  ]

  test.each(expectations)('Turning a domain in an valid href', (url, expected) => {
    expect(domainToHref(url), `Expected ${url} to be ${expected}`).toBe(expected)
  })
})
