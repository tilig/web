import { FormTypes } from '@/utils/formDetection'
import { InputElementConfidence, InputTypes } from '@/utils/inputTyper'

type TestCase = {
  name: string
  html: string
  expectedGuess: InputTypes | null
  expectedConfidence?: InputElementConfidence
  expectedFormTypeOverride?: FormTypes | null
  selector?: string
}

export const cases: TestCase[] = [
  {
    name: 'Email detection from label',
    html: `
    <form>
      <label>Email</label>
      <input type='text'></input>
        <button type="submit">Login</button>
    </form>
    `,
    expectedGuess: InputTypes.Email,
    expectedConfidence: InputElementConfidence.Low,
  },
  {
    name: 'Email detection from sibling child label (nephew?)',
    html: `
    <form>
      <div>
        <label>Email</label>
      </div>
      <input type='text'></input>
        <button type="submit">Login</button>
    </form>
    `,
    expectedGuess: InputTypes.Email,
    expectedConfidence: InputElementConfidence.Low,
  },
  {
    name: 'Username detection from label',
    html: `
      <form>
        <label>Your Username</label>
        <input type='text'></input>
        <button type="submit">Create Account</button>
      </form>
    `,
    expectedGuess: InputTypes.Username,
    expectedConfidence: InputElementConfidence.Low,
  },
  {
    name: 'Ignore label that are not siblings',
    html: `
      <form>
        <label>Your email here</label>
        <input type='text'></input>
        <div>
          <input type='text' id='testInput'></input>
        </div>
        <button type="submit">Create Account</button>
      </form>
    `,
    expectedGuess: null,
    selector: '#testInput',
  },
  {
    // Seen on Microsoft Teams join call screen for your name
    name: 'Ignore inputs with autocomplete set to off',
    html: `
    <input sanitized="" maxlength="50" id="username" name="username" placeholder="Enter name" autocomplete="off" autofocus="">
    `,
    expectedGuess: InputTypes.Username,
    expectedConfidence: InputElementConfidence.Low,
  },
  {
    name: 'Override change password "old_password" style forms',
    html: `
    <input type="password" name="user[old_password]" id="user_old_password" required="required" autocomplete="current-password" class="form-control form-control">
    `,
    expectedGuess: InputTypes.Password,
    expectedFormTypeOverride: FormTypes.Login,
    expectedConfidence: InputElementConfidence.Low,
  },
]
