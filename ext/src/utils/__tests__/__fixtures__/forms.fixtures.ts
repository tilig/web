import { FormTypes, FormResultType } from '@/utils/formDetection'

type TestCase = {
  name: string
  html: string
  expected: FormResultType
  reason: RegExp
}

export const cases: TestCase[] = [
  {
    name: 'Basic Login',
    html: `
      <form>
        <input type="email" id="email" />
        <input type="password" id="password" />
      </form>
    `,
    expected: FormTypes.Login,
    reason: /Basic Login/i,
  },
  {
    name: 'Login form with forgot password link',
    html: `
      <form>
        <input type="email" id="email" />
        <input type="password" id="password" />
        <a>Forgot Password</a>
      </form>
    `,
    expected: FormTypes.Login,
    reason: /Forgot Password/i,
  },
  {
    name: 'Signup with submit button',
    html: `
      <form>
        <input type="email" id="email" />
        <input type="password" id="password" />
        <button type="submit">Create Account</button>
      </form>
    `,
    expected: FormTypes.Signup,
    reason: /Submit Button Match/i,
  },
  {
    // As seen on https://company.testrail.io/index.php?/auth/login/
    name: 'Signup with one (not type=submit) button',
    html: `
      <form>
        <input type="email" id="email" />
        <input type="password" id="password" />
        <button>Create Account</button>
      </form>
    `,
    expected: FormTypes.Signup,
    reason: /Action Button Match/i,
  },
  {
    name: 'Multiple password inputs',
    html: `
      <form>
        <input type="email" id="email" />
        <input type="password" id="password" />
        <input type="password" id="password_again" />
      </form>
    `,
    expected: FormTypes.Signup,
    reason: /Two Password/i,
  },
  {
    name: 'Login without form element wrapper',
    html: `
      <div class='content'>
        <div class="myinputs">
          <input type="email" id="email" />
          <input type="password" id="password" />
        </div>
        <button>Login</button>
      </div>
    `,
    expected: FormTypes.Login,
    reason: /Action Button Match/i,
  },
  {
    name: 'Login form with id="login"',
    html: `
      <form id='login'>
        <input type="email" id="email" />
        <button type="submit">Continue</button>
      </form>
    `,
    expected: FormTypes.Login,
    reason: /Form Attribute/i,
  },
  {
    name: 'Login form with action=".../login"',
    html: `
      <form action="https://signup.website.com/action/login">
        <input type="email" id="email" />
        <button type="submit">Continue</button>
      </form>
    `,
    expected: FormTypes.Login,
    reason: /Form Attribute/i,
  },
  {
    name: 'Change password form with three password inputs',
    html: `
      <form action="/myform">
        <div class="myinputs">
          <input type="password" id="old_password" />
          <input type="password" id="new_password" />
          <input type="password" id="confirm_password" />
        </div>
        <button>Click me</button>
      </form>
    `,
    expected: FormTypes.ChangePassword,
    reason: /Three/i,
  },
  {
    name: 'Change password form with form action that looks like a change',
    html: `
      <form action="/update-password">
        <div class="myinputs">
          <input type="password" id="new_password" />
          <input type="password" id="confirm_password" />
        </div>
        <button>Click me</button>
      </form>
    `,
    expected: FormTypes.ChangePassword,
    reason: /Form attribute/i,
  },
  {
    name: 'Change password form with form submit button that looks like a change password button',
    html: `
      <form action="">
        <div class="myinputs">
          <input type="password" id="new_password" />
          <input type="password" id="confirm_password" />
        </div>
        <button>Update Password</button>
      </form>
    `,
    expected: FormTypes.ChangePassword,
    reason: /button/i,
  },
]
