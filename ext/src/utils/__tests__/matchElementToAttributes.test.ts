import { describe, expect, test } from 'vitest'

import { matchFormToAttributes, matchInputToAttributes } from '../matchElementToAttributes'

const makeElementFromHTML = (html: string) => {
  const div = document.createElement('div')
  div.innerHTML = html
  return div.children[0]
}

describe('matching form and input attributes', () => {
  test('That we correctly match on input attributes, including data', () => {
    const attributesWeCareAbout = ['id', 'name', 'class', 'placeholder', 'autocomplete']
    for (const attribute of attributesWeCareAbout) {
      const input = makeElementFromHTML(`<input ${attribute}="foo-Tilig-baz" />`)
      expect(matchInputToAttributes(input as HTMLInputElement, [/tilig/i])).toBeTruthy()
    }
    const dataInput = makeElementFromHTML(`<input data-something="foo-Tilig-baz" />`)
    expect(matchInputToAttributes(dataInput as HTMLInputElement, [/tilig/i])).toBeTruthy()
  })

  test('That we correctly match on form attributes, including action', () => {
    const actionForm = makeElementFromHTML(`<form action="/lets/Login-with-Tilig/again" />`)
    expect(matchFormToAttributes(actionForm as HTMLFormElement, [/tilig/i])).toBeTruthy()
    const idForm = makeElementFromHTML(`<form id="anotherTiligId" />`)
    expect(matchFormToAttributes(idForm as HTMLFormElement, [/tilig/i])).toBeTruthy()
  })

  // As seen on Netflix
  test('That we handle empty actions', () => {
    const actionForm = makeElementFromHTML(`<form action />`)
    expect(matchFormToAttributes(actionForm as HTMLFormElement, [/tilig/i])).toBeFalsy()
  })
})
