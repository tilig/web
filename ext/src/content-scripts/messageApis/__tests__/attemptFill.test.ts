import { afterAll, beforeEach, expect, test, vi } from 'vitest'
import { FillProps } from '@/background/fillUtils'
import {
  dispatchTrackFillSuccess,
  dispatchTrackFillFailure,
} from '@/background/messageApis/trackEvent'
import ContentScriptContext from '@/content-scripts/classes/ContentScriptContext'
import { handler as attemptFill } from '../attemptFill'
import { basicLogin } from '../../forms/__tests__/__fixtures__/basicForms.fixtures'

// Required as we only fill in visible input elements
vi.mock('@/utils/isVisible', () => ({ default: vi.fn().mockReturnValue(true) }))
vi.mock('@/background/messageApis/trackEvent', () => {
  return {
    dispatchTrackFillSuccess: vi.fn(),
    dispatchTrackFillFailure: vi.fn(),
  }
})

beforeEach(() => {
  document.body.innerHTML = ''
})

afterAll(() => {
  vi.clearAllMocks()
})

const createFillProps = (overides: object = {}): FillProps => {
  const fillProps: FillProps = {
    username: 'test',
    password: 'test',
    domain: 'test.com',
    trigger: 'Page Load',
    userInitiated: false,
  }
  return { ...fillProps, ...(overides as FillProps) }
}

const windowMock = Object.create(window)

Object.defineProperty(windowMock, 'location', {
  value: { href: 'https://www.test.com' },
})
Object.defineProperty(windowMock, 'top', {
  value: {
    location: {
      href: 'https://www.test.com',
    },
  },
})

test('attempting a fill on an empty document should return false', () => {
  const context = new ContentScriptContext(windowMock)
  expect(attemptFill(createFillProps(), context)).toBe(false)
})

test('attempting a fill on a login form should return true', () => {
  document.body.innerHTML = basicLogin
  const context = new ContentScriptContext(windowMock)
  expect(attemptFill(createFillProps(), context)).toBe(true)
})

test('autofilling a login form should report a fill event', () => {
  document.body.innerHTML = basicLogin
  const context = new ContentScriptContext(windowMock)
  expect(attemptFill(createFillProps(), context)).toBe(true)
  expect(dispatchTrackFillSuccess).toHaveBeenCalledOnce()
})

test('Failure to autofill on page load should NOT report a failed attempt', () => {
  /**
   * As we track page udpates and continue to try to autofill, this would result in numerous false 'failures'.
   * */
  document.body.innerHTML = ''
  const context = new ContentScriptContext(windowMock)
  expect(attemptFill(createFillProps({ userInitiated: false }), context)).toBe(false)
  expect(dispatchTrackFillFailure).not.toHaveBeenCalled()
})

test('Failure to autofill a login form when user requested one should report a failed attempt', () => {
  document.body.innerHTML = ''
  const context = new ContentScriptContext(windowMock)
  expect(attemptFill(createFillProps({ userInitiated: true }), context)).toBe(false)
  expect(dispatchTrackFillFailure).toHaveBeenCalledOnce()
})
