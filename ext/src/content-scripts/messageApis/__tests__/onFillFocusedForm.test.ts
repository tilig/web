import { afterAll, beforeEach, expect, test, vi } from 'vitest'
import { FillProps } from '@/background/fillUtils'
import {
  dispatchTrackFillSuccess,
  dispatchTrackFillFailure,
} from '@/background/messageApis/trackEvent'
import ContentScriptContext from '@/content-scripts/classes/ContentScriptContext'
import { handler as onFillFocussedForm } from '../onFillFocussedForm'
import { basicLogin } from '../../forms/__tests__/__fixtures__/basicForms.fixtures'

// Required as we only fill in visible input elements
vi.mock('@/utils/isVisible', () => ({ default: vi.fn().mockReturnValue(true) }))
vi.mock('@/background/messageApis/trackEvent', () => {
  return {
    dispatchTrackFillSuccess: vi.fn(),
    dispatchTrackFillFailure: vi.fn(),
  }
})

beforeEach(() => {
  document.body.innerHTML = ''
})

afterAll(() => {
  vi.clearAllMocks()
})

const createFillProps = (overides: object = {}): FillProps => {
  const fillProps: FillProps = {
    username: 'test',
    password: 'test',
    domain: 'test.com',
    trigger: 'Page Load',
    userInitiated: false,
  }
  return { ...fillProps, ...(overides as FillProps) }
}

const windowMock = Object.create(window)

Object.defineProperty(windowMock, 'location', {
  value: { href: 'https://www.test.com' },
})
Object.defineProperty(windowMock, 'top', {
  value: {
    location: {
      href: 'https://www.test.com',
    },
  },
})

Object.defineProperty(windowMock, 'activeElement', document.body)

test('filling an invalid login form should return false', () => {
  const context = new ContentScriptContext(windowMock)
  expect(onFillFocussedForm(createFillProps(), context)).toBe(false)
})

test('filling a valid login form should return true', () => {
  document.body.innerHTML = basicLogin
  const context = new ContentScriptContext(windowMock)
  expect(onFillFocussedForm(createFillProps(), context)).toBe(true)
})

test('filling a login form should report a fill event', () => {
  document.body.innerHTML = basicLogin
  const context = new ContentScriptContext(windowMock)
  expect(onFillFocussedForm(createFillProps(), context)).toBe(true)
  expect(dispatchTrackFillSuccess).toHaveBeenCalledOnce()
})

test('failing to fill a login form should report a failure event', () => {
  document.body.innerHTML = ''
  const context = new ContentScriptContext(windowMock)
  expect(onFillFocussedForm(createFillProps(), context)).toBe(false)
  expect(dispatchTrackFillFailure).toHaveBeenCalledOnce()
})
