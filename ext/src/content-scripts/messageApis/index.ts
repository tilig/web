import { createLogger } from '@core/utils/logger'
import ContentScriptContext from '../classes/ContentScriptContext'
import {
  key as getCurrentContextKey,
  handler as getCurrentContextHandler,
} from './getCurrentContext'
import { key as attemptFillKey, handler as attempFillHandler } from './attemptFill'
import {
  key as onAutocaptureModalMessageKey,
  handler as onAutocaptureModalMessageHandler,
} from './onAutocaptureModalMessage'
import {
  key as onFillFocussedFormKey,
  handler as onFillFocussedFormHandler,
} from './onFillFocussedForm'
import {
  key as onFillFocussedInputKey,
  handler as onFillFocussedInputHandler,
} from './onFillFocussedInput'
import {
  key as onFillPasswordInputsKey,
  handler as onFillPasswordInputsHandler,
} from './onFillPasswordInputs'
import {
  key as onUpdateCredentialMessageKey,
  handler as onUpdateCredentialMessageHandler,
} from './onUpdateCredentialMessage'

const logger = createLogger('ext:contentScriptAPI')

type MessageEventHandler = (
  message: any,
  sender: chrome.runtime.MessageSender,
  sendResponse: (response?: any) => void
) => void | boolean

export type Message = any

/**
 * Message handlers expect to receive
 * - data passed from the caller
 * - the calling tab
 * - our store (They shouldn't handle importing stores directly)
 *
 * The reason for injecting the store is to keep it out of the context
 * of the content-script which will also import these handlers to use
 * their helper functions
 */
export type MessageHandler = (msg: any, context: ContentScriptContext) => Promise<any> | any

type MessageHandlers = { [key: string]: MessageHandler }

const messageHandlers: MessageHandlers = {
  [getCurrentContextKey]: getCurrentContextHandler,
  [attemptFillKey]: attempFillHandler,
  [onAutocaptureModalMessageKey]: onAutocaptureModalMessageHandler,
  [onFillFocussedFormKey]: onFillFocussedFormHandler,
  [onFillFocussedInputKey]: onFillFocussedInputHandler,
  [onFillPasswordInputsKey]: onFillPasswordInputsHandler,
  [onUpdateCredentialMessageKey]: onUpdateCredentialMessageHandler,
}

/**
 * Handles the complexity async Promises vs sync results
 * Chrome expect message handlers to return the result via
 * a `response` callback.
 *
 * If the response is handled syncronously, we call response(result)
 * and return false.
 *
 * If the response is handle asyncronously, we return true, and when
 * reaady, we `response` with our result
 */
export const contentScriptMessageListener = (
  context: ContentScriptContext
): MessageEventHandler => {
  return ({ msg, data = {}, headers = {} }, sender, response) => {
    logger.debug(`Content Script Msg: ${msg}`, data)
    if (!messageHandlers[msg]) {
      return false
    }
    if (headers && headers.domain) {
      const currentDomain = context.domain
      if (currentDomain !== headers.domain) {
        logger(`❌ Message for incorrect domain: ${currentDomain} != ${headers.domain}`)
        return false
      }
    }
    logger.debug(`Content Script Msg: ${msg}`, data)
    // Handle the response for sync and async messageHandlers
    const responseHandler = (result: Promise<any> | any) => {
      if (result instanceof Promise) {
        result
          .then((res) => {
            logger.debug(`Content-Script Async Response to ${msg}`, res)
            response(res)
          })
          .catch((err) => {
            logger.debug(`Error on ${msg}:`, err)
          })
        return true
      }
      logger.debug(`Content-Script Response to ${msg}`, result)
      response(result)
      return false
    }

    const result = messageHandlers[msg](data, context)
    return responseHandler(result)
  }
}
