import { parseDomain } from '@core/utils/domainUtils'
import {
  dispatchTrackFillFailure,
  dispatchTrackFillSuccess,
} from '@/background/messageApis/trackEvent'
import { FillProps } from '@/background/fillUtils'
import { FILL_FOCUSSED_FORM_TAB_MESSAGE } from '@/utils/messageConstants'
import ContentScriptContext from '../classes/ContentScriptContext'
import fillCredentialsAroundElement from '../forms/fillCredentialsAroundElement'

export const key = FILL_FOCUSSED_FORM_TAB_MESSAGE

/**
 * Fill the document with the credentials/FillProps provided
 * This is used for Autofilling on page load, as well as user initiated Autofill from the Popup or Dropdown
 * @param {import("../../background/fillUtils").FillProps} fillProps The username, password and trigger
 * @param {import("../classes/ContentScriptContext")} context Context of the content-script
 */
export const handler = (fillProps: FillProps, context: ContentScriptContext): boolean => {
  const { domain } = fillProps

  const windowDomain = parseDomain(context.location)

  if (domain !== windowDomain) {
    return false
  }

  if (context.document.activeElement) {
    const filled = fillCredentialsAroundElement(fillProps, context.document.activeElement)

    context.ui.hideDropdown()

    if (filled) {
      dispatchTrackFillSuccess(fillProps)
    } else {
      dispatchTrackFillFailure(fillProps)
    }
    return filled
  }
  return false
}
