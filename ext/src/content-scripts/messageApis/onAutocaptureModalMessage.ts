import { AUTOCAPTURE_MODAL_TAB_MESSAGE } from '@/utils/messageConstants'
import { sendToTab } from '@/utils/messageSender'
import ContentScriptContext from '../classes/ContentScriptContext'

export const key = AUTOCAPTURE_MODAL_TAB_MESSAGE

type MessageData = {
  action: 'launch' | 'dismiss'
}

export const handler = ({ action }: MessageData, context: ContentScriptContext): void => {
  if (!context.isTopWindow) return
  if (action === 'dismiss') {
    context.ui.dismissAutocaptureModal()
  } else if (action === 'launch') {
    context.ui.showAutocaptureModal()
  }
}

export const dismissAutocaptureModal = (tab: chrome.tabs.Tab) => {
  if (!tab.id) return
  return sendToTab<void>(tab.id, key, { action: 'dismiss' })
}

export const launchAutocaptureModal = (tab: chrome.tabs.Tab) => {
  if (!tab.id) return
  return sendToTab<void>(tab.id, key, { action: 'launch' })
}
