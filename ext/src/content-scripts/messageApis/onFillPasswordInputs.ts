import { FILL_PASSWORD_INPUTS_TAB_MESSAGE } from '@/utils/messageConstants'
import focusNextInput from '../forms/focusNextInput'
import findInputWrapper from '../forms/findInputWrapper'
import fillFieldsWithUserCredentials from '../forms/fillFieldsWithUserCredentials'
import findInputsInWrapper from '../forms/findInputsInWrapper'
import ContentScriptContext from '../classes/ContentScriptContext'
import { FormTypes } from '@/utils/formDetection'
import { sendToTabAndDomain } from '@/utils/messageSender'
import { profileInput } from '@/utils/inputTyper'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('onFillPasswordHandler')

export const key = FILL_PASSWORD_INPUTS_TAB_MESSAGE

/**
 * Fill the focused password
 * Used for filling a password inputs with a suggested value
 * @param {String} value The value to fill
 * @param {import("../classes/ContentScriptContext")} context Context of the content-script
 */
export const handler = ({ value }: { value: string }, context: ContentScriptContext) => {
  logger.debug(`Password Fill with suggestion`)
  if (!context.document.activeElement) return

  const wrapper = findInputWrapper(context.document.activeElement)
  const { passwordInputs } = findInputsInWrapper(wrapper)
  const filteredPasswordInputs = passwordInputs.filter((input) => {
    const profiledInput = profileInput(input)
    return profiledInput.inputResult?.formTypeOverride !== FormTypes.Login
  })

  fillFieldsWithUserCredentials(filteredPasswordInputs, value)

  context.ui.hideDropdown()
  focusNextInput(context.document.activeElement)
}

export const sendFillPasswordInputs = async (tabId: number, domain: string, value: string) => {
  await sendToTabAndDomain(tabId, domain, key, { value })
}
