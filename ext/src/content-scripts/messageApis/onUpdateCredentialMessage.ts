import { CREDENTIAL_MODAL_TAB_MESSAGE } from '@/utils/messageConstants'
import { sendToActiveTab, sendToTab } from '@/utils/messageSender'
import ContentScriptContext from '../classes/ContentScriptContext'

export const key = CREDENTIAL_MODAL_TAB_MESSAGE

type CredentialMessageData = {
  action: 'launch' | 'close'
}

export const handler = ({ action }: CredentialMessageData, context: ContentScriptContext) => {
  if (!context.isTopWindow) return
  if (action == 'close') {
    context.ui.hideCredentialsModal()
  } else if (action == 'launch') {
    context.ui.showCrendentialsModal()
  }
}

export const launchCredentialModal = (tab?: chrome.tabs.Tab) => {
  if (tab && tab.id) {
    return sendToTab<void>(tab.id, key, { action: 'launch' })
  } else {
    return sendToActiveTab<void>(key, { action: 'launch' })
  }
}

export const closeCredentialModal = (tab?: chrome.tabs.Tab) => {
  if (tab && tab.id) {
    return sendToTab<void>(tab.id, key, { action: 'close' })
  } else {
    return sendToActiveTab<void>(key, { action: 'close' })
  }
}
