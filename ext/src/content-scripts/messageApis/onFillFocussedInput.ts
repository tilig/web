import { FILL_FOCUSSED_INPUT_TAB_MESSAGE } from '@/utils/messageConstants'
import { sendToTabAndDomain } from '@/utils/messageSender'
import ContentScriptContext from '../classes/ContentScriptContext'
import fillInput from '../forms/fillInput'
import focusNextInput from '../forms/focusNextInput'

export const key = FILL_FOCUSSED_INPUT_TAB_MESSAGE

/**
 * Fill the focused input
 * Used for filling single inputs with suggested values
 * @param {String} value The value to fill
 * @param {import("../classes/ContentScriptContext")} context Context of the content-script
 */
export const handler = ({ value }: { value: string }, context: ContentScriptContext) => {
  if (!context.document.activeElement) return

  fillInput(context.document.activeElement as HTMLInputElement, value)

  context.ui.hideDropdown()
  focusNextInput(context.document.activeElement)
}

export const sendFillFocusedInput = async (tabId: number, domain: string, value: string) => {
  await sendToTabAndDomain(tabId, domain, key, { value })
}
