import { createLogger } from '@core/utils/logger'
import { parseDomain } from '@core/utils/domainUtils'
import {
  dispatchTrackFillFailure,
  dispatchTrackFillSuccess,
} from '@/background/messageApis/trackEvent'
import { FillProps } from '@/background/fillUtils'
import { FILL_FORM_TAB_MESSAGE } from '@/utils/messageConstants'
import fillCredentialsInWrapper from '../forms/fillCredentialsInWrapper'
import ContentScriptContext from '../classes/ContentScriptContext'

const logger = createLogger('ext:attemptFill')

export const key = FILL_FORM_TAB_MESSAGE

/**
 * Autofill the current page
 * @param {import("../../background/fillUtils").FillProps} fillProps The username, password and trigger
 * @param {import("../classes/ContentScriptContext")} context Context of the content-script
 * @return {boolean} Returns true is fill was successfull
 */
export const handler = (fillProps: FillProps, context: ContentScriptContext): boolean => {
  // If this attempt is triggered by autofill
  if (!fillProps.userInitiated) {
    // Don't autofill if it's already happened
    if (context.wasAutoFilled) return true

    // Don't autofill on a signup page
    if (context.isSignupUrl) return true
  }

  const { domain } = fillProps
  const windowDomain = parseDomain(context.location)

  if (domain !== windowDomain) {
    logger.error(`❌ No hostname match`, windowDomain, domain)
    return false
  }
  logger.debug(`✅ Hostname match`, windowDomain)

  let filledAnything = false

  context.document.querySelectorAll('form,body').forEach((wrapper) => {
    if (fillCredentialsInWrapper(fillProps, wrapper)) {
      filledAnything = true
    }
  })

  if (filledAnything) {
    dispatchTrackFillSuccess(fillProps)
    context.wasAutoFilled = true
    context.ui.hideDropdown()
  } else if (fillProps.userInitiated) {
    // Only track if we attempted to fill the page manually, not from 'Page Load'
    dispatchTrackFillFailure(fillProps)
  }
  return filledAnything
}
