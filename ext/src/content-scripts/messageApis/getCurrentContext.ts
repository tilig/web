import { GET_CONTEXT_TAB_MESSAGE } from '@/utils/messageConstants'
import { sendToActiveTab } from '@/utils/messageSender'
import { queryActiveTab } from '@/utils/tabUtils'
import ContentScriptContext, { ContextMessage } from '../classes/ContentScriptContext'

export const key = GET_CONTEXT_TAB_MESSAGE

/**
 * Autofill the current page
 * @param {import("../../background/fillUtils").FillProps} fillProps The username, password and trigger
 * @param {import("../classes/ContentScriptContext")} context Context of the content-script
 * @return {boolean} Returns true is fill was successfull
 */
export const handler = (_data: any, context: ContentScriptContext): ContextMessage => {
  return context.toJSON()
}

export const getCurrentContext = async (): Promise<ContextMessage | null> => {
  const tab = await queryActiveTab()
  if (tab) {
    return sendToActiveTab<ContextMessage>(key)
  }
  return null
}
