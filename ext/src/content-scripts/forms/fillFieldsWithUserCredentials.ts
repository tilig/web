import fillInput from './fillInput'

/**
 * Attempts to fill the given input fields with the given value
 * @param {Array} inputs - Fields to fill
 * @param {string} value - Value to fill them with
 * @returns true if any fields were filled.
 */
const fillFieldsWithUserCredentials = (inputs: HTMLInputElement[], value: string) => {
  if (typeof value !== 'string' || value.length === 0) return false
  inputs.forEach((field) => {
    fillInput(field, value)
  })
  return inputs.length > 0
}

export default fillFieldsWithUserCredentials
