const findInputWrapper = (input) => {
  return input.form || document.body
}

export default findInputWrapper
