import colorFilledInput from './colorFilledInput'

const fireKeyEvent = (input: EventTarget, type: string) => {
  input.dispatchEvent(
    new KeyboardEvent(type, {
      altKey: false,
      bubbles: true,
      cancelable: true,
      charCode: 0,
      code: '',
      ctrlKey: false,
      detail: 0,
      isComposing: false,
      key: '',
      keyCode: 0,
      location: 0,
      metaKey: false,
      repeat: false,
      shiftKey: false,
      view: null,
      which: 0,
    })
  )
}

export default (input: HTMLInputElement, value: string) => {
  input.value = value
  // Some sites listen to the input field for changes
  // Fire various events to ensure the value change is picked up
  fireKeyEvent(input, 'keydown')
  fireKeyEvent(input, 'keypress')
  fireKeyEvent(input, 'keyup')
  fireKeyEvent(input, 'keydown')
  fireKeyEvent(input, 'keypress')
  fireKeyEvent(input, 'keyup')
  input.dispatchEvent(new Event('input', { bubbles: true, cancelable: true }))
  input.dispatchEvent(new Event('change', { bubbles: true, cancelable: true }))
  colorFilledInput(input)
}
