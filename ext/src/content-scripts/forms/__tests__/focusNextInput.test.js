import isVisible from '@/utils/isVisible'
import focusNextInput from '../focusNextInput'

vi.mock('@/utils/isVisible')

afterEach(() => (document.body.innerHTML = ''))

test('focusses the next input', () => {
  document.body.innerHTML = `
    <input type="text" id="first" />
    <input type="text" id="second" />
  `
  const second = document.getElementById('second')

  document.getElementById('first').focus()

  isVisible.mockReturnValue(true)

  focusNextInput(document.activeElement)

  expect(isVisible).toHaveBeenCalledWith(second)
  expect(document.activeElement).toEqual(second)
})

test('does not select a previous input', () => {
  document.body.innerHTML = `
    <input type="text" id="first" />
    <input type="text" id="second" />
  `
  document.getElementById('second').focus()
  focusNextInput(document.activeElement)

  expect(document.activeElement).not.toEqual(document.getElementById('first'))
})

// I know, I'm not really testing this case, but I figured it'd be good to keep track
// of this example in case we refactor this method in the future.
// These are the most common types of "hidden" inputs I could think of
test('only focusses the next visible input', () => {
  document.body.innerHTML = `
    <input type="text" id="initial" />
    <input type="hidden" />
    <input type="text" style="display: none;" />
    <div style="display: none">
      <input type="text" />
    </div>
    <input type="text" id="expected" />
  `
  const expected = document.getElementById('expected')

  document.getElementById('initial').focus()

  isVisible.mockImplementation((element) => element === expected)

  focusNextInput(document.activeElement)

  expect(isVisible).toHaveBeenCalledWith(expected)
  expect(document.activeElement).toEqual(expected)
})
