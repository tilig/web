import fillInput from '../fillInput'

test('changes the value of the input', () => {
  const input = document.createElement('input')

  fillInput(input, 'test')
  expect(input.value).toBe('test')
})

test('triggers an input event once the value is changed', () => {
  const input = document.createElement('input')
  const inputHandler = vi.fn()
  input.addEventListener('input', inputHandler)

  fillInput(input, 'test')
  expect(inputHandler).toHaveBeenCalled()
})

test('changes the color of the input with a value', () => {
  const input = document.createElement('input')
  fillInput(input, 'test')
  expect(input.style.background).toBe('rgb(236, 235, 255)')
  expect(input.style.color).toBe('rgb(0, 0, 0)')
})

test('does not change the color of the input with an empty value', () => {
  const input = document.createElement('input')
  fillInput(input, '')
  expect(input.style.background).not.toBe('rgb(236, 235, 255)')
  expect(input.style.color).not.toBe('rgb(0, 0, 0)')
})
