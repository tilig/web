import findInputsInWrapper from '../findInputsInWrapper'
import isVisible from '@/utils/isVisible'
vi.mock('@/utils/isVisible', () => ({ default: vi.fn() }))

beforeEach(() => {
  document.body.innerHTML = ''
  // isValidInput checks isVisible, which needs to be mocked in tests
  isVisible.mockReturnValue(true)
})

afterAll(() => {
  vi.clearAllMocks()
})

test('only returns elements inside of the wrapper', () => {
  document.body.innerHTML = `
    <input type="text" id="username" />
    <form>
      <input type="email" id="email" />
      <input type="password" id="password" />
    </form>
  `

  findInputsInWrapper(document.querySelector('form'))
  const { passwordInputs, usernameInputs, emailInputs } = findInputsInWrapper(
    document.querySelector('form')
  )
  expect(usernameInputs).toHaveLength(0)
  expect(emailInputs).toHaveLength(1)
  expect(passwordInputs).toHaveLength(1)
})

test('finds all inputs in the wrapper', () => {
  document.body.innerHTML = `
    <input type="text" id="username" />
    <input type="email" id="email" />
    <input type="password" id="password" />
  `

  const { passwordInputs, usernameInputs, emailInputs } = findInputsInWrapper(document.body)
  expect(usernameInputs).toHaveLength(1)
  expect(emailInputs).toHaveLength(1)
  expect(passwordInputs).toHaveLength(1)
})

test('Ignores "non signin" password inputs', () => {
  document.body.innerHTML = `
    <input type="text" id="username" />
    <input type="email" id="email" />
    <input type="password" id="new-password" />
    <input type="password" id="confirmpassword" />
  `
  const { passwordInputs, usernameInputs, emailInputs } = findInputsInWrapper(document.body, {
    excludeSignupInputs: true,
  })
  expect(usernameInputs).toHaveLength(1)
  expect(emailInputs).toHaveLength(1)
  expect(passwordInputs).toHaveLength(0)
})
