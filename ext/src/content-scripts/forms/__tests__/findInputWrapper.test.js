import findInputWrapper from '../findInputWrapper'

afterEach(() => {
  document.body.innerHTML = ''
})

test('calls findInputsInWrapper with the form when applicable', () => {
  document.body.innerHTML = `
    <input type="text" id="username" />
    <form>
      <input type="email" id="email" />
      <input type="password" id="password" />
    </form>
  `

  expect(findInputWrapper(document.querySelector('#email'))).toEqual(document.querySelector('form'))
})

test('calls findInputsInWrapper with body when the input is not inside of a form', () => {
  document.body.innerHTML = `
    <input type="text" id="username" />
    <input type="email" id="email" />
    <input type="password" id="password" />
  `

  expect(findInputWrapper(document.querySelector('#email'))).toEqual(document.body)
})

test('calls findInputsInWrapper with body when input is not inside of a form (with form present)', () => {
  document.body.innerHTML = `
  <input type="text" id="username" />
  <form>
    <input type="email" id="email" />
    <input type="password" id="password" />
  </form>
  `

  expect(findInputWrapper(document.querySelector('#username'))).toEqual(document.body)
})
