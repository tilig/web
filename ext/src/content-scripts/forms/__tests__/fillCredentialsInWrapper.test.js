import fillCredentialsInWrapper from '../fillCredentialsInWrapper'
import fillFieldsWithUserCredentials from '../fillFieldsWithUserCredentials'
import findInputsInWrapper from '../findInputsInWrapper'

vi.mock('../fillFieldsWithUserCredentials')
vi.mock('../findInputWrapper')
vi.mock('../findInputsInWrapper')

const createEmailInput = () => {
  const emailInput = document.createElement('input')
  emailInput.type = 'email'
  return emailInput
}

const createFillProps = (username, password) => {
  return {
    username,
    password,
    domain: 'foo.com',
    trigger: 'test',
    userInitiated: true,
  }
}

test('fills out the appropriate fields when both username and email fields are present and an email address is given', () => {
  const usernameInputs = [document.createElement('input')]
  const emailInputs = [createEmailInput()]
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('email@address.com', 'password')
  fillCredentialsInWrapper(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(
    usernameInputs,
    'email@address.com'
  )

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(emailInputs, 'email@address.com')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})

test('fills out the appropriate fields when only a username field is present', () => {
  const usernameInputs = [document.createElement('input')]
  const emailInputs = []
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('username', 'password')
  fillCredentialsInWrapper(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(usernameInputs, 'username')

  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(emailInputs, 'username')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})

test('fills out the appropriate fields when only an email field is present and only an email is given', () => {
  const usernameInputs = []
  const emailInputs = [document.createElement('input')]
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('email@address.com', 'password')
  fillCredentialsInWrapper(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(usernameInputs, 'email@adress.com')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(emailInputs, 'email@address.com')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})

test('fills out the appropriate fields when only a username field is present and only an email is provided', () => {
  const usernameInputs = [document.createElement('input')]
  const emailInputs = []
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('email@address.com', 'password')
  fillCredentialsInWrapper(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(usernameInputs, 'email@address.com')

  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(emailInputs, 'email@address.com')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})

test('returns true when a field was filled', () => {
  const usernameInputs = [document.createElement('input')]
  const emailInputs = []
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  fillFieldsWithUserCredentials.mockReturnValue(true)

  const fillProps = createFillProps('username', 'password')
  const filled = fillCredentialsInWrapper(fillProps, document.createElement('input'))

  expect(filled).toBe(true)
})

test('returns false if no fields were filled', () => {
  const usernameInputs = []
  const emailInputs = []
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('email', 'password')
  const filled = fillCredentialsInWrapper(fillProps, document.createElement('input'))

  expect(filled).toBe(true)
})
