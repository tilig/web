import fillCredentialsAroundElement from '../fillCredentialsAroundElement'
import findInputsInWrapper from '../findInputsInWrapper'
import fillFieldsWithUserCredentials from '../fillFieldsWithUserCredentials'

vi.mock('../fillFieldsWithUserCredentials')
vi.mock('../findInputWrapper')
vi.mock('../findInputsInWrapper')

const createEmailInput = () => {
  const emailInput = document.createElement('input')
  emailInput.type = 'email'
  return emailInput
}

const createFillProps = (username, password) => {
  return {
    username,
    password,
    domain: 'foo.com',
    trigger: 'test',
    userInitiated: true,
  }
}

test('fills out the appropriate fields when a username is present', () => {
  const usernameInputs = [document.createElement('input')]
  const emailInputs = [createEmailInput()]
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('username', 'password')
  fillCredentialsAroundElement(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(usernameInputs, 'username')

  // The username has preference, so email should not be filled in
  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(emailInputs, 'username')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})

test('fills out the appropriate fields when only a username field is present', () => {
  const usernameInputs = [document.createElement('input')]
  const emailInputs = []
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('username', 'password')
  fillCredentialsAroundElement(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(usernameInputs, 'username')

  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(emailInputs, 'username')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})

test('fills out the appropriate fields when only an email field is present and a username is not given', () => {
  const usernameInputs = []
  const emailInputs = [document.createElement('input')]
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('email@address.com', 'password')
  fillCredentialsAroundElement(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(
    usernameInputs,
    'username@address.com'
  )

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(emailInputs, 'email@address.com')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})

test('fills out the appropriate fields when only a username field is present and only an email is provided', () => {
  const usernameInputs = [document.createElement('input')]
  const emailInputs = []
  const passwordInputs = []

  findInputsInWrapper.mockReturnValue({
    usernameInputs,
    emailInputs,
    passwordInputs,
  })

  const fillProps = createFillProps('email@address.com', 'password')
  fillCredentialsAroundElement(fillProps, document.createElement('input'))

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(usernameInputs, 'email@address.com')

  expect(fillFieldsWithUserCredentials).not.toHaveBeenCalledWith(emailInputs, 'email')

  expect(fillFieldsWithUserCredentials).toHaveBeenCalledWith(passwordInputs, 'password')
})
