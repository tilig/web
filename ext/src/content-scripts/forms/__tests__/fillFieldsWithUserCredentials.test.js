import fillFieldsWithUserCredentials from '../fillFieldsWithUserCredentials'

test('sets the value of the fields', () => {
  const fields = [document.createElement('input')]

  const filled = fillFieldsWithUserCredentials(fields, 'new value')

  expect(filled).toBe(true)
  expect(fields[0].value).toBe('new value')
})

test('overwrites the existing value', () => {
  const fields = [document.createElement('input')]
  fields[0].value = 'old value'

  fillFieldsWithUserCredentials(fields, 'new value')

  expect(fields[0].value).toBe('new value')
})

test('does not overwrite the existing value when no value is provided', () => {
  const fields = [document.createElement('input')]
  fields[0].value = 'old value'

  fillFieldsWithUserCredentials(fields, '')

  expect(fields[0].value).toBe('old value')
})

test('does not overwrite the existing value when no value is provided (no string)', () => {
  const fields = [document.createElement('input')]
  fields[0].value = 'old value'

  fillFieldsWithUserCredentials(fields, undefined)

  expect(fields[0].value).toBe('old value')
})

test('sets the value of all provided elements', () => {
  const fields = [document.createElement('input'), document.createElement('input')]

  fillFieldsWithUserCredentials(fields, 'new value')

  fields.forEach((field) => expect(field.value).toBe('new value'))
})
