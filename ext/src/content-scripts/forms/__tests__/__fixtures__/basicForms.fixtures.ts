export const basicLogin = `
  <form>
    <input type="email" id="email" />
    <input type="password" id="password" />
  </form>
`
export const multiplePasswords = `
  <form>
    <input type="email" id="email" />
    <input type="password" id="password" />
    <input type="password" id="password_confirmation" />
  </form>
`

export const multiplePasswordsDifferentForms = `
  <div>
    <form>
      <input type="email" id="email" />
      <input type="password" id="password" />
    </form>
    <form>
      <input type="email" id="email" />
      <input type="password" id="password" />
    </form>
  </div>
`
