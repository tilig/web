import isVisible from '@/utils/isVisible'

const focusNextInput = (input) => {
  const inputs = getSiblingInputs(input)
  const currentIndex = inputs.indexOf(input)
  const nextInput = inputs.find(
    (input, index) =>
      index > currentIndex &&
      input.type !== 'hidden' &&
      // https://github.com/jquery/jquery/blob/master/src/css/hiddenVisibleSelectors.js
      // when an element does not have an offsetWidth or height, it's not visible,
      // with the exception of elements that are position: fixed.
      !!isVisible(input)
  )

  if (!nextInput) return

  nextInput.focus()
}

const getSiblingInputs = (input) => {
  if (input.form) return Array.from(input.form.elements)

  const wrapper = document.body
  // This selector comes from
  // https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/elements
  return Array.from(wrapper.querySelectorAll('button,fieldset,input,object,output,select,textarea'))
}

export default focusNextInput
