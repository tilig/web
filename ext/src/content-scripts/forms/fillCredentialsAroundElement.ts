import { FillProps } from '@/background/fillUtils'
import findInputWrapper from './findInputWrapper'
import fillCredentialsInWrapper from './fillCredentialsInWrapper'

// Only used to fill credentials, never for signup
const fillCredentialsAroundElement = (fillProps: FillProps, element: Element): boolean => {
  if (!element) return false

  const wrapper = findInputWrapper(element)

  return fillCredentialsInWrapper(fillProps, wrapper)
}

export default fillCredentialsAroundElement
