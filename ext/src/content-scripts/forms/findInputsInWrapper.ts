import { FormTypes, guessFormType } from '@/utils/formDetection'
import { isValidInput, guessInputType, InputTypes } from '@/utils/inputTyper'

type FindInputsInWrapperOpts = {
  excludeSignupInputs?: boolean
}

// Find input elements that we can attempt to autoFill with credentials
// Optionally, limit this to fields that don't look like signup/regitration/change password fields
// Useful when autofilling and we need to be more conservative on fills
const findInputsInWrapper = (wrapper: Document | Element, opts?: FindInputsInWrapperOpts) => {
  let inputs = Array.from(wrapper.querySelectorAll('input')).filter((input) => isValidInput(input))

  const usernameInputs = []
  const emailInputs = []
  const passwordInputs = []

  // On pageload autofill, exclude any inputs that are part of signup forms.
  // But on user initiated fill be more aggressive about filling in inputs
  if (opts && opts.excludeSignupInputs) {
    inputs = inputs.filter((input) => guessFormType(input).match !== FormTypes.Signup)
  }

  for (const input of inputs) {
    const inputType = guessInputType(input)?.match
    if (inputType == InputTypes.Email) {
      emailInputs.push(input)
    } else if (inputType == InputTypes.Password) {
      passwordInputs.push(input)
    } else if (inputType == InputTypes.Username) {
      usernameInputs.push(input)
    }
  }

  return { usernameInputs, emailInputs, passwordInputs }
}

export default findInputsInWrapper
