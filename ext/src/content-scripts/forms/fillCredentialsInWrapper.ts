import fillFieldsWithUserCredentials from './fillFieldsWithUserCredentials'
import findInputsInWrapper from './findInputsInWrapper'
import { isEmailAddress } from '@core/utils/isEmailAddress'
import { FillProps } from '@/background/fillUtils'
import { profileInput } from '@/utils/inputTyper'
import { FormTypes } from '@/utils/formDetection'

/**
 * Attempt to fill in any input fields found in the wrapper with the given values
 * @returns true if any input field was autofill, false otherwise
 */
const fillCredentialsInWrapper = (fillProps: FillProps, wrapper: Document | Element): boolean => {
  const { usernameInputs, emailInputs, passwordInputs } = findInputsInWrapper(wrapper, {
    // On autofill (page load), be less aggressive and exclude filling in 'signup' looking inputs
    // If this was user initiated, fill everything that seems like an input
    excludeSignupInputs: !fillProps.userInitiated,
  })
  const { username: loginId, password, userInitiated } = fillProps

  // Looks like we can call these functions more efficienctly,
  // but actually we do need their side effects.
  // These booleans are just here to track if anything was filled,
  // for the analytics.
  let filledUsernameOrEmail = false
  let filledPassword = false

  // On autofill, filter inputs that we don't have much confidence in
  // But on manual fill, be much more aggressive
  const filteredUsernameInputs = userInitiated
    ? usernameInputs
    : usernameInputs.filter((input) => {
        return profileInput(input).confidence
      })

  const filteredEmailInputs = userInitiated
    ? emailInputs
    : emailInputs.filter((input) => {
        return profileInput(input).confidence
      })

  const filteredPasswordInputs = passwordInputs.filter((input) => {
    const profiledInput = profileInput(input)
    // Only fill 'login' form inputs
    return (
      profiledInput.inputResult?.formTypeOverride === FormTypes.Login ||
      profiledInput.formResult.match === FormTypes.Login ||
      profiledInput.formResult.match === null
    )
  })

  // We're assuming either a username OR email is given.
  if (loginId) {
    if (isEmailAddress(loginId)) {
      // Attempt to fill in email fields first
      if (emailInputs.length > 0) {
        filledUsernameOrEmail = fillFieldsWithUserCredentials(filteredEmailInputs, loginId)
        // Then move on to username if that's not possible
      } else if (usernameInputs.length > 0) {
        filledUsernameOrEmail = fillFieldsWithUserCredentials(filteredUsernameInputs, loginId)
      }
    } else {
      if (usernameInputs.length > 0) {
        filledUsernameOrEmail = fillFieldsWithUserCredentials(filteredUsernameInputs, loginId)
      } else if (emailInputs.length > 0) {
        filledUsernameOrEmail = fillFieldsWithUserCredentials(filteredEmailInputs, loginId)
      }
    }
  }

  if (password) {
    filledPassword = fillFieldsWithUserCredentials(filteredPasswordInputs, password)
  }
  return filledUsernameOrEmail || filledPassword
}

export default fillCredentialsInWrapper
