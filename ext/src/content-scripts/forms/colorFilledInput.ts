const ATTR_IS_COLORED = 'tilig-is-colored'
const ATTR_ORIGINAL_BG_COLOR = 'tilig-original-bg-color'
const ATTR_ORIGINAL_TEXT_COLOR = 'tilig-original-text-color'

const FILLED_BG_COLOR = 'rgb(236, 235, 255)'
const FILLED_TEXT_COLOR = 'rgb(0, 0, 0)'

// Not sure if this is overkill, maybe only keyup will work
const EVENT_TYPES = ['change', 'keyup', 'keydown']

const setFilledStyle = (input: HTMLInputElement) => {
  if (!input.getAttribute(ATTR_IS_COLORED)) {
    // Remember the original css values
    input.setAttribute(ATTR_ORIGINAL_BG_COLOR, input.style.backgroundColor)
    input.setAttribute(ATTR_ORIGINAL_TEXT_COLOR, input.style.color)
    input.setAttribute(ATTR_IS_COLORED, 'true')
  }

  input.style.backgroundColor = FILLED_BG_COLOR
  input.style.color = FILLED_TEXT_COLOR
}

const setClearedStyle = (input: HTMLInputElement) => {
  // Set back the original values
  input.style.backgroundColor = input.getAttribute(ATTR_ORIGINAL_BG_COLOR) ?? 'inherit'
  input.style.color = input.getAttribute(ATTR_ORIGINAL_TEXT_COLOR) ?? 'inherit'
  input.removeAttribute(ATTR_IS_COLORED)
  EVENT_TYPES.forEach((eventType) => {
    input.removeEventListener(eventType, onChange)
  })
}

const setInputStyle = (input: HTMLInputElement) => {
  if (input.value.length) {
    setFilledStyle(input)
  } else {
    setClearedStyle(input)
  }
}

const onChange = (event: Event) => {
  const input = event.target as HTMLInputElement
  if (input) {
    setInputStyle(input)
  }
}

export default (input: HTMLInputElement) => {
  if (!input.getAttribute(ATTR_IS_COLORED)) {
    setInputStyle(input)
    EVENT_TYPES.forEach((eventType) => {
      input.addEventListener(eventType, onChange)
    })
  }
}
