import { parseDomain } from '@core/utils/domainUtils'
import { isSignupUrl } from '@/utils/isSignupUrl'
import findInputsInWrapper from '../forms/findInputsInWrapper'
import ShadowDoms from './ShadowDoms'

export type ContextMessage = {
  hasLoginOrSignupForm: boolean
  isSignupUrl: boolean
  domain: string | null
}

/**
 * Apple uses a 3rd party iframe for login. It seems to be one of the only
 * major sites using this method, so we make an exception for now
 * TODO: @mdp find a better way to cull unneeded 3rd party iframes (mostly ad iframes)
 *       While it would be trivial to load the content-script on every iframe
 *       some pages haves tens of them which would be pretty wasteful
 */
const WHITELISTED_IFRAMES = ['apple.com']

/**
 * ContentScriptContext
 * Holds our basic context information about the content-script page in one place
 */
export default class ContentScriptContext {
  public window: Window
  public wasAutoFilled: boolean

  // Used for getter caching
  private _ui: ShadowDoms | null

  constructor(window: Window) {
    // window access in one place
    this.window = window

    this.wasAutoFilled = false

    // Holds all our shadow doms (iframes) of the UI
    this._ui = null
  }

  // Returns true if we are on the top location (not an iframe / root page), or if
  // we are on an iframe, but it's in the same domain (1st party iframe)
  public get isTabDomain(): boolean {
    try {
      // This will throw an error in 3rd party iframes. We suppress it and return false
      // as the error means we are definitely not on the tab domain
      if (!this.window.top) return false
      return parseDomain(this.window.top.location.href) === this.domain
    } catch (_) {
      // Allow certain iframed domains to load the Tilig content-script
      if (this.domain && WHITELISTED_IFRAMES.includes(this.domain)) return true
      return false
    }
  }

  // Returns false if we are in an iframe of any type
  public get isTopWindow(): boolean {
    try {
      // 3rd party iframes do not have access to top
      // suppress the error and return false
      return this.window === this.window.top
    } catch (e) {
      return false
    }
  }

  // Return true is the page looks like a signup page
  public get isSignupUrl(): boolean {
    return isSignupUrl(this.document.location.href)
  }

  // Return true if we may be on a Tilig page
  // NOTE: This is not secure. Don't abuse this.
  // See https://gitlab.com/subshq/clients/webapp/-/issues/68W
  public get isOnTilig(): boolean {
    return !!this.document.querySelector("meta[itemprop=name][content='Tilig Password Manager']")
  }

  // Getter to hold our UI elements
  public get ui(): ShadowDoms {
    if (this._ui == null) {
      this._ui = new ShadowDoms(this.document, this.window)
    }
    return this._ui
  }

  public get document(): Document {
    return this.window.document
  }

  public get location(): string {
    return this.window.location.href
  }

  public get domain(): string | null {
    return parseDomain(this.location)
  }

  public get hasLoginOrSignupForm(): boolean {
    const { usernameInputs, emailInputs, passwordInputs } = findInputsInWrapper(this.document)
    if ([usernameInputs, emailInputs, passwordInputs].find((c) => c.length > 0)) {
      return true
    }
    return false
  }

  // Serialize for users like Popup.html
  public toJSON(): ContextMessage {
    return {
      hasLoginOrSignupForm: this.hasLoginOrSignupForm,
      isSignupUrl: this.isSignupUrl,
      domain: this.domain,
    }
  }
}
