import inputLogo from '@/assets/images/input_logo_circle.png'
import { debounce, DebouncedFunc, throttle } from 'lodash'
import { POSITION_UPDATE_INTERVAL } from './ShadowDoms'

const MINIMUM_ICON_SIZE = 16

export default class TiligInputLogo {
  public rootDocument: Document
  public onResize: DebouncedFunc<(_size?: { width: number; height: number }) => void> = throttle(
    this.positionUpdate,
    POSITION_UPDATE_INTERVAL
  )
  public debouncedOnScroll: DebouncedFunc<(input: HTMLInputElement) => void> = debounce(
    this.showInInput,
    500
  )
  public logoElement: HTMLImageElement

  public currentInput: HTMLInputElement | null = null
  private logoContainer: HTMLElement
  private interval: ReturnType<typeof setInterval> | null = null

  constructor(doc: Document) {
    this.rootDocument = doc
    this.logoContainer = doc.createElement('tilig-logo-container')

    const logoShadow = this.logoContainer.attachShadow({ mode: 'closed' })
    this.logoElement = doc.createElement('img')
    this.logoElement.style.position = 'fixed'
    this.logoElement.style.zIndex = '2147483645'
    this.logoElement.style.cursor = 'pointer'

    this.logoElement.src = new URL(inputLogo, import.meta.url).href
    logoShadow.appendChild(this.logoElement)
  }

  hide() {
    this.currentInput = null
    this.logoContainer.remove()
    this.cancelIntervalWatcher()
    this.debouncedOnScroll.cancel()
  }

  // Remove the icon on scroll, but put it back when the user stops scrolling
  onScroll() {
    if (this.currentInput) {
      this.cancelIntervalWatcher()
      this.logoContainer.hidden = true
      this.debouncedOnScroll(this.currentInput)
    }
  }

  showInInput(input: HTMLInputElement) {
    this.currentInput = input
    this.positionUpdate()

    this.rootDocument.body.appendChild(this.logoContainer)
    // this.addMutationObserver()
    this.addIntervalWatcher()
  }

  positionUpdate() {
    if (!this.currentInput) {
      this.cancelIntervalWatcher()
      return
    }
    this.logoContainer.hidden = false
    const clientRects = this.currentInput.getClientRects()[0]

    const padding = clientRects.height / 4
    const height = clientRects.height - 2 * padding

    let inputPadding = parseInt(
      this.rootDocument.defaultView
        ?.getComputedStyle(this.currentInput, '')
        .getPropertyValue('padding-right') || '0',
      10
    )

    // Normalize the icon size to a minimum of 16px
    // Then offset it so that it's in the middle of the input
    let iconSize = height
    let iconSizeOffset = 0
    if (height < MINIMUM_ICON_SIZE) {
      iconSize = MINIMUM_ICON_SIZE
      iconSizeOffset = iconSize / 2 - height / 2
      inputPadding = MINIMUM_ICON_SIZE / 4
    }

    this.logoElement.style.left = `${clientRects.right - height - padding - inputPadding}px`
    this.logoElement.style.top = `${clientRects.top + padding - iconSizeOffset}px`
    this.logoElement.style.height = `${iconSize}px`
    this.logoElement.style.width = `${iconSize}px`
  }

  addIntervalWatcher() {
    this.cancelIntervalWatcher()
    this.interval = setInterval(() => {
      this.positionUpdate()
    }, POSITION_UPDATE_INTERVAL)
  }

  cancelIntervalWatcher() {
    if (this.interval) {
      clearInterval(this.interval)
      this.interval = null
    }
  }
}
