const IFRAME_WIDTH = 500
const IFRAME_HEIGHT = 150
const IFRAME_MARGIN = 15

export default class TiligAutocapture {
  isHidden = true
  bgContainer = null
  updateContainer = null
  updateIframe = null
  rootDocument = null
  rootWindow = null

  constructor(doc, window) {
    this.rootDocument = doc
    this.rootWindow = window
    this.updateContainer = doc.createElement('tilig-modal-container')
    this.updateIframe = doc.createElement('iframe')
    this.updateIframe.style.position = 'fixed'
    this.updateIframe.style.zIndex = 2147483647
    this.updateIframe.setAttribute('frameborder', 0)
    this.updateIframe.setAttribute('allow', 'clipboard-write')

    const updateContainerShadow = this.updateContainer.attachShadow({
      mode: 'closed',
    })

    updateContainerShadow.appendChild(this.updateIframe)
  }

  hide() {
    this.updateContainer.remove()
    this.bgContainer?.remove()
    this.isHidden = true
  }

  show() {
    if (!this.isHidden) {
      // Reset for a new modal
      this.hide()
    }
    const left = Math.floor(
      // Take into account scrollbar width vs innerWidth
      this.rootWindow.document.documentElement.clientWidth - IFRAME_WIDTH - IFRAME_MARGIN
    )
    this.updateIframe.style.left = `${left}px`
    this.updateIframe.style.top = `${IFRAME_MARGIN}px`
    this.updateIframe.style.height = `${IFRAME_HEIGHT}px`
    this.updateIframe.style.width = `${IFRAME_WIDTH}px`

    this.updateIframe.src = chrome.runtime.getURL('/src/ui/autocapture/index.html')
    this.rootDocument.body.appendChild(this.updateContainer)
    this.isHidden = false
  }

  // Ignore height for now
  onResize({ width: width, height: _h }) {
    const left = Math.floor((width - IFRAME_WIDTH) / 2.0)
    this.updateIframe.style.left = `${left}px`
  }
}
