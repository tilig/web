import ContentScriptContext from '../ContentScriptContext'
import { describe, expect, test } from 'vitest'

// TODO: write more tests for this class
describe('the basic ContentScriptContext class', () => {
  test('ensure that it works properly', () => {
    const context = new ContentScriptContext(window)
    expect(context).toBeInstanceOf(ContentScriptContext)
  })

  test('ensure isTabDomain returns false for different domains', () => {
    const windowMock = Object.create(window)

    // isTabDomain uses publicsuffix.org data to determine this
    Object.defineProperty(windowMock, 'location', {
      value: { href: 'https://www.tilig.com' },
    })
    Object.defineProperty(windowMock, 'top', {
      value: {
        location: {
          href: 'https://www.mozilla.org',
        },
      },
    })
    const context = new ContentScriptContext(windowMock)
    expect(context.isTabDomain).toBe(false)
  })

  test('ensure isTabDomain returns true for same domains TLDs', () => {
    const windowMock = Object.create(window)

    Object.defineProperty(windowMock, 'location', {
      value: { href: 'https://mail.yahoo.com' },
    })
    Object.defineProperty(windowMock, 'top', {
      value: {
        location: {
          href: 'https://www.yahoo.com',
        },
      },
    })
    const context = new ContentScriptContext(windowMock)
    expect(context.isTabDomain).toBe(true)
  })
})
