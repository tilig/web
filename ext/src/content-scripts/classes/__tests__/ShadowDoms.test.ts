import ShadowDoms from '../ShadowDoms'
import { describe, expect, test } from 'vitest'

// TODO: write more tests for this class
describe('the basic Shadowdoms class', () => {
  test('ensure that it works properly', () => {
    const shadowDoms = new ShadowDoms(document, window)
    expect(shadowDoms).toBeInstanceOf(ShadowDoms)
  })
})
