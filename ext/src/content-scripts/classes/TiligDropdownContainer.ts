import { guessInputType } from '@/utils/inputTyper'
import { getRandomGif } from '@/utils/randomGif'
import { DebouncedFunc, throttle } from 'lodash'
import { POSITION_UPDATE_INTERVAL } from './ShadowDoms'

export default class TiligDropdownContainer {
  public isExplicitlyHidden = false
  public isVisible = false
  public gifsEnabled = false
  public dropdownContainer: HTMLElement
  public dropdownIframe: HTMLIFrameElement
  public gifContainer: HTMLElement
  public rootDocument: Document
  public onResize: DebouncedFunc<(_size?: { width: number; height: number }) => void> = throttle(
    this.positionUpdate,
    POSITION_UPDATE_INTERVAL
  )
  private currentInput: HTMLInputElement | null = null
  private interval: ReturnType<typeof setInterval> | null = null

  constructor(document: Document) {
    this.rootDocument = document
    this.dropdownContainer = document.createElement('tilig-dropdown-container')
    this.dropdownIframe = document.createElement('iframe')
    this.gifContainer = document.createElement('div')

    const dropdownShadow = this.dropdownContainer.attachShadow({
      mode: 'closed',
    })

    this.dropdownIframe.style.position = 'fixed'
    this.dropdownIframe.style.zIndex = '2147483647'
    this.dropdownIframe.setAttribute('frameborder', '0')

    dropdownShadow.appendChild(this.dropdownIframe)
    dropdownShadow.appendChild(this.gifContainer)
  }

  showUnderInput(
    input: HTMLInputElement,
    { explicit = false, isSignup }: { explicit?: boolean; isSignup?: boolean }
  ) {
    if (!explicit && this.isExplicitlyHidden) return
    if (explicit) this.isExplicitlyHidden = false

    // Don't try to show it when it's already visible
    if (!this.isHidden() && this.currentInput === input) {
      return
    }

    this.currentInput = input
    const inputTypeGuess = guessInputType(input)?.match
    const clientRects = input.getClientRects()[0]

    this.dropdownIframe.style.left = `${clientRects.left}px`
    this.dropdownIframe.style.top = `${clientRects.bottom + 1}px`
    // If we don't set this, it defaults to 300px, which is too small for longer suggestions.
    this.dropdownIframe.style.width = `600px`
    // This loads the Dropdown chunk and passes the input type
    this.dropdownIframe.src = `${chrome.runtime.getURL(
      'src/ui/dropdown/index.html'
    )}#/?inputType=${inputTypeGuess}&isSignup=${isSignup ? 'true' : 'false'}`
    this.rootDocument.body.appendChild(this.dropdownContainer)
    this.addIntervalWatcher()

    if (this.gifsEnabled) {
      this.showGifAboveInput(input)
    }
  }

  showGifAboveInput(input: HTMLInputElement) {
    const clientRects = input.getClientRects()[0]
    const height = 30

    this.gifContainer.style.position = 'absolute'
    this.gifContainer.style.left = `${clientRects.left}px`
    this.gifContainer.style.top = `${clientRects.top - height}px`
    this.gifContainer.style.height = `${height}px`
    this.gifContainer.style.width = `${clientRects.width}px`

    // Clear previous gif
    this.gifContainer.childNodes.forEach((child) => child.remove())

    const gifElement = this.rootDocument.createElement('img')
    gifElement.style.position = 'absolute'
    gifElement.style.bottom = '0'

    gifElement.src = new URL(getRandomGif(), import.meta.url).href
    this.gifContainer.appendChild(gifElement)
  }

  hide({ explicit = false } = {}) {
    if (explicit) this.isExplicitlyHidden = true

    this.currentInput = null

    this.dropdownContainer.remove()
    this.cancelIntervalWatcher()
  }

  isHidden() {
    return !this.dropdownContainer.parentNode
  }

  toggle(
    input: HTMLInputElement,
    { explicit = false, isSignup }: { explicit?: boolean; isSignup?: boolean } = {}
  ) {
    if (this.isHidden()) {
      this.showUnderInput(input, { explicit, isSignup })
    } else {
      this.hide({ explicit: true })
    }
  }

  positionUpdate() {
    if (!this.currentInput) {
      this.cancelIntervalWatcher()
      return
    }
    const clientRects = this.currentInput.getClientRects()[0]

    this.dropdownIframe.style.left = `${clientRects.left}px`
    this.dropdownIframe.style.top = `${clientRects.bottom + 1}px`
  }

  addIntervalWatcher() {
    this.cancelIntervalWatcher()
    this.interval = setInterval(() => {
      this.onResize()
    }, POSITION_UPDATE_INTERVAL)
  }

  cancelIntervalWatcher() {
    if (this.interval) {
      clearInterval(this.interval)
      this.interval = null
    }
  }
}
