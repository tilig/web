import { createLogger } from '@core/utils/logger'
const logger = createLogger('modal:credentials')

const IFRAME_WIDTH = 500
const IFRAME_MAX_HEIGHT = 750
export default class TiligCredentials {
  isHidden = true
  bgContainer = null
  updateContainer = null
  updateIframe = null
  rootDocument = null
  rootWindow = null

  constructor(doc, window) {
    this.rootDocument = doc
    this.rootWindow = window
    this.updateContainer = doc.createElement('tilig-modal-container')
    this.updateIframe = doc.createElement('iframe')
    this.updateIframe.style.position = 'fixed'
    this.updateIframe.style.zIndex = 2147483647
    this.updateIframe.setAttribute('frameborder', 0)
    this.updateIframe.setAttribute('allow', 'clipboard-write')

    const updateContainerShadow = this.updateContainer.attachShadow({
      mode: 'closed',
    })

    updateContainerShadow.appendChild(this.updateIframe)
  }

  hide() {
    this.updateContainer.remove()
    this.bgContainer?.remove()
  }

  show() {
    // 10px margin around the popup
    // const left = rootEl.clientWidth - IFRAME_WIDTH - 10
    const left = Math.floor((this.rootWindow.innerWidth - IFRAME_WIDTH) / 2.0)
    this.updateIframe.style.left = `${left}px`
    this.updateIframe.style.top = '50px'
    this.updateIframe.style.height = `${this.calculateFrameHeight(this.rootWindow.innerHeight)}px`
    this.updateIframe.style.width = `${IFRAME_WIDTH}px`

    this.bgContainer = this.rootDocument.createElement('tilig-modal-bg')
    this.bgContainer.style.position = 'fixed'
    this.bgContainer.style.zIndex = 2147483646
    this.bgContainer.style.top = 0
    this.bgContainer.style.bottom = 0
    this.bgContainer.style.left = 0
    this.bgContainer.style.right = 0
    this.bgContainer.style.background = 'rgba(0, 0, 0, 0.75)'

    this.updateIframe.src = chrome.runtime.getURL('/src/ui/credentials/index.html')
    this.rootDocument.body.appendChild(this.updateContainer)
    this.rootDocument.body.appendChild(this.bgContainer)
  }

  onResize({ width: width, height: height }) {
    const left = Math.floor((width - IFRAME_WIDTH) / 2.0)
    this.updateIframe.style.left = `${left}px`
    this.updateIframe.style.height = `${this.calculateFrameHeight(height)}px`
  }

  calculateFrameHeight(windowHeight) {
    const height = windowHeight < IFRAME_MAX_HEIGHT ? windowHeight - 100 : IFRAME_MAX_HEIGHT
    logger.debug(`Modal height: ${height}`)
    return height
  }
}
