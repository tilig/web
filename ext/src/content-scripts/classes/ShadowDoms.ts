import TiligDropdownContainer from './TiligDropdownContainer'
import TiligInputLogo from './TiligInputLogo'
import TiligAutocapture from './TiligAutocapture'
import TiligCredentials from './TiligCredentials'
import { DebouncedFunc, throttle } from 'lodash'
import { createLogger } from '@core/utils/logger'

export const POSITION_UPDATE_INTERVAL = 250

const logger = createLogger('ext:ShadowDom')

// Class for handling all the UI changes to the current page
// Ideally we never need to make a call directly to shadowDom directly
// and instead can simply make calls to this root class
export default class ShadowDoms {
  public tiligInputLogo: TiligInputLogo
  public tiligDropdownContainer: TiligDropdownContainer
  public tiligAutocapture: TiligAutocapture
  public tiligCredentials: TiligCredentials
  public onResize: DebouncedFunc<(size: { width: number; height: number }) => void> = throttle(
    this.onResizeHandler,
    200
  )

  public onInputLogoClick?: (event: MouseEvent) => void

  constructor(document: Document, window: Window) {
    /**
     * Create a ShadowDoms instance
     * @param {Document} document is the root document for Shadow DOMs to attach to
     */
    this.tiligInputLogo = new TiligInputLogo(document)
    this.tiligDropdownContainer = new TiligDropdownContainer(document)
    this.tiligAutocapture = new TiligAutocapture(document, window)
    this.tiligCredentials = new TiligCredentials(document, window)

    this.tiligInputLogo.logoElement.addEventListener('mousedown', (event: MouseEvent) => {
      event.preventDefault()
      if (this.onInputLogoClick) this.onInputLogoClick(event)
    })
  }

  showInputLogo(elem: HTMLInputElement) {
    if (!this.tiligInputLogo) return
    this.tiligInputLogo.showInInput(elem)
  }

  hideInputLogo() {
    if (!this.tiligInputLogo) return
    this.tiligInputLogo.hide()
  }

  onScroll() {
    this.hideDropdown()
    if (!this.tiligInputLogo) return
    this.tiligInputLogo.onScroll()
  }

  showDropdown(elem: HTMLInputElement, { isSignup }: { isSignup: boolean }) {
    logger.debug(`showDropdown`)
    if (!this.tiligDropdownContainer) return
    this.tiligDropdownContainer.showUnderInput(elem, { isSignup: isSignup })
  }

  hideDropdown(options?: { explicit: boolean }) {
    logger.debug(`hideDropdown`)
    if (!this.tiligDropdownContainer) return
    this.tiligDropdownContainer.hide(options)
  }

  toggleDropdown(
    elem: HTMLInputElement,
    { isSignup, explicit }: { isSignup: boolean; explicit: boolean }
  ) {
    if (!this.tiligDropdownContainer) return
    this.tiligDropdownContainer.toggle(elem, { isSignup, explicit })
  }

  showAutocaptureModal() {
    if (!this.tiligAutocapture) return
    this.tiligAutocapture.show()
  }

  dismissAutocaptureModal() {
    if (!this.tiligAutocapture) return
    this.tiligAutocapture.hide()
  }

  showCrendentialsModal() {
    if (!this.tiligCredentials) return
    this.tiligCredentials.show()
  }

  hideCredentialsModal() {
    if (!this.tiligCredentials) return
    this.tiligCredentials.hide()
  }

  // Debounced resize handler
  private onResizeHandler({ width, height }: { width: number; height: number }) {
    this.tiligAutocapture.onResize({ width, height })
    this.tiligCredentials.onResize({ width, height })
    this.tiligDropdownContainer.onResize({ width, height })
    this.tiligInputLogo.onResize({ width, height })
  }
}
