import { throttle } from 'lodash'
// Using throttle vs debounce. See: https://gitlab.com/subshq/clients/browser/-/merge_requests/183#note_979416623

export default class ThrottledMutationObserver {
  /**
   * Create a ThrottledMutationObserver
   * @param {Object} options for ThrottledMutationObserver
   * @param {number} options.throttleTime throttle timeout
   * @param {number} options.timeout timeout for observer
   */
  constructor({ throttleTime, timeout }, callback) {
    this.throttleTime = throttleTime
    this.timeout = timeout
    this.timeoutId = null
    this.callback = callback
    this.observing = false
    this.throttledCallback = throttle(callback, throttleTime, {
      leading: false,
      trailing: true,
    })

    /**
     * Our mutation observer
     * @type {MutationObserver}
     */
    this.observer = new MutationObserver((mutations) => {
      for (const mutation of mutations) {
        if (mutation.addedNodes.length > 0) {
          for (const addedNode of mutation.addedNodes) {
            if (addedNode.nodeType === 1 && addedNode.tagName !== 'script') {
              this.throttledCallback()
            }
          }
        } else if (mutation.type === 'attributes') {
          this.throttledCallback()
        }
      }
    })
  }

  observe(element) {
    this.observer.observe(element, { childList: true, subtree: true, attributes: true })
    this.timeoutId = setTimeout(() => this.cancel(), this.timeout)
    this.observing = true
  }

  cancel() {
    if (this.observing) {
      // Stop observing and cancel the timeout
      this.observer.disconnect()
      if (this.timeoutId) clearTimeout(this.timeoutId)

      this.observing = false
    }
  }
}
