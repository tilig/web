import { createLogger } from '@core/utils/logger'
import { updateDomainState } from '@/background/messageApis/draftAccounts'
import { attemptAutofillLogin } from '@/background/messageApis/autofillLogin'
import { sendOnPageLoad } from '@/background/messageApis/onPageLoad'
import { DraftUpdateData } from '@/background/stores/draft'
//import { useSettingsStore } from '@/background/stores/settings'
import { FormTypes } from '@/utils/formDetection'
import {
  guessInputType,
  InputOverallConfidence,
  InputTypes,
  profileInput,
} from '@/utils/inputTyper'
import ThrottledMutationObserver from './classes/ThrottledMutationObserver'
import ContentScriptContext from './classes/ContentScriptContext'
import { contentScriptMessageListener } from './messageApis'
import { throttle } from 'lodash'
import { getSettings } from '@/background/messageApis/settings'

// Number of chars the user should type before the dropdown disappears
const SHOW_DROPDOWN_UNTIL_CHARS_TYPED = 3

const logger = createLogger('ext')

const context: ContentScriptContext = new ContentScriptContext(window)

const draftData: DraftUpdateData = {}

let currentInputListener: ((event: Event) => void) | null = null

const mutationObserver = new ThrottledMutationObserver({ throttleTime: 750, timeout: 9000 }, () => {
  if (context.wasAutoFilled === true) {
    mutationObserver.cancel()
  } else {
    attemptAutofillLogin({ trigger: 'Page Load', userInitiated: false })
  }
})

const savePreAutoFill = () => {
  // Save any browser driven email autofills into the drafts
  // This allows a user to pick a suggested password and the credential modal
  // will also know about the browser autofilled email
  const inputs = Array.from(document.getElementsByTagName('input'))
    .filter((input) => guessInputType(input)?.match == InputTypes.Email)
    .filter((input) => input.value && input.value.length > 0)
  if (inputs.length > 0) {
    draftData['username'] = inputs[0].value
    logger('update with prefill')
    updateDomainState(draftData)
  }
}

// Profile the given input for type and formType and based on context of the current page (URL)
const profileInputWithContext = (input: HTMLInputElement) => {
  const { formResult, inputResult, confidence } = profileInput(input)
  // `isSignup` Determines if we should show a credential dropdown or suggested fill (email/password)
  const isSignup =
    (context.isSignupUrl ||
      formResult.match === FormTypes.Signup ||
      formResult.match === FormTypes.ChangePassword) &&
    // This could be a three password input form (old, new, confirm), in which case
    // We flagged 'old password' as a login type and override the parent form determination
    inputResult?.formTypeOverride !== FormTypes.Login
  return {
    formResult,
    inputResult,
    confidence,
    isSignup,
  }
}

const attachDomListeners = async () => {
  // Shows the Tilig dropdown when the user focusses
  // a field
  document.addEventListener('focusin', (event: Event) => {
    focusInHandler(event.target as HTMLElement)
  })

  // Hides the Tilig dropdown when the input field is
  // no longer focussed
  document.addEventListener('focusout', () => {
    context.ui.hideInputLogo()
    context.ui.hideDropdown()
  })

  // Lets hide on the escape key
  document.addEventListener('keyup', (event: KeyboardEvent) => {
    if (event.code === 'Escape') {
      context.ui.hideDropdown()
      context.ui.hideCredentialsModal()
      context.ui.dismissAutocaptureModal()
    }
  })

  const onScroll = throttle(() => {
    context.ui.onScroll()
  }, 200)

  document.addEventListener('scroll', onScroll)

  window.visualViewport?.addEventListener('resize', () => {
    if (!window.visualViewport) return
    const { width, height } = window.visualViewport
    context.ui.onResize({ width, height })
  })

  // activeElement is the element that is currently focussed
  // For pages that have autofocus on forms
  focusInHandler(document.activeElement as HTMLElement)

  // Fetch a list of usernames for this domain for use in Dropdown
  context.ui.onInputLogoClick = (_event) => {
    const activeElement = context.document.activeElement as HTMLInputElement | null
    if (!activeElement) return
    const { isSignup } = profileInputWithContext(activeElement)
    context.ui.toggleDropdown(activeElement, {
      isSignup,
      explicit: true,
    })
  }

  // Attempt to autofill, if it returns true it means Tilig has credentials for
  // the user.
  if (await attemptAutofillLogin({ trigger: 'Page Load', userInitiated: false })) {
    mutationObserver.observe(context.document.documentElement)
  }
}

const focusInHandler = (element: HTMLElement | null) => {
  // Quickly return if we are already on a Tilig page or the element is not an input
  if (context.isOnTilig || !element || !(element instanceof HTMLInputElement)) return

  const { formResult, inputResult, confidence, isSignup } = profileInputWithContext(element)

  // Stop if we can't even guess an input type
  if (!inputResult) {
    logger.extend('ui:event')("Ignoring Input since we can't guess it's type")
    return
  }

  logger.extend('ui:event')(`
    Focus In Handler:
      InputType: ${inputResult.match}
      InputTypeConfidence: ${inputResult.confidence}
      InputFormOverride: ${inputResult.formTypeOverride}
      Is sign-up looking URL: ${context.isSignupUrl}
      Form Guess: ${formResult.match} - ${formResult.name}${
    formResult.description ? ':' + formResult.description : ''
  }
      Overall Confidence: ${confidence}
  `)

  // Skip unless confidence is high
  if (!confidence || confidence < InputOverallConfidence.High) {
    logger.debug('Low confidence and no form match, skipping')
    // Skip if we are on a signup page and the input is not an email or password (eg. username)
  } else if (isSignup && ![InputTypes.Email, InputTypes.Password].includes(inputResult.match)) {
    logger.debug(`Signup page, skipping ${inputResult.match}`)
  } else {
    // Add event listener to watch input field, but make sure to remove existing ones
    if (currentInputListener) {
      element.removeEventListener('input', currentInputListener)
    }
    currentInputListener = handleElementInput(inputResult.match, isSignup)
    element.addEventListener('input', currentInputListener)

    // Always show the logo on an element that we can autofill
    context.ui.showInputLogo(element)

    // Only show the dropdown if the input is empty
    if (element.value.length <= SHOW_DROPDOWN_UNTIL_CHARS_TYPED) {
      context.ui.showDropdown(element, { isSignup })
    }
  }
}

// Hide the dropdown and save a draft of the input to the domain state
const handleElementInput = (inputType: InputTypes, isSignup: boolean): ((event: Event) => void) => {
  logger.debug(`handleElementInput: ${inputType}'`)
  return (event: Event) => {
    if (!event.target) return
    const target = event.target as HTMLInputElement

    if (inputType === InputTypes.Password) {
      draftData.password = target.value
    } else if (inputType === InputTypes.Email || inputType === InputTypes.Username) {
      draftData.username = target.value
    }

    updateDomainState(draftData)
    // Decide if we should hide or show the dropdown
    if (target.value.length <= SHOW_DROPDOWN_UNTIL_CHARS_TYPED) {
      context.ui.showDropdown(target, { isSignup: isSignup })
    } else {
      context.ui.hideDropdown()
    }
  }
}

const attachRuntimeListener = () => {
  chrome.runtime.onMessage.addListener(contentScriptMessageListener(context))
}

if (context.isOnTilig) {
  const extensionIdentifier = document.createElement('meta')
  extensionIdentifier.setAttribute('name', 'tilig:extension')
  extensionIdentifier.setAttribute('content', 'installed')
  document.head.appendChild(extensionIdentifier)
}

const main = async (): Promise<void> => {
  if (context.isTabDomain && !context.isOnTilig) {
    sendOnPageLoad()
    savePreAutoFill()
    attachRuntimeListener()
    attachDomListeners()
    const settings = await getSettings()
    context.ui.tiligDropdownContainer.gifsEnabled = settings.gifsEnabled
    logger.debug(`Settings loaded:`, settings)
  }
}

main().catch((err) => {
  // We need console here in case anything fails, including logger
  // eslint-disable-next-line no-console
  console.error('Error loading Tilig content script on page', err)
})
