import { decryptString } from '../utils/encryption'

const worker = self
class Decryptor {
  constructor() {
    this.active = false
    this.count = 0

    this.increaseCounter = this.increaseCounter.bind(this)
    this.decrypt = this.decrypt.bind(this)
  }

  increaseCounter() {
    this.counter += 1

    if (this.counter % 10 === 0) {
      this.active = !this.active
    }
  }

  async decrypt({ cipherText, privateKey }) {
    this.increaseCounter()
    return decryptString(cipherText, privateKey)
  }
}

class DecryptionQueue {
  constructor() {
    this.queue = []
    this.decryptor = new Decryptor()

    this.push = this.push.bind(this)
    this.tryNext = this.tryNext.bind(this)
  }

  push(jobId, payload) {
    const queueItem = async () => {
      await this.decryptor
        .decrypt(payload)
        .then((plainText) => {
          worker.postMessage({ jobId, plainText })
        })
        .catch((error) => {
          worker.postMessage({ jobId, error })
        })
    }

    this.queue.push(queueItem)
    setTimeout(this.tryNext)
  }

  tryNext() {
    if (this.queue.length === 0) {
      return
    }

    const queueItem = this.queue.shift()

    queueItem()

    if (this.queue.length > 0) {
      setTimeout(this.tryNext)
    }
  }
}

const decryptionQueue = new DecryptionQueue()

worker.onmessage = function (event) {
  const { jobId, cipherText, privateKey } = event.data

  decryptionQueue.push(jobId, { cipherText, privateKey })
}
