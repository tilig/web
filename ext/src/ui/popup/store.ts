import { defineStore } from 'pinia'

import { Brand } from '@core/clients/tilig/brands'
import { ItemState, filterItemsByItemType, ItemType, filterItems } from '@core/stores/items'
import { parseDomain } from '@core/utils/domainUtils'
import {
  Contact,
  DecryptedDetails,
  DecryptedItem,
  DecryptedOverview,
  Template,
  UserProfile,
} from '@core/clients/tilig'
import { createLogger } from '@core/utils/logger'

import { ContextMessage } from '@/content-scripts/classes/ContentScriptContext'
import { queryActiveTab } from '@/utils/tabUtils'
import { requestWebRequestPermissions } from '@/utils/requestPermissions'
import {
  fetchItems as fetchItems,
  updateItem as updateItemRequest,
  createItem as createItemRequest,
  deleteItem as deleteItemRequest,
  getItem as getDecryptedItem,
  createShare as createShareRequest,
  deleteShare as deleteShareRequest,
  getFolderMemberPublicKey as getFolderMemberPublicKeyRequest,
  addFolderMembership as addFolderMembershipRequest,
  revokeFolderMembership as revokeFolderMembershipRequest,
  refreshItem,
} from '@/background/messageApis/accounts'
import {
  getCurrentUser,
  logoutUser,
  refreshUserProfile as refreshUserProfileMsg,
} from '@/background/messageApis/auth'
import { fetchActiveTabBrand } from '@/background/messageApis/getBrand'
import { getSettings, setAutosaveLogin, setGifsEnabled } from '@/background/messageApis/settings'
import { SettingsState } from '@/background/stores/settings'
import { fetchContacts as fetchContactsRequest } from '@/background/messageApis/contacts'

const logger = createLogger('popup:store')

const ITEM_LIMIT = 1000

const SORT_PLACEHOLDER = 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'

// Used by loadmore to quickly update the list and not include duplicates
const updateAccountList = (list: ItemState[], updates: ItemState[]) => {
  const existingIds = list.map((acc) => acc.item.id)
  updates.forEach((acc) => {
    const existingIdx = existingIds.indexOf(acc.item.id)
    if (existingIdx >= 0) {
      list[existingIdx] = acc
    } else {
      list.push(acc)
    }
  })
  list.sort((a, b) => {
    const aName = a.overview?.name?.toLocaleLowerCase() || SORT_PLACEHOLDER
    const bName = b.overview?.name?.toLocaleLowerCase() || SORT_PLACEHOLDER
    return aName.localeCompare(bName)
  })
}

interface PopupState {
  activeTab: chrome.tabs.Tab | null | undefined
  activeItemType: ItemType | null
  currentItem: DecryptedItem | null
  currentContext: ContextMessage | null | undefined
  brand: Brand | null
  itemStates: ItemState[]
  totalItemStates: number
  tabItemStates: ItemState[]
  searchItemStates: ItemState[]
  searchString: string
  searchLoading: boolean
  user: UserProfile | null
  userLoaded: boolean
  settings: SettingsState | null
  error: string | null
  // Performance hack, limit the loading of items until the user requests more
  offset: number
  limit: number
  itemStatesLoading: boolean
  itemStatesLoaded: boolean
  isCreatingShare: boolean
  isDeleting: boolean
  isUpdating: boolean
  isDeletingShare: boolean
  isAddingFolderMembership: boolean
  isRevokingFolderMembership: boolean
  publicKeyFetchError: 'own-email' | null
  contacts: Contact[]
  itemsPageScrollPosition: number
}

// IFrame UI components can't persist anything due to 3rd
// party cookie rules. Stores need to sync with the backend.js
// and manage state via UI specific stores
export const usePopupStore = defineStore('PopupStore', {
  state: (): PopupState => ({
    itemStates: [] as ItemState[],
    activeItemType: null,
    currentItem: null,
    currentContext: null,
    totalItemStates: 0,
    tabItemStates: [] as ItemState[],
    searchItemStates: [] as ItemState[],
    searchString: '',
    searchLoading: false,
    user: null,
    userLoaded: false,
    settings: null,
    activeTab: undefined,
    brand: null,
    error: null,
    limit: ITEM_LIMIT,
    offset: 0,
    isUpdating: false,
    isDeleting: false,
    itemStatesLoading: false,
    itemStatesLoaded: false,
    isCreatingShare: false,
    isDeletingShare: false,
    isAddingFolderMembership: false,
    isRevokingFolderMembership: false,
    publicKeyFetchError: null,
    contacts: [],
    itemsPageScrollPosition: 0,
  }),
  actions: {
    async onItemStatesUpdated() {
      return Promise.all([
        this.fetchItemStates(),
        this.fetchActiveTabItemStates(),
        this.setSearchString(this.searchString),
      ])
    },
    loadMore() {
      this.offset = this.offset + this.limit
      fetchItems({
        limit: this.limit,
        offset: this.offset,
        itemType: this.activeItemType,
      }).then((accountList) => {
        if (accountList && accountList.accounts) {
          updateAccountList(this.itemStates, accountList.accounts)
        }
      })
    },
    async fetchItemStates(forceUpdate = false) {
      this.offset = 0
      logger.debug('activeItemType', this.activeItemType)
      return fetchItems({
        forceUpdate,
        limit: this.limit,
        itemType: this.activeItemType,
      }).then((itemStateList) => {
        logger.debug('item state', itemStateList)
        if (itemStateList && itemStateList.accounts) {
          this.itemStates = itemStateList.accounts
          this.totalItemStates = itemStateList.totalAvailable
        }
      })
    },
    async fetchActiveTabItemStates() {
      return queryActiveTab()
        .then((tab) => {
          this.$patch({
            activeTab: tab,
          })
          const domain = tab && tab.url ? parseDomain(tab.url) || null : null
          if (domain) {
            fetchItems({ onlyCurrentTab: true }).then((itemStateList) => {
              if (itemStateList && itemStateList.accounts)
                this.tabItemStates = itemStateList.accounts
            })
          }

          // We don't know if we'll have a Brand, so fill the credential in with basic info
          this.$patch({
            activeTab: tab,
          })
        })
        .catch((err) => {
          this.$patch({
            error: `${err}`,
          })
        })
    },
    sync() {
      fetchActiveTabBrand().then((brand) => {
        this.$patch({
          brand,
        })
      })

      getCurrentUser().then((user) => {
        logger.debug('Fetched current user', user)
        this.user = user || null
        this.userLoaded = true
      })

      getSettings().then((settings) => {
        this.settings = settings
      })

      this.itemStatesLoading = true
      Promise.all([
        this.fetchItemStates(false).then(() => {
          // Quickly fetch the latest, then force an update but don't wait on it
          this.fetchItemStates(true)
        }),
        this.fetchActiveTabItemStates(),
      ]).then(() => {
        this.itemStatesLoaded = true
        this.itemStatesLoading = false
      })
    },
    async setSearchString(searchString: string) {
      this.searchString = searchString
      this.searchLoading = true
      if (searchString.length == 0) {
        this.searchItemStates = []
        this.searchLoading = false
        return
      }
      return fetchItems({ searchString }).then((itemStateList) => {
        if (itemStateList.accounts)
          this.searchItemStates = filterItems(itemStateList.accounts, {
            itemType: this.activeItemType,
          })
        this.searchLoading = false
      })
    },

    getItemState(id: string): ItemState | null {
      const item =
        this.itemStates.find((i) => i.item.id === id) ||
        this.tabItemStates.find((i) => i.item.id === id) ||
        this.searchItemStates.find((i) => i.item.id === id) ||
        null
      return item
    },

    updateItemState(decryptedItem: DecryptedItem): void {
      const { share, item, overview } = decryptedItem
      const itemState = { item, overview, share }
      const itemIdx = this.itemStates.findIndex((i) => i.item.id === decryptedItem.item.id)
      if (itemIdx >= 0) {
        this.itemStates[itemIdx] = itemState
      } else {
        this.itemStates.push(itemState)
      }
      // Store the most recent decrypted details for quick loads
      this.currentItem = decryptedItem
    },

    deleteItemState(id: string): void {
      const itemIdx = this.itemStates.findIndex((i) => i.item.id === id)
      if (itemIdx >= 0) {
        delete this.itemStates[itemIdx]
      }
      this.currentItem = null
    },

    async createItem(
      template: Template,
      overview: DecryptedOverview,
      details: DecryptedDetails
    ): Promise<string | null> {
      const id = await createItemRequest(template, overview, details)
      if (id) {
        const itemState = await getDecryptedItem(id)
        this.updateItemState(itemState)
        this.onItemStatesUpdated()
        return id
      }
      return null
    },

    async readItem(id: string): Promise<DecryptedItem | null> {
      if (this.currentItem && this.currentItem.item.id === id) return this.currentItem
      this.currentItem = null

      const decryptedItem = await getDecryptedItem(id)
      if (!decryptedItem) {
        logger.error('Unable to retrieve item id: ', id)
        return null
      }
      this.updateItemState(decryptedItem)
      this.currentItem = decryptedItem
      return decryptedItem
    },

    async updateItem(
      id: string,
      overview: DecryptedOverview,
      details: DecryptedDetails
    ): Promise<boolean> {
      this.isUpdating = true
      if (await updateItemRequest(id, overview, details)) {
        const decryptedItem = await getDecryptedItem(id)
        this.updateItemState(decryptedItem)
        this.onItemStatesUpdated()
        this.isUpdating = false
        return true
      }
      this.isUpdating = false
      return false
    },

    async deleteItem(id: string): Promise<void> {
      this.isDeleting = true
      await deleteItemRequest(id)
      this.deleteItemState(id)
      // Pause until we have updated the account list after a deletion
      await this.onItemStatesUpdated()
      this.isDeleting = false
      return
    },

    async toggleAutosaveLogin(): Promise<void> {
      const enabledAutosave = !this.settings?.autosaveLogin

      // This must be requested in the users UI interaction
      if (enabledAutosave) await requestWebRequestPermissions()
      this.settings = await setAutosaveLogin(enabledAutosave)
    },

    async toggleGifs(): Promise<void> {
      const enable = !this.settings?.gifsEnabled
      this.settings = await setGifsEnabled(enable)
    },

    async refreshLastDecryptedItem() {
      const itemId = this.currentItem?.item.id
      if (!itemId) {
        logger.error('Cannot refresh an item that does not exist')
        return
      }

      await refreshItem(itemId)
      const decryptedItem = await getDecryptedItem(itemId)
      this.updateItemState(decryptedItem)
      this.onItemStatesUpdated()
    },

    async createShare(itemId: string): Promise<boolean> {
      if (!itemId) return false
      const itemState = this.getItemState(itemId)
      if (!itemState) return false

      this.isCreatingShare = true
      const { item } = itemState

      try {
        const shareLink = await createShareRequest(item)
        if (shareLink) {
          await this.refreshLastDecryptedItem()

          return true
        }
        return false
      } catch (e) {
        logger.error('Error creating share %O', e)
        return false
      } finally {
        this.isCreatingShare = false
      }
    },

    async deleteShare(itemId: string): Promise<boolean> {
      if (!itemId) return false
      const itemState = this.getItemState(itemId)
      if (!itemState) return false

      this.isDeletingShare = true
      try {
        await deleteShareRequest(itemId)
        await this.refreshLastDecryptedItem()

        return true
      } catch (e) {
        logger.error('Error creating share %O', e)
        return false
      } finally {
        this.isDeletingShare = false
      }
    },

    async getFolderMemberPublicKey(email: string) {
      email = email.toLocaleLowerCase().trim()
      if (!email) return null

      if (email === this.user?.email) {
        this.publicKeyFetchError = 'own-email'
        return null
      }

      try {
        const publicKey = await getFolderMemberPublicKeyRequest(email)

        if (publicKey) {
          this.publicKeyFetchError = null
        }
        return publicKey
      } catch (e) {
        logger.error('Error fetching public key %O', e)
        return null
      }
    },

    async addFolderMembership(id: string, email: string) {
      if (!email) return false
      this.isAddingFolderMembership = true

      const itemState = this.getItemState(id)
      if (!itemState) return false

      try {
        const publicKey = await this.getFolderMemberPublicKey(email)

        if (publicKey) {
          await addFolderMembershipRequest(id, publicKey)
          await this.refreshLastDecryptedItem()

          return true
        }
        return false
      } catch (e) {
        logger.error('Error adding membership %O', e)
        return false
      } finally {
        this.isAddingFolderMembership = false
      }
    },

    async revokeFolderMembership(id: string, memberId: string) {
      if (!memberId) return false
      const itemState = this.getItemState(id)
      if (!itemState || !itemState.item.folder) return false

      this.isRevokingFolderMembership = true

      try {
        await revokeFolderMembershipRequest(itemState.item.folder.id, memberId)
        await this.refreshLastDecryptedItem()
      } catch (e) {
        logger.error('Error revoking folder membership %O', e)
        return null
      } finally {
        this.isRevokingFolderMembership = false
      }
    },

    async fetchContacts() {
      try {
        const contactsList = await fetchContactsRequest()
        this.contacts = contactsList ?? []
        return this.contacts
      } catch (error) {
        logger.error('Error fetching contacts', error)
        return null
      }
    },

    async refreshUserProfile() {
      // This refreshes the auth store
      await refreshUserProfileMsg()
      // Updat the user on this store
      this.user = await getCurrentUser()
      logger.debug(`this.user`, this.user)
    },

    logout(): void {
      logoutUser()
      this.user = null
    },
  },

  getters: {
    loggedIn: (state) => !!state.user,
    /**
     * Returns true if the user has any custom items
     */
    hasCustomItems(state) {
      return !!filterItemsByItemType(state.itemStates, ItemType.Custom).length
    },
    /**
     * Items filtered on the current query filter
     */
    filteredItems(state) {
      return filterItems(state.itemStates, {
        query: state.searchString,
        itemType: state.activeItemType,
      })
    },
    /**
     * Search Items filtered using the current query filter
     */
    filteredSearchItems(state) {
      return filterItems(state.searchItemStates, {
        query: state.searchString,
        itemType: state.activeItemType,
      })
    },
  },
})
