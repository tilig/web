import { createPinia } from 'pinia'
import { afterEach, beforeEach, describe, expect, it, vi } from 'vitest'
import { flushPromises } from '@vue/test-utils'
import { fetchItems } from '@/background/messageApis/accounts'
import { setAutosaveLogin } from '@/background/messageApis/settings'
import { usePopupStore } from '../store'

const store = createPinia()

vi.mock('../../../background/messageApis/accounts', () => ({
  fetchItems: vi.fn(),
}))

afterEach(() => {
  vi.clearAllMocks()
})

beforeEach(() => {
  const popupStore = usePopupStore(store)
  popupStore.$reset()
})

describe('Basic basic popup store usage', () => {
  it('updating the search string should fetch new accounts', async () => {
    const popupStore = usePopupStore(store)
    vi.mocked(fetchItems).mockResolvedValueOnce({ accounts: [], totalAvailable: 0 })
    popupStore.setSearchString('foo')
    await flushPromises()
    expect(popupStore.searchLoading).not.toEqual(true)
    expect(popupStore.searchItemStates?.length).toBe(0)
  })
})

vi.mock('../../../background/messageApis/settings', () => ({
  setAutosaveLogin: vi.fn(),
}))

describe('Settings in the Popup Store', () => {
  const chromeStub = {
    webRequest: undefined,
    permissions: {
      request: vi.fn(),
    },
  } as unknown as typeof chrome

  beforeEach(() => {
    vi.stubGlobal('chrome', chromeStub)
    vi.clearAllMocks()
  })

  it('should try and get webrequest permissions when it is toggled on', async () => {
    vi.mocked(chromeStub.permissions.request).mockImplementation((_permissions, callback) => {
      if (callback) callback(true)
    })
    vi.mocked(setAutosaveLogin).mockResolvedValueOnce({ autosaveLogin: true, gifsEnabled: false })

    const popupStore = usePopupStore(store)
    await popupStore.toggleAutosaveLogin()
    await flushPromises()
    expect(chromeStub.permissions.request).toHaveBeenCalled()
    expect(popupStore.settings?.autosaveLogin).toEqual(true)
  })

  it('should not try and get webrequest permissions when they already exist', async () => {
    // We only look and see if it exists, it never get exercised here, so we just need to fake
    // that it's available
    chromeStub.webRequest = {} as unknown as typeof chrome.webRequest
    vi.mocked(setAutosaveLogin).mockResolvedValueOnce({ autosaveLogin: false, gifsEnabled: false })

    const popupStore = usePopupStore(store)
    await popupStore.toggleAutosaveLogin()
    await flushPromises()
    expect(chromeStub.permissions.request).not.toHaveBeenCalled()
    expect(popupStore.settings?.autosaveLogin).toEqual(false)
  })
})
