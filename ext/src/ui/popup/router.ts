import { createRouter, createWebHashHistory } from 'vue-router'

import { ItemType } from '@core/stores/items'

import Item from './pages/Item.vue'
import EditItem from './pages/EditItem.vue'
import CreateItem from './pages/CreateItem.vue'
import Items from './pages/Items.vue'
import Settings from './pages/Settings.vue'
import Folders from './pages/Folders.vue'
import PasswordGenerator from './pages/PasswordGenerator.vue'
import Tools from './pages/Tools.vue'

const supportedItemTemplateRegex = [
  ItemType.Logins,
  ItemType.SecureNotes,
  ItemType.WiFi,
  ItemType.CreditCards,
  ItemType.Custom,
].join('|')

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: `/:type(${supportedItemTemplateRegex})?`, name: 'secrets', component: Items },
    { path: '/secrets/:id', name: 'secret', component: Item, props: true },
    { path: '/secrets/:id/edit', name: 'edit-secret', component: EditItem, props: true },
    { path: '/secrets/new', name: 'new-secret', component: CreateItem },
    { path: '/settings', name: 'settings', component: Settings },
    { path: '/folders', name: 'folders', component: Folders },
    { path: '/generator', name: 'generator', component: PasswordGenerator },
    { path: '/tools', name: 'tools', component: Tools },
    { path: '/:anyRoute(.*)', redirect: '/' },
  ],
})

export default router
