import { createApp } from 'vue'
import { createPinia } from 'pinia'

import { sentrySetupVue } from '@/utils/captureException'

import '@/index.css'

import Popup from './Popup.vue'
import router from './router'

const app = createApp(Popup)

sentrySetupVue({ context: 'popup' }, { app, router })

const pinia = createPinia()

app.use(router).use(pinia).mount('#app')
