import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import { createPinia } from 'pinia'

import '@/index.css'

import Dropdown from './Dropdown.vue'
import List from './pages/List.vue'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [{ name: 'list', path: '/', component: List }],
})

const pinia = createPinia()

const app = createApp(Dropdown)

app.use(router).use(pinia).mount('#app')
