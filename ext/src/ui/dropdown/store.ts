import { defineStore } from 'pinia'
import { ItemState } from '@core/stores/items'
import { fetchItems } from '@/background/messageApis/accounts'
import { CurrentUserResult, getCurrentUser } from '@/background/messageApis/auth'

interface DropdownState {
  accounts: ItemState[] | null
  currentUser: CurrentUserResult | false
  error: string | null
}

// IFrame UI components can't persist anything due to 3rd
// party cookie rules. Stores need to sync with the backend.js
// and manage state via UI specific stores
export const useDropdownStore = defineStore('dropdownState', {
  state: () =>
    ({
      currentUser: false,
      error: null,
      accounts: null,
    } as DropdownState),
  actions: {
    sync() {
      fetchItems({ onlyCurrentTab: true }).then(({ accounts }) => {
        this.accounts = accounts
      })
      getCurrentUser().then((currentUser) => {
        this.currentUser = currentUser
      })
    },
  },
  getters: {
    loggedIn(): boolean {
      return !!this.currentUser
    },
    synced(): boolean {
      return this.accounts !== null && this.currentUser !== false
    },
  },
})
