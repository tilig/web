import { defineStore } from 'pinia'

import { Brand } from '@core/clients/tilig/brands'
import { DecryptedItem, initializeItem } from '@core/clients/tilig'
import { useLoginForm } from '@core/composables/forms'

import { getParentTab } from '@/background/messageApis/tab'
import { fetchDomainState } from '@/background/messageApis/draftAccounts'
import { fetchActiveTabBrand } from '@/background/messageApis/getBrand'
import { parseDomain } from '@core/utils/domainUtils'
import { titleToName } from '@/utils/titleToName'
import { ref } from 'vue'
import { ItemState } from '@core/stores/items'
import { fetchItems, getItem } from '@/background/messageApis/accounts'
import { cloneDeep } from 'lodash'
import { DraftData } from '@/background/stores/draft'

type SaveModalState = {
  synced: boolean
  newItem: DecryptedItem
  currentItem: DecryptedItem
  newPassword: string
  existingItems: ItemState[]
  matchingItem: ItemState | null
  brand: Brand | null
  error: string | null
}

const determineBestMatch = (
  domainState: DraftData | null,
  existingItems: ItemState[]
): ItemState | null => {
  const username = domainState?.username || ''
  const matchingItem = existingItems.find((item) => {
    return item.overview?.info == username
  })
  if (matchingItem) {
    return matchingItem
  } else if (existingItems.length && !username) {
    // If we didn't provide a username, assume it's an update if there's existing items
    return existingItems[0]
  }
  return null
}

// IFrame UI components can't persist anything due to 3rd
// party cookie rules. Stores need to sync with the backend.js
// and manage state via UI specific stores
export const useCredentialsStore = defineStore('updateCredentialsStore', {
  state: () => {
    const newItem = initializeItem()
    const currentItem = initializeItem()
    return {
      synced: false,
      brand: null,
      newItem,
      currentItem,
      newPassword: '',
      existingItems: [],
      matchingItem: null,
      error: null,
    } as SaveModalState
  },
  getters: {
    isNewItem: (state) => !state.currentItem.item.id,
  },
  actions: {
    sync() {
      // These are both quick actions and allow us to render quickly
      Promise.all([getParentTab(), fetchDomainState(), fetchItems({ onlyCurrentTab: true })])
        .then(([tab, domainState, existingItems]) => {
          const newItem = ref(cloneDeep(this.$state.currentItem))
          const { username, password, name, website } = useLoginForm(newItem)
          const domain = tab?.url ? parseDomain(tab?.url) : null
          name.value = titleToName(tab?.url || '', tab?.title || '') || 'Untitled'
          website.value = domain || ''
          username.value = ''
          password.value = ''
          if (domainState) {
            username.value = domainState.username || ''
            password.value = domainState.password || ''
          }
          const bestMatch = determineBestMatch(domainState, existingItems.accounts)
          this.$patch({
            newItem: newItem.value,
            currentItem: newItem.value,
            newPassword: password.value,
            synced: true,
            existingItems: existingItems.accounts,
            matchingItem: bestMatch,
          })
          if (bestMatch) {
            this.changeCurrentSelection(bestMatch.item.id)
          }
        })
        .catch((err) => {
          this.$patch({
            error: `${err}`,
          })
        })

      // This may return slowly depending on network, so avoid relying on it to render
      fetchActiveTabBrand()
        .then((brand) => {
          const newItem = this.$state.currentItem
          newItem.overview.name = this.$state.currentItem.overview.name || brand?.name || 'Untitled'
          this.$patch({
            currentItem: newItem,
            brand,
          })
        })
        .catch((err) => {
          this.$patch({
            error: `${err}`,
          })
        })
    },
    async updatePassword(password: string | null) {
      this.$state.newPassword = password || ''
    },
    async changeCurrentSelection(id: string | null) {
      if (id) {
        const newSelection = await getItem(id)
        const { password } = useLoginForm(ref(newSelection))
        password.value = this.$state.newPassword
        this.$state.currentItem = cloneDeep(newSelection)
      } else {
        this.$state.currentItem = cloneDeep(this.$state.newItem)
      }
    },
  },
})
