import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import { createPinia } from 'pinia'

import '@/index.css'
import './index.css'

import Credentials from './Credentials.vue'
import ModalPage from './pages/ModalPage.vue'
import { sentrySetupVue } from '@/utils/captureException'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [{ name: 'modal', path: '/', component: ModalPage }],
})

const pinia = createPinia()

const app = createApp(Credentials)
sentrySetupVue({ context: 'credentials' }, { app, router })

app.use(router).use(pinia).mount('#app')
