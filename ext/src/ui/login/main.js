import { createApp } from 'vue'
import { createPinia } from 'pinia'

import '@/index.css'
const pinia = createPinia()

import LogIn from './LogIn.vue'
import { sentrySetupVue } from '@/utils/captureException'

const app = createApp(LogIn)

sentrySetupVue({ context: 'login' }, { app })

app.use(pinia).mount('#app')
