import { createApp } from 'vue'
import { createPinia } from 'pinia'

import { sentrySetupVue } from '@/utils/captureException'

import '@/index.css'
import Welcome from './Welcome.vue'
import router from './router'

const pinia = createPinia()

const app = createApp(Welcome)

sentrySetupVue({ context: 'welcome' }, { app })
app.use(router).use(pinia).mount('#app')
