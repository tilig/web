import { createRouter, createWebHashHistory } from 'vue-router'

import OnboardingPinExtension from '../pages/OnboardingPinExtension.vue'
import OnboardingPermission from '../pages/OnboardingPermission.vue'
import OnboardingDone from '../pages/OnboardingDone.vue'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      component: OnboardingPinExtension,
      name: 'onboarding-step-pin',
    },
    {
      path: '/welcome/permission',
      component: OnboardingPermission,
      name: 'onboarding-step-permission',
    },
    {
      path: '/welcome/done',
      component: OnboardingDone,
      name: 'onboarding-step-done',
    },
  ],
})

export default router
