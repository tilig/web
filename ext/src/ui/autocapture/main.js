import { createApp } from 'vue'
import { createPinia } from 'pinia'

import '@/index.css'
import './index.css'

import Autocapture from './Autocapture.vue'

const pinia = createPinia()

const app = createApp(Autocapture)

app.use(pinia).mount('#app')
