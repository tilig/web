import { defineStore } from 'pinia'
import { DecryptedItem } from '@core/clients/tilig'
import { AutocapturedItem } from '@/background/stores/autocapture'
import { getAutocaptureItem } from '@/background/messageApis/autocapture'
import { getItem } from '@/background/messageApis/accounts'

type SaveModalState = {
  loaded: boolean
  autocaptureItem: AutocapturedItem | null
  item: DecryptedItem | null
  isNewItem: boolean
  error: string | null
}

// Fetch the latest autocaptured item for this iframe's parent tab, if it exists
export const useAutocaptureModalStore = defineStore('useAutocaptureModalStore', {
  state: () =>
    ({
      loaded: false,
      autocaptureItem: null,
      item: null,
      error: null,
      isNewItem: true,
    } as SaveModalState),
  actions: {
    async load() {
      // These are both quick actions and allow us to render quickly
      try {
        const autocaptureItem = await getAutocaptureItem()
        this.autocaptureItem = autocaptureItem
        if (autocaptureItem && autocaptureItem.id) {
          this.item = await getItem(autocaptureItem.id)
          if (this.item.details.history && this.item.details.history.length > 0) {
            this.isNewItem = false
          }
        }
        this.loaded = true
      } catch (err) {
        this.$patch({
          error: `${err}`,
        })
      }
    },
  },
})
