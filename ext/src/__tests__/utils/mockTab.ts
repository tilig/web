/** Eventually we should just move to a Pick<chrome.tabs.Tab> type with a limited subset of
 *  fields, but for now just mock it out. TODO: @mdp
 * */

export const createMockTabForUrl = (url: string): chrome.tabs.Tab => {
  return {
    /**
     * Either loading or complete.
     */
    status: 'complete',
    /** The zero-based index of the tab within its window. */
    index: 0,
    /**
     * Optional.
     * The ID of the tab that opened this tab, if any. This property is only present if the opener tab still exists.
     * @since Chrome 18.
     */
    openerTabId: undefined,
    /**
     * Optional.
     * The title of the tab. This property is only present if the extension's manifest includes the "tabs" permission.
     */
    title: 'My Mock Tab title',
    /**
     * Optional.
     * The URL the tab is displaying. This property is only present if the extension's manifest includes the "tabs" permission.
     */
    url,
    /**
     * The URL the tab is navigating to, before it has committed.
     * This property is only present if the extension's manifest includes the "tabs" permission and there is a pending navigation.
     * @since Chrome 79.
     */
    pendingUrl: undefined,
    /**
     * Whether the tab is pinned.
     * @since Chrome 9.
     */
    pinned: false,
    /**
     * Whether the tab is highlighted.
     * @since Chrome 16.
     */
    highlighted: false,
    /** The ID of the window the tab is contained within. */
    windowId: 0,
    /**
     * Whether the tab is active in its window. (Does not necessarily mean the window is focused.)
     * @since Chrome 16.
     */
    active: true,
    /**
     * Optional.
     * The URL of the tab's favicon. This property is only present if the extension's manifest includes the "tabs" permission. It may also be an empty string if the tab is loading.
     */
    favIconUrl: 'favicon.ico',
    /**
     * Optional.
     * The ID of the tab. Tab IDs are unique within a browser session. Under some circumstances a Tab may not be assigned an ID, for example when querying foreign tabs using the sessions API, in which case a session ID may be present. Tab ID can also be set to chrome.tabs.TAB_ID_NONE for apps and devtools windows.
     */
    id: 1,
    /** Whether the tab is in an incognito window. */
    incognito: false,
    /**
     * Whether the tab is selected.
     * @deprecated since Chrome 33. Please use tabs.Tab.highlighted.
     */
    selected: false,
    /**
     * Whether the tab is discarded. A discarded tab is one whose content has been unloaded from memory, but is still visible in the tab strip. Its content gets reloaded the next time it's activated.
     * @since Chrome 54.
     */
    discarded: false,
    /**
     * Whether the tab can be discarded automatically by the browser when resources are low.
     * @since Chrome 54.
     */
    autoDiscardable: false,
    /**
     * The ID of the group that the tab belongs to.
     * @since Chrome 88
     */
    groupId: 99,
  }
}
