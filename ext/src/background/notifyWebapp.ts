import { closeWebstore } from '@/utils/closeWebstore'
import { openExtensionTab } from '@/utils/tabUtils'
import { executeScript, findTiligTab } from '@/utils/webApp'

export const notifyWebapp = async () => {
  /**
   * If the user installed the extension as part of the the onboarding flow
   * in the webapp, we want to notify them but continue the onboarding in the extension.
   * If that's not the case, we open the extension's welcome page.
   */
  const tiligTab = await findTiligTab()

  if (tiligTab) {
    // We no longer want to activate the tab since the setup continues in the extension now
    chrome.tabs.update(tiligTab.id, { active: false })

    // Send a message to the webapp that the extension has been installed
    await executeScript(tiligTab.id, `window.postMessage({ extensionInstalled: true });`)

    // Set the same meta tag that the content-script also sets on page load
    await executeScript(
      tiligTab.id,
      `const extensionIdentifier = document.createElement("meta");
      extensionIdentifier.setAttribute("name", "tilig:extension");
      extensionIdentifier.setAttribute("content", "installed");
      document.head.appendChild(extensionIdentifier);`
    )
  }

  openExtensionTab('/src/ui/welcome/index.html').then(closeWebstore)
}
