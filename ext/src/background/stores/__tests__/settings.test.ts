import { createPinia } from 'pinia'
import { createApp } from 'vue'
import { beforeEach, describe, expect, it, vi } from 'vitest'
import { flushPromises } from '@vue/test-utils'
import { useSettingsStore } from '../settings'

const settingsLocalStorageFixture = {
  autosaveLogin: true,
}

describe('Settings store', () => {
  const noWebRequestChromeStub = {
    webRequest: undefined,
    permissions: {
      request: vi.fn(),
    },
  } as unknown as typeof chrome

  const webRequestChromeStub = {
    webRequest: {},
    permissions: {
      request: vi.fn(),
    },
  } as unknown as typeof chrome

  beforeEach(() => {
    localStorage.clear()
    vi.clearAllMocks()

    const app = createApp({})
    const pinia = createPinia()
    app.use(pinia)
  })

  it('should allow for enabling autosaveLogin', async () => {
    vi.stubGlobal('chrome', webRequestChromeStub)
    vi.mocked(webRequestChromeStub.permissions.request).mockImplementation(
      (_permissions, callback) => {
        if (callback) callback(true)
      }
    )
    const settingStore = useSettingsStore()
    settingStore.$reset()

    expect(settingStore.autosaveLogin).toBe(false)
    settingStore.setAutosaveLogin(true)
    expect(settingStore.autosaveLogin).toBe(true)
  })

  it('should compute autosaveLogin == false without webRequest', async () => {
    vi.stubGlobal('chrome', noWebRequestChromeStub)
    const settingStore = useSettingsStore()
    settingStore.$reset()

    expect(settingStore.autosaveLogin).toBe(false)
    settingStore.setAutosaveLogin(true)

    // We should still save the intended state, but mark it false if we don't have permissions
    expect(settingStore.state.autosaveLogin).toBe(true)
    expect(settingStore.autosaveLogin).toBe(false)
  })

  it('should use localStorage', async () => {
    vi.stubGlobal('chrome', webRequestChromeStub)
    localStorage.setItem('settings', JSON.stringify(settingsLocalStorageFixture))

    const settingStore = useSettingsStore()
    expect(settingStore.autosaveLogin).toBe(true)
    settingStore.setAutosaveLogin(false)
    expect(settingStore.autosaveLogin).toBe(false)

    // Ensure Pinia is done with the udpate and it's subscribed actions
    await flushPromises()
    expect(JSON.parse(localStorage.getItem('settings') || '{}')).toEqual({ autosaveLogin: false })
  })
})
