import { createPinia } from 'pinia'
import { createMockTabForUrl } from '@/__tests__/utils/mockTab'
import { afterAll, vi, beforeEach, it, describe, expect } from 'vitest'
import { DRAFT_DATA_MAX_RECORDS, DRAFT_DATA_TIMEOUT, useDraftStore } from '../draft'

const store = createPinia()

afterAll(() => {
  vi.clearAllMocks()
})

// TODO: @mdp - Instead of testing individual message handlers here
// we should test the MessageBackgroundListener as it's own utility
// and test message handlers on their own
describe('Basic draft store behaviour', () => {
  beforeEach(() => {
    const draftStore = useDraftStore(store)
    draftStore.reset()
  })
  it('should return a draft for a domain', () => {
    const mockTab = createMockTabForUrl('https://example.com')
    const draftStore = useDraftStore(store)
    draftStore.update({
      data: { username: 'mark' },
      tab: mockTab,
    })
    const draft = draftStore.byTabDomain(mockTab)
    expect(draft?.username).toEqual('mark')
  })
  it('should not return a draft that has expired', () => {
    const now = Date.now()
    vi.spyOn(Date, 'now').mockReturnValueOnce(now)
    const mockTab = createMockTabForUrl('https://example.com')
    const draftStore = useDraftStore(store)
    draftStore.update({
      data: { username: 'mark' },
      tab: mockTab,
    })
    vi.spyOn(Date, 'now').mockReturnValueOnce(now + DRAFT_DATA_TIMEOUT + 1)
    const draft = draftStore.byTabDomain(mockTab)
    expect(draft).toEqual(null)
  })
  it('should keep tabs seperate', () => {
    const draftStore = useDraftStore(store)
    const mockTab = createMockTabForUrl('https://example.com')
    draftStore.update({
      data: { username: 'mark' },
      tab: mockTab,
    })
    expect(draftStore.$state.byDomain.size).toEqual(1)
    mockTab.id = 999
    draftStore.update({
      data: { username: 'anotheruser' },
      tab: mockTab,
    })
    expect(draftStore.$state.byDomain.size).toEqual(2)
  })
  it('should purge expired draft', () => {
    const now = Date.now()
    const draftStore = useDraftStore(store)
    vi.spyOn(Date, 'now').mockReturnValue(now)

    for (let i = 0; i < 20; i++) {
      const mockTab = createMockTabForUrl(`https://example${i}.com`)
      draftStore.update({
        data: { username: 'mark' },
        tab: mockTab,
      })
    }
    expect(draftStore.$state.byDomain.size).toEqual(20)
    vi.spyOn(Date, 'now').mockReturnValue(now + DRAFT_DATA_TIMEOUT + 1)
    draftStore.purge()
    expect(draftStore.$state.byDomain.size).toEqual(0)
  })
  it('should limit how many items can be stored and purge oldest items', () => {
    const draftStore = useDraftStore(store)
    for (let i = 0; i < DRAFT_DATA_MAX_RECORDS + 10; i++) {
      const mockTab = createMockTabForUrl(`https://example${i}.com`)
      draftStore.update({
        data: { username: 'mark' },
        tab: mockTab,
      })
    }
    expect(draftStore.$state.byDomain.size).toEqual(DRAFT_DATA_MAX_RECORDS)
    let mockTab = createMockTabForUrl(`https://example0.com`)
    expect(draftStore.byTabDomain(mockTab)).toBe(null)
    mockTab = createMockTabForUrl(`https://example20.com`)
    expect(draftStore.byTabDomain(mockTab)).not.toBe(null)
  })
})
