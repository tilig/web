import { createPinia } from 'pinia'
import { createApp } from 'vue'
import { flushPromises } from '@vue/test-utils'
import { useEngagementStore } from '../engagement'
import { sendGoogleAnalyticsEvent } from '@core/clients/analytics'

const engagementLocalStorageFixture = {
  autofills: 2,
}

vi.mock('@core/clients/analytics', () => {
  return {
    sendGoogleAnalyticsEvent: vi.fn(),
  }
})

describe('Engagement store', () => {
  beforeEach(() => {
    localStorage.clear()
    vi.clearAllMocks()

    const app = createApp({})
    const pinia = createPinia()
    app.use(pinia)
  })

  it('should track the first login only', async () => {
    const engagementStore = useEngagementStore()
    engagementStore.$reset()
    await engagementStore.trackLogin()
    expect(sendGoogleAnalyticsEvent).toHaveBeenCalledTimes(1)
    await engagementStore.trackLogin()
    expect(sendGoogleAnalyticsEvent).toHaveBeenCalledTimes(1)
  })

  it('should use localStorage', async () => {
    localStorage.setItem('engagement', JSON.stringify(engagementLocalStorageFixture))

    const engagementStore = useEngagementStore()
    expect(engagementStore.state.autofills).toBe(2)
    await engagementStore.trackAutofillEvent()
    expect(engagementStore.state.autofills).toBe(3)

    // Ensure Pinia is done with the udpate and it's subscribed actions
    await flushPromises()
    expect(JSON.parse(localStorage.getItem('engagement') || '{}').autofills).toEqual(3)
  })
})
