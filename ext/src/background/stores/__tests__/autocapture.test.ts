import { createPinia } from 'pinia'

import { AUTOCAPTURE_TIMEOUT, useAutocaptureStore } from '@/background/stores/autocapture'
import { createMockTabForUrl } from '@/__tests__/utils/mockTab'
import { afterAll, describe, expect, it, vi } from 'vitest'

const store = createPinia()

afterAll(() => {
  vi.clearAllMocks()
})

// TODO: @mdp - Instead of testing individual message handlers here
// we should test the MessageBackgroundListener as it's own utility
// and test message handlers on their own
describe('Basic autocapture store behaviour', () => {
  it('should return an item for the same tab domain', () => {
    const autocaptureStore = useAutocaptureStore(store)
    autocaptureStore.update('123abc', 'https://example.com')
    const mockTab = createMockTabForUrl('https://example.com')
    const item = autocaptureStore.getAutocaptureItem(mockTab)
    expect(item?.id).toEqual('123abc')
  })
  it('should return null for a different domain', () => {
    const autocaptureStore = useAutocaptureStore(store)
    autocaptureStore.update('123abc', 'https://domain.com')
    const mockTab = createMockTabForUrl('https://example.com')
    const item = autocaptureStore.getAutocaptureItem(mockTab)
    expect(item).toBeNull()
  })
  it('should return null after expiration', () => {
    const autocaptureStore = useAutocaptureStore(store)
    autocaptureStore.update('123abc', 'https://example.com')

    let now = (autocaptureStore.capturedAt as number) + AUTOCAPTURE_TIMEOUT + 1
    vi.spyOn(Date, 'now').mockReturnValue(now)
    const mockTab = createMockTabForUrl('https://example.com')
    expect(autocaptureStore.getAutocaptureItem(mockTab)).toBeNull()

    now = (autocaptureStore.capturedAt as number) + AUTOCAPTURE_TIMEOUT - 1
    vi.spyOn(Date, 'now').mockReturnValue(now)
    expect(autocaptureStore.getAutocaptureItem(mockTab)?.id).toEqual('123abc')
  })
})
