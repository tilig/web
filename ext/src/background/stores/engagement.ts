import { useStorage, RemovableRef } from '@vueuse/core'
import { defineStore } from 'pinia'
import { cloneDeep } from 'lodash'

import { sendGoogleAnalyticsEvent } from '@core/clients/analytics'
import { TrackingProperties } from '@core/clients/tilig/tracking'
import { AUTOFILL_SELECTED, TrackingEvent } from '@core/utils/trackingConstants'
import browserExtensionVersion from '@core/utils/browserExtensionVersion'

export const NAMESPACE = 'engagement'

export type EngagementState = {
  state: 'newUser' | 'activeUser'
  autofills: number
}

const initialState: EngagementState = {
  state: 'newUser',
  autofills: 0,
}

interface TrackingPayload {
  event: TrackingEvent
  properties?: TrackingProperties
}

export const useEngagementStore = defineStore(NAMESPACE, () => {
  const state: RemovableRef<EngagementState> = useStorage(NAMESPACE, initialState, localStorage)

  // See all the tracked events but filter for the ones we care about
  const trackEvent = ({ event }: TrackingPayload) => {
    if (event === AUTOFILL_SELECTED) {
      return trackAutofillEvent()
    }
  }

  const trackLogin = async (): Promise<void> => {
    // Needs to be called on every login
    // But only tracks the first login/install
    if (state.value.state === 'newUser') {
      await sendGoogleAnalyticsEvent('extension_installed', {
        browserExtensionVersion,
      })
      state.value.state = 'activeUser'
    }
  }

  const trackAutofillEvent = async (): Promise<void> => {
    state.value.autofills = state.value.autofills + 1
    if (state.value.autofills === 1) {
      return sendGoogleAnalyticsEvent('first_autofill')
    } else if (state.value.autofills === 3) {
      return sendGoogleAnalyticsEvent('third_autofill')
    }
  }

  const $reset = () => (state.value = cloneDeep(initialState))
  return { state, trackAutofillEvent, trackLogin, trackEvent, $reset }
})
