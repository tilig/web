import { defineStore } from 'pinia'
import { parseDomain } from '@core/utils/domainUtils'

export const NAMESPACE = 'draft'
export const DRAFT_DATA_MAX_RECORDS = 50
export const DRAFT_DATA_TIMEOUT = 5 * 60 * 1000

export type DraftUpdateData = {
  username?: string
  password?: string
  ignoreAutosave?: boolean
}

export type DraftData = {
  username?: string
  password?: string
  ignoreAutosave?: boolean
  updatedAt: number
}

type DraftTab = Pick<chrome.tabs.Tab, 'url' | 'id'>

interface DraftPayload {
  tab: DraftTab
  data: DraftUpdateData
}

type DraftState = {
  byDomain: Map<string, DraftData>
}

const isDraftExpired = (draft: DraftData) => {
  return Date.now() - draft.updatedAt > DRAFT_DATA_TIMEOUT
}

// We need to keep tabs on the same domain seperate
const draftKey = (tab: DraftTab) => {
  if (!tab.url) throw new Error('Cannot store draft dat for a tab without a url')
  const domain = parseDomain(tab.url)
  return `${tab.id}|${domain}`
}

export const useDraftStore = defineStore(NAMESPACE, {
  state: (): DraftState => ({
    byDomain: new Map<string, DraftData>(),
  }),
  getters: {
    byTabDomain: (state) => {
      return (tab: DraftTab): DraftData | null => {
        if (tab?.url) {
          const key = draftKey(tab)
          const data = state.byDomain.get(key)
          if (data) {
            if (!isDraftExpired(data)) return data
          }
        }
        return null
      }
    },
  },
  actions: {
    update({ data, tab }: DraftPayload) {
      if (!tab.url) return
      const key = draftKey(tab)
      const now = Date.now()
      const existingState: DraftData = this.byDomain.get(key) || {
        updatedAt: now,
      }
      existingState.updatedAt = now
      const newState = Object.assign(existingState, data)
      this.byDomain.set(key, newState)
      this.purge()
    },
    delete(tab: DraftTab) {
      if (!tab.url) return
      const domain = parseDomain(tab.url)
      if (!domain) return
      this.byDomain.delete(domain)
      this.purge()
    },
    reset() {
      this.byDomain.clear()
    },
    purge() {
      for (const [key, value] of this.byDomain.entries()) {
        if (isDraftExpired(value)) this.byDomain.delete(key)
      }
      if (this.byDomain.size > DRAFT_DATA_MAX_RECORDS) {
        let idx = this.byDomain.size
        for (const [key] of this.byDomain.entries()) {
          if (idx > DRAFT_DATA_MAX_RECORDS) this.byDomain.delete(key)
          idx = idx - 1
        }
      }
    },
  },
})
