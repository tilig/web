/**
 * This store handles keeping track of credentials we recently autocaptured
 * We need to keep track of these in the store in the backgound in order
 * to display the autocapture modal on subsequent page views
 */

import { defineStore } from 'pinia'
import { parseDomain } from '@core/utils/domainUtils'

const NAMESPACE = 'autocapture'

/**
 * This is the timeout for subsequent page loads to display the modal.
 * After the timeout we will no longer return an item from this store.
 * The user can 'dismiss' the modal and we will manually delete the
 * item from the store
 */

export const AUTOCAPTURE_TIMEOUT = 30000

type CurrentTab = Pick<chrome.tabs.Tab, 'url'>

export type AutocapturedItem = {
  id: string | null
  domain: string | null
  capturedAt: number | null
}

export const useAutocaptureStore = defineStore(NAMESPACE, {
  state: (): AutocapturedItem => ({
    id: null,
    domain: null,
    capturedAt: null,
  }),
  getters: {
    getAutocaptureItem: (state) => {
      return (tab: CurrentTab | null): AutocapturedItem | null => {
        if (!tab) return null
        if (tab?.url) {
          const domain = parseDomain(tab.url)
          if (!domain) return null
          // Here's where we only return autocapture items that haven't timed out
          if (state.capturedAt && state.capturedAt + AUTOCAPTURE_TIMEOUT < Date.now()) return null
          if (domain === state.domain) {
            return {
              id: state.id,
              domain: state.domain,
              capturedAt: state.capturedAt,
            }
          }
        }
        return null
      }
    },
  },
  actions: {
    update(id: string, url: string): AutocapturedItem {
      const domain = parseDomain(url)
      if (!domain) throw new Error(`Could not save autocapture data for ${url}`)

      this.id = id
      this.capturedAt = Date.now()
      this.domain = domain
      return {
        id,
        domain: url,
        capturedAt: this.capturedAt,
      }
    },

    /**
     * Allow the user to explicitly 'dismiss' the autocaptured item and
     * delete it from the store
     */
    dismiss() {
      this.id = null
      this.domain = null
      this.capturedAt = null
    },
  },
})
