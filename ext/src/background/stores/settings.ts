import { useStorage, RemovableRef } from '@vueuse/core'
import { computed, ref } from 'vue'
import { defineStore } from 'pinia'
import { cloneDeep } from 'lodash'

export const NAMESPACE = 'settings'

export type SettingsState = {
  autosaveLogin: boolean
  gifsEnabled: boolean
}

const initialState = {
  autosaveLogin: false,
  gifsEnabled: false,
}

const checkWebRequestPermissions = (): boolean => {
  return !!chrome.webRequest
}

export const useSettingsStore = defineStore(NAMESPACE, () => {
  const state: RemovableRef<SettingsState> = useStorage(NAMESPACE, initialState, localStorage)
  const webRequestEnabled = ref(checkWebRequestPermissions())

  // Getters
  const autosaveLogin = computed(() => {
    const autosaveLoginState = state.value.autosaveLogin
    if (webRequestEnabled.value === false) return false
    return autosaveLoginState
  })

  const serializedState = computed((): SettingsState => {
    return {
      autosaveLogin: autosaveLogin.value,
      gifsEnabled: state.value.gifsEnabled,
    }
  })

  const setAutosaveLogin = (autosaveOn: boolean) => {
    // On each change, check if we have webRequest permissions and update the reference
    webRequestEnabled.value = checkWebRequestPermissions()
    state.value.autosaveLogin = autosaveOn
  }

  const setGifsEnabled = (enabled: boolean) => {
    state.value.gifsEnabled = enabled
  }

  const $reset = () => (state.value = cloneDeep(initialState))
  return { state, serializedState, autosaveLogin, $reset, setAutosaveLogin, setGifsEnabled }
})
