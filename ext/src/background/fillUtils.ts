import { FILL_FOCUSSED_FORM_TAB_MESSAGE, FILL_FORM_TAB_MESSAGE } from '../utils/messageConstants'
import { sendToTabAndDomain } from '../utils/messageSender'

export type FillProps = {
  username?: string | null
  password?: string | null
  domain: string
  trigger: string
  userInitiated: boolean // Did a the user ask for the fill, or was it autofill
}

/**
 * Pass the autofill credentials to the content page
 * @param {Object} tab - The tab
 * @param {FillProps} autoFillProps - The plain password, plain username and domain
 */
export const fillOnTab = (tab: chrome.tabs.Tab, autoFillProps: FillProps) => {
  if (!tab.id) return
  return sendToTabAndDomain<void>(
    tab.id,
    autoFillProps.domain,
    FILL_FORM_TAB_MESSAGE,
    autoFillProps
  )
}

/**
 * Autofill the given given autoFillProps on the tab that's currently active.
 * @param {FillProps} fillProps - The plain password, plain username and domain
 */
export const fillOnActiveTab = async (fillProps: FillProps) => {
  chrome.tabs.query({ active: true, currentWindow: true }, async (tabs) => {
    fillOnTab(tabs[0], fillProps)
  })
}

/**
 * Fill in the credentials on the form that currently has focus, on the active tab.
 * The content script will execute this.
 * @param {FillProps} fillProps - The plain password, plain username and domain
 */
export const dispatchFillFocussedForm = (fillProps: FillProps) => {
  chrome.tabs.query({ active: true, currentWindow: true }, async (tabs) => {
    if (!tabs[0].id) return
    return sendToTabAndDomain<void>(
      tabs[0].id,
      fillProps.domain,
      FILL_FOCUSSED_FORM_TAB_MESSAGE,
      fillProps
    )
  })
}
