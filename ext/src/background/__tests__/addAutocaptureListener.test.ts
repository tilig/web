import { beforeEach, describe, expect, it, vi } from 'vitest'
import { createPinia } from 'pinia'
import {
  addAutocaptureListener,
  validAuthRequests,
  onBeforeRequestListener,
  onCompletedListener,
  RING_BUFFER_MAX,
  BeforeWebRequest,
} from '../addAutocaptureListener'
import { attemptSaveDraft } from '../messageApis/saveDraft'
import { useDraftStore } from '../stores/draft'
import { useSettingsStore } from '../stores/settings'

const mockWebRequest: typeof chrome.webRequest = {
  onBeforeRequest: {
    addListener: vi.fn(),
  },
  onCompleted: {
    addListener: vi.fn(),
  },
} as unknown as typeof chrome.webRequest

describe('The valid request ring buffer', () => {
  it('should work like a ring buffer', () => {
    const testRequestNumber = 3 * RING_BUFFER_MAX
    for (let i = 1; i <= testRequestNumber; i++) {
      validAuthRequests.add(`idx${i}`, { url: `https://idx${i}.com`, xhr: false })
    }
    expect(validAuthRequests.get(`idx${testRequestNumber}`)).not.toBe(null)
    expect(validAuthRequests.get(`idx${testRequestNumber}`)).toEqual({
      url: `https://idx${testRequestNumber}.com`,
      xhr: false,
    })
    expect(validAuthRequests.get(`idx${testRequestNumber - RING_BUFFER_MAX}`)).toBe(null)
    expect(validAuthRequests.get(`idx${1}`)).toBe(null)
    expect(validAuthRequests.keys.length).toEqual(RING_BUFFER_MAX)
    expect(validAuthRequests.values.length).toEqual(RING_BUFFER_MAX)
  })
})

describe('Adding the listeners to webRequest', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  const store = createPinia()

  it('should handle webRequest being unavailable', () => {
    vi.stubGlobal('chrome', {
      webRequest: undefined,
    })

    expect(addAutocaptureListener(store)).toBe(false)
  })
  it('should add listeners only if the webrequest object is available on chrome', () => {
    vi.stubGlobal('chrome', {
      webRequest: mockWebRequest,
    })
    expect(addAutocaptureListener(store)).toBe(true)
    expect(chrome.webRequest.onBeforeRequest.addListener).toHaveBeenCalled()
    expect(chrome.webRequest.onBeforeRequest.addListener).toHaveBeenCalled()
  })
})

describe('The behaviour of the webRequest listeners', () => {
  vi.stubGlobal('chrome', {
    webRequest: mockWebRequest,
  })

  const store = createPinia()
  const draftStore = useDraftStore(store)
  const settingStore = useSettingsStore(store)

  beforeEach(() => {
    validAuthRequests.keys.fill('0')
    draftStore.$reset()
    settingStore.$reset()
    vi.clearAllMocks()
    vi.resetAllMocks()
  })

  describe('onBeforeRequest', () => {
    it('save requests which include a draft password in the message body', () => {
      const onBeforeRequestFn = onBeforeRequestListener(store)
      const jsonRequestString = JSON.stringify({
        login: { psswrd: 'p4ssw0rd' },
      })
      const requestBody: BeforeWebRequest = {
        requestId: '123abc',
        type: 'main_frame',
        tabId: 1,
        requestBody: {
          raw: [{ bytes: Buffer.from(jsonRequestString) }],
        },
        url: 'https://login.com',
      }
      draftStore.update({
        data: { username: '', password: 'p4ssw0rd' },
        tab: { id: 1, url: requestBody.url },
      })
      settingStore.setAutosaveLogin(true)

      onBeforeRequestFn(requestBody)
      expect(validAuthRequests.get('123abc')?.url).toBe(requestBody.url)
    })

    it('save requests which include a draft password in the form', () => {
      const onBeforeRequestFn = onBeforeRequestListener(store)
      const requestBody: BeforeWebRequest = {
        requestId: '123abc',
        type: 'main_frame',
        tabId: 1,
        requestBody: {
          formData: {
            auth: ['p4ssw0rd'],
          },
        },
        url: 'https://login1.com',
      }
      draftStore.update({
        data: { username: '', password: 'p4ssw0rd' },
        tab: { id: 1, url: requestBody.url },
      })
      settingStore.setAutosaveLogin(true)

      onBeforeRequestFn(requestBody)
      expect(validAuthRequests.get('123abc')?.url).toBe(requestBody.url)
    })

    it('does not save requests when settings are off', () => {
      const onBeforeRequestFn = onBeforeRequestListener(store)
      const requestBody: BeforeWebRequest = {
        requestId: '123abc',
        type: 'main_frame',
        tabId: 1,
        requestBody: {
          formData: {
            auth: ['p4ssw0rd'],
          },
        },
        url: 'https://login1.com',
      }
      draftStore.update({
        data: { username: '', password: 'p4ssw0rd' },
        tab: { id: 1, url: requestBody.url },
      })
      settingStore.setAutosaveLogin(true)

      onBeforeRequestFn(requestBody)
      expect(validAuthRequests.get('123abc')?.url).toBe(requestBody.url)
      settingStore.setAutosaveLogin(false)
      validAuthRequests.keys.fill('0')

      onBeforeRequestFn(requestBody)
      expect(validAuthRequests.get('123abc')).toBe(null)
    })
  })

  describe('onCompleted', () => {
    vi.mock('../messageApis/saveDraft')
    it('should save credentials from a successful form post', () => {
      const mockTab = { url: 'https://foo.com/success' }
      vi.stubGlobal('chrome', {
        tabs: {
          get: (_id: number, callback: (tab: Pick<chrome.tabs.Tab, 'url'>) => void) => {
            callback(mockTab)
          },
        },
      })
      const onCompletedFn = onCompletedListener(store)
      validAuthRequests.add('onCompleted123', { url: 'https://foo.com', xhr: false })
      onCompletedFn({
        requestId: 'onCompleted123',
        statusCode: 200,
        tabId: 1,
        type: 'main_frame',
        url: 'https://foo.com/success',
      })
      expect(attemptSaveDraft).toHaveBeenCalled()
    })

    it('should NOT save credentials from a form post when the URL does not change', () => {
      const mockTab = { url: 'https://foo.com' }
      vi.stubGlobal('chrome', {
        tabs: {
          get: (_id: number, callback: (tab: Pick<chrome.tabs.Tab, 'url'>) => void) => {
            callback(mockTab)
          },
        },
      })
      const onCompletedFn = onCompletedListener(store)
      validAuthRequests.add('onCompleted123', { url: 'https://foo.com', xhr: false })
      onCompletedFn({
        requestId: 'onCompleted123',
        statusCode: 200,
        tabId: 1,
        type: 'main_frame',
        url: 'https://foo.com',
      })
      expect(attemptSaveDraft).not.toHaveBeenCalled()
    })

    it('should save credentials from a successful XHR post', () => {
      const mockTab = { url: 'https://foo.com' }
      vi.stubGlobal('chrome', {
        tabs: {
          get: (_id: number, callback: (tab: Pick<chrome.tabs.Tab, 'url'>) => void) => {
            callback(mockTab)
          },
        },
      })
      const onCompletedFn = onCompletedListener(store)
      validAuthRequests.add('onCompleted123', { url: 'https://foo.com', xhr: true })
      onCompletedFn({
        requestId: 'onCompleted123',
        statusCode: 200,
        tabId: 1,
        url: 'https://foo.com',
        type: 'xmlhttprequest',
      })
      expect(attemptSaveDraft).toHaveBeenCalled()
    })
    it('should NOT save credentials from a successful XHR post', () => {
      const mockTab = { url: 'https://foo.com' }
      vi.stubGlobal('chrome', {
        tabs: {
          get: (_id: number, callback: (tab: Pick<chrome.tabs.Tab, 'url'>) => void) => {
            callback(mockTab)
          },
        },
      })
      const onCompletedFn = onCompletedListener(store)
      validAuthRequests.add('onCompleted123', { url: 'https://foo.com', xhr: true })
      onCompletedFn({
        requestId: 'onCompleted123',
        statusCode: 403,
        tabId: 1,
        url: 'https://foo.com',
        type: 'xmlhttprequest',
      })
      expect(attemptSaveDraft).not.toHaveBeenCalled()
    })
  })
})
