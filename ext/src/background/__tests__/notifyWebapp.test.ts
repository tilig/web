import { beforeEach, describe, expect, it, vi } from 'vitest'
import { notifyWebapp } from '../notifyWebapp'
import { executeScript, findTiligTab } from '@/utils/webApp'

vi.mock('@/utils/webApp', () => {
  return {
    executeScript: vi.fn(() => Promise.resolve(null)),
    findTiligTab: vi.fn(() => Promise.resolve({ id: '1234' })),
  }
})

const createMock = vi.fn()
const updateMock = vi.fn()

vi.stubGlobal('chrome', {
  runtime: { getURL: (src: string) => `chrome://extensionid${src}` },
  tabs: { create: createMock, update: updateMock },
})

describe('Notifying the web extension', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  // Handle a install when we are in the Tilig Webapp onboarding process
  it('should let the webapp know if we have installed the extension and we have a tab open with the welcome page', async () => {
    await notifyWebapp()
    expect(updateMock).toHaveBeenCalled()
    expect(vi.mocked(findTiligTab)).toHaveBeenCalledOnce()
    expect(vi.mocked(executeScript)).toHaveBeenCalledTimes(2)
  })

  // Handle a fresh install from the app store without the Tilig Webapp onboarding us
  it('should open the chrome extension welcome screen if we do not have Tilig onboarding open', async () => {
    vi.mocked(findTiligTab).mockResolvedValue(null)
    await notifyWebapp()
    expect(vi.mocked(findTiligTab)).toHaveBeenCalledOnce()
    expect(updateMock).not.toHaveBeenCalled()
    expect(createMock).toHaveBeenCalledWith(
      {
        url: 'chrome://extensionid/src/ui/welcome/index.html',
      },
      expect.any(Function)
    )
  })
})
