import { Pinia } from 'pinia'

import { Brand, getBrand } from '@core/clients/tilig/brands'
import { createLogger } from '@core/utils/logger'
import { sendToRuntime } from '@/utils/messageSender'
import { queryActiveTab } from '@/utils/tabUtils'
import { GET_BRAND_WORKER_MESSAGE } from '@/utils/messageConstants'

const logger = createLogger('background:messageAPI:getBrand')

export const key = GET_BRAND_WORKER_MESSAGE

/**
 * Handler for getting a brand for a url
 * @param {url} string The url to get the brand for
 */
export const handler = async (
  { url }: { url: string },
  _tab: chrome.tabs.Tab | null,
  _store: Pinia
): Promise<Brand | null> => {
  try {
    const brand = await getBrand(url)
    logger.error('Got Brand', brand.data)
    return brand.data
  } catch (err) {
    logger.error('Unable to fetch brand', err)
    return null
  }
}

/**
 * Get the brand for a specific url
 * @param {url} url The url of the brand to fetch
 */
export const fetchBrand = (url: string) => {
  return sendToRuntime<Brand | null>(key, { url })
}

/**
 * Get the brand for the url of the active tab
 */
export const fetchActiveTabBrand = async (): Promise<Brand | null> => {
  const tab = await queryActiveTab()
  if (tab?.url?.startsWith('http')) {
    return sendToRuntime<Brand | null>(key, { url: tab.url })
  }
  return null
}
