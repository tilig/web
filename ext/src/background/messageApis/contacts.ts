import { Pinia } from 'pinia'

import { Contact, getContacts } from '@core/clients/tilig'
import { createLogger } from '@core/utils/logger'
import { sendToRuntime } from '@/utils/messageSender'
import { CONTACTS_STORE_WORKER_MESSAGE } from '@/utils/messageConstants'

const logger = createLogger('background:messageAPI:getContacts')

export const key = CONTACTS_STORE_WORKER_MESSAGE

/**
 * Handler for getting user contacts
 */
export const handler = async (
  _message: string,
  _tab: chrome.tabs.Tab | null,
  _store: Pinia
): Promise<Contact[] | null> => {
  try {
    const { data } = await getContacts()
    logger.debug('Got Contacts', data.contacts)
    return data.contacts
  } catch (err) {
    logger.error('Unable to fetch contacts', err)
    return null
  }
}

/**
 * Get the contacts of the authenticated user
 */
export const fetchContacts = () => {
  return sendToRuntime<Contact[] | null>(key)
}
