import { describe, expect, it, vi } from 'vitest'
import { createPinia } from 'pinia'
import { AutocapturedItem, useAutocaptureStore } from '@/background/stores/autocapture'
import { AUTOCAPTURE_WORKER_MESSAGE } from '@/utils/messageConstants'
import { createMockTabForUrl } from '@/__tests__/utils/mockTab'
import { backgroundMessageListener } from '..'

vi.mock('@core/stores/items', () => {
  return {
    useItemsStore: vi.fn(),
  }
})

const store = createPinia()
const listener = backgroundMessageListener(store)

// TODO: @mdp - Instead of testing individual message handlers here
// we should test the MessageBackgroundListener as it's own utility
// and test message handlers on their own
describe('Basic operation of the message listener', () => {
  it('should handle asycnchronous responses to messages', () => {
    const autocaptureStore = useAutocaptureStore(store)
    autocaptureStore.update('123abc', 'https://example.com')
    const mockTab = createMockTabForUrl('https://example.com')
    const sender = {
      tab: mockTab,
    }
    return new Promise<void>((resolve) => {
      const callback = (item: AutocapturedItem) => {
        expect(item.id).toEqual('123abc')
        resolve()
      }
      const isAsync = listener(
        { msg: AUTOCAPTURE_WORKER_MESSAGE, data: { action: 'get' } },
        sender,
        callback
      )
      // Listener returning false signals to chrome that the callback is syncronous/immediate
      expect(isAsync).toBe(true)
    })
  })
})
