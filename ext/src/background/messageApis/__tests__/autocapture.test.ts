import { beforeAll, describe, expect, it, vi } from 'vitest'
import { createPinia } from 'pinia'
import { findInMain } from '@core/clients/tilig'
import { handler } from '../autocapture'
import { getDecryptedItemStub } from './__fixtures__/items.fixtures'
import { createMockTabForUrl } from '@/__tests__/utils/mockTab'

const store = createPinia()
const getAutocaptureItem = vi.fn()
const dismiss = vi.fn()
vi.mock('@/background/stores/autocapture', () => {
  return {
    useAutocaptureStore: () => {
      return {
        getAutocaptureItem,
        dismiss,
      }
    },
  }
})

const deleteItem = vi.fn()
const getItemById = vi.fn()
const updateItem = vi.fn()

vi.mock('@core/stores/items', () => {
  return {
    useItemsStore: () => {
      return {
        deleteItem,
        getItemById,
        updateItem,
      }
    },
  }
})

vi.mock('@/content-scripts/messageApis/onAutocaptureModalMessage', () => {
  return {
    dismissAutocaptureModal: vi.fn(),
  }
})

describe('The autocapture background handler', () => {
  beforeAll(() => {
    vi.clearAllMocks()
  })

  it('should allow us to get an autocaptured item', async () => {
    const mockTab = createMockTabForUrl('https://example.com')
    await handler({ action: 'get' }, mockTab, store)
    expect(getAutocaptureItem).toHaveBeenCalled()
  })
  it('should allow us to dismiss an autocaptured item', async () => {
    const mockTab = createMockTabForUrl('https://example.com')
    await handler({ action: 'dismiss' }, mockTab, store)
    expect(dismiss).toHaveBeenCalled()
  })
  it('should allow us to undo an autocaptured item', async () => {
    getItemById.mockResolvedValueOnce(getDecryptedItemStub())
    const mockTab = createMockTabForUrl('https://example.com')
    await handler({ action: 'undo', id: '123' }, mockTab, store)
    expect(deleteItem).toHaveBeenCalled()
    expect(dismiss).toHaveBeenCalled()
  })
  it('should allow us to undo an autocaptured updated item', async () => {
    const mockDecryptedItem = getDecryptedItemStub()
    mockDecryptedItem.details.history.push({
      kind: 'password',
      value: 'myoldPassword',
      replaced_at: '2022-11-02T02:28:01.694Z',
    })
    getItemById.mockResolvedValueOnce(mockDecryptedItem)
    const mockTab = createMockTabForUrl('https://example.com')
    await handler({ action: 'undo', id: '123' }, mockTab, store)
    expect(deleteItem).not.toHaveBeenCalled()
    expect(updateItem).toHaveBeenCalled()
    expect(findInMain(updateItem.mock.lastCall?.at(2), 'password')).toEqual('myoldPassword')
    expect(dismiss).toHaveBeenCalled()
  })
})
