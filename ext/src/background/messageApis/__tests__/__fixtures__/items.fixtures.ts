import { DecryptedDetails, DecryptedItem, DecryptedOverview } from '@core/clients/tilig'
import { Template } from '@core/clients/tilig/item'
import { ItemState } from '@core/stores/items'
import { cloneDeep } from 'lodash'

const itemStub: ItemState = {
  item: {
    id: '74ba7416-34e3-4a00-be11-657edd2f4dca',
    created_at: '2022-11-02T02:27:42.135Z',
    updated_at: '2022-11-02T02:28:01.739Z',
    encryption_version: 2,
    template: Template.Login,
    website: 'airbnb.com',
    brand: {
      id: '6d323445-2e67-486f-b30c-a98a16ed698d',
      name: 'Airbnb',
      domain: 'airbnb.com',
      public_suffix_domain: 'airbnb.com',
      totp: false,
      main_color_hex: '#cc0000',
      main_color_brightness: '43',
      logo_source: 'https://asset.brandfetch.io/idhidc5593/idsiqVrXz6.png',
      logo_icon_source: 'https://asset.brandfetch.io/idhidc5593/idv0JKZqrw.png',
      is_fetched: true,
    },
    name: 'AirBnb',
    notes: null,
    otp: null,
    android_app_id: null,
    password: '',
    username: '',
    rsa_encrypted_dek: '',
    encrypted_overview: '',
    encrypted_details: '',
    encrypted_dek: '',
    share_link: null,
    folder: null,
    encrypted_folder_dek: null,
  },
  overview: {
    name: 'Airbnb',
    info: 'squirrel@tilig.com',
    urls: [
      {
        url: 'https://airbnb.com',
        name: 'airbnb',
      },
    ],
    android_app_ids: [],
  },
}

export const decryptedItemStub: DecryptedDetails = {
  notes: null,
  main: [
    {
      kind: 'username',
      value: 'squirrel@tilig.com',
    },
    {
      kind: 'password',
      value: 'abcdefg',
    },
    {
      kind: 'totp',
      value: null,
    },
  ],
  history: [
    // {
    //   kind: 'password',
    //   value: 'myoldPassword',
    //   replaced_at: '2022-11-02T02:28:01.694Z',
    // },
  ],
  custom_fields: [],
}

export const getItemStateStub = (): ItemState => {
  return cloneDeep(itemStub)
}

export const getDecryptedItemStub = (): DecryptedItem => {
  const { item, overview } = getItemStateStub()
  return cloneDeep({
    item,
    overview: overview as DecryptedOverview,
    details: decryptedItemStub,
    share: null,
  })
}
