import { beforeEach, describe, expect, it, vi } from 'vitest'
import { createPinia } from 'pinia'
import { setInMain } from '@core/clients/tilig'
import { ItemState } from '@core/stores/items'
import { useDraftStore } from '@/background/stores/draft'
import { launchAutocaptureModal } from '@/content-scripts/messageApis/onAutocaptureModalMessage'
import { createMockTabForUrl } from '@/__tests__/utils/mockTab'
import { handler } from '../saveDraft'
import { getDecryptedItemStub, getItemStateStub } from './__fixtures__/items.fixtures'

const mockTrackingStore = {
  trackEvent: vi.fn(),
}

vi.mock('@core/stores/tracking', () => {
  return {
    useTrackingStore: () => {
      return mockTrackingStore
    },
  }
})

const mockItemStore = {
  getItemById: vi.fn(),
  createItem: vi.fn(),
  updateItem: vi.fn(),
  itemStates: [] as ItemState[],
}

vi.mock('@core/stores/items', () => {
  return {
    useItemsStore: () => {
      return mockItemStore
    },
  }
})

vi.mock('@/content-scripts/messageApis/onAutocaptureModalMessage', () => {
  return {
    launchAutocaptureModal: vi.fn().mockReturnValue('foo'),
  }
})

const store = createPinia()
const draftStore = useDraftStore(store)

describe('Saving draft items in Tilig after a signup or login', () => {
  // Login behaviour
  beforeEach(() => {
    vi.clearAllMocks()
    mockItemStore.getItemById.mockResolvedValue(getDecryptedItemStub())
    mockItemStore.createItem.mockResolvedValue(getDecryptedItemStub())
    mockItemStore.updateItem.mockResolvedValue(true)
    mockItemStore.itemStates = [getItemStateStub()]
  })

  // Create new credentials for a domain without any accounts
  it('should create credentials on login if they do not already exist', async () => {
    const mockTab = createMockTabForUrl('https://example.com')
    draftStore.update({
      data: { username: 'mark', password: 'superSecret' },
      tab: mockTab,
    })
    await handler({ saveType: 'login' }, mockTab, store)
    expect(mockItemStore.createItem).toHaveBeenCalled()
    expect(launchAutocaptureModal).toHaveBeenCalled()
  })

  // We already have an account on this url, but the usernames don't match, then create the item
  it('should create a new item from the draft state if the username does not exist for the url', async () => {
    const mockTab = createMockTabForUrl('https://airbnb.com')
    draftStore.update({
      data: { username: 'mark@tilig.com', password: 'superSecret' },
      tab: mockTab,
    })
    await handler({ saveType: 'login' }, mockTab, store)
    expect(mockItemStore.createItem).toHaveBeenCalled()
    expect(launchAutocaptureModal).toHaveBeenCalled()
  })

  describe('should update credentials for a login if they already exist', () => {
    it('and the passwords are different', async () => {
      const mockTab = createMockTabForUrl('https://airbnb.com')
      draftStore.update({
        data: { username: 'squirrel@tilig.com', password: 'superSecret' },
        tab: mockTab,
      })
      await handler({ saveType: 'login' }, mockTab, store)
      expect(mockItemStore.createItem).not.toHaveBeenCalled()
      expect(mockItemStore.updateItem).toHaveBeenCalled()
      expect(launchAutocaptureModal).toHaveBeenCalled()
    })
    it('but not if the passwords are the same', async () => {
      const mockDecryptedItem = getDecryptedItemStub()
      setInMain(mockDecryptedItem.details, 'password', 'theSamePassword')
      mockItemStore.getItemById.mockResolvedValueOnce(mockDecryptedItem)
      const mockTab = createMockTabForUrl('https://airbnb.com')
      draftStore.update({
        data: { username: 'squirrel@tilig.com', password: 'theSamePassword' },
        tab: mockTab,
      })
      await handler({ saveType: 'login' }, mockTab, store)
      expect(mockItemStore.createItem).not.toHaveBeenCalled()
      expect(mockItemStore.updateItem).not.toHaveBeenCalled()
      expect(vi.mocked(launchAutocaptureModal)).not.toHaveBeenCalled()
    })
  })
})
