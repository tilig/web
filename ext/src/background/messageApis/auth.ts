import { Pinia } from 'pinia'
import { AUTH_MESSAGE_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { HandoffProperties, useAuthStore } from '@core/stores/auth'
import { UserProfile } from '@core/clients/tilig'
import { until } from '@vueuse/core'

export type AUTH_ACTIONS = 'auth-state' | 'current-user' | 'handoff' | 'logout' | 'refresh'

export const key = AUTH_MESSAGE_WORKER_MESSAGE

export type AuthCallProps = {
  action: AUTH_ACTIONS
  handoff?: HandoffProperties
}

export type CurrentUserResult = UserProfile | null

export type AuthStateResult = {
  initialized: boolean
  firebaseInitialized: boolean
  isAuthenticated: boolean
  isSettled: boolean
}

// export type AuthHandoffProps = {
//   user: User
//   authResult: UserCredential
//   accessToken: TiligAccessToken
//   keyPair: KeyPair
//   legacyKeyPair: LegacyKeyPair
// }

// API for allowing the popup to handoff auth tokens to the background from the popup
// basic actions on those tabs
export const handler = async (
  message: AuthCallProps,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<void | boolean | AuthStateResult | CurrentUserResult> => {
  const { action } = message
  if (action === 'auth-state') {
    return new Promise((resolve) => {
      const authStore = useAuthStore(store)
      resolve({
        initialized: authStore.initialized,
        firebaseInitialized: authStore.firebaseInitialized,
        isAuthenticated: authStore.isAuthenticated,
        isSettled: authStore.isSettled,
      })
    })
  } else if (action === 'current-user') {
    return new Promise((resolve) => {
      const authStore = useAuthStore(store)
      if (authStore.isAuthenticated) {
        resolve(authStore.tiligUser)
      }
      resolve(null)
    })
  } else if (action === 'handoff') {
    // Will later be async due to storage requirements
    return new Promise((resolve, reject) => {
      const authStore = useAuthStore(store)
      if (!message.handoff) return reject('Bad handoff message')
      authStore
        .handoff(message.handoff)
        .then((result) => {
          if (!result) return false

          // We initialized the auth login flow, and it settles asynchronously.
          // We can only be certain of the auth state when it's settled.
          return until(() => authStore.isSettled)
            .toBe(true)
            .then(() => authStore.isAuthenticated)
        })
        .then(resolve)
    })
  } else if (action === 'refresh') {
    const authStore = useAuthStore(store)
    return authStore.refreshProfile()
  } else if (action === 'logout') {
    const authStore = useAuthStore(store)
    return authStore.signOut()
  } else {
    throw new Error('Unknown action for auth message api')
  }
}

export const handoffAuth = (authHandoff: HandoffProperties) => {
  const message: AuthCallProps = {
    action: 'handoff',
    handoff: authHandoff,
  }
  return sendToRuntime<boolean>(AUTH_MESSAGE_WORKER_MESSAGE, message)
}

export const getCurrentUser = () => {
  const message: AuthCallProps = {
    action: 'current-user',
  }
  return sendToRuntime<CurrentUserResult>(AUTH_MESSAGE_WORKER_MESSAGE, message)
}

export const getAuthState = () => {
  const message: AuthCallProps = {
    action: 'auth-state',
  }
  return sendToRuntime<AuthStateResult>(AUTH_MESSAGE_WORKER_MESSAGE, message)
}

export const logoutUser = () => {
  const message: AuthCallProps = {
    action: 'logout',
  }
  return sendToRuntime<CurrentUserResult>(AUTH_MESSAGE_WORKER_MESSAGE, message)
}

export const refreshUserProfile = () => {
  const message: AuthCallProps = {
    action: 'refresh',
  }
  return sendToRuntime<void>(AUTH_MESSAGE_WORKER_MESSAGE, message)
}
