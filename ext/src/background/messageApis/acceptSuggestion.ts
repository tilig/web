import { Pinia } from 'pinia'
import { useTrackingStore } from '@core/stores/tracking'
import { FILL_FOCUSSED_INPUT as FILL_FOCUSSED_INPUT_EVENT } from '@core/utils/trackingConstants'
import { ACCEPT_SUGGESTION_WORKER_MESSAGE } from '@/utils/messageConstants'
import { DraftUpdateData, useDraftStore } from '@/background/stores/draft'
import { sendToRuntime } from '@/utils/messageSender'
import { createLogger } from '@core/utils/logger'
import { sendFillPasswordInputs } from '@/content-scripts/messageApis/onFillPasswordInputs'
import { sendFillFocusedInput } from '@/content-scripts/messageApis/onFillFocussedInput'

const logger = createLogger('ext:accept-suggestions')

export const key = ACCEPT_SUGGESTION_WORKER_MESSAGE

export const handler = async (
  { inputType, suggestion, domain }: AcceptSuggestionProps,
  tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<void> => {
  logger.debug(`Suggestion for ${inputType} on domain ${domain}: ${suggestion}`)

  const trackingStore = useTrackingStore(store)
  const draftStore = useDraftStore(store)

  if (!tab || !tab.id) return
  const draftCrendential: DraftUpdateData = {}
  if (inputType == 'password') {
    /**
     * Ignore the autosave for this password, because we just generated it
     * and the Modal will prompt the user to save it.
     * Sites like Wordpress.com will send the credentials to their server
     * as soon as it's entered, which our autosave will try and save as well.
     * See #839
     */
    draftCrendential.ignoreAutosave = true
    draftCrendential['password'] = suggestion
    await sendFillPasswordInputs(tab.id, domain, suggestion)
  } else if (inputType == 'email' || inputType == 'username') {
    draftCrendential.username = suggestion
    await sendFillFocusedInput(tab.id, domain, suggestion)
  }
  draftStore.update({ tab, data: draftCrendential })

  trackingStore.trackEvent({
    event: FILL_FOCUSSED_INPUT_EVENT,
    properties: { inputType },
  })
}

type AcceptSuggestionProps = {
  inputType: string
  suggestion: string
  domain: string
}

/**
 * Send the suggestion (email or password) to background.js from a
 * 3rd party iframe (Dropdown) to be filled into the page input or inputs
 */
export const sendSuggestion = ({ inputType, suggestion, domain }: AcceptSuggestionProps) => {
  return sendToRuntime(key, {
    inputType,
    suggestion,
    domain,
  })
}
