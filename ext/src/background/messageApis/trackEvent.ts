import { Pinia } from 'pinia'
import { TrackingEvent, AUTOFILL_FAILED, AUTOFILL_SELECTED } from '@core/utils/trackingConstants'
import { useTrackingStore } from '@core/stores/tracking'
import { TRACK_EVENT_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { FillProps } from '../fillUtils'
import { useEngagementStore } from '../stores/engagement'

type TrackProps = {
  event: TrackingEvent
  properties: any
}

export const key = TRACK_EVENT_WORKER_MESSAGE

/**
 * Handler for an tracking events on the extension.
 * @param {TrackProps} trackProps The event and properties we wish to track
 */
export const handler = async (
  { event, properties }: TrackProps,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
) => {
  const trackingStore = useTrackingStore(store)
  const engagementStore = useEngagementStore(store)

  await trackingStore.trackEvent({
    event,
    properties,
  })

  await engagementStore.trackEvent({
    event,
    properties,
  })
  return
}

/**
 * Send the background a message with the autofillProps for tracking a successful fill
 * @param {FillProps} fillProps The fillProps from wich trigger and userInitiated are pulled
 */
export const dispatchTrackFillSuccess = ({ userInitiated, trigger }: FillProps) => {
  return sendToRuntime<void>(key, {
    event: AUTOFILL_SELECTED,
    properties: { userInitiated, trigger },
  })
}

/**
 * Send the background a message with the autofillProps for tracking a failed fill
 * @param {FillProps} fillProps The fillProps from wich trigger and userInitiated are pulled
 */
export const dispatchTrackFillFailure = ({ userInitiated, trigger }: FillProps) => {
  return sendToRuntime<void>(key, {
    event: AUTOFILL_FAILED,
    properties: { userInitiated, trigger },
  })
}

/**
 * Send the background a message with the tracking event to send to Mixpanel
 * @param {event} Event The event name
 */
export const dispatchTrackEvent = (event: TrackingEvent, properties: any = {}) => {
  return sendToRuntime<void>(key, {
    event,
    properties,
  })
}
