import { Pinia } from 'pinia'
import { DraftData, DraftUpdateData, useDraftStore } from '@/background/stores/draft'
import { DRAFT_STORE_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'

export const key = DRAFT_STORE_WORKER_MESSAGE

type actions = 'fetch' | 'update' | 'delete'

export const handler = async (
  { action, data }: { action: actions; data: DraftUpdateData },
  tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<DraftData | null | void> => {
  if (!tab) return
  const draftStore = useDraftStore(store)
  if (action === 'update') {
    draftStore.update({ data, tab })
  } else if (action === 'delete') {
    draftStore.delete(tab)
  } else if (action === 'fetch') {
    return draftStore.byTabDomain(tab) as DraftData | null
  }
  return
}

// Get the saved state for the current domain
// Use case: saving draft credentials on multipage signups
export const fetchDomainState = () => {
  return sendToRuntime<DraftData | null>(key, { action: 'fetch' })
}

// Update the saved state for the current domain
export const updateDomainState = (draftCrendentials: DraftUpdateData) => {
  return sendToRuntime<void>(key, { action: 'update', data: draftCrendentials })
}

// Delete the saved state for the current domain
export const deleteDomainState = () => {
  return sendToRuntime<void>(key, { action: 'delete' })
}
