import { createLogger } from '@core/utils/logger'
import { queryActiveTab } from '@/utils/tabUtils'
import { key as acceptSuggestionKey, handler as acceptSuggestionHandler } from './acceptSuggestion'
import { key as authHandoffKey, handler as authHandoffHandler } from './auth'
import { key as accountsKey, handler as accountsHandler } from './accounts'
import { key as autocaptureKey, handler as autocaptureHandler } from './autocapture'
import { key as attemptAutoFillKey, handler as attemptAutoFillHandler } from './autofillLogin'
import { key as draftStoreKey, handler as draftStoreHandler } from './draftAccounts'
import { key as flagsKey, handler as flagsHandler } from './flags'
import { key as getBrandKey, handler as getBrandHandler } from './getBrand'
import { key as onPageLoadKey, handler as onPageLoadHandler } from './onPageLoad'
import { key as saveDraftKey, handler as saveDraftHandler } from './saveDraft'
import { key as settingsKey, handler as settingsHandler } from './settings'
import { key as trackEventKey, handler as trackEventHandler } from './trackEvent'
import { key as tabApiKey, handler as tabApiHandler } from './tab'
import { key as contactsKey, handler as contactsApiHandler } from './contacts'

const logger = createLogger('ext:backgroundAPI')

/**
 * Requires a seperate import due to issues with Services
 * using vuex-store, which use localStorage and are not
 * permitted on 3rd party iframes (Dropdown, etc.)
 */
import { Pinia } from 'pinia'
import captureException from '@/utils/captureException'

type MessageEventHandler = (
  message: any,
  sender: chrome.runtime.MessageSender,
  sendResponse: (response?: any) => void
) => void | boolean

export type Message = any

/**
 * Message handlers expect to receive
 * - data passed from the caller
 * - the calling tab
 * - our store (They shouldn't handle importing stores directly)
 *
 * The reason for injecting the store is to keep it out of the context
 * of the content-script which will also import these handlers to use
 * their helper functions
 */
export type MessageHandler = (
  msg: Message,
  tab: chrome.tabs.Tab | null,
  store: Pinia
) => Promise<any> | any

type MessageHandlers = { [key: string]: MessageHandler }

const messageHandlers: MessageHandlers = {
  [autocaptureKey]: autocaptureHandler,
  [acceptSuggestionKey]: acceptSuggestionHandler,
  [accountsKey]: accountsHandler,
  [attemptAutoFillKey]: attemptAutoFillHandler,
  [authHandoffKey]: authHandoffHandler,
  [draftStoreKey]: draftStoreHandler,
  [flagsKey]: flagsHandler,
  [getBrandKey]: getBrandHandler,
  [onPageLoadKey]: onPageLoadHandler,
  [saveDraftKey]: saveDraftHandler,
  [settingsKey]: settingsHandler,
  [tabApiKey]: tabApiHandler,
  [trackEventKey]: trackEventHandler,
  [contactsKey]: contactsApiHandler,
}

const externalMessages: MessageHandlers = {
  [settingsKey]: settingsHandler,
}

const responseHandler = (
  msg: string,
  result: Promise<any> | any,
  response: (result: any) => any
) => {
  if (result instanceof Promise) {
    result
      .then((res) => {
        logger.debug(`Background Async Response to ${msg}`, res)
        response(res)
      })
      .catch((err) => {
        logger.error(`Async error on ${msg}:`, err)
        captureException(err)
      })
    return true
  }
  logger.debug(`Background Response to ${msg}`, result)
  response(result)
  return false
}

/**
 * Handles the complexity async Promises vs sync results
 * Chrome expect message handlers to return the result via
 * a `response` callback.
 *
 * If the response is handled syncronously, we call response(result)
 * and return false.
 *
 * If the response is handle asyncronously, we return true, and when
 * reaady, we `response` with our result
 */
export const backgroundMessageListener = (store: Pinia): MessageEventHandler => {
  return ({ msg, data = {} }, sender, response) => {
    logger.debug(`Background Msg: ${msg}`, data)

    if (!messageHandlers[msg]) {
      return false
    }
    // Handle the response for sync and async messageHandlers

    // sender.tab is undefined when called from the popup or background pages
    if (!sender.tab) {
      queryActiveTab().then((activeTab) => {
        const result = messageHandlers[msg](data, activeTab, store)
        responseHandler(msg, result, response)
      })
      return true
    }
    try {
      const result = messageHandlers[msg](data, sender.tab, store)
      return responseHandler(msg, result, response)
    } catch (err) {
      logger.error(`Error on ${msg}:`, err)
      captureException(err)
    }
  }
}

export const externalMessageListener = (store: Pinia): MessageEventHandler => {
  return ({ msg, data = {} }, sender, response) => {
    logger.debug(`Background Msg: ${msg}`, data)

    if (!externalMessages[msg]) {
      return false
    }

    try {
      const result = externalMessages[msg](data, null, store)
      return responseHandler(msg, result, response)
    } catch (err) {
      logger.error(`Error on ${msg}:`, err)
      captureException(err)
    }
  }
}
