import { Pinia } from 'pinia'
import { FLAGS_API_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { useFlagsStore } from '@core/stores/flags'

export const key = FLAGS_API_WORKER_MESSAGE

export type IsEnabledProps = {
  action: 'isEnabled'
  flag: string
  track?: boolean
}

// Allow us to see if a flag is enabled from the UI/iFrame context
export function handler(
  props: IsEnabledProps,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<boolean> {
  const { action, flag, track } = props
  const flagsStore = useFlagsStore(store)
  if (action === 'isEnabled') {
    return Promise.resolve(flagsStore.isEnabled(flag, { track }))
  }
  throw new Error(`Unknown action ${action}`)
}

// Get the status of a particular flag
export const isFlagEnabled = (flag: string, track = false) => {
  return sendToRuntime<boolean>(key, { action: 'isEnabled', flag, track })
}
