import { Pinia } from 'pinia'
import { AUTOCAPTURE_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { AutocapturedItem, useAutocaptureStore } from '../stores/autocapture'
import { dismissAutocaptureModal } from '@/content-scripts/messageApis/onAutocaptureModalMessage'
import { useItemsStore } from '@core/stores/items'
import { DecryptedItem, setInMain } from '@core/clients/tilig'

export const key = AUTOCAPTURE_WORKER_MESSAGE

export type AutocaptureMessage =
  | AutocaptureDismissMessage
  | AutocaptureUndoMessage
  | AutocaptureGetMessage

/**
 * Explicitly 'dismiss' the item from the autocapture store
 * and prevent the modal from loading on future pages
 */
export type AutocaptureDismissMessage = {
  action: 'dismiss'
}

/**
 * A user may choose to 'undo' an autocapture by sending this message
 * along with the id of the item that needs to be undone.
 */
export type AutocaptureUndoMessage = {
  action: 'undo'
  id: string
}

/**
 * This message handler involves messages from the content-script about the
 * autocaptured items stored in the background.
 * On page load, we check to see if there are any autocaptured items for the
 * current tab. If so, we will display a modal on the current page.
 */
export type AutocaptureGetMessage = {
  action: 'get'
}

export async function handler(
  msg: AutocaptureDismissMessage,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<void>

export async function handler(
  msg: AutocaptureUndoMessage,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<void>

export async function handler(
  msg: AutocaptureGetMessage,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<AutocapturedItem | null>

export async function handler(
  message: AutocaptureMessage,
  tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<AutocapturedItem | null | void> {
  const { action } = message
  const autocaptureStore = useAutocaptureStore(store)
  const itemStore = useItemsStore(store)
  if (action === 'get') {
    return autocaptureStore.getAutocaptureItem(tab)
  } else if (action === 'dismiss') {
    if (!tab) throw new Error('Autocapture message api recieved a message from null tab')
    autocaptureStore.dismiss()
    dismissAutocaptureModal(tab)
  } else if (action === 'undo') {
    const { id } = message
    if (!tab) throw new Error('Autocapture message api recieved a message from null tab')

    // Quickly dismiss the autocapture modal. Don't wait for the request to complete
    dismissAutocaptureModal(tab)

    const item = await itemStore.getItemById(id)
    if (item) {
      const oldPassword = getRecentPasswordChange(item)
      if (oldPassword) {
        setInMain(item.details, 'password', oldPassword)
        await itemStore.updateItem(id, item.overview, item.details)
      } else if (isNewItem(item)) {
        // Be cautious before deleting an item, only delete items that are confirmed as 'new'
        await itemStore.deleteItem(id)
      } else {
        throw new Error(`Tried to 'undo' an item that can't be rolled back or deleted: id(${id})`)
      }
    }
    autocaptureStore.dismiss()
  }
}

// See if we have any autocaptured data for the current tab to display
export const getAutocaptureItem = () => {
  return sendToRuntime<AutocapturedItem>(key, { action: 'get' })
}

// Dismiss the autocaptured item
export const dismissAutocaptureItem = () => {
  return sendToRuntime<void>(key, { action: 'dismiss' })
}

// Undo/delete the autocaptured item
export const undoAutocaptureItem = (id: string) => {
  return sendToRuntime<void>(key, { action: 'undo', id })
}

const getRecentPasswordChange = (item: DecryptedItem): string | null => {
  if (item.details.history.length > 0) {
    const mostRecentChange = item.details.history[0]
    if (mostRecentChange.kind === 'password') {
      return mostRecentChange.value
    }
  }
  return null
}

const isNewItem = (item: DecryptedItem): boolean => {
  return item.details.history.length === 0
}
