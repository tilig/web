import { Pinia } from 'pinia'
import { TAB_API_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'

export const key = TAB_API_WORKER_MESSAGE

export type TabApiProps = {
  action: 'close' | 'get-parent-tab'
}

// API for allowing the content-scripts to get information about active tabs and take some
// basic actions on those tabs
export const handler = (
  { action }: TabApiProps,
  tab: chrome.tabs.Tab | null,
  _store: Pinia
): chrome.tabs.Tab | void => {
  if (!tab) return
  if (action === 'close') {
    if (tab.id) chrome.tabs.remove(tab.id)
  } else if (action === 'get-parent-tab') {
    return tab
  }
  return
}

// Close the tab of the sender
export const closeTab = () => {
  return sendToRuntime<boolean>(key, { action: 'close' })
}

// Get the parent tab of the sender
// Use case: Sender is the dropdown iframe and needs to know the tab/parent url
// iframe security disallow's access to the parent url, so this is the only
// way to get the tab url from the dropdown
export const getParentTab = async (): Promise<chrome.tabs.Tab | null> => {
  const tab = await sendToRuntime<chrome.tabs.Tab | null>(key, { action: 'get-parent-tab' })
  if (tab && tab.id) return tab
  return null
}
