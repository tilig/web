import { Pinia } from 'pinia'
import { parseDomain } from '@core/utils/domainUtils'
import { createLogger } from '@core/utils/logger'
import { DecryptedItem, findInMain, initializeItem, setInMain } from '@core/clients/tilig'
import { ItemState, useItemsStore } from '@core/stores/items'
import { launchAutocaptureModal } from '@/content-scripts/messageApis/onAutocaptureModalMessage'
import { useDraftStore } from '@/background/stores/draft'
import { SAVE_DRAFT_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { titleToName } from '@/utils/titleToName'
import { useAutocaptureStore } from '../stores/autocapture'
import { ref } from 'vue'
import { useLoginForm } from '@core/composables/forms'
import {
  BROWSER_EXTENSION_AUTOSAVE_NEW_LOGIN,
  BROWSER_EXTENSION_AUTOSAVE_UPDATE_LOGIN,
} from '@core/utils/trackingConstants'
import { useTrackingStore } from '@core/stores/tracking'

const logger = createLogger('background:messageAPI:safeDraft')

export const key = SAVE_DRAFT_WORKER_MESSAGE

type BuildDecryptedItemArgs = {
  username: string
  password: string
  title: string
  domain: string
}
const buildDecryptedItem = ({
  username,
  password,
  title,
  domain,
}: BuildDecryptedItemArgs): DecryptedItem => {
  // Build draft item

  const draftItem = ref(initializeItem())
  const item = useLoginForm(draftItem)

  item.name.value = title
  item.website.value = domain
  item.username.value = username
  item.password.value = password
  return draftItem.value
}

const attemptUpdateItem = async (
  store: Pinia,
  match: ItemState,
  password: string
): Promise<string | null> => {
  const itemStore = useItemsStore(store)
  const trackingStore = useTrackingStore(store)

  logger.debug('Updating a matched item')
  const decrypted = await itemStore.getItemById(match.item.id)
  if (!decrypted || !decrypted.details) throw new Error(`Unable to decrypt ${match.item.id}`)
  const oldPassword = findInMain(decrypted?.details, 'password')
  if (oldPassword !== password) {
    setInMain(decrypted.details, 'password', password)
    if (await itemStore.updateItem(match.item.id, decrypted.overview, decrypted.details)) {
      logger.debug('Updated a matched item', match.item.id)
      await trackingStore.trackEvent({
        event: BROWSER_EXTENSION_AUTOSAVE_UPDATE_LOGIN,
      })
      return match.item.id
    }
  } else {
    logger.debug('Password has not changed, not updating item', match.item.id)
  }
  return null
}

// Create a new item from autocatpure data
const createItem = async (
  store: Pinia,
  username: string,
  password: string,
  domain: string,
  title: string
): Promise<string | null> => {
  const itemStore = useItemsStore(store)
  const trackingStore = useTrackingStore(store)

  // Build draft item
  const draftItem = buildDecryptedItem({
    username,
    password,
    domain,
    title,
  })
  const item = await itemStore.createItem(draftItem)
  if (item) {
    logger.debug('Created a new item', item.item.id)
    await trackingStore.trackEvent({
      event: BROWSER_EXTENSION_AUTOSAVE_NEW_LOGIN,
    })
    return item.item.id
  }
  return null
}

export const attemptSaveDraft = async (store: Pinia, tab: chrome.tabs.Tab) => {
  const itemStore = useItemsStore(store)
  const draftStore = useDraftStore(store)
  const autocaptureStore = useAutocaptureStore(store)
  const draft = draftStore.byTabDomain(tab)

  if (!draft) return

  const username = draft.username?.trim() || ''
  const password = draft.password
  if (!username || username.length === 0) return
  if (!password || password.length === 0) return

  const domain = parseDomain(tab.url || null)
  if (!domain) return

  logger.debug(`Found a draft for ${domain}:${username}, attempting to autosave/autoupdate`)

  const match = itemStore.itemStates.find((item) => {
    const matchingUrls = item.overview?.urls?.find((url) => {
      if (url && parseDomain(url.url) === domain) {
        if (item.overview?.info?.trim() === username) return true
      }
    })
    return !!matchingUrls
  })

  // Update or create the autocaptured item
  let itemId = null
  if (match) {
    logger.debug(`Found a match for ${match.item.username}, attempting to update`)
    itemId = await attemptUpdateItem(store, match, password)
  } else {
    logger.debug(`No existing match for ${domain}:${username}, adding a new item`)
    const title = titleToName(tab.url || '', tab.title || '') ?? domain
    itemId = await createItem(store, username, password, domain, title)
  }
  if (itemId) {
    // Flag this item as autocaptured, used by the iframe modal
    autocaptureStore.update(itemId, domain)
    // Remove the draft
    draftStore.delete(tab)
    // Launch the model on the current page
    launchAutocaptureModal(tab)
  }
}

export const handler = async (
  { saveType }: SaveDraftProps,
  tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<void> => {
  if (!tab) return

  // See if we have draft values to save
  if (saveType === 'login') {
    return attemptSaveDraft(store, tab)
  } else if (saveType === 'signup') {
    // Do nothing for the first iteration
  }
}

type SaveDraftProps = {
  saveType: 'signup' | 'login'
}

/**
 * Send a message to save the current draft state for the sending tab
 * Should happen when we capture a login or signup event.
 */
export const sendSaveDraft = ({ saveType }: SaveDraftProps) => {
  return sendToRuntime<void>(key, { saveType })
}
