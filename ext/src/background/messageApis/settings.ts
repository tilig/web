import { Pinia } from 'pinia'
import { SETTINGS_API_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { SettingsState, useSettingsStore } from '../stores/settings'
import { addAutocaptureListener } from '../addAutocaptureListener'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('ext:messageApi:settings')

export const key = SETTINGS_API_WORKER_MESSAGE

type SettingsProps = GetSettingsProps | SetAutosaveLoginProps | SetGifsEnabledProps

export type GetSettingsProps = {
  action: 'get'
}

export type SetAutosaveLoginProps = {
  action: 'setAutosaveLogin'
  value: boolean
}

export type SetGifsEnabledProps = {
  action: 'setGifsEnabled'
  value: boolean
}

export async function handler(
  msg: GetSettingsProps,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<SettingsState>

export async function handler(
  msg: SetAutosaveLoginProps,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<SettingsState>

// API for allowing the content-scripts to get information about active tabs and take some
// basic actions on those tabs
export async function handler(
  props: SettingsProps,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<SettingsState> {
  const { action } = props
  const settingsStore = useSettingsStore(store)
  if (action === 'get') {
    return settingsStore.serializedState
  } else if (action === 'setAutosaveLogin') {
    settingsStore.setAutosaveLogin(props.value)
    if (settingsStore.autosaveLogin) {
      addAutocaptureListener(store)
    }
    return settingsStore.serializedState
  } else if (action === 'setGifsEnabled') {
    logger.debug('SET GIFS ENABLED')
    settingsStore.setGifsEnabled(props.value)
    return settingsStore.serializedState
  }
  throw new Error(`Unknown action ${action}`)
}

// Get the state of settings
export const getSettings = () => {
  return sendToRuntime<SettingsState>(key, { action: 'get' })
}

// Update the state of settings
export const setAutosaveLogin = (on: boolean) => {
  return sendToRuntime<SettingsState>(key, { action: 'setAutosaveLogin', value: on })
}

// Update the state of settings
export const setGifsEnabled = (on: boolean) => {
  return sendToRuntime<SettingsState>(key, { action: 'setGifsEnabled', value: on })
}
