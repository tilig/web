import { Pinia } from 'pinia'
import { createLogger } from '@core/utils/logger'
import { useAuthStore } from '@core/stores/auth'
import { ItemState, ItemType, useItemsStore } from '@core/stores/items'
import { parseDomain } from '@core/utils/domainUtils'
import {
  DecryptedItem,
  DecryptedDetails,
  DecryptedOverview,
  initializeItem,
  EncryptedItem,
  Template,
  Share,
  PublicKeyResult,
} from '@core/clients/tilig'
import { ACCOUNTS_STORE_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { queryActiveTab } from '@/utils/tabUtils'
import { useFoldersStore } from '@core/stores/folders'

const logger = createLogger('background:messageAPI:accounts')

// Message to pass back to a Dropdown
export type AccountListResponse = {
  accounts: ItemState[]
  offset?: number
  totalAvailable: number
  error?: string | null
}

export type AccountStoreMessage =
  | AccountStoreFetchMessage
  | AccountStoreReadMessage
  | AccountStoreDeleteMessage
  | AccountStoreCreateMessage
  | AccountStoreUpdateMessage
  | AccountStoreCreateShareMessage
  | AccountStoreDeleteShareMessage
  | AccountStoreGetFolderMemberPublicKeyMessage
  | AccountStoreAddFolderMembershipMessage
  | AccountStoreRevokeFolderMembershipMessage
  | AccountStoreRefreshItemMessage

export type AccountStoreFetchMessage = {
  action: 'fetch'
  fetchQuery: AccountFetchQuery
}

export type AccountStoreReadMessage = {
  action: 'read'
  id: string
}

export type AccountStoreRefreshItemMessage = {
  action: 'refresh'
  id: string
}

export type AccountStoreDeleteMessage = {
  action: 'delete'
  id: string
}

export type AccountStoreCreateMessage = {
  action: 'create'
  template: Template
  overview: DecryptedOverview
  details: DecryptedDetails
}

export type AccountStoreUpdateMessage = {
  action: 'update'
  id: string
  overview: DecryptedOverview
  details: DecryptedDetails
}

export type AccountFetchQuery = {
  forceUpdate?: boolean
  onlyCurrentTab?: boolean
  searchString?: string | null
  itemType?: ItemType | null
  limit?: number
  offset?: number
}

export type AccountStoreCreateShareMessage = {
  action: 'create-share'
  item: EncryptedItem
}

export type AccountStoreDeleteShareMessage = {
  action: 'delete-share'
  id: string
}

export type AccountStoreGetFolderMemberPublicKeyMessage = {
  action: 'get-public-key'
  email: string
}

export type AccountStoreAddFolderMembershipMessage = {
  action: 'add-folder-membership'
  id: string
  folderMember: PublicKeyResult
}

export type AccountStoreRevokeFolderMembershipMessage = {
  action: 'revoke-folder-membership'
  id: string
  memberId: string
}

export const SUPPORTED_TEMPLATES = [
  Template.Login,
  Template.SecureNote,
  Template.WiFi,
  Template.CreditCard,
  Template.Custom,
]

const onlySupportedTemplates = (itemState: ItemState): boolean => {
  const template = itemState.item.template
  if (template && SUPPORTED_TEMPLATES.includes(template)) return true
  return false
}

export const key = ACCOUNTS_STORE_WORKER_MESSAGE

export const fetchHandler = async (
  store: Pinia,
  originTab: chrome.tabs.Tab | null,
  query: AccountFetchQuery
): Promise<AccountListResponse> => {
  const authStore = useAuthStore(store)
  const accountsStore = useItemsStore(store)

  const results: AccountListResponse = {
    accounts: [],
    totalAvailable: 0,
    error: null,
  }

  if (!authStore.isAuthenticated) {
    results.error = 'Must be authenticated to fetch accounts'
    return results
  }

  try {
    if (!accountsStore.isLoaded || accountsStore.itemStates.length == 0 || query.forceUpdate) {
      await accountsStore.fetchItems()
    }
    const offset = query.offset || 0
    const limit = query.limit || 50
    if (query.onlyCurrentTab) {
      const tab = originTab || (await queryActiveTab())
      if (tab && tab.url) {
        const domain = parseDomain(tab.url)
        // TODO: filter secrets by url match
        const domainAccounts = accountsStore.itemStates
          .filter((a) => {
            return a.overview?.urls.some((u) => u.url && parseDomain(u.url) == domain)
          })
          .filter(onlySupportedTemplates)
        results.accounts = domainAccounts.slice(offset, offset + limit)
        results.totalAvailable = domainAccounts.length
      }
    } else if (query.searchString) {
      accountsStore.$patch({ query: query.searchString })
      results.accounts = accountsStore.filteredItems.slice(offset, offset + limit)
      results.totalAvailable = accountsStore.filteredItems.filter(onlySupportedTemplates).length
    } else {
      results.accounts = accountsStore.itemStates.slice(offset, offset + limit)
      results.totalAvailable = accountsStore.itemStates.filter(onlySupportedTemplates).length
    }
    return results
  } catch (error) {
    logger.debug('Error fetching credentials for dropdown', error)
  }
  return results
}

export async function handler(
  msg: AccountStoreFetchMessage,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<AccountListResponse>

export async function handler(
  msg: AccountStoreReadMessage,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<DecryptedItem | null>

export async function handler(
  msg: AccountStoreDeleteMessage,
  _tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<boolean>

// TODO: clean up return signature (tie to action)
export async function handler(
  message: AccountStoreMessage,
  tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<
  | AccountListResponse
  | DecryptedItem
  | null
  | boolean
  | string
  | { share_link: Share }
  | PublicKeyResult
  | void
> {
  const { action } = message
  switch (action) {
    case 'fetch': {
      const { fetchQuery } = message
      return fetchHandler(store, tab, fetchQuery)
    }

    case 'read': {
      const accountsStore = useItemsStore(store)
      const { id } = message
      const item = await accountsStore.getItemById(id)
      return item
    }

    case 'refresh': {
      const accountsStore = useItemsStore(store)
      const { id } = message
      return await accountsStore.refreshItem(id)
    }

    case 'delete': {
      const accountsStore = useItemsStore(store)
      const { id } = message
      return accountsStore.deleteItem(id)
    }

    case 'update': {
      const accountsStore = useItemsStore(store)
      const { id, overview, details } = message
      return accountsStore.updateItem(id, overview, details)
    }

    case 'create': {
      const accountsStore = useItemsStore(store)
      const { template, overview, details } = message
      const decryptedAccount: DecryptedItem = initializeItem({ overview, details })
      decryptedAccount.item.template = template

      const accountState = await accountsStore.createItem(decryptedAccount)
      const newId = accountState?.item.id
      if (newId) {
        await accountsStore.updateItem(newId, overview, details)
        return newId
      }
      return false
    }

    case 'create-share': {
      const accountsStore = useItemsStore(store)
      const { item } = message
      return accountsStore.createShare(item)
    }

    case 'delete-share': {
      const accountsStore = useItemsStore(store)
      const { id } = message
      return accountsStore.deleteShare(id)
    }

    case 'get-public-key': {
      const folders = useFoldersStore(store)
      const { email } = message
      return folders.getFolderMemberPublicKey(email)
    }

    case 'add-folder-membership': {
      const accountsStore = useItemsStore(store)
      const { folderMember, id } = message
      return accountsStore.addFolderMembership(id, folderMember)
    }

    case 'revoke-folder-membership': {
      const foldersStore = useFoldersStore(store)
      const { memberId, id } = message
      return foldersStore.revokeFolderMembership(id, memberId)
    }

    default:
      throw new Error('Unable to perform action')
  }
}

// Get the credentials belonging to the domain of the current/calling tab
export const fetchItems = async (fetchQuery: AccountFetchQuery) => {
  const message: AccountStoreMessage = {
    action: 'fetch',
    fetchQuery,
  }
  return sendToRuntime<AccountListResponse>(key, message)
}

export const getItem = (id: string) => {
  const message: AccountStoreReadMessage = {
    action: 'read',
    id,
  }
  return sendToRuntime<DecryptedItem>(key, message)
}

export const refreshItem = (id: string) => {
  const message: AccountStoreRefreshItemMessage = {
    action: 'refresh',
    id,
  }
  return sendToRuntime<boolean>(key, message)
}

export const deleteItem = (id: string) => {
  const message: AccountStoreDeleteMessage = {
    action: 'delete',
    id,
  }
  return sendToRuntime<boolean>(key, message)
}

export const updateItem = (id: string, overview: DecryptedOverview, details: DecryptedDetails) => {
  const message: AccountStoreUpdateMessage = {
    action: 'update',
    id,
    overview,
    details,
  }
  return sendToRuntime<boolean>(key, message)
}

export const createItem = async (
  template: Template,
  overview: DecryptedOverview,
  details: DecryptedDetails
): Promise<string | false> => {
  const message: AccountStoreCreateMessage = {
    action: 'create',
    template,
    overview,
    details,
  }
  return sendToRuntime<string | false>(key, message)
}

export const createShare = (item: EncryptedItem) => {
  const message: AccountStoreCreateShareMessage = {
    action: 'create-share',
    item,
  }
  return sendToRuntime<{ share_link: Share }>(key, message)
}

export const deleteShare = (id: string) => {
  const message: AccountStoreDeleteShareMessage = {
    action: 'delete-share',
    id,
  }
  return sendToRuntime<boolean>(key, message)
}

export const getFolderMemberPublicKey = (email: string) => {
  const message: AccountStoreGetFolderMemberPublicKeyMessage = {
    action: 'get-public-key',
    email,
  }
  return sendToRuntime<PublicKeyResult>(key, message)
}

export const addFolderMembership = (id: string, folderMember: PublicKeyResult) => {
  const message: AccountStoreAddFolderMembershipMessage = {
    action: 'add-folder-membership',
    id,
    folderMember,
  }
  return sendToRuntime<void>(key, message)
}

export const revokeFolderMembership = (id: string, memberId: string) => {
  const message: AccountStoreRevokeFolderMembershipMessage = {
    action: 'revoke-folder-membership',
    id,
    memberId,
  }
  return sendToRuntime<void>(key, message)
}
