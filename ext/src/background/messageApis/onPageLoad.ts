import { Pinia } from 'pinia'
import { createLogger } from '@core/utils/logger'
import { launchAutocaptureModal } from '@/content-scripts/messageApis/onAutocaptureModalMessage'
import { ON_PAGE_LOAD_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { useAutocaptureStore } from '../stores/autocapture'

const logger = createLogger('background:messageAPI:onPageLoad')

export const key = ON_PAGE_LOAD_WORKER_MESSAGE

// Handle incoming page load notifications and take action depending on the tab data
export const handler = (
  _data: any,
  tab: chrome.tabs.Tab | null,
  store: Pinia
): chrome.tabs.Tab | void => {
  if (!tab) return
  const autoCaptureStore = useAutocaptureStore(store)
  const autoCapture = autoCaptureStore.getAutocaptureItem(tab)
  if (autoCapture) {
    logger.debug('Launching autocapture')
    launchAutocaptureModal(tab)
  }
  return
}

// Let the background worker know we have loaded a new page
// Used for modal that may appear on subsequent page loads
// e.g. Autocapture Modal
export const sendOnPageLoad = () => {
  return sendToRuntime<void>(key)
}
