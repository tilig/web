import { Pinia } from 'pinia'
import { parseDomain } from '@core/utils/domainUtils'
import { useItemsStore } from '@core/stores/items'
import { findInMain } from '@core/clients/tilig'
import { ATTEMPT_AUTOFILL_WORKER_MESSAGE } from '@/utils/messageConstants'
import { sendToRuntime } from '@/utils/messageSender'
import { dispatchFillFocussedForm, fillOnTab } from '../fillUtils'
import { useDraftStore } from '../stores/draft'

export const key = ATTEMPT_AUTOFILL_WORKER_MESSAGE

/**
 * AutoFill the first account we can find for the current website.
 */
export const handler = async (
  props: AttemptAutofillLoginProps,
  tab: chrome.tabs.Tab | null,
  store: Pinia
): Promise<boolean> => {
  // Domain can be undefined, for example for local files
  const accountsStore = useItemsStore(store)
  const draftStore = useDraftStore(store)
  if (!tab || !tab.url) return false
  const domain = parseDomain(tab.url) || ''
  const draft = draftStore.byTabDomain(tab)

  let decryptedAccount = null
  if (props.id) {
    decryptedAccount = await accountsStore.getItemById(props.id)
  } else {
    const domain = parseDomain(tab.url) || ''
    const domainAccounts = accountsStore.itemStates.filter((a) => {
      return a.overview?.urls.some((u) => u.url && parseDomain(u.url) == domain)
    })
    if (domainAccounts.length > 1) {
      // If the user selected an account on a previous page, use that
      const selectedUsername = draft?.username
      const matchingDraftAccount = domainAccounts.find((a) => selectedUsername === a.overview?.info)
      const domainAccount = matchingDraftAccount || domainAccounts[0]
      decryptedAccount = await accountsStore.getItemById(domainAccount.item.id)
    } else {
      decryptedAccount = await accountsStore.getItemById(domainAccounts[0].item.id)
    }
  }

  // If there's no account for the page, return false and stop trying to autofill
  if (!decryptedAccount) return false

  draftStore.update({
    data: {
      username: findInMain(decryptedAccount.details, 'username') || '',
    },
    tab,
  })

  const autoFillDetails = {
    domain,
    username: findInMain(decryptedAccount.details, 'username') ?? '',
    password: findInMain(decryptedAccount.details, 'password') ?? '',
    trigger: props.trigger,
    userInitiated: props.userInitiated,
  }

  if (props.focusedForm) {
    dispatchFillFocussedForm(autoFillDetails)
  } else {
    fillOnTab(tab, autoFillDetails)
  }
  // Return true if we have credentials, indicates the autofill can keep trying to fill as page loads
  return true
}

type AttemptAutofillLoginProps = {
  userInitiated: boolean
  focusedForm?: boolean
  trigger: string
  id?: string
}

// Request autofill on the current page
export const attemptAutofillLogin = (props: AttemptAutofillLoginProps) => {
  return sendToRuntime<boolean>(key, props)
}
