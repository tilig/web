import { Pinia } from 'pinia'
import { createLogger } from '@core/utils/logger'
import { matchWebRequestBodyToPassword } from '@/utils/webRequestUtils'
import { attemptSaveDraft } from './messageApis/saveDraft'
import { useDraftStore } from './stores/draft'
import { useSettingsStore } from './stores/settings'

/**
 * We need to inspect outgoing web requests (beforeRequest) along with the
 * response from the request (onCompleted).
 *
 * On the outoing request we check the draft store to see if any piece of
 * the request body or form fields match a password. If there's a match
 * we store the request ID in the ring buffer.
 *
 * When a request completes we quickly check the ring buffer to see if
 * it's a request we care about (stored ID from beforeRequest). Then
 * we look at the request to see if it looks like it was succesful.
 *
 * e.g. An XHR request that returns <400 or a form POST that results in
 * a 302 to a different URL vs going back to the same URL (implying a
 * failed login attempt)
 */

export const RING_BUFFER_MAX = 50

let listenersAdded = false

export type BeforeWebRequest = Pick<
  chrome.webRequest.WebRequestBodyDetails,
  'tabId' | 'requestBody' | 'requestId' | 'type' | 'url'
>

export type CompletedWebRequest = Pick<
  chrome.webRequest.WebResponseCacheDetails,
  'tabId' | 'requestId' | 'type' | 'url' | 'statusCode'
>

type RequestDetails = {
  url: string
  xhr: boolean
}

const createRingBuffer = <T>(length: number) => {
  let pointer = 0
  const keys: Array<string> = new Array(length)
  const values: Array<T> = new Array(length)

  return {
    keys,
    values,
    contains: (key: string): boolean => keys.includes(key),
    get: (key: string): T | null => values[keys.indexOf(key)] || null,
    add: (key: string, details: T): void => {
      keys[pointer] = key
      values[pointer] = details
      pointer = (pointer + 1) % length
    },
  }
}

export const validAuthRequests = createRingBuffer<RequestDetails>(RING_BUFFER_MAX)

export const onBeforeRequestListener = (store: Pinia) => {
  const settingStore = useSettingsStore(store)
  const draftStore = useDraftStore(store)

  return (req: BeforeWebRequest) => {
    try {
      if (!settingStore.autosaveLogin) return

      // Request from background are also caught as tab.id == -1
      if (req.tabId < 0) return

      if (!req.requestBody) return

      const draft = draftStore.byTabDomain({ id: req.tabId, url: req.url })
      if (!(draft && draft.password) || draft.ignoreAutosave) return
      logger.debug(`OnBeforeRequest has draft credentials ${req.url}`)

      if (matchWebRequestBodyToPassword(req.requestBody, draft.password)) {
        logger.debug(`OnBeforeRequest valid request ${req.url}`)
        validAuthRequests.add(req.requestId, {
          xhr: req.type === 'xmlhttprequest',
          url: req.url,
        })
      }
    } catch (err) {
      logger.error(`Error intercepting request ${err}`)
    }
  }
}

export const onCompletedListener = (store: Pinia) => {
  return (req: CompletedWebRequest) => {
    try {
      // Only fire for requests we marked as valid in the previous listenerh
      const requestDetails = validAuthRequests.get(req.requestId)
      if (!requestDetails) return
      logger.debug('Oncomplete Request', requestDetails, req)

      // NOOP on anything >= 400 to prevent autocapturing bad passwords
      if (req.statusCode >= 400) {
        logger.debug(`Oncomplete Request - Bad status code ${req.statusCode}`)
        return
      }

      /**
       * For form submissions, see if the url stayed the same and if it did
       * then we can assume the login failed and abort the autocapture save.
       * BE AWARE: The COMPLETED request includes the url of the final
       * destination of the redirect and the status code of that destination.
       * onBeforeRedirect has access to the original url and status code.
       *
       * e.g. POST '/login' 302 => '/success' 200 - Will result in a completed
       * request of { url: '/success', statusCode: 200}
       * */
      if (!requestDetails.xhr && requestDetails.url === req.url) {
        logger.debug(
          `Oncomplete Request - Form submission failed - Same url after successful POST ${req.url} === ${requestDetails.url}`
        )
        return
      }

      chrome.tabs.get(req.tabId, (tab) => {
        if (!tab) return

        logger.debug(`Attempting to save a draft for ${tab.url}`)
        /**
         * Make a call to attemptSaveDraft, which will look at the draft and
         * make a determination whether it can actually save or update an item.
         */
        attemptSaveDraft(store, tab)
      })
    } catch (err) {
      logger.error(`Error intercepting request ${err}`)
    }
  }
}

const logger = createLogger('background:autocaptureListener')

export const addAutocaptureListener = (store: Pinia): boolean => {
  /**
   * In order to have access to 'webRequest' (which is an optional permisssion)
   * the user must grant the permission, which must be fired from a UI event/context.
   * We can't ask for this permission on the backend and can only check for it, and
   * fail gracefully when it's not available.
   */
  if (chrome.webRequest) {
    // Ensure that we only add these listeners once
    if (listenersAdded) return true

    // We only care about xhr requests and regular form submission ('main_frame') types
    chrome.webRequest.onBeforeRequest.addListener(
      onBeforeRequestListener(store),
      { types: ['xmlhttprequest', 'main_frame'], urls: ['*://*/*', '<all_urls>'] },
      ['requestBody']
    )
    logger.debug('Added a listener to onBeforeRequest')

    chrome.webRequest.onCompleted.addListener(onCompletedListener(store), {
      types: ['xmlhttprequest', 'main_frame'],
      urls: ['*://*/*', '<all_urls>'],
    })
    logger.debug('Added a listener to onCompleted')
    listenersAdded = true
    return true
  }
  logger.debug('webRequest unavailable: autocapture disabled')
  return false
}
