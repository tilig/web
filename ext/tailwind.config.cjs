const tailwindConfig = require('../core/tailwind.config.cjs')

module.exports = {
  ...tailwindConfig,
  content: [
    'index.html',
    './src/**/*.{vue,js,ts,jsx,tsx,html}',
    '../core/src/**/*.{vue,js,ts,jsx,tsx}',
  ],
  variants: {
    extend: {
      display: ['group-hover'],
    },
  },
}
