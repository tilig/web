/// <reference types="vitest" />
import { fileURLToPath, URL } from 'node:url'

import { defineConfig, Plugin } from 'vite'
import vue from '@vitejs/plugin-vue'
import webExtension from '@samrum/vite-plugin-web-extension'
import svgLoader from 'vite-svg-loader'
import icons from 'unplugin-icons/vite'

// Get version of extension to store in defined ENV APP_VERSION

import * as packageJson from './package.json'
import { buildManifest } from './manifest'

const APP_VERSION = JSON.stringify(packageJson.version)
const COMMIT_REF = JSON.stringify(process.env.COMMIT_REF || 'DEVELOPMENT')

/**
 * Note, vite use .env.* files to populate import.meta.env, but that won't be
 * available here, only client side. You'll need to set these evironment variables
 * manually if you want to use them here from process.env
 */
const VITE_DEVSERVER = process.env['VITE_DEVSERVER'] || 'localhost:5173'
const VITE_APIROOT = process.env['VITE_APIROOT']

export default defineConfig(({ command, mode }) => {
  /**
   * In HMR mode (`vite serve`), we need to ensure that we force all assets to load via localhost
   * This is because the origin is "chome-extension://abc/ui/index.html" and not the "localhost:5173"
   * that vite expects it to be served from.
   */
  const devServer = command == 'serve' ? VITE_DEVSERVER : null
  const isProd = mode == 'production'

  // TODO: Make this not hard-coded
  const apiEndpoint =
    VITE_APIROOT || (isProd ? 'https://api.tilig.com/' : 'https://api-staging.tilig.com/')
  const firebaseEndpoint = isProd
    ? 'https://tilig-prod.firebaseapp.com/'
    : 'https://tilig-dev.firebaseapp.com/'

  return {
    plugins: [
      vue(),
      // @ts-expect-error Vite Plugin types are broken
      webExtension({
        manifest: buildManifest({ mode, apiEndpoint, firebaseEndpoint, devServer }),
      }) as Plugin,
      svgLoader({
        defaultImport: 'url',
      }),
      icons({ compiler: 'vue3' }),
    ],
    build: {
      sourcemap: true,
    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
        '@core': fileURLToPath(new URL('../core/src', import.meta.url)),
      },
    },
    define: {
      APP_VERSION,
      COMMIT_REF,
    },
    server: {
      origin: devServer ? `http://${devServer}` : null,
    },
    test: {
      clearMocks: true,
      environment: 'jsdom',
      globals: true,
    },
  }
})
