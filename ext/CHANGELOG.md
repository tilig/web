# Tilig Browser Extension Changelog

## 3.11.0

- [New extension layout](https://gitlab.com/subshq/clients/webapp/-/merge_requests/812)
- [Fix Extension search filter with item type](https://gitlab.com/subshq/clients/webapp/-/merge_requests/840)
- [Extension Getting started banner redesign](https://gitlab.com/subshq/clients/webapp/-/merge_requests/843)

## 3.10.0

- Clean up async message handling !835
- Login Item shared with web, supports custom fields now !827
- Better handle change passwords !805
- Improve dropdown UI !813
- Setting page updates !837
- Don't track web credential handoff until fully authenticated !833
- When the user accepts a generated password, don't autocapture !823

## 3.9.0

- Support all item types !800
- Change item limit from 50 to 1000 !804
- Shared Edit screen !786
- I was born before 1999 !789
- Remove useBroadcast debugging and fix Sentry logging !799

## 3.8.0

- Fix 2FA on login items !797
- Form bugfix !796
- Input color fixes !783
- Update credentials in modal !780
- Small UI fixes Extension !782
- Color inputs !783

## 3.7.5

- Save Modal bug fixes !760
- Signout tracking fixes !767
- Route to onboarding when user clicks on explore tilig !775
- Fix caching issue with Item page !784

## 3.7.3

- Input detection improvements !753
- Autocaptures improvements !753

## 3.7.2

- Improvements to form detection !742
- Trigger input events !742
- Update icon to circle !742
- Don't signout on render login page !747
- UI copy changes !748

## 3.7.1

- Autofill everywhere !723
- Limit localhost to dev !721

## 3.7.0

- Fix logo issues with Tilig icon
- Autosave toggle fix and minor improvements
- Await signout tracking
- Add additional context to auth and items stores

## 3.6.0

- Fix autosave issues with new onboarding

## 3.5.1

- Css fixes to items, and add back Microsoft Icon that for some reason got overwritten by the Onboarding MR
- Anon tracking of Webrequest API status
- Add missing import icon

## 3.5.0

- Hotfix for password inputs with autocomplete === 'off'
- Extension changes for onboarding improvments

## 3.4.0

- Bugfix for emtpy URL #673
- Search result bugfix #672
- Add Sentry to background !639
- Add Sentry to more UI views and fix cancellation issue on failed API request !650 and #677
- Fix libsodium async usage !649
- Tracker auth capture from web !644
- Fix bug with Items with no URL !638
- Improved signup/login detection !621
- Google analytics tracking new user conversion !614

## 3.3.0

- Add secure notes and item templates to support more item types !591
- Standardize on composable to fix overview/details issues !591
- Add feature flags
  - Add Microsoft login behind a flag and fix extension flags !611
- Auto expanding textarea !607
- Unrestrict notes length in secure notes, but keep 190 limit in Login items !616

## 3.2.0

- Add Microsoft login !527
- Fix issue with logins not showing in list #589
- Check elements for aria-hidden to determine visibility #590
- Add img.tilig.com to the CSP policy #600
- Fix UI issue with Load More and secure notes #572
- Minor CSS fix for Welcome login with Microsoft button added

## 3.1.0

- Add settings page to Popup !498
- Search flicker fixes !533
- Remove SASS and ensure logged in users are displayed a welcome screen !529
- Better signup matching !535
- Move all tracking constants to core !525

## 3.0.2

- Implements autocapture, but it's turned off by default. Can be enabled by testers.
- Fixes #469 (closed) Uses new password generator
- Fixes #452 (closed) New lines in notes works
- Suggest an email in the username/email field of new credentials (Just suggests the users logged in email for now) See: #454
- Cursor on suggestions hover (also #454)
- Focus on Title field on New, and remove "Untitled" (#454)
- Invalidate the cache on app update, based on package.json version. This will solve most caching issues, as the cache will be recreated on any version update. Fixes #482 (closed)

## 3.0.0

- Migration to shared Pinia stores and v3 Encryption
- Update to Items store to allow incremental updates and persistence
- Fix Firebase "Logged-out" bug by ensuring Firebase only initializes when needed, not on import
- Allow for authentication handoff from popup to background
- All state is maintained by background.ts, with message passing from UI's for any backend interaction

## 2.0.1

- Bugfix load more button
- On Tilig Don't show "Add tilig.com" button and matches
- Add Empty Accounts page
- Dismiss dropdown on input

## 2.0.0

- [Implemented v3 of the redesign](https://gitlab.com/subshq/clients/webapp/-/merge_requests/358)

## 1.9.10

- [Improved dropdown signup/login detection](https://gitlab.com/subshq/clients/webapp/-/merge_requests/360/)

## 1.9.9

- [Hotfix for clipboard issue on password and notes + minor fix for seach bar](https://gitlab.com/subshq/clients/webapp/-/merge_requests/355/)

## 1.9.8

### Notes

Most important changes:

- [Disable chrome password saving and address autofill automatically](https://gitlab.com/subshq/clients/webapp/-/merge_requests/305). The step that explains users how to do this manually as part of the onboarding is also removed.
- [Fix signed out dropdown](https://gitlab.com/subshq/clients/webapp/-/merge_requests/335)
- [Ext: Fix autofill events and add password events](https://gitlab.com/subshq/clients/webapp/-/merge_requests/345)

Invisible changes:

- [Move tab utils into one typescript file, update types](https://gitlab.com/subshq/clients/webapp/-/merge_requests/334)
- [Cleanup config for extension development](https://gitlab.com/subshq/clients/webapp/-/merge_requests/340)

## 1.9.7

### Notes

Most important changes:

- [Better signup detection](https://gitlab.com/subshq/clients/webapp/-/merge_requests/315)
- [Add debug logging](https://gitlab.com/subshq/clients/webapp/-/merge_requests/324)

## 1.9.6 / released

### Notes

Most important changes:

- [Fix 3rd party iframes](https://gitlab.com/subshq/clients/webapp/-/merge_requests/303)
- [Ext: Fix blank username by allowing content-script access to username](https://gitlab.com/subshq/clients/webapp/-/merge_requests/284)
