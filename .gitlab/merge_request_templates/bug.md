#### Issue reference

    Fixes #[issue_id]

#### What was the reason?

#### What is the solution?

#### Checklist

- [ ] I fixed|updated|added unit tests and integration tests for this fix (if applicable).
- [ ] No error nor warning in the console.
- [ ] I have attached a screenshot of the app before and after the fix (if applicable)
      [upload the screenshot here]
