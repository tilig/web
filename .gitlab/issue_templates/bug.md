#### Where was this bug discovered?

- [ ] Development
- [ ] Staging
- [ ] Production

#### How to reproduce the bug?

#### Expected Behaviour
