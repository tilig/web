module.exports = {
  '*.{cjs,js,ts,vue}': ['eslint --fix', 'prettier --write'],
  '*.{css,html,json,md,scss,yml}': 'prettier --write',
}
