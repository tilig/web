import { danger, markdown, schedule, warn } from 'danger'

if (danger.gitlab.mr.labels.length === 0) {
  warn(
    'This MR does not have any labels, please add `pkg::` labels to indicate which package this MR applies to.'
  )
}

const linesOfCodeDelta = async () => {
  const { diffForFile } = danger.git

  const [createdFilesDiffs, modifiedFilesDiffs, deletedFilesDiffs] = await Promise.all([
    Promise.all(danger.git.created_files.map((path) => diffForFile(path))),
    Promise.all(danger.git.modified_files.map((path) => diffForFile(path))),
    Promise.all(danger.git.deleted_files.map((path) => diffForFile(path))),
  ])

  let additions = createdFilesDiffs
    .map((diff) => (!diff ? 0 : diff.added === '' ? 0 : diff.added.split('\n').length))
    .reduce((mem, value) => mem + value, 0)
  let deletions = deletedFilesDiffs
    .map((diff) => (!diff ? 0 : diff.removed === '' ? 0 : diff.removed.split('\n').length))
    .reduce((mem, value) => mem + value, 0)
  const modifiedLines = modifiedFilesDiffs.map((diff) => [
    !diff ? 0 : diff.added === '' ? 0 : diff.added.split('\n').length,
    !diff ? 0 : diff.removed === '' ? 0 : diff.removed.split('\n').length,
  ])

  additions = modifiedLines.reduce((mem, value) => mem + value[0], additions)
  deletions = modifiedLines.reduce((mem, value) => mem + value[1], deletions)

  return additions - deletions
}

schedule(async () => {
  const locDelta = await linesOfCodeDelta()

  if (locDelta < -50) {
    markdown('Wow, you are really cleaning up the joint! :tada:\n/label ~"cleanup on aisle four"\n')
  }
})
