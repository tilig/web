module.exports = {
  colors: {
    transparent: 'transparent',
    // Commented are names Martijn used in his Sketch doc
    white: '#fff',
    black: '#000',
    gray: {
      darkest: '#A1A1BA', // Grey 5
      dark: '#B9B9DB', // Grey 4
      DEFAULT: '#D0D0DC', // Grey 2
      light: '#E8E8ED', // Grey 1
      lightest: '#F7F7F9', // Grey 0
    },
    blue: {
      darkest: '#05003F', // Blue dark 0 or blue 0
      dark: '#080066', // Blue dark 1 or blue 1
      DEFAULT: '#0C008F', // Blue dark 2 or blue 2
      light: '#413E6C', // Opacity 0.75 (used for some design elements)
      lightest: '#7E7B9C', // Opacity 0.5 (for use in input and many more things)
    },
    indigo: {
      // darkest: '#574AEE', // Old value?
      darkest: '#352ce1', // Button press, darkened 10% from dark
      dark: '#4E46E5', // button hover
      DEFAULT: '#695CFF', // Blue 1 (the main "blue" according to Martijn)
      light: '#C7C2FF', // Blue 2
      light2: '#978DFE',
      lighter: '#ECEBFF', // Blue 3
      lightest: '#F5F5FF ', // Blue 4
    },
    pink: {
      dark: '#FA385F', // button hover
      DEFAULT: '#FB607F', // Pink 0
      light: '#FC9CAF', // Pink 1
      lightest: '#FEEBEF', // Pink 2
    },
    green: {
      darkest: '#499274', // Green darkest (for hover, not in sketch doc yet)
      darker: '#52A382', // Green darker
      DEFAULT: '#91C7B1', // Green 0
      light: '#C9E4D9', // Green 1
      lightest: '#F1F8F5', // Green 2
    },
    red: {
      DEFAULT: '#F9103F', // Red warning
    },
    yellow: {
      dark: '#E0AC00', // button hover
      DEFAULT: '#FCC200', // Yellow 0
      light: '#FFE285', // Yellow 1
      lighter: '#FFFAEB', // Yellow 2
    },
    cyan: {
      DEFAULT: '#10c2e4',
    },
    gray2: {
      DEFAULT: '#827FA4',
    },
  },
  fontWeight: {
    book: 300,
    normal: 400,
    medium: 500,
    bold: 600,
  },
  fontSize: {
    xxs: ['0.625rem', '1.4'], // h9 - 10px - 14px
    xs: ['0.75rem', '1.33'], // h8 - 12px - 16px
    sm: ['0.875rem', '1.71'], // h7 - 14px - 24px
    base: ['1rem', '1.5'], // h6 - 16px - 24px
    lg: ['1.125rem', '1.6'], // h5 - 18px - 24px
    xl: ['1.25rem', '1.17'], // h4 - 20 - 28
    '2xl': ['1.5rem', '1.083'], // h3 - 24 - 26
    '3xl': ['1.75rem', '1.14'], // h2 - 28 - 32
    '4xl': ['2.5rem', '1.3'], // h1 - 40 - 52
  },
  boxShadow: {
    none: '0 0 #0000',
    xs: '0 1px 2px 0 rgba(0,0,0,0.25)',
    DEFAULT: '0 1px 4px 0 rgba(0, 0, 0, 0.2)',
    md: '0 4px 20px 0 rgba(0, 0, 0, 0.1)',
    lg: '0 4px 20px 0 rgba(0, 0, 0, 0.2)',
    xl: '0px 55px 55px -5px rgba(0,0,0,0.15);',
  },
  extend: {
    fontFamily: {
      sans: ['Stolzl2', 'ui-sans-serif', 'system-ui'],
      spoof: ['Spoof2'],
      stolzl: ['Stolzl2'],
      mono: ['SpaceMono', 'ui-monospace', 'SFMono-Regular'],
      'roboto-mono': ['RobotoMono'],
    },
    lineHeight: {
      form: '1.375rem', // Line height for form items and buttons
    },
    maxWidth: {
      xxs: '18rem', // 2tai40px
    },
    screens: {
      //Temp screensize to allow us switch login details to mobile layout early
      // We will remove this once we redesign the layout and remove the 3-columns
      '2md': '985px',
    },
    animation: {
      'spin-slow': 'spin 3s linear infinite',
      'ping-slow': 'ping 1.5s linear infinite',
    },
  },
}
