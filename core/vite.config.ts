import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import svgLoader from 'vite-svg-loader'

export default defineConfig({
  plugins: [
    vue(),
    svgLoader({
      defaultImport: 'url',
    }),
  ],
  resolve: {
    alias: {
      '@core': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  // @ts-expect-error vite complains about vitest types
  test: {
    environment: 'jsdom',
    exclude: ['lib'],
    setupFiles: [fileURLToPath(new URL('./vitest.start.ts', import.meta.url))],
  },
})
