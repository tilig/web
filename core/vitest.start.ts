import { vi } from 'vitest'
import { webcrypto } from 'node:crypto'

// Jsdom does not expose window.crypto. Luckily, node
// implements the Web Crypto API.
vi.stubGlobal('crypto', webcrypto)
