module.exports = {
  content: ['./src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: require('./theme.config.cjs'),
  plugins: [require('@tailwindcss/forms')],
}
