import { describe, test, expect, vi } from 'vitest'
import { render, fireEvent } from '@testing-library/vue'

import LoginItemDetails from '../LoginItemDetails.vue'
import { initializeItem, setInMain } from '@core/clients/tilig'
import { TrackingEvent } from '@core/utils/trackingConstants'

vi.mock('@core/utils/copyStringToClipboard')

const trackEventMock = vi.fn()
vi.mock('@core/stores/tracking', () => ({
  useTrackingStore: vi.fn(() => ({
    trackEvent: trackEventMock,
  })),
}))

const item = initializeItem()

item.overview.name = 'Spotify'
item.overview.urls = [{ url: 'https://clickme.com' }]
setInMain(item.details, 'username', 'John')
setInMain(item.details, 'password', '19803')
setInMain(item.details, 'totp', '39099')
item.details.notes = 'Notes here'

describe('Account details', () => {
  test('should render properly ', () => {
    const { getByText, unmount } = render(LoginItemDetails, {
      props: {
        item,
      },
    })

    expect(getByText('John')).toBeTruthy()
    unmount()
  })

  test('Renders the title label as Username if the username value is not email type', async () => {
    setInMain(item.details, 'username', 'John')
    const { findByText, unmount } = render(LoginItemDetails, {
      props: {
        item,
        saving: false,
      },
    })

    const label = await findByText('Username')
    expect(label.textContent).toBe('Username')
    unmount()
  })

  test('Renders the title label as Email if the username value is of email type', async () => {
    setInMain(item.details, 'username', 'john@example.com')
    const { findByText, unmount } = render(LoginItemDetails, {
      props: {
        item,
        saving: false,
      },
    })

    const label = await findByText('Email')
    expect(label.textContent).toBe('Email')
    unmount()
  })

  test('track event is triggered when the copy button is clicked', async () => {
    setInMain(item.details, 'username', 'johndoe')
    const { getAllByText, unmount } = render(LoginItemDetails, {
      props: {
        item,
      },
    })

    await fireEvent.click(getAllByText('Copy')[0])

    expect(trackEventMock).toHaveBeenLastCalledWith({
      event: TrackingEvent.USERNAME_COPIED,
    })
    unmount()
  })
})
