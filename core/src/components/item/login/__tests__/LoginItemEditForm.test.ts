import { describe, test, expect, vi } from 'vitest'
import { renderWithPinia } from '@core/utils/tests'

import LoginItemEditForm from '@core/components/item/login/LoginItemEditForm.vue'
import { initializeItem, setInMain } from '@core/clients/tilig'

vi.mock('@core/utils/copyStringToClipboard')

const trackEventMock = vi.fn()
vi.mock('@core/stores/tracking', () => ({
  useTrackingStore: vi.fn(() => ({
    trackEvent: trackEventMock,
  })),
}))

const item = initializeItem()

item.overview.name = 'Spotify'
item.overview.urls = [{ url: 'https://clickme.com' }]
setInMain(item.details, 'username', 'John')
setInMain(item.details, 'password', '19803')
setInMain(item.details, 'totp', '39099')
item.details.notes = 'Notes here'

describe('LoginItem Form', () => {
  test('should render properly ', async () => {
    const { findByText } = renderWithPinia(LoginItemEditForm, {
      props: {
        item,
        saving: false,
      },
    })

    const usernameInputLabel = await findByText('Username')
    const websiteInputLabel = await findByText('Website')

    expect(usernameInputLabel.textContent).toBe('Username')
    expect(websiteInputLabel.textContent).toBe('Website')
  })

  test('should render the username field label as email when the value is an email ', async () => {
    setInMain(item.details, 'username', 'johndoe@example.com')
    const { findByText } = renderWithPinia(LoginItemEditForm, {
      props: {
        item,
        saving: false,
      },
    })

    const emailLabel = await findByText('Email')
    expect(emailLabel.textContent).toBe('Email')
  })
})
