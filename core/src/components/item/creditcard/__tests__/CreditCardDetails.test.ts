import { describe, test, expect, vi } from 'vitest'
import { render } from '@testing-library/vue'

import CreditCardDetails from '../CreditCardDetails.vue'
import { initializeItem, setInMain } from '@core/clients/tilig'
import { TrackingEvent } from '@core/utils/trackingConstants'

vi.mock('@/utils/copyStringToClipboard')

const trackEventMock = vi.fn()
vi.mock('@core/stores/tracking', () => ({
  useTrackingStore: vi.fn(() => ({
    trackEvent: trackEventMock,
  })),
}))

const item = initializeItem()

item.overview.name = 'Amex Card'
item.overview.info = '9487'
const ccNumber = '78484773884890879088'
const ccHolder = 'John Doe'
const ccExp = '12/32'
const ccv = '909'
const pin = '1784'
const zipCode = 'P-1OE-Y01'
const notes = 'Any extra info goes here'
setInMain(item.details, 'ccnumber', ccNumber)
setInMain(item.details, 'ccholder', ccHolder)
setInMain(item.details, 'ccexp', ccExp)
setInMain(item.details, 'cvv', ccv)
setInMain(item.details, 'pin', pin)
setInMain(item.details, 'zipcode', zipCode)
item.details.notes = notes

describe('Credit Card details', () => {
  test('should render properly ', () => {
    const { getByText } = render(CreditCardDetails, {
      props: {
        item,
      },
    })

    expect(getByText(ccHolder)).toBeTruthy()
    expect(getByText(ccExp)).toBeTruthy()
    expect(getByText(zipCode)).toBeTruthy()
  })

  test('track event is triggered when the copy button is clicked', async () => {
    render(CreditCardDetails, {
      props: {
        item,
      },
    })

    expect(trackEventMock).toHaveBeenCalledWith({
      event: TrackingEvent.CREDIT_CARD_VIEWED,
    })
  })
})
