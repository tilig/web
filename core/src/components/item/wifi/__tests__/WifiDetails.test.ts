import { describe, test, expect, vi } from 'vitest'
import { render } from '@testing-library/vue'

import WifiDetails from '../WifiDetails.vue'
import { initializeItem, setInMain } from '@core/clients/tilig'
import { TrackingEvent } from '@core/utils/trackingConstants'

vi.mock('@/utils/copyStringToClipboard')

const trackEventMock = vi.fn()
vi.mock('@core/stores/tracking', () => ({
  useTrackingStore: vi.fn(() => ({
    trackEvent: trackEventMock,
  })),
}))

const item = initializeItem()

item.overview.name = 'HomeWifi'
setInMain(item.details, 'ssid', 'connectAtYourPeril')
setInMain(item.details, 'password', '998548700')
item.details.notes = 'I promise to not steal your data'

describe('Wifi details', () => {
  test('should render properly ', () => {
    const { getByText } = render(WifiDetails, {
      props: {
        item,
      },
    })

    expect(getByText('connectAtYourPeril')).toBeTruthy()
  })

  test('should fire the wifi item viewed tracking event ', () => {
    render(WifiDetails, {
      props: {
        item,
      },
    })

    expect(trackEventMock).toHaveBeenLastCalledWith({
      event: TrackingEvent.WIFI_ITEM_VIEWED,
    })
  })
})
