import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { describe, test, expect, vi } from 'vitest'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import { render } from '@testing-library/vue'

import AddWifiItem from '../AddWifiItem.vue'

const trackEventMock = vi.fn()
vi.mock('@core/stores/tracking', () => ({
  useTrackingStore: vi.fn(() => ({
    trackEvent: trackEventMock,
  })),
}))

const renderComponent = () => {
  const pinia = createPinia()
  const app = createApp({})
  pinia.use(piniaPluginPersistedstate)
  app.use(pinia)

  return render(AddWifiItem, {
    global: {
      plugins: [pinia],
    },
  })
}

describe('Add wifi item', () => {
  test('should render properly', async () => {
    const { findByText } = renderComponent()

    const networkNameInputLabel = await findByText('Network name')
    const passwordInputLabel = await findByText('Network password')
    const saveButton = await findByText('Save')

    expect(networkNameInputLabel.textContent).toBe('Network name')
    expect(passwordInputLabel.textContent).toBe('Network password')
    expect(saveButton.textContent).toBe('Save')
  })
})
