<script setup lang="ts">
import { computed, markRaw, onUnmounted, ref, type Component } from 'vue'
import { useRouter } from 'vue-router'
import { storeToRefs } from 'pinia'
import { PlusCircleIcon } from '@heroicons/vue/solid'

import Modal from '@core/components/Modal.vue'
import Button from '@core/components/Button.vue'
import WifiIcon from '@core/components/icons/PurpleWifiIcon.vue'
import CreditCardIcon from '@core/components/icons/CreditCardIcon.vue'
import AddLogin from '@core/components/item/login/AddLogin.vue'
import AddSecureNote from '@core/components/item/securenote/AddSecureNote.vue'
import AddWifiItem from '@core/components/item/wifi/AddWifiItem.vue'
import AddCreditCardItem from '@core/components/item/creditcard/AddCreditCardItem.vue'

import { useAddItemStore } from '@core/stores/addItem'
import { ItemType, useItemsStore } from '@core/stores/items'
import { itemTypeLabels } from '@core/utils/itemTypeLabel'
import { Template } from '@core/clients/tilig/item'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('ui:AddItem')

const componentMap = new Map<ItemType, Component>([
  [ItemType.Logins, AddLogin],
  [ItemType.SecureNotes, AddSecureNote],
  [ItemType.WiFi, AddWifiItem],
  [ItemType.CreditCards, AddCreditCardItem],
])

const itemTypeTextMap = new Map([
  [ItemType.CreditCards, 'card'],
  [ItemType.Logins, 'login'],
  [ItemType.SecureNotes, 'note'],
  [ItemType.WiFi, 'WiFi network'],
])

const itemTypeIconMap = new Map([
  [ItemType.WiFi, WifiIcon],
  [ItemType.CreditCards, CreditCardIcon],
])

const router = useRouter()
const addItemStore = useAddItemStore()
const itemsStore = useItemsStore()

const { activeItemType, query } = storeToRefs(itemsStore)

const typeMenuIsOpen = ref(false)
const itemCreationModalIsOpen = ref(false)
const itemTypeToCreate = ref<ItemType | null>(null)
const componentToRender = ref<Component | null>(null)

const buttonLabel = computed(() => {
  if (activeItemType.value && itemTypeTextMap.has(activeItemType.value)) {
    const label = itemTypeTextMap.get(activeItemType.value)
    return `Add a new ${label}`
  }
  return 'Add a new item'
})

const headerText = computed(() => {
  if (itemTypeToCreate.value) {
    const label = itemTypeTextMap.get(itemTypeToCreate.value)
    return `Add a new ${label}`
  }
  return 'Add a new item'
})

const itemTypeIcon = computed(() =>
  itemTypeToCreate.value ? itemTypeIconMap.get(itemTypeToCreate.value) : null
)

const closeTypeMenuModal = () => {
  logger.debug('Closing type modal')
  typeMenuIsOpen.value = false
  // Reset addItem store after the modal is hidden
  setTimeout(() => addItemStore.$reset(), 500)
}

const closeItemCreationModal = () => {
  logger.debug('Closing creation modal')
  itemCreationModalIsOpen.value = false
  closeTypeMenuModal()
}

// In case the user has an activate item type, open modal for that type.
// Otherwise, open the option menu
const openAddItemModal = () => {
  if (activeItemType.value && activeItemType.value !== ItemType.Custom) {
    openItemCreationModal(activeItemType.value)
  } else {
    openTypeMenuModal()
  }
}

const openTypeMenuModal = () => {
  logger.debug('Opening type modal')
  typeMenuIsOpen.value = true
}

const openItemCreationModal = (itemType: ItemType) => {
  logger.debug('Opening creation modal')
  itemTypeToCreate.value = itemType

  componentToRender.value = itemTypeToCreate.value
    ? markRaw(componentMap.get(itemTypeToCreate.value) || {})
    : null

  itemCreationModalIsOpen.value = true
}

const createItem = async (template: Template) => {
  addItemStore.newItem.item.template = template
  const createdItem = await addItemStore.addItem()

  if (createdItem) {
    query.value = ''

    closeItemCreationModal()

    router.push({
      name: 'item',
      params: {
        id: createdItem.item.id,
        type: activeItemType.value,
      },
    })
  }
}

// Allow opening modal
const { off: unsubscribe } = addItemStore.onOpenAddModal(openAddItemModal)
onUnmounted(unsubscribe)
</script>

<template>
  <Modal
    :title="headerText"
    :open="itemCreationModalIsOpen"
    close-button
    @close-modal="closeItemCreationModal"
  >
    <template v-if="itemTypeIcon" #title-icon>
      <div class="add-new-item-icon-container">
        <component :is="itemTypeIcon" class="add-new-item-icon" />
      </div>
    </template>

    <div class="content">
      <component :is="componentToRender" @create-item="createItem" />
    </div>
  </Modal>

  <Button button-style="primary" class="add-item-button" @click="openAddItemModal">
    <template v-if="typeMenuIsOpen">
      <div class="item-types-overlay" @click.stop="closeTypeMenuModal" />
      <div class="item-types">
        <Button
          v-for="[itemType, label] in itemTypeLabels"
          :key="itemType"
          button-style="primary"
          class="add-item-button item-type"
          @click="openItemCreationModal(itemType)"
        >
          <template #icon>
            <PlusCircleIcon class="plus-icon" aria-hidden="true" />
          </template>
          {{ label }}
        </Button>
      </div>
    </template>

    <template #icon>
      <PlusCircleIcon class="plus-icon" aria-hidden="true" />
      <!-- <XIcon class="plus-icon" aria-hidden="true" /> -->
    </template>
    {{ buttonLabel }}
  </Button>
</template>

<style scoped>
.content {
  @apply w-[calc(100vw-2rem)] md:max-w-[30rem];
}
.add-new-item {
  @apply min-h-[25rem] rounded-xl bg-white px-3 pb-5;
}

.title {
  @apply font-spoof text-xl font-book leading-8 text-indigo lg:text-2xl;
}

.close-button {
  @apply absolute right-3 top-2.5 h-6 w-6 cursor-pointer text-blue-lightest hover:text-blue-darkest;
}

.add-item-button {
  @apply relative z-40 flex w-full items-center justify-center;
}

.add-new-item-header {
  @apply -mx-6 flex h-16 items-center border-b border-gray-light pl-6 md:h-20;
}

.plus-icon {
  @apply -ml-1 mb-0.5 mr-1.5 h-6 w-6;
}

.item-type {
  @apply capitalize;
}

.item-types-selection {
  @apply absolute;
}

.item-types {
  @apply absolute bottom-20 right-0 z-20 flex max-w-[6rem] flex-col items-end gap-5;
}

.add-item-button-container {
  @apply relative;
}

.item-types-overlay {
  @apply fixed inset-0 z-10 h-full cursor-default bg-blue-lightest bg-opacity-75 transition-opacity;
}

.add-new-item-icon-container {
  @apply mr-3 flex h-10 w-10 items-center justify-center rounded-lg border border-gray-light bg-white;
}

.add-new-item-icon {
  @apply h-6 w-6 rounded-sm;
}
</style>
