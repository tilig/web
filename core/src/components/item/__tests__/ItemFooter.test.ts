import { describe, test, expect } from 'vitest'
import { render, fireEvent } from '@testing-library/vue'

import ItemFooter from '@core/components/item/ItemFooter.vue'

describe('Item Footer', () => {
  test('should render properly - edit button is visible', () => {
    const { getByText, unmount } = render(ItemFooter)
    expect(getByText('Edit')).toBeTruthy()
    unmount()
  })

  test('Emits the edit event when the edit button confirmation button is clicked', async () => {
    const { getByText, emitted, unmount } = render(ItemFooter)
    const editButton = getByText('Edit')
    await fireEvent.click(editButton)
    expect(emitted('edit')).toBeTruthy()
    unmount()
  })
})
