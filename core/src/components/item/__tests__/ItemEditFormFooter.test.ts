import { beforeEach, describe, test, expect, vi } from 'vitest'
import { render, fireEvent } from '@testing-library/vue'

import ItemEditFormFooter from '@core/components/item/ItemEditFormFooter.vue'
import { Template } from '@core/clients/tilig/item'

beforeEach(() => {
  window.IntersectionObserver = vi.fn().mockImplementationOnce(() => ({
    root: null,
    rootMargin: '',
    thresholds: [],
    disconnect: vi.fn(),
    observe: vi.fn(),
    takeRecords: vi.fn(() => []),
    unobserve: vi.fn(),
  }))
})

const itemName = 'Spotify'

const props = {
  itemName,
  deleting: false,
  template: Template.Login,
  saving: false,
  dirty: false,
  invalid: false,
}
describe('Item Edit Form Footer', () => {
  test('should render properly - delete, save and cancel buttons are visible', () => {
    const { getByText, unmount } = render(ItemEditFormFooter, {
      props,
    })
    expect(getByText('Delete')).toBeTruthy()
    expect(getByText('Save')).toBeTruthy()
    expect(getByText('Cancel')).toBeTruthy()
    unmount()
  })

  test('should show the delete dialog when the delete button is clicked', async () => {
    const { getByText, getByTestId, unmount } = render(ItemEditFormFooter, {
      props: {
        ...props,
        template: Template.SecureNote,
      },
    })
    const deleteButton = getByText('Delete')
    await fireEvent.click(deleteButton)

    const deleteItemTitle = getByText('Remove this note forever?')
    const deleteItemButton = getByText('Yes, remove forever!')
    const cancelButton = getByTestId('cancel-delete')
    expect(deleteItemTitle).toBeTruthy()
    expect(deleteItemButton).toBeTruthy()
    expect(cancelButton).toBeTruthy()
    unmount()
  })

  test('Emits the cancel event when the cancel button is clicked', async () => {
    const { emitted, getByTestId, unmount } = render(ItemEditFormFooter, {
      props,
    })
    const cancelButton = getByTestId('cancel-edit')
    await fireEvent.click(cancelButton)
    expect(emitted('cancel')).toBeTruthy()
    unmount()
  })

  test('Emits the delete event when the delete button confirmation button is clicked', async () => {
    const { getByText, emitted, getByTestId, unmount } = render(ItemEditFormFooter, {
      props,
    })
    const deleteButton = getByText('Delete')
    await fireEvent.click(deleteButton)
    const deleteConfirmButton = getByTestId('confirm-delete')
    await fireEvent.click(deleteConfirmButton)
    expect(emitted('delete')).toBeTruthy()
    unmount()
  })
})
