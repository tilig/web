import { describe, test, expect } from 'vitest'
import { render, fireEvent } from '@testing-library/vue'

import DeleteShare from '../DeleteShare.vue'

const renderComponent = () =>
  render(DeleteShare, {
    props: {
      isDeleting: false,
      title: 'Delete the share-link?',
      body: 'This will make the information inaccessible to anyone who has this link.',
    },
  })

describe('Delete Share', () => {
  test('should render properly - heading and description texts are visible', () => {
    const { getByText, unmount } = renderComponent()
    expect(getByText('Delete the share-link?')).toBeTruthy()
    expect(
      getByText('This will make the information inaccessible to anyone who has this link.')
    ).toBeTruthy()
    unmount()
  })

  test('Cancel Button and Delete Buttons are visible', () => {
    const { getByText, unmount } = renderComponent()
    expect(getByText('Yes, delete')).toBeTruthy()
    expect(getByText('Cancel')).toBeTruthy()
    unmount()
  })

  test('"close" event is emitted when the cancel button is clicked', async () => {
    const { getByText, unmount, emitted } = renderComponent()
    const cancelButton = getByText('Cancel')
    await fireEvent.click(cancelButton)

    expect(emitted('close')).toBeTruthy()
    unmount()
  })

  test('"deleteShare" event is emitted when the delete button is clicked', async () => {
    const { getByText, unmount, emitted } = renderComponent()
    const deleteConfirmButton = getByText('Yes, delete')
    await fireEvent.click(deleteConfirmButton)

    expect(emitted('deleteShare')).toBeTruthy()
    unmount()
  })
})
