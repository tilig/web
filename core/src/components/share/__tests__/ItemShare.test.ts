import { beforeEach, describe, test, expect, vi } from 'vitest'
import { render, fireEvent } from '@testing-library/vue'

import ItemShare from '../ItemShare.vue'
import { DecryptedItem, Template } from '@core/clients/tilig'

const itemName = 'Booking'

const item: DecryptedItem = {
  item: {
    id: 'b8d71b1f-a3d3-40ac-9a37-5e7d3a6b8799',
    template: 'login/1' as Template,
    website: 'www.booking.com',
    encryption_version: 2,
    name: itemName,
    brand: null,
    share_link: null,
    encrypted_dek: '',
    created_at: '',
    encrypted_details: '',
    encrypted_folder_dek: '',
    folder: null,
    encrypted_overview: '',
    updated_at: '',
    rsa_encrypted_dek: '',
  },
  overview: {
    name: itemName,
    info: null,
    urls: [],
    android_app_ids: [],
  },
  details: {
    main: [],
    notes: null,
    history: [],
    custom_fields: [],
  },
  share: null,
}

const share = {
  id: '98b9daf6-0625-4328-963c-294aa7bd0e17',
  timesViewed: 0,
  masterSecret: 'gHW-6eYE4pQlckgdnZnpONWp6XXwmV_2_x2VZYNqv4M',
}

const renderShareComponent = (withShareLink = false) => {
  if (withShareLink) {
    item.share = share
  } else {
    item.share = null
  }

  return render(ItemShare, {
    props: {
      item,
      isDeleting: false,
      isCreating: false,
      publicKeyFetchError: null,
      isAddingMembership: false,
      isRevokingMembership: false,
      contacts: [],
    },
  })
}

describe('Item Share - Link Creation', () => {
  test('should render properly - heading and create share link button are visible', () => {
    const { getByText, unmount } = renderShareComponent()
    expect(getByText('Share this login')).toBeTruthy()
    expect(getByText(`Give a friend or co-worker access to ${itemName}`)).toBeTruthy()
    unmount()
  })

  test('clicking the create share link button emits the "createShareLink" event', async () => {
    const { getByText, emitted, unmount } = renderShareComponent()
    const createShareLinkButton = getByText('Create a share-link')
    await fireEvent.click(createShareLinkButton)
    expect(emitted('createShare')).toBeTruthy()
    unmount()
  })
})

describe('Item Share - Share Link View and Delete', () => {
  beforeEach(() => {
    window.IntersectionObserver = vi.fn().mockImplementationOnce(() => ({
      root: null,
      rootMargin: '',
      thresholds: [],
      disconnect: vi.fn(),
      observe: vi.fn(),
      takeRecords: vi.fn(() => []),
      unobserve: vi.fn(),
    }))
  })

  test('should render properly - heading, url and delete buttons are visible', () => {
    const { queryByText, findByText, unmount } = renderShareComponent(true)
    const shareLinkHash = item.share?.masterSecret as string
    expect(queryByText('This login is shared')).toBeTruthy()
    expect(queryByText('This login is shared')).toBeTruthy()
    expect(queryByText('Delete share-link')).toBeTruthy()
    expect(queryByText('Share this login')).toBeFalsy()
    expect(queryByText('Create a share-link')).toBeFalsy()
    expect(findByText(shareLinkHash)).toBeTruthy()
    unmount()
  })

  test('Emit deleteShare link when the confirmation button is clicked', async () => {
    const { getByText, emitted, getByTestId, unmount } = renderShareComponent(true)

    const deleteShareLinkButton = getByText('Delete share-link')
    await fireEvent.click(deleteShareLinkButton)

    const deleteConfirmButton = getByTestId('confirm-delete')
    await fireEvent.click(deleteConfirmButton)

    expect(emitted('deleteShare')).toBeTruthy()
    unmount()
  })
})
