import { describe, test, expect } from 'vitest'
import { render } from '@testing-library/vue'

import ItemShareLink from '../ItemShareLink.vue'

describe('Item Share Link', () => {
  const shareSecret = 'gHW-6eYE4pQlckgdnZnpONWp6XXwmV_2_x2VZYNqv4M'
  const linkText = new URL(`/s#${shareSecret}`, window.location.origin).toString()
  test('should render the correct link properly', () => {
    const { getByText } = render(ItemShareLink, {
      props: {
        shareSecret,
      },
    })
    expect(getByText(linkText)).toBeTruthy()
    expect(getByText('Click to copy this share-link')).toBeTruthy()
  })
})
