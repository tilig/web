<script setup lang="ts">
import { computed, ref, Ref, toRefs, watchEffect } from 'vue'
import { onClickOutside } from '@vueuse/core'

import FormInput from '@core/components/FormInput.vue'
import Button from '@core/components/Button.vue'
import Warning from '@core/assets/warning.svg?component'
import Modal from '@core/components/Modal.vue'
import DeleteShare from './DeleteShare.vue'
import InviteFolderMember from './InviteFolderMember.vue'
import FolderMember from './FolderMember.vue'

import { Contact, Folder, FolderMembership, UserProfile } from '@core/clients/tilig'
import { isEmailAddress } from '@core/utils/isEmailAddress'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('ui:folder-members')
logger.info('Folder Members viewed')

const email = ref<string | null>(null)
const showRevokeMembershipModal = ref(false)
const memberToRevoke = ref<FolderMembership | null>(null)
const contactSelectionDialogIsVisible = ref(false)
const contactSelectionDialog: Ref<null | HTMLInputElement> = ref(null)
const contactForm: Ref<null | HTMLInputElement> = ref(null)

const submitButtonIsDisabled = computed(() => !email.value || !isEmailAddress(email.value))

const emit = defineEmits<{
  (e: 'addFolderMembership', email: string): void
  (e: 'resetPublicKeyFetchErrorState'): void
  (e: 'revokeFolderMembership', memberId: string): void
}>()

const props = defineProps<{
  folder: Folder | null
  currentUser?: UserProfile | null
  contacts?: Contact[]
  publicKeyFetchError?: 'own-email' | null
  isAddingMembership?: boolean
  isRevokingMembership?: boolean
}>()

const { folder } = toRefs(props)

// Memberships of the current user are filtered out
const folderMemberships = computed(
  () => folder.value?.folder_memberships.filter(({ is_me }) => !is_me) ?? []
)
const folderInvitations = computed(() => folder.value?.folder_invitations ?? [])
const hasInvitationsOrMembers = computed(
  () => folderMemberships.value.length + folderInvitations.value.length > 0
)

const addFolderMembership = () => {
  // just to be sure that the email.value exists
  if (!email.value) return
  setContactSelectionDialogVisibility(false)
  emit('addFolderMembership', email.value)
}

const resetPublicKeyFetchErrorState = () => {
  emit('resetPublicKeyFetchErrorState')
}

const confirmRevokeMembership = () => {
  showRevokeMembershipModal.value = false
  emit('revokeFolderMembership', memberToRevoke.value?.id as string)
}

const startRevokeMembership = (member: FolderMembership) => {
  memberToRevoke.value = member
  showRevokeMembershipModal.value = true
}

const contactSelected = (contactEmail: string) => {
  logger.debug('Selected contact %s', contactEmail)
  email.value = contactEmail

  setContactSelectionDialogVisibility(false)
}

const setContactSelectionDialogVisibility = (visible: boolean) => {
  if (!props.contacts?.length) return

  if (visible) {
    // we want to apply focus on the element so it can disappear on blur
    contactSelectionDialog.value?.focus()
  }
  contactSelectionDialogIsVisible.value = visible
}

onClickOutside(contactForm, () => {
  setContactSelectionDialogVisibility(false)
})

watchEffect(() => {
  if (!props.isAddingMembership || !showRevokeMembershipModal.value) {
    email.value = ''
  }
})
</script>

<template>
  <div>
    <form ref="contactForm" class="email-share" @submit.prevent="addFolderMembership">
      <FormInput
        v-model="email"
        required
        type="email"
        placeholder="Type an email address"
        class="email-share-input peer"
        @focus="setContactSelectionDialogVisibility(true)"
      />

      <Button
        class="email-share-button"
        type="submit"
        full-width
        :loading="isAddingMembership"
        :disabled="submitButtonIsDisabled"
        button-style="primary"
        >Share</Button
      >

      <div
        ref="contactSelectionDialog"
        class="invite-member-dialog"
        :class="{ 'is-visible': contactSelectionDialogIsVisible }"
      >
        <InviteFolderMember
          :email="email"
          :contacts="contacts || []"
          @contact-selected="contactSelected"
        />
      </div>
    </form>

    <section v-if="publicKeyFetchError" class="share-link-prompt-container">
      <div class="share-link-prompt">
        <div class="prompt-header">
          <Warning />
          <p>Error!</p>
        </div>

        <div class="prompt-body">
          {{ publicKeyFetchError === 'own-email' && 'You cannot share an item with yourself.' }}
        </div>
      </div>

      <Button class="close-button" @click="resetPublicKeyFetchErrorState">Ok</Button>
    </section>

    <section v-if="hasInvitationsOrMembers" class="members">
      <h3 class="heading">shared with</h3>

      <FolderMember
        v-if="currentUser"
        class="text-indigo"
        name="You"
        :email="currentUser.displayName || currentUser.email"
        :picture="currentUser.photoURL"
        hide-revoke
      />

      <!-- Memberships go here -->
      <FolderMember
        v-for="membership in folderMemberships"
        :key="membership.id"
        :email="membership.user.email"
        :picture="membership.user.picture"
        :pending="membership.pending"
        @revoke-access="startRevokeMembership(membership)"
      />

      <!-- Invitations go here -->
      <FolderMember
        v-for="invitation in folderInvitations"
        :key="invitation.id"
        :email="invitation.user.email"
        :picture="invitation.user.picture"
        :pending="invitation.pending"
        @revoke-access="startRevokeMembership(invitation)"
      />
    </section>

    <Modal :open="showRevokeMembershipModal" @close-modal="showRevokeMembershipModal = false">
      <DeleteShare
        :is-deleting="isRevokingMembership"
        title="Revoke access"
        :body="`Are you sure you want to revoke access for ${memberToRevoke?.user.email}?`"
        @delete-share="confirmRevokeMembership"
        @close="showRevokeMembershipModal = false"
      >
        <template #confirmation-text>Yes, revoke</template>
      </DeleteShare>
    </Modal>
  </div>
</template>

<style scoped>
.email-share {
  @apply relative my-2 mb-5 grid w-full max-w-[428px] grid-cols-[1fr_90px] rounded-lg ring-1 ring-gray focus-within:ring-2 focus-within:ring-indigo;
}

.email-share-button {
  @apply rounded-l-none;
}

.email-share-input {
  @apply rounded-r-none border-none text-sm focus:border-none;
}

.invite-member-dialog {
  @apply invisible absolute z-20 col-span-2 w-full translate-y-11 transform transition-transform;
}

.invite-member-dialog.is-visible {
  @apply visible translate-y-[3.3rem];
}

.members {
  @apply my-3 flex max-w-full flex-col gap-5 md:gap-3;
}

.share-link-prompt-container {
  @apply grid grid-cols-[1fr_37px] gap-2 rounded-lg bg-[#FFF2F5] px-3 py-2 text-xs;
}

.share-link-prompt {
  @apply flex flex-col gap-2;
}

.prompt-header {
  @apply flex items-center gap-2;
}

.prompt-body {
  @apply grid grid-cols-[1fr];
}

.close-button {
  @apply m-0 w-full justify-center border-none bg-transparent p-0 text-indigo shadow-transparent hover:bg-transparent;
}

.heading {
  @apply my-1 text-xxs uppercase text-gray-darkest md:text-xs;
}
</style>
