import { ref, onUnmounted, watch, computed } from 'vue'
import { generateTotp } from '@core/utils/totp'
import { get, MaybeRef } from '@vueuse/shared'

export function useTotp(urlString: MaybeRef<string | null>) {
  const code = ref('')
  const passedSeconds = ref(0)
  const remainingSeconds = computed(() => 30 - passedSeconds.value)
  let timer: ReturnType<typeof setInterval>

  watch(
    () => urlString,
    () => {
      if (timer) {
        clearInterval(timer)
      }

      if (get(urlString)) {
        timer = setInterval(() => {
          const _passedSeconds = Math.round(Date.now() / 1000) % 30
          if (_passedSeconds === 0 || !code.value) {
            code.value = generateTotp(get(urlString)) || ''
          }
          passedSeconds.value = _passedSeconds
        }, 100)
      } else {
        code.value = ''
      }
    },
    {
      immediate: true,
    }
  )

  onUnmounted(() => timer && clearInterval(timer))

  return { code, remainingSeconds }
}
