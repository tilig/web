import { computed } from 'vue'
import { MaybeRef, get } from '@vueuse/shared'

import { ItemType } from '@core/stores/items'

export function useItemTypeTabs(hasCustomItems: MaybeRef<boolean> = false) {
  const tabs = computed(() => {
    const tabs = [
      {
        name: 'Logins',
        itemType: ItemType.Logins,
      },
      {
        name: 'Notes',
        itemType: ItemType.SecureNotes,
      },
      {
        name: 'WiFi',
        itemType: ItemType.WiFi,
      },
      {
        name: 'Cards',
        itemType: ItemType.CreditCards,
      },
    ]
    if (get(hasCustomItems)) {
      tabs.push({ name: 'Other', itemType: ItemType.Custom })
    }
    return tabs
  })

  return {
    tabs,
  }
}
