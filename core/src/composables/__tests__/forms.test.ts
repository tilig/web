import { describe, test, expect } from 'vitest'
import { ref } from 'vue'
import { findInMain, initializeItem } from '@core/clients/tilig'
import { convertToUrl } from '@core/utils/url'

import { useLoginForm } from '../forms'

describe('the useLoginForm composable', () => {
  test('that we update the appropriate `overview.info` field with username', () => {
    const newItem = ref(initializeItem())
    const composable = useLoginForm(newItem)
    const username = 'MyNewUsername'
    composable.username.value = username
    expect(newItem.value.overview.info).toEqual(username)
    expect(findInMain(newItem.value.details, 'username')).toEqual(username)
  })
  test('that we handle website updates in the overview urls', () => {
    const newItem = ref(initializeItem())
    const composable = useLoginForm(newItem)
    const website = 'tilig.com'
    composable.website.value = website
    expect(newItem.value.overview.urls[0].url).toEqual(convertToUrl(website))
    expect(newItem.value.item.website).toEqual(website)
  })
})
