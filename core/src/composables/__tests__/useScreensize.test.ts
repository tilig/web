import { describe, test, expect, vi } from 'vitest'

import { useScreenSize } from '../useScreensize'

let activeQuery: string | null = null

vi.stubGlobal(
  'matchMedia',
  vi.fn((query) => ({
    matches: activeQuery === query,
    media: query,
    onchange: null,
    addListener: vi.fn(), // deprecated
    removeListener: vi.fn(), // deprecated
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn(),
  }))
)

describe('useScreensize composable', () => {
  test('returns onMobile:true when screensize < 768px', () => {
    activeQuery = '(max-width: 767.9px)'
    const { onMobile, onTablet, onDesktop } = useScreenSize()
    expect(onMobile.value).toBe(true)
    expect(onTablet.value).toBe(false)
    expect(onDesktop.value).toBe(false)
  })

  test('returns onTablet:true when screensize >= 768px && < 985px', () => {
    activeQuery = '(min-width: 768px) and (max-width: 984.9px)'
    const { onMobile, onTablet, onDesktop } = useScreenSize()
    expect(onMobile.value).toBe(false)
    expect(onTablet.value).toBe(true)
    expect(onDesktop.value).toBe(false)
  })

  test('returns onDesktop:true when screensize >= 985px', () => {
    activeQuery = '(min-width: 985px)'
    const { onMobile, onTablet, onDesktop } = useScreenSize()
    expect(onMobile.value).toBe(false)
    expect(onTablet.value).toBe(false)
    expect(onDesktop.value).toBe(true)
  })
})
