import { useBreakpoints } from '@vueuse/core'

export function useScreenSize() {
  const breakpoints = useBreakpoints({
    mediumBreakpoint: 768,
    //Temp screensize to allow us switch login details to mobile layout early
    // We will return this to 992px once we redesign the layout and remove the 3-columns
    largeBreakpoint: 985,
  })

  const onMobile = breakpoints.smaller('mediumBreakpoint')
  const onDesktop = breakpoints.greaterOrEqual('largeBreakpoint')
  const onTablet = breakpoints.between('mediumBreakpoint', 'largeBreakpoint')

  return {
    onMobile,
    onDesktop,
    onTablet,
  }
}
