import { Ref, computed } from 'vue'

import { DecryptedItem, FieldKind, findInMain, setInMain } from '@core/clients/tilig'
import { stripSchema, convertToUrl, isValidUrl } from '@core/utils/url'
import { DecryptedShare } from '@core/clients/tilig/privateLink'

type OnUpdate = (ref: DecryptedItem | DecryptedShare, value: string | null) => void

const writableForKind = (
  ref: Ref<DecryptedItem | DecryptedShare | null>,
  kind: FieldKind,
  onUpdate?: OnUpdate
) => {
  return computed({
    get() {
      return ref.value && findInMain(ref.value.details, kind)
    },
    set(newValue: string | null) {
      if (!ref.value) return
      setInMain(ref.value.details, kind, newValue)
      onUpdate?.(ref.value, newValue)
    },
  })
}

type DetailWritables = 'notes'

const writableForDetails = (
  ref: Ref<DecryptedItem | DecryptedShare | null>,
  field: DetailWritables,
  onUpdate?: OnUpdate
) => {
  return computed({
    get() {
      return ref.value && ref.value.details[field]
    },
    set(newValue: string | null) {
      if (!ref.value) return
      ref.value.details[field] = newValue
      onUpdate?.(ref.value, newValue)
    },
  })
}

type OverviewWritables = 'name' | 'info'

const writableForOverview = (
  ref: Ref<DecryptedItem | DecryptedShare | null>,
  field: OverviewWritables,
  onUpdate?: OnUpdate
) => {
  return computed({
    get() {
      return ref.value && ref.value.overview[field]
    },
    set(newValue: string | null) {
      if (!ref.value) return
      ref.value.overview[field] = newValue
      onUpdate?.(ref.value, newValue)
    },
  })
}

const writableForWebsite = (ref: Ref<DecryptedItem | DecryptedShare | null>) => {
  return computed({
    get() {
      if (!ref.value) return null
      const url = ref.value.overview.urls[0]?.url
      return url ? stripSchema(url) : null
    },
    set(newValue: string | null) {
      if (!ref.value) return

      // Synchronize unencrypted website field
      if ('item' in ref.value && ref.value.item) {
        ref.value.item.website = newValue || null
      }

      if (newValue) {
        ref.value.overview.urls = [{ url: convertToUrl(newValue) }]
      } else {
        ref.value.overview.urls = []
      }
    },
  })
}

/**
 * Adapters to reactively modify login/1 template
 */
export const useLoginForm = (item: Ref<DecryptedItem | DecryptedShare | null>) => {
  const primaryUrl = computed(() => {
    const url = item.value?.overview.urls[0]?.url ?? null
    return url && isValidUrl(url) ? url : null
  })
  const username = writableForKind(
    item,
    'username',
    (item, value) => (item.overview.info = value || null)
  )
  const password = writableForKind(item, 'password')
  const totp = writableForKind(item, 'totp')
  const notes = writableForDetails(item, 'notes')
  const name = writableForOverview(item, 'name')
  const website = writableForWebsite(item)

  return {
    primaryUrl,
    username,
    password,
    totp,
    notes,
    name,
    website,
  }
}

/**
 * Adapters to reactively modify securenote/1 template
 */
export const useSecureNotesForm = (item: Ref<DecryptedItem | null>) => {
  const name = writableForOverview(item, 'name')
  const notes = writableForDetails(item, 'notes')

  return {
    name,
    notes,
  }
}

/**
 * Adapters to reactively modify wifi/1 template
 */
export const useWifiForm = (item: Ref<DecryptedItem | null>) => {
  const name = writableForOverview(item, 'name')
  const networkName = writableForKind(item, 'ssid')
  const password = writableForKind(item, 'password')
  const notes = writableForDetails(item, 'notes')

  return {
    name,
    networkName,
    password,
    notes,
  }
}

/**
 * Adapters to reactively modify creditcard/1 template
 */
export const useCreditCardsForm = (item: Ref<DecryptedItem | null>) => {
  const name = writableForOverview(item, 'name')
  const notes = writableForDetails(item, 'notes')
  const ccNumber = writableForKind(item, 'ccnumber', (item, value) => {
    item.overview.info = value?.slice(-4) ?? null
  })
  const ccHolder = writableForKind(item, 'ccholder')
  const ccExp = writableForKind(item, 'ccexp')
  const cvv = writableForKind(item, 'cvv')
  const pin = writableForKind(item, 'pin')
  const zipCode = writableForKind(item, 'zipcode')

  return {
    name,
    notes,
    ccNumber,
    ccHolder,
    ccExp,
    cvv,
    pin,
    zipCode,
  }
}
