/**
 * The URL of the Rails app.
 * @returns String of the URL, with trailing slash.
 */
export default function backendURL(): string {
  return import.meta.env.VITE_APIROOT
}
