import { shallowRef, readonly } from 'vue'
import { WatchDebouncedOptions, watchDebounced } from '@vueuse/core'

/**
 * A computed that is only updated in a debounced fashion
 * @param fn Computed source
 * @param options Additional options
 */
export const computedDebounced = <T>(fn: () => T, options?: WatchDebouncedOptions<false>) => {
  const result = shallowRef<T>(fn())

  watchDebounced(
    fn,
    (val) => {
      result.value = val
    },
    options
  )

  return readonly(result)
}
