import { isBrowserExtension } from './isBrowserExtension'
// TODO: Bowser is unmaintained - Look for alternatives. See: #webapp/276
import Bowser from 'bowser'

const browserEnv = Bowser.parse(window.navigator.userAgent)

/**
 * Platform name as used in Mixpanel
 */
export const platformName = ((osName) => {
  if (isBrowserExtension()) {
    // Lowercase for backwards compatibility with Mixpanel
    return 'browser'
  }

  switch (osName) {
    case 'iOS':
    case 'Android':
      return 'WebMobile'
    case 'Chrome OS':
    case 'Linux':
    case 'macOS':
    case 'Windows':
      return 'Web'
    default:
      // In case we see Unknown values ending up in Mixpanel, check the osName
      // property of these events to find out which os.name we're missing here.
      return 'Unknown'
  }
})(browserEnv.os.name)

export const browser = browserEnv.browser
export const os = browserEnv.os
export const platform = browserEnv.platform
export const engine = browserEnv.engine

export const getAppVersion = () =>
  import.meta.env.MODE === 'production'
    ? APP_VERSION
    : `${APP_VERSION} (${COMMIT_REF ?? 'development'})`

const BROWSERS_SUPPORTING_REDIRECT = ['Brave', 'Chrome', 'chromium', 'Microsoft Edge', 'Opera']

/**
 * Does this browser support signing in with firebase through a redirect?
 */
export const supportsRedirectSignIn = browser.name
  ? BROWSERS_SUPPORTING_REDIRECT.includes(browser.name)
  : false

export default {
  isBrowserExtension,
  getAppVersion,
  platformName,
  ...browserEnv,
}
