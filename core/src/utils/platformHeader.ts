import { isBrowserExtension } from './isBrowserExtension'
import { os } from './browserEnvironment'

// `browser` is lowercase due to backwards data compatability in Mixpanel
const BROWSER = 'browser'
const UNKNOWN = 'Unknown'

export const platformHeader = () => {
  if (isBrowserExtension()) {
    return BROWSER
  }

  switch (os.name) {
    case 'iOS':
    case 'Android':
      return 'WebMobile'
    case 'Chrome OS':
    case 'Linux':
    case 'macOS':
    case 'Windows':
      return 'Web'
    default:
      // In case we see Unknown values ending up in Mixpanel, check the osName
      // property of these events to find out which os.name we're missing here.
      return UNKNOWN
  }
}
