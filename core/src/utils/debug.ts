/**
 * Check if we are in a debugging session
 *
 * Can be turned on by setting `localStorage.tiligDebug` to any non-empty string.
 */
export const isDebugging = () => !!localStorage.tiligDebug
