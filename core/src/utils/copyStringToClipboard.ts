export const copyStringToClipboard = async (str: string) => {
  if (navigator.clipboard && navigator.clipboard.writeText) {
    await navigator.clipboard.writeText(str)
    return true
  }
  return false
}
