import debug, { Debugger } from 'debug'

// Always enable logging unless overriden
if (['staging', 'development'].includes(import.meta.env.MODE) && !localStorage.debug) {
  debug.enable('*')
}

/**
 * A logger that is callable and has subtypes
 *
 * We want to expose individual log types even if the current
 * implementation just puts everything in console.log
 */
interface EnhancedDebugger extends Debugger {
  log: Debugger
  info: Debugger
  debug: Debugger
  error: Debugger
  warn: Debugger
}

export const createLogger = (tag: string) => {
  const logger = debug(tag) as EnhancedDebugger
  logger.info = logger
  logger.debug = logger
  logger.error = logger
  logger.warn = logger

  return logger
}

export const logger = createLogger('*')
