export function creditCardExpDateFormat(expiryDate: string) {
  return expiryDate.replace(/(.{2})\/?/, '$1/')
}
