import { isProdEnvironment } from './environment'
import { logger } from './logger'

// TODO: Clean-up and unify logging into a proper logging setting

const detectDebug = function () {
  return !isProdEnvironment()
}

// Allow 'any' message type since this is passed to `logger` for debug purposes
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const debugPrint = (message: any, isError = false) => {
  if (detectDebug()) {
    if (isError) {
      logger.error(message)
    } else {
      logger.info(message)
    }
  }
}

export default debugPrint
