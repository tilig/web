export default function baseURL(): string {
  switch (import.meta.env.MODE) {
    case 'development':
      return 'http://localhost:3001/'
    case 'staging':
      return 'https://tilig-staging.netlify.app/'
    default:
      return 'https://app.tilig.com/'
  }
}
