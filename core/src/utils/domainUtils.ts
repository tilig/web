import * as psl from 'psl'

type Resolver = (i: any) => string

/**
 *  Find matches based on domain
 *
 * @param arr Array of accounts or other objects to filter
 * @param domain The domain to compare against
 * @param resolver Optional resolver of array items
 * @returns An array
 */
export const filterByDomain = <T>(arr: T[], domain: string | null, resolver?: Resolver): T[] => {
  if (!domain) return []
  const needle = parseDomain(domain)
  return arr.filter((item) => {
    resolver = resolver || ((account) => account.website)
    const domain = parseDomain(resolver(item))
    if (domain == null) {
      return false
    }
    return domain === needle
  })
}

const HAS_SCHEMA_PREFIX = /^https?:\/\//i

/**
 * Extract hostname from a (potential) URL
 *
 * @param str Full url
 * @returns Extracted hostname
 */
const extractHostname = (str: string): string | null => {
  str = addSchema(str)

  try {
    const url = new URL(str)
    return url.hostname
  } catch {
    return null
  }
}

/**
 * Parse and normalize a string or URL into a domain
 *
 * @param str The full domain or url
 * @returns The normalized domain
 */
export const parseDomain = (str: string | null): string | null => {
  if (!str) return null
  const hostname = extractHostname(str)
  if (!hostname) return null

  return psl.get(hostname)
}

export const addSchema = (str: string) => {
  return HAS_SCHEMA_PREFIX.test(str) ? str : `https://${str}`
}
