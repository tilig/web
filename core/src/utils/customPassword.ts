import { cryptoReady } from '@core/clients/tilig/crypto'
import { randomShuffle, randomString } from './secureRandom'

/**
 * We use the base58 alphabet, since it reduces confusion wrt l/I and O/0.
 *
 * See https://tools.ietf.org/id/draft-msporny-base58-01.html#rfc.section.2
 */
const LOWER = 'abcdefghijkmnopqrstuvwxyz'
const UPPER = 'ABCDEFGHJKLMNPQRSTUVWXYZ'
const DIGIT = '123456789'

export const BASE56_ALPHABET = LOWER + UPPER + DIGIT

const COUNT_LOWER_GROUP = 5
const GROUP_JOINER = '-'

/**
 * Generate a random password using the Tilig method.
 */
export const customPassword = async () => {
  await cryptoReady
  // Generate lowercase base of each group
  const capitalGroupLowerChars = randomString(LOWER, COUNT_LOWER_GROUP)
  const digitGroupLowerChars = randomString(LOWER, COUNT_LOWER_GROUP)

  // Randomly position capital and digit at beginning or end of each group
  const capitalGroup = randomShuffle([capitalGroupLowerChars, randomString(UPPER, 1)]).join('')
  const digitGroup = randomShuffle([digitGroupLowerChars, randomString(DIGIT, 1)]).join('')

  // Join the two groups in a random order, using  the join char
  return randomShuffle([capitalGroup, digitGroup]).join(GROUP_JOINER)
}
