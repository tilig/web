import chromeExtensionIcon from '@core/assets/onboarding/chrome-ext-icon.svg'
import edgeExtensionIcon from '@core/assets/onboarding/edge-ext-icon.svg'
import operaExtensionIcon from '@core/assets/onboarding/opera-ext-icon.svg'

const extensionIconMapping = new Map([
  ['Chrome', chromeExtensionIcon],
  ['Chrome OS', chromeExtensionIcon],
  ['chromium', chromeExtensionIcon],
  ['Brave', chromeExtensionIcon],
  ['Opera', operaExtensionIcon],
  ['Microsoft Edge', edgeExtensionIcon],
])

export const getBrowserExtensionIcon = (browserName: string) =>
  extensionIconMapping.get(browserName)
