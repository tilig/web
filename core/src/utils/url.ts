const STARTS_WITH_SCHEMA = /^([a-z][a-z0-9+\-.]*):\/\//

export const convertToUrl = (str: string) => {
  if (STARTS_WITH_SCHEMA.test(str)) {
    return str
  } else {
    return `https://${str}`
  }
}

export const stripSchema = (url: string) => {
  if (url.startsWith('https://')) {
    return url.replace(STARTS_WITH_SCHEMA, '')
  } else {
    return url
  }
}

export const isValidUrl = (url: string) => {
  try {
    new URL(url)
    return true
  } catch {
    return false
  }
}
