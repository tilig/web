import { randomString } from '@core/utils/secureRandom'
import { BASE56_ALPHABET } from '@core/utils/customPassword'

export const randomId = () => {
  return `id-${randomString(BASE56_ALPHABET, 8)}`
}
