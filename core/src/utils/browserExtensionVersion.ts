/* eslint-env webextensions */

import BrowserAPI, { isBrowserAPIAvailable } from './BrowserAPI'
import { isBrowserExtension } from './isBrowserExtension'

/**
 * The version number of the browser extension
 * @returns String or null, The version number.
 */
const browserExtensionVersion = (): string | null => {
  if (!isBrowserExtension()) {
    return null
  }
  // Fix for test suite, doesn't have access to `browser` object
  if (!isBrowserAPIAvailable()) {
    return 'Unknown (`browser` API not available)'
  }
  const browser = BrowserAPI()
  return browser.runtime.getManifest().version
}

export default browserExtensionVersion
