import { render } from '@testing-library/vue'
import { createTestingPinia } from '@pinia/testing'
import { vi } from 'vitest'

/**
 * Little helper function to flush all promises
 *
 * Only use this in test or a nodeJs context
 */
export const flushPromises = (): Promise<void> => {
  return new Promise((resolve) => setImmediate(resolve))
}

type RenderParams = Parameters<typeof render>

export const renderWithPinia = (
  testComponent: RenderParams[0],
  renderOptions?: RenderParams[1],
  piniaOptions?: Parameters<typeof createTestingPinia>
) => {
  const { global = {}, ...options } = renderOptions ?? {}

  const pinia = createTestingPinia({ createSpy: vi.fn, ...piniaOptions })
  global.plugins = [...(global.plugins ?? []), pinia]
  return render(testComponent, { global, ...options })
}
