import { browser, platform, os } from '@core/utils/browserEnvironment'

const chromeWebStore = {
  browserName: 'Chrome',
  webstoreName: 'Chrome Web Store',
  webstoreLink:
    'https://chrome.google.com/webstore/detail/tilig-password-manager/fnnhineegblcmjlhmmogickjhhobgpjo',
}

const edgeWebstore = {
  browserName: 'Edge',
  webstoreName: 'Edge Add-on store',
  webstoreLink:
    'https://microsoftedge.microsoft.com/addons/detail/tilig-password-manager/gidnnnbibkapnoapddmkepgbjcleekbc',
}

// const firefoxWebstore = {
//   extensionName: 'Firefox Add-on',
//   webstoreName: 'Firefox Browser Add-on store',
//   webstoreLink: 'https://addons.mozilla.org/en-US/firefox/addon/tilig-password-manager/',
// }

const operaWebstore = {
  browserName: 'Opera',
  webstoreName: 'Chrome Web Store',
  webstoreLink:
    'https://chrome.google.com/webstore/detail/tilig-password-manager/fnnhineegblcmjlhmmogickjhhobgpjo',
}

const browserWebstoreMapping = {
  Chrome: chromeWebStore,
  chromium: chromeWebStore,
  'Chrome OS': chromeWebStore,
  Brave: chromeWebStore,
  'Microsoft Edge': edgeWebstore,
  //Firefox: firefoxWebstore,
  Opera: operaWebstore,
}

//const unsupportedBrowsers = ['Safari', 'Firefox']

export const getBrowserSpecificStore = () => {
  if (platform.type !== 'desktop') {
    return null
  }
  return browserWebstoreMapping[browser.name]
}

export { os }
