import { ItemType } from '@core/stores/items'

const itemTypeLabel = new Map([
  [ItemType.Logins, 'login'],
  [ItemType.SecureNotes, 'note'],
  [ItemType.WiFi, 'WiFi'],
  [ItemType.CreditCards, 'card'],
])

export const getItemTypeLabel = (itemType: ItemType) => itemTypeLabel.get(itemType)
export const itemTypeLabels = Array.from(itemTypeLabel.entries())
