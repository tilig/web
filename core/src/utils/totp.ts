import generator from 'totp-generator'
import { createLogger } from './logger'

const logger = createLogger('utils:totp')

const extractParams = (url: string) => {
  try {
    const parsed = new URL(url)
    return {
      secret: parsed.searchParams.get('secret'),
      digits: parsed.searchParams.get('digits'),
    }
  } catch {
    return null
  }
}

const generateTotp = (url: string | null) => {
  if (!url) return null

  let secret: string | null | undefined = null
  let digits: string | null | undefined = null

  if (url.startsWith('otpauth://')) {
    logger.debug('URL-like TOTP value')
    const params = extractParams(url)
    secret = params?.secret
    digits = params?.digits
  } else {
    logger.debug('Plain string TOTP value')
    secret = url.replaceAll(/\s/g, '')
  }
  if (!secret) return null

  try {
    return generator(secret, { digits: digits ? Number(digits) : undefined })
  } catch (e) {
    logger.error('Error generating TOTP value %s %O', secret, e)
    return null
  }
}

export { generateTotp }
