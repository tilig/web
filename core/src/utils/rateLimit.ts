import { chunk } from 'lodash'

/**
 * Modifies an async function so it will not execute a second
 * time if the previous execution is not yet resolved.
 *
 * @param fn Async function
 */
export const singleCall = <T>(fn: () => Promise<T>) => {
  /**
   * Current execution promise of `fn`
   */
  let execution: Promise<T> | null = null

  return () => {
    if (!execution) {
      // Execute fn, and store the promise in invocation,
      // clearing the promise when fn() resolves.
      execution = new Promise((resolve, reject) =>
        fn()
          .then(resolve, reject)
          .finally(() => (execution = null))
      )
    }

    return execution
  }
}

const timer = (wait: number) => new Promise((resolve) => setTimeout(resolve, wait))

interface ChunkedForEachOptions {
  size?: number
  wait?: number
}

/**
 * Map a function over an array and limit the amount of
 * concurrent invocations.
 *
 * @param fn Async function
 * @param arr Array of items to iterate over
 * @param options Specify concurrency and wait period
 */
export const chunkedForEach = async <T, R = void>(
  fn: (arg: T) => Promise<R>,
  arr: T[],
  options?: ChunkedForEachOptions
) => {
  const { size = 5, wait = 0 } = options ?? {}

  const chunked = chunk(arr, size)

  for (const chnk of chunked) {
    await Promise.all(chnk.map(fn))
    if (wait) await timer(wait)
  }
}
