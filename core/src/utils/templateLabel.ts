import { Template } from '@core/clients/tilig/item'

const templateLabel = new Map([
  [Template.Login, 'login'],
  [Template.SecureNote, 'note'],
  [Template.CreditCard, 'card'],
  [Template.WiFi, 'WiFi'],
  [Template.Custom, 'item'],
])

export const getTemplateLabel = (template: Template) => templateLabel.get(template)
