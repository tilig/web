// Inspired by https://github.com/rails/rails/blob/main/actionview/lib/action_view/helpers/output_safety_helper.rb

type ToSentenceOptions = {
  wordsConnector?: string
  twoWordsConnector?: string
  lastWordConnector?: string
}

export const toSentence = (arr: unknown[], options?: ToSentenceOptions) => {
  const {
    wordsConnector = ', ',
    twoWordsConnector = ' and ',
    lastWordConnector = ', and ',
  } = options ?? {}

  const strings = arr.map(String)

  switch (strings.length) {
    case 0:
      return ''
    case 1:
      return strings[0]
    case 2:
      return strings.join(twoWordsConnector)
    default:
      return [
        strings.slice(0, strings.length - 1).join(wordsConnector),
        strings[strings.length - 1],
      ].join(lastWordConnector)
  }
}
