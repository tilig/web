import * as sodium from 'libsodium-wrappers'

/**
 * Get a random integer up to a bound
 *
 * @param upperBound - Upper bound (exclusive)
 */
export const randomInt = (upperBound: number) => {
  return sodium.randombytes_uniform(upperBound)
}

/**
 * Generate a random string from an alphabet.
 *
 * @param from - Alphabet
 * @param count - Number of chars to pick
 */
export const randomString = (from: string, count: number) => {
  const chars: string[] = []

  for (let i = 0; i < count; i++) {
    const pos = randomInt(from.length)
    chars.push(from.charAt(pos))
  }
  return chars.join('')
}

/**
 * Securely shuffle an array.
 *
 * Uses Fisher-Yates.
 * @param arr - An array to shuffle in place
 */
export const randomShuffle = <T>(arr: T[]) => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = randomInt(i + 1)
    const temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp
  }

  return arr
}
