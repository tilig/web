/**
 * Are we in the context of the browser extension? If not, then it must be the webapp.
 * @returns Bool
 */
export const isBrowserExtension = (): boolean => {
  return import.meta.env.VITE_TILIG_APP_MODE == 'extension'
}
