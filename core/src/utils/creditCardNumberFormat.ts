export function creditCardNumberFormat(ccNumber: string) {
  return ccNumber.replace(/(.{4})/g, '$1 ').trim()
}
