const getEnvironment = () => {
  return import.meta.env.MODE
}

const isDevEnvironment = () => {
  return import.meta.env.DEV
}

const isProdEnvironment = () => {
  return import.meta.env.PROD
}

export { isDevEnvironment, isProdEnvironment, getEnvironment }
