export enum TrackingEvent {
  ACCOUNT_LIST_VIEWED = 'Account List Viewed', // user views the list of accounts available
  ACCOUNT_VIEWED = 'Account Viewed', // user views an account **Deprecated

  AUTOFILL_FAILED = 'AutoFill Failed', // autofill fails to run
  AUTOFILL_SELECTED = 'AutoFill Selected', // autofill in the extension  is selected
  FILL_FOCUSSED_INPUT = 'Fill Focussed Input',
  FILL_FOCUSSED_FORM = 'Fill Focussed Form',

  SELECT_ACCOUNT_TYPE_APPEARED = 'Select Account Type Appeared', // personal vs business screen appeared
  ACCOUNT_TYPE_SELECTED = 'Account Type Selected', // user selected personal or business
  SET_TEAM_NAME_APPEARED = 'Set Team Name Appeared', // set a team name appeared
  INVITE_MEMBERS_APPEARED = 'Invite Members Appeared', // the invite members component appeared
  INVITE_MEMBERS_SKIPPED = 'Invite Members Skipped', // user skips the invite members step during onboarding
  PENDING_INVITES_APPEARED = 'Pending Invites Appeared', // user views their pending invites
  INVITATION_ACCEPTED_APPEARED = 'Invitation Accepted Appeared', // user view the invitation accepted page
  NO_PENDING_INVITATION = 'No Pending Invitation', // user view the invitation accepted page but doesn't have any current invitation
  TEAM_OVERVIEW_APPEARED = 'Team Overview Appeared', // the user opened the team user overview

  BROWSER_EXTENSION_AUTOSAVE_NEW_LOGIN = 'Browser Extension Autosaved New Login',
  BROWSER_EXTENSION_AUTOSAVE_UPDATE_LOGIN = 'Browser Extension Autosaved Updated Login',

  BROWSER_EXTENSION_CAPTURE_WEB_CREDENTIALS = 'Capture Web Credentials',
  BROWSER_EXTENSION_INSTALL_SKIPPED = 'Browser Extension Install Skipped', // user skips installing the browser extension installation
  BROWSER_EXTENSION_SETUP_STARTED = 'Browser Extension Setup Started', // user sees the page to install the browser extension
  BROWSER_EXTENSION_INSTALLED = 'Browser Extension Installed', // user installs the browser extension
  BROWSER_EXTENSION_INSTALLED_FROM_WEB = 'Browser Extension Installed From Web', // A desktop - not mobile - user installs the browser extension
  BROWSER_EXTENSION_INSTRUCTIONS_APPEARED = 'Browser Extension Instructions Appeared', // user sees the browser extension setup instructions in the onboarding process
  BROWSER_EXTENSION_INSTALL_SKIP_DIALOG_APPEARED = 'Browser Extension Install Skip Dialog Appeared', // user sees the skip dialog, after clicking the skip extension install link
  BROWSER_EXTENSION_LOGIN = 'Browser Extension Login',
  BROWSER_EXTENSION_OPEN_POPUP = 'Open Extension Popup',
  BROWSER_EXTENSION_STORE_OPENED = 'Browser Extension Store Opened', // user clicks on the browser extension store button to open the browser extension store page
  BROWSER_EXTENSION_UNINSTALLED = 'Browser Extension Uninstalled', // user uninstalls the browser extension
  BROWSER_EXTENSION_UPDATED = 'Browser Extension Updated', // user's browser extension version gets updated

  BROWSER_EXTENSION_SETUP_DONE_APPEARED = 'Extension Setup Done Appeared', // Extension is succesfully installed and user is signed in

  PIN_INSTRUCTIONS_APPEARED = 'Pin Instructions Appeared', // user sees the browser extension pin instructions in the onboarding process
  AUTOSAVE_INSTRUCTIONS_APPEARED = 'Autosave Instructions Appeared', // user sees the browser extension permission instructions in the onboarding process
  AUTOSAVE_ENABLED = 'Autosave Enabled', // user enables autosave
  AUTOSAVE_DISABLED = 'Autosave Disabled', // user disables autosave
  BROWSER_EXTENSION_OPEN_INSTRUCTION_APPEARED = 'Open Extension Instructions Appeared', // user disables autosave

  GIFS_ENABLED = 'Gifs Enabled', // user enables gifs
  GIFS_DISABLED = 'Gifs Disabled', // user disables gifs

  FIRST_PASSWORD_WEBSITE_SELECTED = 'First Password Website Selected', // user selects the first password account from the list we provide in the onboarding process

  // Import
  IMPORT_CHOOSE_CURRENT_BROWSER = 'Import-Tili-Password Source - Browser', // user chooses current browser
  IMPORT_CHOOSE_CURRENT_PASSWORD_MANAGER = 'Import-Tili-Password Source - Password Manager', // user chooses password manager
  IMPORT_CONTINUE = 'Import-Tili-Continue', // user chooses to continue or stop the import process
  IMPORT_DOWNLOAD_TEMPLATE = 'Import-Tili-Download Template', // user clicks on the download template button
  IMPORT_FEATURE_RATING = 'Import-Tili-Rating', // user rates the feature
  IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED = 'Import-Tili-Instructions Open Link', // user clicks on any of the links embedded in the export instructions
  IMPORT_PASSWORD_FILE_UPLOAD_ERROR = 'Import-Tili-File Upload Error', // user sess the upload error message
  IMPORT_PASSWORD_FILE_UPLOADED = 'Import-Tili-File Uploaded', // user uploads the password file
  IMPORT_1PASSWORD_VAULTS = 'Import-Tili 1Password Vaults',
  IMPORT_PASSWORD_SPECIFY_ACCOUNT_COLUMNS = 'Import-Tili-Extra Importing Step', // user gets notified to specify the columns when tili cannot automatically map the columns
  IMPORT_PASSWORD_STORAGE_OPTION_SELECTED = 'Import-Tili-Password Storing', // user selects the password storage option
  IMPORT_PASSWORDS_COMPLETED_FULLY = 'Import-Tili-Import Completed All', // All the accounts are imported successfully
  IMPORT_PASSWORDS_COMPLETED_PARTIALLY = 'Import-Tili-Import Completed Partially', // Some of the accounts were not imported
  IMPORT_PASSWORDS_STARTED = 'Import-Tili-Import Started', // once the accounts start getting imported
  IMPORT_SHOW_IMPORTED_ACCOUNTS_BUTTON_CLICKED = 'Import-Tili-Show Imported Accounts Selected', // user clicks on the button to show the imported accounts
  IMPORT_STARTED = 'Import-Tili-Start', // user starts the import process

  MOBILE_APP_INSTALLATION_PROMPT = 'Mobile App Installation Prompt', // User prompted to install the mobile app
  MOBILE_APP_INSTALLATION_PROMPT_ANDROID_CLICKED = 'Mobile App Installation Prompt Android Clicked', // User opened the Android play store
  MOBILE_APP_INSTALLATION_PROMPT_APPLE_CLICKED = 'Mobile App Installation Prompt Apple Clicked', // User opened the Apple Appstore
  MOBILE_APP_INSTALLATION_PROMPT_SKIPPED = 'Mobile App Installation Prompt Skipped', // User skipped the download app prompt

  PASSWORD_REVEALED = 'Password Revealed', // User revealed the password
  PASSWORD_GENERATED = 'Password Generated', // User generated a new password

  USERNAME_COPIED = 'Username Copied', // User copied the Username
  PASSWORD_COPIED = 'Password Copied', // User copied the Password
  WEBSITE_OPENED = 'Website Opened', // User opened the website
  OTP_COPIED = 'OTP Copied', // User copied the OTP

  SKIP_CREATE_FIRST_ACCOUNT = 'Skip create first account', // user clicks on the skip button and doesn't add a first account from the list we provide in the onboarding process

  UNINSTALL_SURVERY_ANSWERED = 'Uninstall Survey Answered', // user submits only the answers in the survey and skips leaving a detailed feedback after uninstalling the browser extension
  UNINSTALL_SURVERY_ANSWERED_FULL = 'Uninstall Survey Answered (Full)', // user submits only the answers in the survey and also leaves a detailed feedback on why after uninstalling the browser extension

  WEB_SURVEY_ENABLED = 'Web Survery Enabled', // user is in the cohort eligble to see the web survey
  WEBREQUEST_API_STATUS = 'WebRequest API Status',
  NOTE_VIEWED = 'Note Viewed', // user views the details of a secure note
  NOTES_DESCRIPTION_COPIED = 'Notes Description Copied', // user copied the details of a secure note
  LOGIN_VIEWED = 'Login Viewed', // user views the login page

  SHARED_ITEM_VIEWED = '[Shared Item] Viewed',
  SHARED_ITEM_USERNAME_COPIED = '[Shared Item] Username Copied',
  SHARED_ITEM_PASSWORD_COPIED = '[Shared Item] Password Copied',
  SHARED_ITEM_WEBSITE_OPENED = '[Shared Item] Website Opened',
  SHARED_ITEM_WEBSITE_COPIED = '[Shared Item] Website Copied',
  SHARED_ITEM_NOTES_COPIED = '[Shared Item] Notes Copied',
  SHARED_ITEM_GETTING_STARTED_SELECTED = '[Shared Item] Getting Started Selected',
  SHARED_ITEM_2FA_CODE_COPIED = '[Shared Item] 2FA Code Copied',

  WIFI_ITEM_VIEWED = 'Wifi Viewed',
  CREDIT_CARD_VIEWED = 'Credit Card Viewed',
  CUSTOM_ITEM_VIEWED = 'Custom Item Viewed',

  SIGNOUT = 'Sign Out',

  REFERRAL_LINK_COPIED = 'Referral Link Copied',
  REFERRAL_LINK_CLICKED = 'Referral Link Clicked',

  // Getting started
  GETTING_STARTED_PROMPT_CLICKED = 'Gettings Started Prompt Clicked',
  GETTING_STARTED_APPEARED = 'Gettings Started Appeared',
  IMPORT_EXPLANATION_APPEARED = 'Import Explanation Appeared',
  AUTOFILL_EXPLANATION_APPEARED = 'Autofill Explanation Appeared',
  AUTOSAVE_EXPLANATION_APPEARED = 'Autosave Explanation Appeared',
  GETTING_STARTED_COMPLETED_APPEARED = 'Getting Started Completed Appeared',
  DISMISS_GETTING_STARTED_APPEARED = 'Getting Started Dismiss Appeared',
  GETTING_STARTED_DISMISSED = 'Getting Started Dismissed',
  GETTING_STARTED_PROMPT_APPEARED = 'Getting Started Prompt Appeared',

  CONTINUE_TO_NEXT_PAGE = 'Continue to Next Page', // Get more insight to how users are moved from route to route
  USER_ONBOARDING_STARTED = 'User Onboarding Started', // User needs to do onboarding
}
// Export enum as constants for backwards compatibility
export const ACCOUNT_LIST_VIEWED = TrackingEvent.ACCOUNT_LIST_VIEWED
export const ACCOUNT_VIEWED = TrackingEvent.ACCOUNT_VIEWED
export const AUTOFILL_FAILED = TrackingEvent.AUTOFILL_FAILED
export const AUTOFILL_SELECTED = TrackingEvent.AUTOFILL_SELECTED
export const FILL_FOCUSSED_INPUT = TrackingEvent.FILL_FOCUSSED_INPUT
export const FILL_FOCUSSED_FORM = TrackingEvent.FILL_FOCUSSED_FORM

export const BROWSER_EXTENSION_AUTOSAVE_NEW_LOGIN =
  TrackingEvent.BROWSER_EXTENSION_AUTOSAVE_NEW_LOGIN
export const BROWSER_EXTENSION_AUTOSAVE_UPDATE_LOGIN =
  TrackingEvent.BROWSER_EXTENSION_AUTOSAVE_UPDATE_LOGIN
export const BROWSER_EXTENSION_CAPTURE_WEB_CREDENTIALS =
  TrackingEvent.BROWSER_EXTENSION_CAPTURE_WEB_CREDENTIALS
export const BROWSER_EXTENSION_INSTALL_SKIPPED = TrackingEvent.BROWSER_EXTENSION_INSTALL_SKIPPED
export const BROWSER_EXTENSION_INSTALLED = TrackingEvent.BROWSER_EXTENSION_INSTALLED
export const BROWSER_EXTENSION_INSTALLED_FROM_WEB =
  TrackingEvent.BROWSER_EXTENSION_INSTALLED_FROM_WEB
export const BROWSER_EXTENSION_INSTRUCTIONS_APPEARED =
  TrackingEvent.BROWSER_EXTENSION_INSTRUCTIONS_APPEARED
export const BROWSER_EXTENSION_INSTALL_SKIP_DIALOG_APPEARED =
  TrackingEvent.BROWSER_EXTENSION_INSTALL_SKIP_DIALOG_APPEARED
export const BROWSER_EXTENSION_LOGIN = TrackingEvent.BROWSER_EXTENSION_LOGIN
export const BROWSER_EXTENSION_OPEN_POPUP = TrackingEvent.BROWSER_EXTENSION_OPEN_POPUP
export const BROWSER_EXTENSION_STORE_OPENED = TrackingEvent.BROWSER_EXTENSION_STORE_OPENED
export const BROWSER_EXTENSION_UNINSTALLED = TrackingEvent.BROWSER_EXTENSION_UNINSTALLED
export const BROWSER_EXTENSION_UPDATED = TrackingEvent.BROWSER_EXTENSION_UPDATED
export const BROWSER_EXTENSION_SETUP_DONE_APPEARED =
  TrackingEvent.BROWSER_EXTENSION_SETUP_DONE_APPEARED
export const PIN_INSTRUCTIONS_APPEARED = TrackingEvent.PIN_INSTRUCTIONS_APPEARED
export const AUTOSAVE_INSTRUCTIONS_APPEARED = TrackingEvent.AUTOSAVE_INSTRUCTIONS_APPEARED
export const AUTOSAVE_ENABLED = TrackingEvent.AUTOSAVE_ENABLED
export const AUTOSAVE_DISABLED = TrackingEvent.AUTOSAVE_DISABLED
export const BROWSER_EXTENSION_OPEN_INSTRUCTION_APPEARED =
  TrackingEvent.BROWSER_EXTENSION_OPEN_INSTRUCTION_APPEARED
export const BROWSER_EXTENSION_SETUP_STARTED = TrackingEvent.BROWSER_EXTENSION_SETUP_STARTED

export const FIRST_PASSWORD_WEBSITE_SELECTED = TrackingEvent.FIRST_PASSWORD_WEBSITE_SELECTED

export const IMPORT_CHOOSE_CURRENT_BROWSER = TrackingEvent.IMPORT_CHOOSE_CURRENT_BROWSER
export const IMPORT_CHOOSE_CURRENT_PASSWORD_MANAGER =
  TrackingEvent.IMPORT_CHOOSE_CURRENT_PASSWORD_MANAGER
export const IMPORT_CONTINUE = TrackingEvent.IMPORT_CONTINUE
export const IMPORT_DOWNLOAD_TEMPLATE = TrackingEvent.IMPORT_DOWNLOAD_TEMPLATE
export const IMPORT_FEATURE_RATING = TrackingEvent.IMPORT_FEATURE_RATING
export const IMPORT_1PASSWORD_VAULTS = TrackingEvent.IMPORT_1PASSWORD_VAULTS
export const IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED =
  TrackingEvent.IMPORT_PASSWORD_EXPORT_INSTRUCTION_LINK_CLICKED
export const IMPORT_PASSWORD_FILE_UPLOAD_ERROR = TrackingEvent.IMPORT_PASSWORD_FILE_UPLOAD_ERROR
export const IMPORT_PASSWORD_FILE_UPLOADED = TrackingEvent.IMPORT_PASSWORD_FILE_UPLOADED
export const IMPORT_PASSWORD_SPECIFY_ACCOUNT_COLUMNS =
  TrackingEvent.IMPORT_PASSWORD_SPECIFY_ACCOUNT_COLUMNS
export const IMPORT_PASSWORD_STORAGE_OPTION_SELECTED =
  TrackingEvent.IMPORT_PASSWORD_STORAGE_OPTION_SELECTED
export const IMPORT_PASSWORDS_COMPLETED_FULLY = TrackingEvent.IMPORT_PASSWORDS_COMPLETED_FULLY
export const IMPORT_PASSWORDS_COMPLETED_PARTIALLY =
  TrackingEvent.IMPORT_PASSWORDS_COMPLETED_PARTIALLY
export const IMPORT_PASSWORDS_STARTED = TrackingEvent.IMPORT_PASSWORDS_STARTED
export const IMPORT_SHOW_IMPORTED_ACCOUNTS_BUTTON_CLICKED =
  TrackingEvent.IMPORT_SHOW_IMPORTED_ACCOUNTS_BUTTON_CLICKED
export const IMPORT_STARTED = TrackingEvent.IMPORT_STARTED

export const MOBILE_APP_INSTALLATION_PROMPT = TrackingEvent.MOBILE_APP_INSTALLATION_PROMPT
export const MOBILE_APP_INSTALLATION_PROMPT_ANDROID_CLICKED =
  TrackingEvent.MOBILE_APP_INSTALLATION_PROMPT_ANDROID_CLICKED
export const MOBILE_APP_INSTALLATION_PROMPT_APPLE_CLICKED =
  TrackingEvent.MOBILE_APP_INSTALLATION_PROMPT_APPLE_CLICKED
export const MOBILE_APP_INSTALLATION_PROMPT_SKIPPED =
  TrackingEvent.MOBILE_APP_INSTALLATION_PROMPT_SKIPPED

export const PASSWORD_REVEALED = TrackingEvent.PASSWORD_REVEALED
export const PASSWORD_GENERATED = TrackingEvent.PASSWORD_GENERATED

export const USERNAME_COPIED = TrackingEvent.USERNAME_COPIED
export const PASSWORD_COPIED = TrackingEvent.PASSWORD_COPIED
export const WEBSITE_OPENED = TrackingEvent.WEBSITE_OPENED
export const OTP_COPIED = TrackingEvent.OTP_COPIED

export const SKIP_CREATE_FIRST_ACCOUNT = TrackingEvent.SKIP_CREATE_FIRST_ACCOUNT

export const UNINSTALL_SURVERY_ANSWERED = TrackingEvent.UNINSTALL_SURVERY_ANSWERED
export const UNINSTALL_SURVERY_ANSWERED_FULL = TrackingEvent.UNINSTALL_SURVERY_ANSWERED_FULL

export const WEB_SURVEY_ENABLED = TrackingEvent.WEB_SURVEY_ENABLED
export const WEBREQUEST_API_STATUS = TrackingEvent.WEBREQUEST_API_STATUS

export const SHARED_ITEM_VIEWED = TrackingEvent.SHARED_ITEM_VIEWED
export const SHARED_ITEM_USERNAME_COPIED = TrackingEvent.SHARED_ITEM_USERNAME_COPIED
export const SHARED_ITEM_PASSWORD_COPIED = TrackingEvent.SHARED_ITEM_PASSWORD_COPIED
export const SHARED_ITEM_WEBSITE_OPENED = TrackingEvent.SHARED_ITEM_WEBSITE_OPENED
export const SHARED_ITEM_NOTES_COPIED = TrackingEvent.SHARED_ITEM_NOTES_COPIED
export const SHARED_ITEM_GETTING_STARTED_SELECTED =
  TrackingEvent.SHARED_ITEM_GETTING_STARTED_SELECTED
export const SHARED_ITEM_WEBSITE_COPIED = TrackingEvent.SHARED_ITEM_WEBSITE_COPIED
export const SHARED_ITEM_2FA_CODE_COPIED = TrackingEvent.SHARED_ITEM_2FA_CODE_COPIED

export const WIFI_ITEM_VIEWED = TrackingEvent.WIFI_ITEM_VIEWED
export const CREDIT_CARD_VIEWED = TrackingEvent.CREDIT_CARD_VIEWED
export const REFERRAL_LINK_COPIED = TrackingEvent.REFERRAL_LINK_COPIED
export const REFERRAL_LINK_CLICKED = TrackingEvent.REFERRAL_LINK_CLICKED
export const GETTING_STARTED_PROMPT_CLICKED = TrackingEvent.GETTING_STARTED_PROMPT_CLICKED
export const GETTING_STARTED_APPEARED = TrackingEvent.GETTING_STARTED_APPEARED
export const IMPORT_EXPLANATION_APPEARED = TrackingEvent.IMPORT_EXPLANATION_APPEARED
export const AUTOFILL_EXPLANATION_APPEARED = TrackingEvent.AUTOFILL_EXPLANATION_APPEARED
export const AUTOSAVE_EXPLANATION_APPEARED = TrackingEvent.AUTOSAVE_EXPLANATION_APPEARED
export const GETTING_STARTED_COMPLETED_APPEARED = TrackingEvent.GETTING_STARTED_COMPLETED_APPEARED
export const DISMISS_GETTING_STARTED_APPEARED = TrackingEvent.DISMISS_GETTING_STARTED_APPEARED
export const GETTING_STARTED_DISMISSED = TrackingEvent.GETTING_STARTED_DISMISSED
export const GETTING_STARTED_PROMPT_APPEARED = TrackingEvent.GETTING_STARTED_PROMPT_APPEARED
