/// <reference types="@types/chrome" />
// Allow for future browser API namespace use
// Chrome/Edge/Opera: chrome
// Firefox/Safari: browser
// TODO: Handle Firefox/Safari difference. Maybe abstract them into a unified layer
// For now, we can use this to make the change easier in the future
// Example:
// ```
// import BrowserAPI from '@core/utils/BrowserAPI'
// const browser = BrowserAPI()
// browser.runtime.sendMessage({msg: "test"})
// ```

export default (): typeof chrome => {
  return chrome
}

export const isBrowserAPIAvailable = (): boolean => {
  return typeof chrome != 'undefined'
}
