import { AuthProvider } from '@core/stores/auth'
import { toSentence } from '@core/utils/language'

const providerNames: Map<AuthProvider, string> = new Map([
  ['apple', 'Apple'],
  ['google', 'Google'],
  ['microsoft', 'Microsoft'],
])

export const nameForProvider = (provider: AuthProvider) => {
  const name = providerNames.get(provider)
  if (!name) throw new Error(`Unknown provider ${provider}`)
  return name
}

export const authProviderNames = (supportedProviders: AuthProvider[]) =>
  toSentence(supportedProviders.map(nameForProvider), {
    twoWordsConnector: ' or ',
    lastWordConnector: ', or ',
  })
