import { describe, expect, it } from 'vitest'

import { ItemType } from '@core/stores/items'
import { getItemTypeLabel } from '../itemTypeLabel'

describe('Item Type Label', () => {
  describe('returns the right label for the item type passed', () => {
    it('returns label for credit cards', () => {
      const label = getItemTypeLabel(ItemType.CreditCards)
      expect(label).toBe('card')
    })

    it('returns label for secure notes', () => {
      const label = getItemTypeLabel(ItemType.SecureNotes)
      expect(label).toBe('note')
    })

    it('returns label for secure login', () => {
      const label = getItemTypeLabel(ItemType.Logins)
      expect(label).toBe('login')
    })

    it('returns label for secure wifi items', () => {
      const label = getItemTypeLabel(ItemType.WiFi)
      expect(label).toBe('WiFi')
    })
  })
})
