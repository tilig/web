import { describe, it, expect, vi, beforeEach, afterAll } from 'vitest'
import { generateTotp } from '../totp'

describe('Time based One Time Password generator', () => {
  beforeEach(() => {
    vi.useFakeTimers()
  })

  afterAll(() => {
    vi.clearAllMocks()
  })

  it('should return the same string given the same secret', () => {
    const url = 'otpauth://totp/Test%20Token?secret=2FASTEST&issuer=2FAS'

    const firstResult = generateTotp(url)
    const secondResult = generateTotp(url)
    expect(firstResult).toEqual(secondResult)
  })

  it('should handle null', () => {
    expect(generateTotp(null)).toBeNull()
  })

  it('should handle malformed strings', () => {
    const notAUrl = 'anything@nothing.com'
    expect(generateTotp(notAUrl)).toBeNull()
  })

  it('should handle urls without required parameters', () => {
    const url = 'otpauth://totp/Test%20Token?issuer=2FAS'
    expect(generateTotp(url)).toBeNull()
  })

  it('allows for multiple digits', () => {
    /**
     * Ensure the same OTP
     * This library will turn '01234567' => '1234567'
     * so tests will randomly fail
     */
    const now = 2147483648000
    vi.spyOn(Date, 'now').mockReturnValue(now)

    const url = 'otpauth://totp/Test%20Token?secret=2FASTEST&issuer=2FAS&digits=8'
    expect(generateTotp(url)?.length).toBe(8)
    // Ensure this is correctly mocked
    expect(generateTotp(url)).toBe('77213791')
  })

  it('should handle plain values', () => {
    const url = '2fast 2fury 2beets'
    expect(generateTotp(url)?.length).toBe(6)
  })
})
