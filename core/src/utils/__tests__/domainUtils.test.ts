import { describe, it, expect } from 'vitest'

import { parseDomain, filterByDomain } from '../domainUtils'

describe('parseDomain', () => {
  it('ignores www.', () => {
    const parsed = parseDomain('www.netflix.com')
    expect(parsed).toEqual('netflix.com')
  })

  it('filters domains on top-level domain', () => {
    const parsed = parseDomain('accounts.airbnb.com')
    expect(parsed).toEqual('airbnb.com')
  })

  it('takes eTLD+1 into account', () => {
    const parsed = parseDomain('https://my-project.github.io')
    expect(parsed).toEqual('my-project.github.io')
  })

  it('parses http URLs', () => {
    const parsed = parseDomain('http://google.com')
    expect(parsed).toEqual('google.com')
  })

  it('ignores ports', () => {
    const parsed = parseDomain('https://google.com:1234')
    expect(parsed).toEqual('google.com')
  })

  it('does not parse non-http(s) schemas', () => {
    const parsed = parseDomain('foo://google.com')
    expect(parsed).toBeNull()
  })

  it('parses domain-like schemas as domains', () => {
    const parsed = parseDomain('foo.com://google.com')
    expect(parsed).toEqual('foo.com')
  })

  it('localhost returns null', () => {
    const parsed = parseDomain('localhost:3001')
    expect(parsed).toBeNull()
  })

  it('non-ascii domains return punycode', () => {
    const parsed = parseDomain('https://español.com')
    expect(parsed).toEqual('xn--espaol-zwa.com')
  })

  it('invalid URLs return null', () => {
    const urls = [':/not a url', '1nvalid://scheme', 'https://🎃']
    urls.forEach((url) => {
      const parsed = parseDomain(url)
      expect(parsed).toBeNull()
    })
  })
})

describe('filterByDomain', () => {
  it('returns items that match the same domain', () => {
    const filtered = filterByDomain(['facebook.com', 'airbnb.com'], 'airbnb.com', (a) => a)

    expect(filtered).toEqual(expect.not.arrayContaining(['facebook.com']))
    expect(filtered).toEqual(expect.arrayContaining(['airbnb.com']))
  })

  it('ignores www. in array', () => {
    const filtered = filterByDomain(['www.airbnb.com'], 'airbnb.com', (a) => a)

    expect(filtered).toEqual(expect.arrayContaining(['www.airbnb.com']))
  })

  it('ignores www. in passed domain', () => {
    const filtered = filterByDomain(['airbnb.com'], 'www.airbnb.com', (a) => a)

    expect(filtered).toEqual(expect.arrayContaining(['airbnb.com']))
  })

  it('works with a complex resolver', () => {
    const filtered = filterByDomain(
      [{ attributes: { host: 'airbnb.com' } }],
      'www.airbnb.com',
      (a) => a.attributes.host
    )

    expect(filtered).toHaveLength(1)
  })

  it('matches multiple domains if available', () => {
    const filtered = filterByDomain(
      ['www.airbnb.com', 'airbnb.com', 'www.airbnb.com'],
      'airbnb.com',
      (a) => a
    )

    expect(filtered).toHaveLength(3)
  })

  it('filters domains on top-level domain', () => {
    const filtered = filterByDomain(['airbnb.com'], 'accounts.airbnb.com', (a) => a)

    expect(filtered).toEqual(expect.arrayContaining(['airbnb.com']))
  })

  it('matches domains based on top-level domain', () => {
    const filtered = filterByDomain(['accounts.airbnb.com'], 'airbnb.com', (a) => a)

    expect(filtered).toEqual(expect.arrayContaining(['accounts.airbnb.com']))
  })
})
