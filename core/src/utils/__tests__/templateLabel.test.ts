import { describe, expect, it } from 'vitest'

import { Template } from '@core/clients/tilig/item'
import { getTemplateLabel } from '../templateLabel'

describe('Template Label', () => {
  describe('returns the right label for the template passed', () => {
    it('returns label for credit cards', () => {
      const label = getTemplateLabel(Template.CreditCard)
      expect(label).toBe('card')
    })

    it('returns label for secure notes', () => {
      const label = getTemplateLabel(Template.SecureNote)
      expect(label).toBe('note')
    })

    it('returns label for secure login', () => {
      const label = getTemplateLabel(Template.Login)
      expect(label).toBe('login')
    })

    it('returns label for secure wifi items', () => {
      const label = getTemplateLabel(Template.WiFi)
      expect(label).toBe('WiFi')
    })
  })
})
