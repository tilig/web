import { test, expect } from 'vitest'
import _ from 'lodash'
import { isEmailAddress } from '../isEmailAddress'

test('returns true for example email addresses', () => {
  const emailAddresses = [
    'gertjan@tilig.com',
    'alfred@sweden.com',
    'fred@apple.co.uk',
    'джон@фэйкпочта.com',
    'джон@фэйкпочта.рф',
    '我@在.中国',
    '我@在.test.中国',
    'john@fakeemail.com',
    'john@fakeemail.com',
    'john.smith@fakeemail.com',
    'john-smith@fakeemail.com',
    'john_smith@fakeemail.com',
    'john.smith+auto_reply@fakeemail.com',
    'jsmith1@fakeemail.com',
    '1john@fakeemail.com',
    '-jsmith@fakeemail.com',
    'jsmith-@fakeemail.com',
    'jsmith.@fakeemail.com',
    'jsmith@fake-email.com',
    'jsmith@fake--email.com',
    'jsmith@fake-email2.com',
    'john@test.fakeemail.com',
    'john@test.makeemail.museum',
    'john.smith.1.2.3.4@a.b.c.d.e.f.com',
  ]

  const valid = _.uniq(emailAddresses.map(isEmailAddress))

  expect(valid).toStrictEqual([true])
})

test('returns false for values that are not email addresses', () => {
  const emailAddresses = [
    '------',
    '!@)#!@#',
    'tilig.com',
    'john',
    'джон@фэйкпочта.рф2',
    'john',
    'john@smith',
    'john@',
    '@smith',
    '@fakeemail.com',
    '.@fakeemail.com',
    'john..smith@fakeemail.com',
    '.jsmith@fakeemail.com',
    'jsmith@.fakeemail.com',
    'jsmith@fakeemail.com.',
    'jsmith@fake-email.2',
  ]

  const valid = _.uniq(emailAddresses.map(isEmailAddress))

  expect(valid).toStrictEqual([false])
})
