const SURVEY_QUESTIONS = [
  {
    name: 'Question 1',
    multiple_choice: true,
    other_option: false, // Should there be an 'other'-option to show a text field?
    question_attributes: {
      content: 'On which devices do you need to access your passwords?',
      survey_token: '74e90abc-6036-4438-8dff-ef0f183b53a9',
      token: '5977c0cb-5145-418b-a2b4-03da2ca0381d',
      answer_options: ['Phone', 'Tablet', 'Computer'],
    },
    next: '277d8e7d-a733-4377-8a7c-32dffd46e5c1',
  },
  {
    name: 'Question 2',
    other_option: false,
    multiple_choice: false,
    question_attributes: {
      content: 'Have you used a password manager before?',
      survey_token: '74e90abc-6036-4438-8dff-ef0f183b53a9',
      token: '277d8e7d-a733-4377-8a7c-32dffd46e5c1',
      answer_options: ['Yes', 'No, but I know how it works', 'No, and I don’t know how it works'],
    },
    next: {
      0: 'cd91eae8-6a6d-4fba-befc-d0aba7046632',
      1: 'a2df4855-6dba-414c-8ddb-18d44f85af83',
      2: 'a2df4855-6dba-414c-8ddb-18d44f85af83',
    },
  },
  {
    name: 'Question 3',
    other_option: true,
    multiple_choice: true,
    question_attributes: {
      content: 'Which password managers do you have experience using?',
      survey_token: '74e90abc-6036-4438-8dff-ef0f183b53a9',
      token: 'cd91eae8-6a6d-4fba-befc-d0aba7046632',
      answer_options: [
        'LastPass',
        'Keeper',
        'Bitwarden',
        '1Password',
        'Apple Keychain',
        'Google/Chrome',
      ],
      next: null,
    },
  },
  {
    name: 'Question 3',
    other_option: false,
    multiple_choice: true,
    question_attributes: {
      content: 'Which of the following apply to you?',
      survey_token: '74e90abc-6036-4438-8dff-ef0f183b53a9',
      token: 'a2df4855-6dba-414c-8ddb-18d44f85af83',
      answer_options: [
        'I reuse the same password in many places',
        'I write down my passwords in a safe place',
        'I share passwords for some accounts with other people',
        "I usually use two-factor authentication, when it's available",
        'I do not change my passwords, unless I have to.',
        'I use different passwords for different accounts that I have.',
      ],
    },
  },
]

export { SURVEY_QUESTIONS }
