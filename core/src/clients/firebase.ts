import { FirebaseApp, initializeApp, setLogLevel } from 'firebase/app'
import {
  Auth,
  AuthProvider,
  browserLocalPersistence,
  deleteUser as deleteFirebaseUser,
  getAuth,
  GoogleAuthProvider,
  OAuthProvider,
  signInWithPopup,
  signInWithRedirect,
  UserCredential,
} from 'firebase/auth'
import { getFirestore, doc, setDoc, getDoc, Firestore, deleteDoc } from 'firebase/firestore'
import { Functions, getFunctions, httpsCallable } from 'firebase/functions'

import { createLogger } from '@core/utils/logger'
import { isDebugging } from '@core/utils/debug'
import { getFirebaseConfig } from './firebase/config'

const logger = createLogger('clients:firebase')

let _client: FirebaseTiligClient | null = null

if (isDebugging()) {
  setLogLevel('debug')
}

export const getFirebaseClient = () => {
  if (_client) {
    return _client
  }
  _client = new FirebaseTiligClient()
  return _client
}

export class FirebaseTiligClient {
  app: FirebaseApp
  auth: Auth
  db: Firestore
  functions: Functions
  ready: Promise<void>

  constructor() {
    this.app = initializeApp(getFirebaseConfig())
    this.auth = getAuth(this.app)
    this.db = getFirestore(this.app)
    this.functions = getFunctions(this.app)

    this.ready = this.auth.setPersistence(browserLocalPersistence).then(
      () => {
        logger.debug('Set persistence to LOCAL')
      },
      (e) => {
        logger.error('Error during persistence setting %O', e)
      }
    )
  }
}

interface KeypairMeta {
  app_platform: string
  app_version: string
}

interface EncryptKeypairRequest {
  key: {
    private_key: string
    public_key: string
    key_type: 'x25519'
  }
  meta: KeypairMeta
}
interface EncryptKeypairResponse {
  token: string
}

interface DecryptKeypairRequest {
  encrypted_private_key: string
}
interface DecryptKeypairResponse {
  private_key: string
}

export const encryptKeypair = (request: EncryptKeypairRequest) => {
  const { functions } = getFirebaseClient()
  return httpsCallable<EncryptKeypairRequest, EncryptKeypairResponse>(
    functions,
    'encryptKeypair'
  )(request)
}
export const decryptKeypair = (request: DecryptKeypairRequest) => {
  const { functions } = getFirebaseClient()
  return httpsCallable<DecryptKeypairRequest, DecryptKeypairResponse>(
    functions,
    'decryptPrivateKey'
  )(request)
}

export const generateTiligToken = () => {
  const { functions } = getFirebaseClient()
  return httpsCallable<void, { token: string }>(functions, 'generateTiligToken')()
}

/**
 * Signing in can use redirect method or a popup
 */
export enum SignInMethod {
  PopUp,
  Redirect,
}

export const signInWithGoogle = (method: SignInMethod = SignInMethod.Redirect) => {
  const provider = new GoogleAuthProvider()
  provider.setCustomParameters({
    prompt: 'select_account',
  })
  provider.addScope('email profile')
  return signInWithProvider(provider, method)
}

export type ProviderSignIn = (method?: SignInMethod) => Promise<UserCredential>

/**
 * Sign in with Apple
 * @param method Redirect or Popup
 */
export const signInWithApple = (method: SignInMethod = SignInMethod.Redirect) => {
  const provider = new OAuthProvider('apple.com')
  provider.addScope('email')
  return signInWithProvider(provider, method)
}

/**
 * Sign in with Microsoft
 * @param method Redirect or Popup
 */
export const signInWithMicrosoft = (method: SignInMethod = SignInMethod.Redirect) => {
  const provider = new OAuthProvider('microsoft.com')
  provider.setCustomParameters({
    prompt: 'select_account',
  })
  provider.addScope('email profile')
  return signInWithProvider(provider, method)
}

export const signInWithProvider = (provider: AuthProvider, method: SignInMethod) => {
  const { auth } = getFirebaseClient()
  if (method === SignInMethod.PopUp) {
    return signInWithPopup(auth, provider)
  }
  return signInWithRedirect(auth, provider)
}

/**
 * All private keys are stored in the /keys/ folder
 */
export const KEY_COLLECTION = 'keys'

export const getLegacyPrivateKey = async () => {
  logger.debug('Fetching private key from firebase')
  const { auth, db } = getFirebaseClient()

  if (!auth.currentUser) throw new Error('No user to get private key for')

  const snapshot = await getDoc(doc(db, KEY_COLLECTION, auth.currentUser.uid))

  if (!snapshot.exists()) {
    logger.debug('Private key from does not exist')
    return null
  }

  logger.debug('Got private key from firebase')
  const data = snapshot.data()

  if (!data.uid || !data.privateKey) {
    throw new Error('Malformed document')
  }

  if (data.uid !== auth.currentUser.uid) {
    throw new Error('Obtained document for wrong account. This should NEVER happen!')
  }

  return data.privateKey as string
}

export const storeLegacyPrivateKey = (privateKey: string) => {
  logger.debug('Storing private key on firebase')
  const { auth, db } = getFirebaseClient()
  if (!auth.currentUser) throw new Error('No user to store private key for')

  return setDoc(
    doc(db, KEY_COLLECTION, auth.currentUser.uid),
    {
      uid: auth.currentUser.uid,
      privateKey: privateKey,
    },
    { merge: true }
  )
}

const deleteLegacyPrivateKey = () => {
  logger.debug('Deleting private key from firebase')
  const { auth, db } = getFirebaseClient()
  if (!auth.currentUser) throw new Error('No user to delete private key for')
  return deleteDoc(doc(db, KEY_COLLECTION, auth.currentUser.uid))
}

export const deleteUser = async () => {
  logger.debug('Deleting user from firebase')
  const { auth } = getFirebaseClient()
  if (!auth.currentUser) {
    throw new Error('Cannot delete without a user')
  }
  await deleteLegacyPrivateKey()
  await deleteFirebaseUser(auth.currentUser)
}
