import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
export { isAxiosError } from 'axios'

import backendURL from '@core/utils/backendURL'
import { isBrowserExtension } from '@core/utils/isBrowserExtension'
import browserExtensionVersion from '@core/utils/browserExtensionVersion'
import { platformHeader } from '@core/utils/platformHeader'
import { browser, os, getAppVersion } from '@core/utils/browserEnvironment'
import { useAuthStore } from '@core/stores/auth'
import { createLogger } from '@core/utils/logger'

let axiosInstance: AxiosInstance
let authenticatedAxiosInstance: AxiosInstance

const logger = createLogger('clients:axios')

const createAxiosInstance = () => {
  const instance = axios.create({
    baseURL: backendURL(),
    headers: {
      'x-tilig-platform': platformHeader(),
      'x-tilig-version': isBrowserExtension()
        ? `Browser ${browserExtensionVersion()}`
        : `${platformHeader()} ${getAppVersion()}`,
      'x-browser-name': browser.name || '',
      'x-browser-version': browser.version || '',
      'x-os-name': os.name || '',
    },
  })

  return instance
}

export const useAxios = () => {
  if (!axiosInstance) {
    axiosInstance = createAxiosInstance()
  }

  return axiosInstance
}

export const useAuthenticatedAxios = () => {
  if (!authenticatedAxiosInstance) {
    const instance = createAxiosInstance()

    instance.interceptors.request.use(
      async (config) => {
        const authStore = useAuthStore()
        await authStore.ready

        if (!authStore.accessToken) {
          logger.error('Cannot make authenticated request while unauthenticated')
          throw new Error('Cannot make authenticated request while unauthenticated')
        }

        config.headers['Authorization'] = `Bearer ${authStore.accessToken.accessToken}`
        return config
      },
      (error) => {
        Promise.reject(error)
      }
    )

    // Response interceptor that refreshes tokens after a 401 response
    // and then retries the request with the new access token.
    instance.interceptors.response.use(
      (response) => {
        return response
      },
      async function (error) {
        const originalRequest = error.config as AxiosRequestConfig & { _retry?: boolean }

        if (error.response?.status !== 401 || originalRequest._retry) {
          // Either this request was already retried or the error is not
          // due to expired authentication.
          logger.error('Non-recoverable error')
          return Promise.reject(error)
        }

        logger.debug('Trying to recover from 401 error')
        const authStore = useAuthStore()
        await authStore.ready

        if (
          authStore.accessToken &&
          typeof originalRequest.headers?.['Authorization'] === 'string' &&
          !originalRequest.headers['Authorization'].includes(authStore.accessToken.accessToken)
        ) {
          // This request was made before the accessToken was updated.
          // We just retry this request. Updated accessToken will be set
          // by the requestInterceptor.
          return instance(originalRequest)
        }

        await authStore.refreshAccessToken()
        originalRequest._retry = true

        // We're setting the token as a default here, but because it will be overridden in
        // the request interceptor, that won't be a problem.
        if (authStore.accessToken) {
          instance.defaults.headers.common[
            'Authorization'
          ] = `Bearer ${authStore.accessToken.accessToken}`
        }

        logger.debug('Retrying request with updated access token')
        return instance(originalRequest)
      }
    )

    authenticatedAxiosInstance = instance
  }

  return authenticatedAxiosInstance
}
