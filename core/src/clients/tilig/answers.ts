import { useAuthenticatedAxios } from '../axios'

export interface Answer {
  skipped: boolean
  chosen_options: string[]
  question_attributes:
    | {
        content: string
        survey_token: string
        token: string
        answer_options: string[]
      }
    | undefined
}

export const createAnswer = (answer: Answer) => {
  const axios = useAuthenticatedAxios()
  return axios.post<Answer>(`api/v3/answers`, answer)
}
