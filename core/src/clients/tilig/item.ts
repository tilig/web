import { cloneDeep } from 'lodash'
import * as z from 'zod'
import * as sodium from 'libsodium-wrappers'

import { convertToUrl, stripSchema } from '@core/utils/url'
import { createLogger } from '@core/utils/logger'

import {
  anonymouslyDecryptPrivateKey,
  anonymouslyEncryptPrivateKey,
  decrypt,
  decryptDek,
  decryptMasterSecret,
  deriveDataFromMasterSecret,
  encodeKeypair,
  encrypt,
  encryptDek,
  encryptMasterSecret,
  generateDek,
  generateMasterSecret,
  generateUnencodedKeypair,
  KeyPair,
  legacyDecryptDek,
  legacyEncryptDek,
} from './crypto'
import {
  decryptStringOrNull as legacyDecrypt,
  encryptStringOrNull as legacyEncrypt,
} from './legacyCrypto'

export const MAX_LOGIN_VALUE_LENGTH = 190

const logger = createLogger('clients:tilig:item')

/**
 * Strict regex for ISO8601 DateTime string
 */
export const iso8601StrictSeparator =
  /^([+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-3])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T]((([01]\d|2[0-3])((:?)[0-5]\d)?|24:?00)([.,]\d+(?!:))?)?(\17[0-5]\d([.,]\d+)?)?([zZ]|([+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/

export enum Template {
  Login = 'login/1',
  CreditCard = 'creditcard/1',
  SecureNote = 'securenote/1',
  WiFi = 'wifi/1',
  Custom = 'custom/1',
}
const TemplateEnum = z.nativeEnum(Template)
export const safeTemplate = (template?: Template | null) => template?.replace('/', '-')

/**
 * Brand information from API
 */
export const Brand = z.object({
  id: z.string(),
  name: z.string().nullable(),
  domain: z.string(),
  public_suffix_domain: z.string(),
  totp: z.boolean(),
  main_color_hex: z.string().nullable(),
  main_color_brightness: z.string().nullable(),
  logo_source: z.string().nullable(),
  logo_icon_source: z.string().nullable(),
  is_fetched: z.boolean(),
})
export type Brand = z.infer<typeof Brand>

/**
 * Item share
 */
export const Share = z.object({
  id: z.string().uuid(),
  times_viewed: z.number(),
  encrypted_master_key: z.string(),
})
export type Share = z.infer<typeof Share>

const Member = z.object({
  id: z.string().uuid().nullable(),
  email: z.string(),
  display_name: z.string().nullable(),
  public_key: z.string().nullable(),
  picture: z.string().nullable(),
})
export type Member = z.infer<typeof Member>

const FolderMembership = z.object({
  id: z.string().uuid(),
  role: z.string(),
  is_me: z.boolean(),
  accepted: z.boolean(),
  pending: z.boolean(),
  missing_private_key: z.boolean(),
  user: Member,
  creator: z
    .object({
      id: z.string().uuid().nullable(),
    })
    .nullable(),
  accepted_at: z.string().regex(iso8601StrictSeparator).nullable(),
  created_at: z.string().regex(iso8601StrictSeparator),
  updated_at: z.string().regex(iso8601StrictSeparator),
})

export type FolderMembership = z.infer<typeof FolderMembership>

export const Folder = z.object({
  id: z.string().uuid(),
  name: z.string().nullable(),
  description: z.string().nullable(),
  single_item: z.boolean(),
  public_key: z.string(),
  my_encrypted_private_key: z.string().nullable(),
  folder_memberships: z.array(FolderMembership),
  folder_invitations: z.array(FolderMembership),
  created_at: z.string().regex(iso8601StrictSeparator),
  updated_at: z.string().regex(iso8601StrictSeparator),
})
export type Folder = z.infer<typeof Folder>

/**
 * Fields as expected in a v1 (legacy) Secret
 *
 * Legacy Secrets will have encryption_version: 1,
 * no dek, overview, or details.
 */
export const EncryptedItemV1 = z.object({
  id: z.string().uuid(),

  created_at: z.string().regex(iso8601StrictSeparator),
  updated_at: z.string().regex(iso8601StrictSeparator),

  encryption_version: z.literal(1),

  template: TemplateEnum.nullable(),
  website: z.string().nullable(),
  brand: Brand.nullable(),

  // TEMP fields
  legacy_encryption_disabled: z.boolean().nullish(),

  // Legacy fields
  name: z.string().nullish(),
  notes: z.string().nullish(),
  otp: z.string().nullish(),
  android_app_id: z.string().nullish(),
  password: z.string().nullish(),
  username: z.string().nullish(),

  encrypted_dek: z.null(),
  rsa_encrypted_dek: z.null(),
  encrypted_overview: z.null(),
  encrypted_details: z.null(),

  share_link: Share.nullable(),
  encrypted_folder_dek: z.null(),
  folder: Folder.nullable(),
})

/**
 * Fields as expected in a v2 Secret
 *
 * Legacy Secrets will have encryption_version: 2,
 * non-null dek, overview, and details.
 */
export const EncryptedItemV2 = z.object({
  id: z.string().uuid(),

  created_at: z.string().regex(iso8601StrictSeparator),
  updated_at: z.string().regex(iso8601StrictSeparator),

  encryption_version: z.literal(2),

  // When the app supports more item templates, add them to this
  // enum. Any item that has an unsupported template will not
  // be visible in the app due to parse errors by Zod.
  template: TemplateEnum,
  website: z.string().nullable(),
  brand: Brand.nullable(),

  encrypted_dek: z.string().nullable(),
  rsa_encrypted_dek: z.string().nullable(),
  encrypted_overview: z.string(),
  encrypted_details: z.string(),

  // Migration fields
  legacy_encryption_disabled: z.boolean().nullish(),

  // Legacy fields
  name: z.string().nullish(),
  notes: z.string().nullish(),
  otp: z.string().nullish(),
  android_app_id: z.string().nullish(),
  password: z.string().nullish(),
  username: z.string().nullish(),

  // Sharing related fields
  share_link: Share.nullable(),
  encrypted_folder_dek: z.string().nullable(),
  folder: Folder.nullable(),
})

/**
 * Items are either a v1 Secret or a v2 Secret
 *
 * We cannot handle items that are inbetween.
 */
export const EncryptedItem = z.discriminatedUnion('encryption_version', [
  EncryptedItemV1,
  EncryptedItemV2,
])
export type EncryptedItem = z.infer<typeof EncryptedItem>

/**
 * Nullable string
 *
 * Defaults to null if it's missing
 */
const DefaultNullableString = z.string().nullable().default(null)

/**
 * Overview payload for v2 Item
 *
 * The overview contains less sensitive information, which can
 * be decrypted as soon as it is received from the API.
 *
 * Any data that is needed for matching, sorting, displaying, etc.,
 * should be present in the overview.
 *
 * We use .passthrough() in multiple places, which means Zod will
 * allow any additional attributes not defined in the schema. This
 * is for forward compatibility with with new fields. If we don't
 * allow unknown attributes to remain as-is, it would stip them
 * from the overview when re-encrypting.
 */
export const DecryptedOverview = z
  .object({
    name: DefaultNullableString,
    info: DefaultNullableString,
    urls: z
      .object({
        // Can be null
        url: DefaultNullableString,
        // Can be null or completely absent
        name: z.string().nullish(),
      })
      .passthrough()
      .array(),
    android_app_ids: z.string().array(),
  })
  .passthrough()
export type DecryptedOverview = z.infer<typeof DecryptedOverview>

/**
 * Different `kinds` of fields that can be present in `main` or `fields`
 *
 * For forwards compatibility, any string is allowed.
 */
const FieldKind = z.string()
export type FieldKind = z.infer<typeof FieldKind>

const CustomField = z
  .object({
    name: z.string(),
    kind: FieldKind,
    value: DefaultNullableString,
  })
  .passthrough()
export type CustomField = z.infer<typeof CustomField>

export const HistoryField = z
  .object({
    kind: FieldKind,
    value: z.string(),
    replaced_at: z.string().regex(iso8601StrictSeparator),
  })
  .passthrough()
export type HistoryField = z.infer<typeof HistoryField>

/**
 * Details payload for v2 Item
 *
 * The details contain the 'meat' of the Secret. This is more
 * sensitive data that sould only be decrypted Just-In-Time (e.g.
 * when (auto)filling or displaying a secret.)
 */
export const DecryptedDetails = z
  .object({
    notes: DefaultNullableString,

    main: z
      .object({
        kind: FieldKind,
        value: DefaultNullableString,
      })
      .passthrough()
      .array(),

    history: HistoryField.array(),

    custom_fields: CustomField.array(),
  })
  .passthrough()
export type DecryptedDetails = z.infer<typeof DecryptedDetails>

/**
 * Fields that were encrypted with RSA in v1 Secrets
 */
export interface LegacyFields {
  username: string | null
  password: string | null
  notes: string | null
  otp: string | null
}

/**
 * Current time as ISO string
 */
const currentIsoTimestamp = () => new Date().toISOString()

export const initializeOverview = (): DecryptedOverview => {
  return {
    name: null,
    info: null,
    urls: [],
    android_app_ids: [],
  }
}

export const initializeDetails = (): DecryptedDetails => {
  return {
    main: [],
    notes: null,
    history: [],
    custom_fields: [],
  }
}

const initializeEncryptedItem = (): EncryptedItem => {
  const currentTime = currentIsoTimestamp()
  return {
    id: '',
    website: null,
    brand: null,
    template: Template.Login,

    // These are assumed to not be null in the Type, but they
    // need empty values.
    encryption_version: 2,
    encrypted_dek: null,
    rsa_encrypted_dek: null,
    encrypted_details: '',
    encrypted_overview: '',

    // Legacy fields
    name: null,
    notes: null,
    otp: null,
    password: null,
    username: null,
    android_app_id: null,

    created_at: currentTime,
    updated_at: currentTime,

    share_link: null,
    folder: null,
    encrypted_folder_dek: null,
  }
}

interface DecryptedShare {
  id: string
  timesViewed: number
  masterSecret: string
}

/**
 * A completely decrypted Item
 */
export interface DecryptedItem {
  item: EncryptedItem
  overview: DecryptedOverview
  details: DecryptedDetails
  share: DecryptedShare | null
}

/**
 * Initialize a new Item, including details and overview
 */
export const initializeItem = (initial?: Partial<DecryptedItem>): DecryptedItem => {
  return {
    item: initializeEncryptedItem(),
    overview: initializeOverview(),
    details: initializeDetails(),
    share: null,
    ...initial,
  }
}

/**
 * @private
 */
export const decryptOverview = (encrypted_overview: string, dek: Uint8Array) => {
  const decrypted = decrypt(encrypted_overview, dek)
  return DecryptedOverview.parse(JSON.parse(decrypted))
}
/**
 * @private
 */
export const encryptOverview = (overview: DecryptedOverview, dek: Uint8Array) => {
  // The overview is re-parsed, so we don't accidentally encrypt malformed
  // overviews.
  const parsed = DecryptedOverview.parse(overview)
  return encrypt(JSON.stringify(parsed), dek)
}
/**
 * @private
 */
export const decryptDetails = (encrypted_details: string, dek: Uint8Array) => {
  const decrypted = decrypt(encrypted_details, dek)
  return DecryptedDetails.parse(JSON.parse(decrypted))
}
/**
 * @private
 */
export const encryptDetails = (details: DecryptedDetails, dek: Uint8Array) => {
  // The details are re-parsed, so we don't accidentally encrypt malformed
  // details.
  const parsed = DecryptedDetails.parse(details)
  return encrypt(JSON.stringify(parsed), dek)
}

const decryptLegacyFields = async (item: EncryptedItem, keyPair: CryptoKeyPair) => {
  const [username, password, notes, otp] = await Promise.all([
    legacyDecrypt(item.username ?? null, keyPair.privateKey),
    legacyDecrypt(item.password ?? null, keyPair.privateKey),
    legacyDecrypt(item.notes ?? null, keyPair.privateKey),
    legacyDecrypt(item.otp ?? null, keyPair.privateKey),
  ])

  return {
    username,
    password,
    notes,
    otp,
  } as LegacyFields
}

const encryptLegacyFields = async (
  item: EncryptedItem,
  originalFields: LegacyFields,
  newFields: LegacyFields,
  keyPair: CryptoKeyPair
) => {
  await Promise.all(
    Object.keys(newFields).map(async (key) => {
      const originalValue = originalFields[key as keyof LegacyFields]
      const newValue = newFields[key as keyof LegacyFields]

      logger.debug(
        'Checking legacy field %s %O %O',
        key,
        originalValue !== newValue,
        originalValue,
        newValue
      )

      if (originalValue !== newValue) {
        item[key as keyof LegacyFields] = await legacyEncrypt(newValue, keyPair.publicKey)
      }
    })
  )

  return item
}

const updateOverviewFromLegacyFields = (
  item: EncryptedItem,
  legacyFields: LegacyFields,
  overview: DecryptedOverview
) => {
  overview.name = item.name || null
  overview.info = legacyFields.username || null

  // Only migrate URL's if don't already have them
  if (overview.urls.length === 0) {
    if (item.website) {
      overview.urls = [{ url: convertToUrl(item.website) }]
    } else {
      overview.urls = []
    }
  }

  if (item.android_app_id) {
    overview.android_app_ids = [item.android_app_id]
  } else {
    overview.android_app_ids = []
  }
}

const updateDetailsFromLegacyFields = (legacyFields: LegacyFields, details: DecryptedDetails) => {
  details.notes = legacyFields.notes
  setInMain(details, 'totp', legacyFields.otp)
  setInMain(details, 'username', legacyFields.username)
  setInMain(details, 'password', legacyFields.password)
}

const trackPasswordChange = (originalDetails: DecryptedDetails, details: DecryptedDetails) => {
  const oldPassword = findInMain(originalDetails, 'password')
  const password = findInMain(details, 'password')
  if (oldPassword && oldPassword !== password) {
    logger.debug('Password changed, storing history')
    details.history.push({
      kind: 'password',
      value: oldPassword,
      replaced_at: currentIsoTimestamp(),
    })
  }
}

export interface PartialDecryption {
  legacyFields?: LegacyFields
  overview: DecryptedOverview
}

export interface FullDecryption extends PartialDecryption {
  details: DecryptedDetails
  share: DecryptedShare | null
}

const getFolderKeyPair = (folder: Folder, keyPair: KeyPair): KeyPair => {
  if (!folder.my_encrypted_private_key) throw new Error('Cannot get keypair without private key')

  const decodedPrivateKey = anonymouslyDecryptPrivateKey(folder.my_encrypted_private_key, keyPair)
  const privateKey = sodium.to_base64(decodedPrivateKey)

  return {
    publicKey: folder.public_key,
    privateKey,
    keyType: 'x25519',
  }
}

const encryptDekForFolder = (dek: Uint8Array, folder: Folder, keyPair: KeyPair) => {
  const folderKeyPair = getFolderKeyPair(folder, keyPair)
  return encryptDek(dek, folderKeyPair)
}

const getDekFromFolder = (item: EncryptedItem, keyPair: KeyPair) => {
  if (!item.folder) throw new Error('Cannot get folder dek without a folder')
  if (!item.encrypted_folder_dek) throw new Error('Missing folder DEK')

  const folderKeyPair = getFolderKeyPair(item.folder, keyPair)
  return decryptDek(item.encrypted_folder_dek, folderKeyPair)
}

/**
 * Decrypt the overview of an encrypted item
 *
 * Also returns details if `true` is passed as a third param.
 * @param item Item to decrypt
 * @param keyPair Libsodium keypair
 * @param legacyKeyPair RSA CryptoKeyPair
 * @param includeDetails Also decrypt item details
 */
export const decryptItem = async <IncludeDetails extends boolean | undefined>(
  item: EncryptedItem,
  keyPair: KeyPair,
  legacyKeyPair: CryptoKeyPair,
  includeDetails: IncludeDetails = false as IncludeDetails
): Promise<IncludeDetails extends true ? FullDecryption : PartialDecryption> => {
  // v1 encryption items have no overview or details
  let overview: DecryptedOverview = initializeOverview()
  let details: DecryptedDetails = initializeDetails()

  // TODO: Remove legacy stuff
  let legacyFields = undefined
  // V1 Login items don't have a template set
  if (!item.template) item.template = Template.Login
  let share: DecryptedShare | null = null

  logger.debug('Decrypting item with includeDetails', item.id, includeDetails)

  let dek: Uint8Array | null = null
  const folderKeyPair = item.folder ? getFolderKeyPair(item.folder, keyPair) : null

  if (item.encrypted_folder_dek) {
    if (!folderKeyPair) throw new Error('Cannot decrypt folder item without folder keypair')
    dek = decryptDek(item.encrypted_folder_dek, folderKeyPair)
  } else if (item.encrypted_dek) {
    logger.debug('Using dek from new encryption', item.id)
    dek = decryptDek(item.encrypted_dek, keyPair)
  } else if (item.rsa_encrypted_dek) {
    logger.debug('Using dek from legacy encryption', item.id)
    dek = await legacyDecryptDek(item.rsa_encrypted_dek, legacyKeyPair)
  }

  // FIXME: After migration DEK should always be available
  if (dek && item.encrypted_overview) {
    overview = decryptOverview(item.encrypted_overview, dek)

    if (includeDetails) {
      details = decryptDetails(item.encrypted_details, dek)

      if (item.share_link) {
        let masterSecret: string

        if (item.folder) {
          if (!folderKeyPair) throw new Error('Cannot decrypt share-link without folder keypair')
          logger.debug('Decrypting master secret using folder keypair')
          masterSecret = decryptMasterSecret(item.share_link.encrypted_master_key, folderKeyPair)
        } else {
          logger.debug('Decrypting master secret using own keypair')
          masterSecret = decryptMasterSecret(item.share_link.encrypted_master_key, keyPair)
        }

        share = {
          id: item.share_link.id,
          timesViewed: item.share_link.times_viewed,
          masterSecret,
        }
      }
    }
  }

  // TODO: Remove legacy stuff
  if (item.template === Template.Login && !item.legacy_encryption_disabled) {
    logger.debug('Decrypting legacy fields', item.id)
    legacyFields = await decryptLegacyFields(item, legacyKeyPair)
    logger.debug('Done decrypting legacy fields, synchronizing', item.id)
    updateOverviewFromLegacyFields(item, legacyFields, overview)

    if (includeDetails) {
      updateDetailsFromLegacyFields(legacyFields, details)
    }
    logger.debug('Done synchronizing legacy fields', item.id)
  }

  logger.debug('Done decrypting item', item.id)

  if (includeDetails) {
    // Assigned to variable for proper typing
    const result: FullDecryption = {
      overview,
      details,
      legacyFields,
      share,
    }
    return result
  }

  const result: PartialDecryption = {
    overview,
    legacyFields,
  }

  // Type cast is necessary to make tsc happy
  return result as IncludeDetails extends true ? FullDecryption : PartialDecryption
}

export interface EncryptItemOptions {
  /**
   * Item to use for DEK decryption
   */
  originalItem?: EncryptedItem
  /**
   * Generate a new DEK
   */
  rotateDek?: boolean
}

/**
 * Update the encrypted fields of an item
 *
 * Automatically syncs legacy fields and does
 * not reencrypt if it isn't needed.
 *
 * Returns a new item object instead of
 * updating in place
 * @param item encrypted item
 * @param overview updated  overview
 * @param details updated details
 * @param keyPair key pair of the user
 * @param legacyKeyPair RSA key pair of the user
 * @returns Updated item
 */
export const encryptItem = async (
  item: EncryptedItem,
  overview: DecryptedOverview,
  details: DecryptedDetails,
  keyPair: KeyPair,
  legacyKeyPair: CryptoKeyPair,
  options?: EncryptItemOptions
) => {
  const { originalItem = null, rotateDek = false } = options ?? {}
  logger.debug('Encrypting item %O %O %O', item, overview, details)

  const decryptionItem = originalItem || item
  const updatedItem = cloneDeep(item)
  // NOTE: item should not be used past this point

  // TODO: Remove legacy stuff
  // V1 Login items don't have their template set
  if (!decryptionItem.template) decryptionItem.template = Template.Login
  if (!updatedItem.template) updatedItem.template = Template.Login

  let dek: Uint8Array | null = null

  if (!rotateDek) {
    if (decryptionItem.encrypted_folder_dek) {
      dek = getDekFromFolder(decryptionItem, keyPair)
    } else if (decryptionItem.encrypted_dek) {
      logger.debug('encrypted_dek present')
      dek = decryptDek(decryptionItem.encrypted_dek, keyPair)
    } else if (decryptionItem.rsa_encrypted_dek) {
      logger.debug('rsa_encrypted_dek present')
      dek = await legacyDecryptDek(decryptionItem.rsa_encrypted_dek, legacyKeyPair)
      updatedItem.encrypted_dek = encryptDek(dek, keyPair)
    }
  }

  if (!dek || rotateDek) {
    logger.debug('Generating new dek, forced: %s', rotateDek)
    dek = generateDek()
    if (updatedItem.folder) {
      logger.debug('Item has folder')
      updatedItem.encrypted_folder_dek = encryptDekForFolder(dek, updatedItem.folder, keyPair)
    } else {
      logger.debug('Item has no folder')
      updatedItem.encrypted_folder_dek = null
      updatedItem.encrypted_dek = encryptDek(dek, keyPair)
      updatedItem.rsa_encrypted_dek = await legacyEncryptDek(dek, legacyKeyPair)
    }
  }

  const originalValues = await decryptItem(decryptionItem, keyPair, legacyKeyPair, true)

  // TODO: Remove legacy stuff
  if (updatedItem.template === Template.Login && !updatedItem.legacy_encryption_disabled) {
    logger.debug('Encrypting legacy fields')
    // Update legacy fields from overview and details
    const newLegacyFields = new LegacyHelper(overview, details).legacyFields
    if (originalValues.legacyFields) {
      await encryptLegacyFields(
        updatedItem,
        originalValues.legacyFields,
        newLegacyFields,
        legacyKeyPair
      )
    }
  } else {
    logger.debug('Not encrypting legacy fields')
  }

  updatedItem.encrypted_overview = encryptOverview(overview, dek)
  trackPasswordChange(originalValues.details, details)
  updatedItem.encrypted_details = encryptDetails(details, dek)

  // TODO: Remove legacy stuff
  if (updatedItem.template === Template.Login && !updatedItem.legacy_encryption_disabled) {
    updatedItem.encryption_version = 2
    updatedItem.name = overview.name
    updatedItem.android_app_id = overview.android_app_ids[0] ?? null
  }

  // TODO: Determine if this needs to be done in a different place
  updatedItem.website = overview.urls[0]?.url ? stripSchema(overview.urls[0].url) : null

  return updatedItem
}

class ShareError {
  name = 'ShareError'
  constructor(public message: string) {}
}

export interface ShareData {
  encryptedDek: string
  encryptedMasterSecret: string
  uuid: string
  accessToken: string
}

export const generateShareData = (item: EncryptedItem, keyPair: KeyPair) => {
  if (!item.encrypted_folder_dek && !item.encrypted_dek) {
    throw new ShareError('Cannot share v1 item')
  }

  const folderKeyPair = item.folder ? getFolderKeyPair(item.folder, keyPair) : null
  let dek: Uint8Array
  if (item.folder) {
    if (!folderKeyPair) throw new ShareError('Cannot get folder dek without folder keypair')
    logger.debug('Getting dek from folder for sharing')
    dek = getDekFromFolder(item, keyPair)
  } else {
    if (!item.encrypted_dek) throw new ShareError('Missing dek')
    logger.debug('Getting dek from owner for sharing')
    dek = decryptDek(item.encrypted_dek, keyPair)
  }

  const masterSecret = generateMasterSecret()
  let encryptedMasterSecret: string
  if (item.folder) {
    if (!folderKeyPair) throw new ShareError('Cannot encrypt master secret without folder keypair')
    encryptedMasterSecret = encryptMasterSecret(masterSecret, folderKeyPair)
  } else {
    encryptedMasterSecret = encryptMasterSecret(masterSecret, keyPair)
  }

  const derivedData = deriveDataFromMasterSecret(masterSecret)
  const encryptedDek = encrypt(dek, derivedData.keyEncryptionKey)

  return {
    encryptedDek,
    encryptedMasterSecret,
    uuid: derivedData.uuid,
    accessToken: derivedData.accessToken,
  }
}

export interface EncryptedFolderData {
  folderPublicKey: string
  encryptedFolderDek: string
  encryptedMasterSecret: string | null
  ownEncryptedPrivateKey: string
  folderMemberEncryptedPrivateKey?: string
}

export const generateFolderDataFromItem = (
  item: EncryptedItem,
  keyPair: KeyPair,
  folderMemberPublicKey: string | null
): EncryptedFolderData => {
  if (!item.encrypted_dek) throw new ShareError('Cannot share v1 item')
  if (item.folder) throw new ShareError('Item already belongs to a folder')

  const dek = decryptDek(item.encrypted_dek, keyPair)

  const decodedFolderKeyPair = generateUnencodedKeypair()
  const folderKeyPair = encodeKeypair(decodedFolderKeyPair)

  const encryptedFolderDek = encryptDek(dek, folderKeyPair)

  let encryptedMasterSecret: string | null = null
  if (item.share_link) {
    const masterSecret = decryptDek(item.share_link.encrypted_master_key, keyPair)
    encryptedMasterSecret = encryptMasterSecret(masterSecret, folderKeyPair)
  }

  const ownEncryptedPrivateKey = anonymouslyEncryptPrivateKey(
    decodedFolderKeyPair.privateKey,
    keyPair.publicKey
  )

  const folderData = {
    folderPublicKey: folderKeyPair.publicKey,
    encryptedFolderDek,
    encryptedMasterSecret,
    ownEncryptedPrivateKey,
  }

  if (folderMemberPublicKey) {
    const folderMemberEncryptedPrivateKey = anonymouslyEncryptPrivateKey(
      decodedFolderKeyPair.privateKey,
      folderMemberPublicKey
    )

    return {
      ...folderData,
      folderMemberEncryptedPrivateKey,
    }
  }

  return folderData
}

export const encryptFolderPrivateKeyForMember = (
  folder: Folder,
  keyPair: KeyPair,
  shareePublicKey: string
) => {
  if (!folder.my_encrypted_private_key)
    throw new ShareError('Cannot add to folder without a known private key')
  const folderPrivateKey = anonymouslyDecryptPrivateKey(folder.my_encrypted_private_key, keyPair)
  return anonymouslyEncryptPrivateKey(folderPrivateKey, shareePublicKey)
}

/**
 * Fetch value for a specific kind of field from details
 * @param details decrypted details of an item
 * @param kind kind of field to fetch
 */
export const findInMain = (details: DecryptedDetails, kind: FieldKind) => {
  return details.main.find((f) => f.kind === kind)?.value ?? null
}

/**
 * Update the value of a field in details
 * @param details decrypted details of an item
 * @param kind kind of field
 * @param value update value for the field
 */
export const setInMain = (details: DecryptedDetails, kind: FieldKind, value: string | null) => {
  const existing = details.main.find((f) => f.kind === kind)
  if (existing) {
    existing.value = value
    return
  }

  details.main.push({ kind, value })
}

// TODO: Not too sure about the userfulness of this helper
/**
 * Helper class that maps overview and details data to lagacy fields
 */
class LegacyHelper {
  constructor(private overview: DecryptedOverview, private details: DecryptedDetails) {}

  get name() {
    return this.overview.name
  }

  set name(newName: string | null) {
    this.overview.name = newName
  }

  get info() {
    return this.overview.info
  }

  set info(newInfo: string | null) {
    this.overview.info = newInfo
  }

  get password(): string | null {
    return findInMain(this.details, 'password')
  }

  set password(newPassword: string | null) {
    setInMain(this.details, 'password', newPassword)
  }

  get username(): string | null {
    return findInMain(this.details, 'username')
  }

  set username(newUsername: string | null) {
    setInMain(this.details, 'username', newUsername)
  }

  get totp(): string | null {
    return findInMain(this.details, 'totp')
  }

  set totp(newTotp: string | null) {
    setInMain(this.details, 'totp', newTotp)
  }

  get notes(): string | null {
    return this.details.notes
  }

  set notes(newNotes: string | null) {
    this.details.notes = newNotes
  }

  get legacyFields() {
    return {
      username: this.username,
      password: this.password,
      otp: this.totp,
      notes: this.notes,
    } as LegacyFields
  }
}
