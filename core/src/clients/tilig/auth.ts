import { createLogger } from '@core/utils/logger'

import { useAuthenticatedAxios, useAxios } from '../axios'
import {
  decryptKeypair as decryptKeyPairInKeyService,
  encryptKeypair,
  generateTiligToken,
  getLegacyPrivateKey,
  storeLegacyPrivateKey,
} from '../firebase'

import { KeyPair, generateKeypair, LegacyKeyPair } from './crypto'
import { generateLegacyKeyPair } from './legacyCrypto'
import { platformHeader } from '@core/utils/platformHeader'
import { getAppVersion } from '@core/utils/browserEnvironment'
import { EncryptedKeyPair, storeLegacyPublicKey, UserKeyPairData } from './profile'

const logger = createLogger('clients:tilig:auth')

export interface TiligAccessToken {
  accessToken: string
  refreshToken: string
}

const getTiligToken = async () => {
  logger.debug('Fetching tilig token')
  const result = await generateTiligToken()
  return result.data.token
}

export type AccessTokenTrigger = 'refresh' | 'handoff' | null

const authEndpointWithParams = (trigger?: AccessTokenTrigger, referralCode?: string | null) => {
  let endpoint = '/api/v3/authenticate'
  if (trigger) {
    endpoint += `?trigger=${trigger}`
  }
  if (referralCode) {
    endpoint += `?referral=${referralCode}`
  }
  return endpoint
}

export const getAccessToken = async (
  trigger?: AccessTokenTrigger,
  referralCode?: string | null
) => {
  logger.debug('Getting access token %s %s', trigger, referralCode)
  const axios = useAxios()
  const tiligToken = await getTiligToken()
  const endpoint = authEndpointWithParams(trigger, referralCode)

  return axios.post<TiligAccessToken>(
    endpoint,
    {},
    {
      headers: { Authorization: `Token ${tiligToken}` },
    }
  )
}

export const refreshAccessToken = async (oldToken: TiligAccessToken) => {
  logger.debug('Refreshing access token')
  const axios = useAxios()
  return axios.post<TiligAccessToken>(
    '/api/v3/refresh',
    {},
    {
      headers: {
        Authorization: `Token ${oldToken.refreshToken}`,
      },
    }
  )
}

/**
 * Store encrypted keyPair on Tilig
 *
 * @param token JWT encoded keyPair from firebase
 */
const storeEncryptedKeyPair = async (token: string) => {
  logger.debug('Storing keypair on tilig')
  const axios = useAuthenticatedAxios()

  await axios.post('/api/v3/keypairs', {
    encrypted_keypair: token,
  })
  logger.debug('Stored keypair on tilig')
}

/**
 * Generate a new Libsodium keypair and store it securely on Tilig
 *
 * Encrypts the private key of the new key pair with the Key Service
 */
const generateAndStoreKeyPair = async () => {
  logger.debug('Generating keypair')
  const keyPair = generateKeypair()

  logger.debug('Encryting keypair')
  const {
    data: { token },
  } = await encryptKeypair({
    key: {
      private_key: keyPair.privateKey,
      public_key: keyPair.publicKey,
      key_type: keyPair.keyType,
    },
    meta: {
      app_platform: platformHeader(),
      app_version: getAppVersion(),
    },
  })
  logger.debug('Keypair encrypted')
  await storeEncryptedKeyPair(token)
  return keyPair
}

const decryptPrivateKey = async (encryptedKeyPair: EncryptedKeyPair) => {
  logger.debug('Decrypting private key')
  const {
    data: { private_key: privateKey },
  } = await decryptKeyPairInKeyService({
    encrypted_private_key: encryptedKeyPair.encrypted_private_key,
  })
  logger.debug('Private key decrypted')
  return privateKey
}

const getOrCreateKeyPair = async (keyPair: EncryptedKeyPair | null) => {
  if (keyPair) {
    logger.debug('Got keypair, decrypting private key')
    const privateKey = await decryptPrivateKey(keyPair)
    const decryptedKeyPair: KeyPair = {
      privateKey,
      publicKey: keyPair.public_key,
      keyType: 'x25519',
    }
    return decryptedKeyPair
  } else {
    logger.debug('No keypair, generating new one')
    return generateAndStoreKeyPair()
  }
}

const getOrCreateLegacyKeyPair = async (legacyPublicKey: string | null) => {
  let legacyPrivateKey = await getLegacyPrivateKey()

  if (!legacyPrivateKey) {
    logger.debug('No private key, generating new keypair')
    const legacyKeyPair: LegacyKeyPair = await generateLegacyKeyPair()
    legacyPrivateKey = legacyKeyPair.privateKey
    legacyPublicKey = legacyKeyPair.publicKey

    logger.debug('Storing new keypair')
    await Promise.all([
      storeLegacyPrivateKey(legacyKeyPair.privateKey),
      storeLegacyPublicKey(legacyKeyPair.publicKey),
    ])
    logger.debug('Stored new keypair')
  }

  if (!legacyPublicKey) {
    throw new Error('Private key exists on firebase but public key is missing from api')
  }

  const legacyKeyPair: LegacyKeyPair = {
    privateKey: legacyPrivateKey,
    publicKey: legacyPublicKey,
  }

  return legacyKeyPair
}

export const getKeyPairs = async (
  keyPairData: UserKeyPairData
): Promise<{ keyPair: KeyPair; legacyKeyPair: LegacyKeyPair }> => {
  logger.debug('Getting key pairs')
  const [keyPair, legacyKeyPair] = await Promise.all([
    getOrCreateKeyPair(keyPairData.encryptedKeyPair),
    getOrCreateLegacyKeyPair(keyPairData.rsaPublicKey),
  ])
  logger.debug('Obtained key pairs')
  return {
    keyPair,
    legacyKeyPair,
  }
}
