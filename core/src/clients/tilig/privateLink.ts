import * as sodium from 'libsodium-wrappers'
import * as z from 'zod'
import { createLogger } from '@core/utils/logger'

import { decrypt, deriveDataFromMasterSecret } from './crypto'
import { Brand, DecryptedDetails, DecryptedOverview, decryptOverview, decryptDetails } from './item'

const logger = createLogger('client:tilig:privateLink')

/**
 * Share as obtained from shares/private_link API
 */
export const EncryptedShare = z.object({
  // Not a UUID, but 32 hex characters
  uuid: z.string(),
  encrypted_dek: z.string(),
  encrypted_overview: z.string(),
  encrypted_details: z.string(),
  // FIXME: Update from multiple template MR
  template: z.enum(['login/1']),
  website: z.string().nullable(),
  domain: z.string().nullable(),
  brand: Brand.nullable(),
})
export type EncryptedShare = z.infer<typeof EncryptedShare>

export interface DecryptedShare {
  share: EncryptedShare
  overview: DecryptedOverview
  details: DecryptedDetails
}

export class PrivateLinkError implements Error {
  public name = 'PrivateLinkError'
  constructor(public message: string) {}
}

export const decryptShare = async (
  share: EncryptedShare,
  masterSecret: string
): Promise<DecryptedShare> => {
  await sodium.ready
  share = EncryptedShare.parse(share)

  const secret = sodium.from_base64(masterSecret)
  const { keyEncryptionKey } = deriveDataFromMasterSecret(secret)
  logger.debug('Derived KEK %O', keyEncryptionKey)
  const dek = decrypt(share.encrypted_dek, keyEncryptionKey, 'uint8array')
  if (dek.length !== sodium.crypto_secretbox_KEYBYTES) {
    throw new PrivateLinkError(`DEK has wrong length ${dek.length}`)
  }
  logger.debug('Decrypted DEK %O', dek)
  const overview = decryptOverview(share.encrypted_overview, dek)
  const details = decryptDetails(share.encrypted_details, dek)

  return {
    share,
    overview,
    details,
  }
}

export const deriveAccessDataFromMasterSecret = async (masterSecret: string) => {
  await sodium.ready
  let secret: Uint8Array

  try {
    secret = sodium.from_base64(masterSecret)
  } catch (e) {
    throw new PrivateLinkError('Could not decode master secret')
  }

  const { uuid, accessToken } = deriveDataFromMasterSecret(secret)
  return {
    uuid,
    accessToken,
  }
}
