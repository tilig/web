import { createLogger } from '@core/utils/logger'
import { useAuthenticatedAxios } from '@core/clients/axios'
import { Brand } from './item'

export { Brand } from './item'

const logger = createLogger('clients:tilig:brands')

export const getBrand = (url: string) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Fetching Brand')
  return axios.get<Brand>(`api/v3/brands?url=${url}`)
}

export const searchBrands = (prefix: string) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Searching Brands')
  return axios.get<Brand[]>(`api/v3/brands/search?prefix=${prefix}&page_size=10`)
}
