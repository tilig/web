import { createLogger } from '@core/utils/logger'
import { useAuthenticatedAxios, useAxios } from '../axios'

const logger = createLogger(`clients:tilig:tracking`)

export type TrackingProperties = {
  [key: string]: unknown
}

export interface TrackingResponse {
  msg: string
}

export const trackEvent = async (event: string, properties: TrackingProperties) => {
  const axios = useAuthenticatedAxios()
  logger.debug(`Sending tracking event: ${event}`, properties)
  return axios.post<TrackingResponse>('api/v3/track', { event, properties })
}

export const trackPerson = async (properties: TrackingProperties) => {
  const axios = useAuthenticatedAxios()
  return axios.post<TrackingResponse>('api/v3/track_person', { properties })
}

export const trackAnonymousEvent = async (event: string, properties: TrackingProperties) => {
  const axios = useAxios()
  return axios.post<TrackingResponse>('api/v3/anonymous_track', { event, properties })
}
