import { useAuthenticatedAxios } from '../axios'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('clients:tilig:referral')

export interface Rank {
  email_short: string
  rank: number
  is_me: boolean
  points: number
}

export interface LeaderBoard {
  me: Rank
  top_list: Rank[]
}

export const getReferralLeaderboard = async () => {
  logger.debug(`Fetching tilig referral leaderboard`)
  const axios = useAuthenticatedAxios()
  return axios.get<{ referral_leaderboard: LeaderBoard }>('/api/v3/referral_leaderboard')
}
