import { describe, expect, it } from 'vitest'
import { deferredDecryptItem } from '../deferredDecryption'
import { findInMain } from '../item'
import { generateRsaCryptoKeyPair } from '../legacyCrypto'
import { v1EncryptedItem, keyPair, rsaCryptoKeyPair } from './__fixtures__/crypto.fixtures'

describe('tilig async decryption of an encrypted item', () => {
  it('should decrypt the item successfully', async () => {
    const result = await deferredDecryptItem(v1EncryptedItem, keyPair, rsaCryptoKeyPair)
    expect(result.overview.name).toEqual('disneyplus.com')
    expect(result.overview.info).toEqual('mark@mdp.im')
  })

  it('should throw an error when decryption fails', async () => {
    const badKeyPair = await generateRsaCryptoKeyPair()
    await expect(deferredDecryptItem(v1EncryptedItem, keyPair, badKeyPair)).rejects.toThrow()
  })

  it('should decrypt the item with details successfully', async () => {
    const result = await deferredDecryptItem(v1EncryptedItem, keyPair, rsaCryptoKeyPair, true)
    expect(result.overview.name).toEqual('disneyplus.com')
    expect(result.overview.info).toEqual('mark@mdp.im')
    expect(findInMain(result.details, 'username')).toEqual('mark@mdp.im')
    expect(findInMain(result.details, 'password')).toEqual('prnlhxfL3!')
  })
})
