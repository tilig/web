import * as sodium from 'libsodium-wrappers'
import { beforeAll, describe, expect, it } from 'vitest'
import { decrypt, decryptDek } from '../crypto'

const testCases = [
  {
    platform: 'android',
    privateKey: 'HVJt_MxHlE1T7JK9nQO9Z5uwumsOEaMYYYKSChmag7s',
    publicKey: '3imoVUEjSEB_jr48iCivmYem2fIuxxdGWmMw9QmUz3k',
    encryptedDek:
      'yuy1J6bgEYEaG0cWAqzsoFmUB5uSdB7H1UWUyj5Ut6IdojKna4Jz370zv5-REvhCI7jDmTQ3_mbV2j4hZVJlJx5DLSQVXX6z',
    encryptedPayload:
      'gVRv39EIFnnT0ZqJUNx7hl5xayBfZcJVsvLKnMKzVboURXXJwDHETuPcd8SCJLMXHMSj8F2A__v2CPA0',
    plainPayload: 'Message from android',
  },
  {
    platform: 'ios',
    privateKey: 'DN25970xN8lE-h6jBJ_cCidduNlamFyIkc73hBCX9gs',
    publicKey: '8lgIsUJ30ds21RyZwwJfxIx98j4ymzM6Ws1MxHmXCng',
    encryptedDek:
      'EcKWbo80Z099cN47-UPTN7C4dchSCgOK0hp4v1fL8CZDyBF018uj6dAyn0zLp5N5FGPmXnlHINX5QguU93Mnn2x5PlxYBnaP',
    encryptedPayload: '90Iub29FZgZiJWOy1bCssmTM7b6lDpbKhgnfbE1yvXrfl-8_RDaF4Og0FZgg8cWW4YU--kaiEA',
    plainPayload: 'iOS is tha 💣',
  },
  {
    platform: 'web',
    privateKey: 'OVkDL2pzAemehYACNdc7tB7wYB3qejxKiZ2WpPOnr50',
    publicKey: '7gpk9z5P-M1LrmMR4upBOCKWsIiSbEno3RlTqOufBA4',
    encryptedDek:
      'btoctTEaM0M6S8zEkrlZX5o8Ww_ssZTaMXDpAhVZVy9NBCN0PMktI2RodL-s1KHydqiTjfnBtUBmJ49o57pCPK6VVpVUipU_',
    encryptedPayload: 'iIipZTSPpWMJsLpDJ53rEuVrY0-V2VVPuLOsuLs1arPxPKoI0vHacz2oUl0wNEmY8Ly_TxhHIBM',
    plainPayload: 'Message from web',
  },
]

describe.each(testCases)('crypto test for platform $platform', (testCase) => {
  beforeAll(async () => {
    await sodium.ready
  })

  const decryptKey = () =>
    decryptDek(testCase.encryptedDek, {
      privateKey: testCase.privateKey,
      publicKey: testCase.publicKey,
      keyType: 'x25519',
    })

  const privateKey = sodium.from_base64(testCase.privateKey)
  const publicKey = sodium.from_base64(testCase.publicKey)

  it('has a rightly sized keypair', () => {
    expect(privateKey.length).toEqual(32)
    expect(publicKey.length).toEqual(32)
  })

  it('has a public key that is derived from the private key', () => {
    const derivedPublicKey = sodium.crypto_scalarmult_base(privateKey)
    expect(derivedPublicKey).toEqual(publicKey)
  })

  it('decrypts the encrypted DEK using the keypair', () => {
    const dek = decryptKey()
    expect(dek.length).toEqual(32)
  })

  it('decrypts they payload with the DEK', () => {
    const decryptedPayload = decrypt(testCase.encryptedPayload, decryptKey())
    expect(decryptedPayload).toEqual(testCase.plainPayload)
  })
})
