import { describe, vi, test, expect } from 'vitest'
import { EncryptedItem, Template } from '../item'
import { createItem, updateItem } from '../items'

const mockedAxios = {
  post: vi.fn(),
  put: vi.fn(),
}

vi.mock('../../axios', () => ({
  useAuthenticatedAxios: vi.fn(() => mockedAxios),
}))

describe('tilig accounts client', () => {
  describe('create account', () => {
    test('it includes only sanitized fields', async () => {
      mockedAxios.post.mockClear()

      const account: EncryptedItem = {
        id: 'not-null',
        website: 'not-null',
        brand: null,
        template: Template.Login,

        // These are assumed to not be null in the Type, but they
        // need empty values.
        encryption_version: 2,
        encrypted_dek: 'not-null',
        rsa_encrypted_dek: 'not-null',
        encrypted_details: 'not-null',
        encrypted_overview: 'not-null',

        // Legacy fields
        name: 'not-null',
        notes: 'not-null',
        otp: 'not-null',
        password: 'not-null',
        username: 'not-null',
        android_app_id: 'not-null',

        created_at: 'not-null',
        updated_at: 'not-null',

        share_link: null,
        folder: null,
        encrypted_folder_dek: 'not-null',
      }

      await createItem(account)

      expect(mockedAxios.post).toHaveBeenCalledOnce()
      expect(mockedAxios.post).toHaveBeenLastCalledWith('/api/v3/items/', {
        item: {
          website: 'not-null',
          template: Template.Login,
          encryption_version: 2,
          encrypted_dek: 'not-null',
          rsa_encrypted_dek: 'not-null',
          encrypted_details: 'not-null',
          encrypted_overview: 'not-null',
          name: 'not-null',
          notes: 'not-null',
          otp: 'not-null',
          password: 'not-null',
          username: 'not-null',
          android_app_id: 'not-null',
          encrypted_folder_dek: 'not-null',
          folder_id: null,
        },
      })
    })
  })

  describe('update account', () => {
    test('it includes only sanitized fields', async () => {
      mockedAxios.put.mockClear()

      const account: EncryptedItem = {
        id: 'not-null',
        website: 'not-null',
        brand: null,
        template: Template.Login,

        // These are assumed to not be null in the Type, but they
        // need empty values.
        encryption_version: 2,
        encrypted_dek: 'not-null',
        rsa_encrypted_dek: 'not-null',
        encrypted_details: 'not-null',
        encrypted_overview: 'not-null',

        // Legacy fields
        name: 'not-null',
        notes: 'not-null',
        otp: 'not-null',
        password: 'not-null',
        username: 'not-null',
        android_app_id: 'not-null',

        created_at: 'not-null',
        updated_at: 'not-null',

        share_link: null,
        folder: null,
        encrypted_folder_dek: 'not-null',
      }

      await updateItem('123', account)

      expect(mockedAxios.put).toHaveBeenCalledOnce()
      expect(mockedAxios.put).toHaveBeenLastCalledWith('/api/v3/items/123', {
        item: {
          website: 'not-null',
          template: Template.Login,
          encryption_version: 2,
          folder_id: null,
          encrypted_dek: 'not-null',
          rsa_encrypted_dek: 'not-null',
          encrypted_details: 'not-null',
          encrypted_overview: 'not-null',
          encrypted_folder_dek: 'not-null',
          name: 'not-null',
          notes: 'not-null',
          otp: 'not-null',
          password: 'not-null',
          username: 'not-null',
          android_app_id: 'not-null',
        },
      })
    })
  })
})
