import { EncryptedItem } from '../../item'
import { importLegacyKeyPair } from '../../legacyCrypto'

export const v1EncryptedItem: EncryptedItem = {
  id: 'fc7b6dd0-49f2-4a64-9f0a-112530638bff',
  android_app_id: null,
  created_at: '2022-10-03T18:45:38.184Z',
  encrypted_dek: null,
  rsa_encrypted_dek: null,
  encrypted_details: null,
  encrypted_overview: null,
  encryption_version: 1,
  name: 'disneyplus.com',
  notes: null,
  otp: null,
  password:
    'vOtgiYidnF2CE2HMlkANg5JZITUAo8hYKixFgJ7gcNxbpdRlx2tGKUB0SMjkYVXRmyv2D9jEi0JM389GQgFEN8dprMfNqYoTg6DiXLw1baomSgncAI3j/EthFot/IH6RfCv+Yb4qfZeoQTpcw12lPJiUMBUod/0+kmvETo5IAJ6s0vWJytl5Wxumc0CqV0W/aqZLpXdCzHZfCKEf0X3WbXoS8J8hHBX6/h3BaIN0uWyQh1J6aAN3QjfI0feRSh5ack9UjdbQXgm68ToT3WIkiHVekrc61yA0ME3CAWzMzDy65CcLWu89maJrutiEKqPaLgdBDYl5yCwSi4sZt0BpaA==',
  template: null,
  updated_at: '2022-10-03T18:59:02.517Z',
  username:
    'jboEV/c+BZhLC27BGafcTymKhQmc7ctBj3J0W3vgkJoBiUdY4hGA+mKefcNBdcY5cKV+GOkhsoXL3QA07I9IwmTgQoolnhRlpC91AV00QEMuniZ7xAvhGQ43jrU59cnjRYL7lnGpa0YeKPNGhPuBuA0Wm+1EIMKGYC9iCne3aA2SaZ/rdFuKcFGFoDuDvXDzpBRJLvtZ7tz7xJHevsroHFYkkAoEj11snNZDVwc5hvy2u8+o+Trd9qy7gDkNE5cT8p421GVp/G1KuurEcYwst15K0cHUk9pZuOxS/vAxmeGenHqUSzj8sEBWptc3ROKA3sA9oTtovCcd0laECPdS/w==',
  website: 'disneyplus.com',
  brand: {
    id: '3ed01477-1364-4134-aa00-2b9691395350',
    name: 'Disney+',
    domain: 'disneyplus.com',
    public_suffix_domain: 'disneyplus.com',
    totp: false,
    main_color_hex: '#0063e5',
    main_color_brightness: '87',
    logo_source: 'https://asset.brandfetch.io/idhQlYRiX2/idXlvTZp9i.png',
    logo_icon_source: 'https://asset.brandfetch.io/idhQlYRiX2/idRXC5mxqs.png',
    is_fetched: true,
  },
  share_link: null,
  folder: null,
  encrypted_folder_dek: null,
}

const privateKey =
  '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDK51eC6tNbStVm4PUhoirdzHy+0t+qWoQRdOJU4UO95obrd/A7fcj4rB0czXGhdR0cFb5Tb07eduSIMbIwURhDacTqf4dt1FuYy0Pa//d/wiov99ZuzTZLxWZCfzAiC6+8SKwfoHuEllGzMTZOXyJfZYPn4FfQ0QUICoEh4nmvc7GQKu9lT1B2iX/VJ9A3TtG4+o02QlBuJU+dQCshqva/RkBYjarHDwpRoH0v124tbukDtrFu5T/DE2aVCptuL6kFxy41mywCJoF/1D+p4Irt9Ga6hJlHS94rCiqpuRGfrdppMwyF3G7eLylxRA8PW38J3s32nQYnDYenPykWt12ZAgMBAAECggEAFKIm74zCtodrIR2iP+vWURzQ2LToAtJWmNHajEgmE+Mj+EEHoH0fmU4jXcD0m7cloSwC4Ugx7ISJo+Ht3WT5wy031aLnWj9cmzKccWJTQF413Q/mcfkTWuw1hHCbq/KlSc/YFRVESkzg/Qh1nc2l2K6hlcxmEbmXueQbkp8Nwc4jMAqo0tpRKElnKqB+P4VyButbmWTt6YnMEIxGXRNS8H+H9focDs+8FtQ+04dc1QCE54yeVQTT2FBhoYvkX4TgoVvWqpDQOVLDbVKNiH5nVuj9+gtJwg6+zNgbz//1g8aNtWmMyywWFTKBpKQNMJoX2HcDN7BWHBDJJqcygNjdAQKBgQDsFMpC0Tq1LqQHc6+beam8t6bNqaMbA4Bu0MK5T13Mss2cX447aiEPJAEIsSZ9Kw6o4C1SZuWK+KUflaVHvjwrfK2zBsKGd38fDc4HlYsFJCTE/NLaSrx25ZJZwSG6glXThV+rnveNuYkpG9/0yJ5ypF+lXOJbOigDZ4+8zdZN2QKBgQDcBe/1N0osIV3HqbehPH6hGcf26UWPtNRo3Jw3Zt+rPJptBfWUs/YISysd6/yUun4jmIBx4vGiHelfS9rfSBzRBBj2LvZY+vQG7KNBPTAsQYewh0XaXIOxbFhP0bmuNLcZMd6zyZvxjbjs8uTZPPOdiSsMJfiYqfxr2/P7txb1wQKBgE7J4XbLPTyahEO4aDMLN4q0AAdDRhwN3x8crOALjNJ0GgrGGUHa+Y2EBpRQCvhbFlll97o4fQMUWIdh+fcnlg0tXwsQhns6BkLm6Iu/bNYaaesUNYqExsEnfOBXhFoqhVpCeNteAmBaO1xs4SFgkwDPutwFU8X+crwwI9hvGcaBAoGAb/JxagHsslFzxsAal2YRQ+S7Mmz0IK8wF/6PWTz1dsnKBMiwcC20c/nTAVFt3TqaLYBNciS3LvYrJHqS7WJijxKXpMK2QwXEnbVPR0VbuljJMk9Rgk3qD58Bu/MIl5Noyd/u4OkmeBvUyHK5b7KsfS4qge7b8skijrqGBD/e5cECgYEA1CBovkirtByRhorxQMQ0EqnrfPHtYqBL+p8OhyoNC8cJkRXW/1xSeUXIa1+w1a6uGjXsElYAgq0uNs6wubgXasNzE3Y+PmbHAaF2QHpS4hPHgfR24zEgj9rDfAelGRnOQi2D2z91w7OFb1GujsdarBZtpuNSORRsJhZOXCZE3sw=\n-----END PRIVATE KEY-----'
const publicKey =
  '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyudXgurTW0rVZuD1IaIq3cx8vtLfqlqEEXTiVOFDveaG63fwO33I+KwdHM1xoXUdHBW+U29O3nbkiDGyMFEYQ2nE6n+HbdRbmMtD2v/3f8IqL/fWbs02S8VmQn8wIguvvEisH6B7hJZRszE2Tl8iX2WD5+BX0NEFCAqBIeJ5r3OxkCrvZU9Qdol/1SfQN07RuPqNNkJQbiVPnUArIar2v0ZAWI2qxw8KUaB9L9duLW7pA7axbuU/wxNmlQqbbi+pBccuNZssAiaBf9Q/qeCK7fRmuoSZR0veKwoqqbkRn63aaTMMhdxu3i8pcUQPD1t/Cd7N9p0GJw2Hpz8pFrddmQIDAQAB\n-----END PUBLIC KEY-----'

export const keyPair = {
  publicKey: 'OiCFvLEeKCHex0YNnQtc5o4CeY2e-Scut9IE5gvKaWY',
  privateKey: 'LLTsyfKDM7h2V9W3d8Bi5TNwab25XOsr4rh5jGEhYOw',
  keyType: 'x25519' as const,
}

export const folderKeyPair = {
  publicKey: 'XhJ_wxf59-gudepGCiyerLeuNaJGgkwPLczetuzB4Qc',
  privateKey: '9sYAnoP5lxt89y6c6W93njHiksZz6hFY5hsoMH6p0DI',
  keyType: 'x25519' as const,
}

export const encryptedFolderPrivateKey =
  'qORZOl_raZiuPgiKdEYrfz9K9TFBHYULr4bJqfLF7hbj62ypIWnNbAeGnjG9xZ4xfXEugMAIxwMnScbsHRqG3KgRL9lCf49xzn3wb72FgD8'

export const legacyKeyPair = {
  privateKey,
  publicKey,
}

export const rsaCryptoKeyPair = await importLegacyKeyPair(legacyKeyPair)
