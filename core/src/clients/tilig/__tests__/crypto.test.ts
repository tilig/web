import * as sodium from 'libsodium-wrappers'
import { describe, test, expect } from 'vitest'
import { keyPair, rsaCryptoKeyPair } from './__fixtures__/crypto.fixtures'
import {
  anonymouslyDecryptPrivateKey,
  anonymouslyEncryptPrivateKey,
  decrypt,
  decryptDek,
  encrypt,
  encryptDek,
  generateDek,
  generateKeypair,
  legacyDecryptDek,
  legacyEncryptDek,
} from '../crypto'

const knownDek = new Uint8Array([
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
])

describe('tilig crypto client', () => {
  describe('key management', () => {
    describe('key encryption key pair', () => {
      test('it generates a DEK of proper length', () => {
        const keyPair = generateKeypair()
        expect(sodium.from_base64(keyPair.privateKey)).toHaveLength(
          sodium.crypto_box_SECRETKEYBYTES
        )
        expect(sodium.from_base64(keyPair.publicKey)).toHaveLength(sodium.crypto_box_PUBLICKEYBYTES)
      })
    })

    describe('data encryption key', () => {
      test('it generates a DEK of proper length', () => {
        const dek = generateDek()
        expect(dek).toHaveLength(sodium.crypto_secretbox_KEYBYTES)
      })

      test('it encrypts a dek', () => {
        const encrypted = encryptDek(knownDek, keyPair)
        expect(encrypted).toBeTruthy()
        expect(encrypted).not.toEqual(sodium.to_base64(knownDek))
      })

      test('it encrypts a dek with legacy encryption', () => {
        const encrypted = legacyEncryptDek(knownDek, rsaCryptoKeyPair)
        expect(encrypted).toBeTruthy()
        expect(encrypted).not.toEqual(sodium.to_base64(knownDek))
      })

      test('it performs a round trip successfully', () => {
        const encrypted = encryptDek(knownDek, keyPair)
        const decrypted = decryptDek(encrypted, keyPair)
        expect(decrypted).toEqual(knownDek)
      })

      test('it performs a round trip successfully using legacy decryption', async () => {
        const encrypted = await legacyEncryptDek(knownDek, rsaCryptoKeyPair)
        const decrypted = await legacyDecryptDek(encrypted, rsaCryptoKeyPair)
        expect(decrypted).toEqual(knownDek)
      })

      test('it throws on a short payload', () => {
        expect(() => decryptDek('AA', keyPair)).toThrowError(/Malformed payload/)
      })

      test('it throws on a wrong payload with legacy decryption', () => {
        expect(legacyDecryptDek('randomstring', rsaCryptoKeyPair)).rejects.toThrowError()
      })
    })
  })

  describe('data encryption', () => {
    test('it encrypts without errors', () => {
      const ciphertext = encrypt('my payload', knownDek)
      expect(ciphertext).toBeTruthy()
    })

    test('it throws errors when using wrong key length', () => {
      expect(() => encrypt('my payload', new Uint8Array(31))).toThrowError(/Key has wrong length/)
    })

    test('it encrypt nondeterministically', () => {
      const payload = 'my payload'
      const ciphertext1 = encrypt(payload, knownDek)
      const ciphertext2 = encrypt(payload, knownDek)
      expect(ciphertext1).not.toEqual(ciphertext2)
    })
  })

  describe('decryption', () => {
    test('it decrypts successfully', () => {
      // Known plaintext: 'my payload' (dek = 1, nonce = 1)
      const ciphertext = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB25a-x4NcK3dXZsBP9UCEU3_2eJ2JeN6EfLQ'
      const payload = decrypt(ciphertext, knownDek)
      expect(payload).toEqual('my payload')
    })

    test('it throws on wrong keylength', () => {
      const dek = new Uint8Array(31)
      expect(() => decrypt('something', dek)).toThrowError(/Key has wrong length/)
    })

    test('it throws on wrong payload size', () => {
      const shortPayload = 'AA' // new Uint8Array([0])
      expect(() => decrypt(shortPayload, knownDek)).toThrowError(/Malformed payload/)
    })

    test('it throws on wrong key', () => {
      // Known plaintext: 'my payload' (dek = random, nonce = 1)
      const otherPayload = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABfTw8QM8i3MbE8VIC98rx04rlvVjiHqg2dW4'
      expect(() => decrypt(otherPayload, knownDek)).toThrowError()
    })

    test('it decrypts with additional newlines', () => {
      // Known plaintext: 'my payload' (dek = 1, nonce = 1)
      const ciphertext =
        '\n\n\nAAAAAAAA\n\n\n\nAAAAAAAAAAAA\nAAAAAAAAAAAB25a\n-x4NcK3dXZsBP9UCEU3_2eJ2JeN6EfLQ\n\n\n'
      const payload = decrypt(ciphertext, knownDek)
      expect(payload).toEqual('my payload')
    })
  })

  describe('round trip', () => {
    test('it performs a round trip successfully', () => {
      const payload = 'another payload'
      const encrypted = encrypt(payload, knownDek)
      const decrypted = decrypt(encrypted, knownDek)
      expect(decrypted).toEqual(payload)
    })
  })

  describe('anonymous encryption', () => {
    test('it performs a round trip successfully', () => {
      const payload = 'anonymous payload'
      const encrypted = anonymouslyEncryptPrivateKey(sodium.from_string(payload), keyPair.publicKey)
      const decrypted = anonymouslyDecryptPrivateKey(encrypted, keyPair)
      expect(sodium.to_string(decrypted)).toEqual(payload)
    })
  })
})
