import { describe, test, expect, vi, beforeEach, it, beforeAll } from 'vitest'
import {
  DecryptedDetails,
  DecryptedOverview,
  EncryptedItem,
  encryptItem,
  decryptItem,
  Template,
  decryptDetails,
  FullDecryption,
} from '../item'
import {
  decrypt,
  decryptDek,
  encryptDek,
  generateDek,
  legacyDecryptDek,
  legacyEncryptDek,
} from '../crypto'
import {
  v1EncryptedItem,
  keyPair,
  encryptedFolderPrivateKey,
  folderKeyPair,
  rsaCryptoKeyPair,
} from './__fixtures__/crypto.fixtures'

const knownDek = new Uint8Array(32)
knownDek[31] = 1

vi.mock('../crypto', async () => {
  const crypto = await vi.importActual<Record<string, unknown>>('../crypto')

  return {
    ...crypto,
    generateDek: vi.fn(() => knownDek),

    // Wrap encryption functions with spies
    decrypt: vi.fn((...args: Parameters<typeof decrypt>) =>
      (crypto.decrypt as typeof decrypt)(...args)
    ),
    encryptDek: vi.fn((...args: Parameters<typeof encryptDek>) =>
      (crypto.encryptDek as typeof encryptDek)(...args)
    ),
    legacyEncryptDek: vi.fn((...args: Parameters<typeof legacyEncryptDek>) =>
      (crypto.legacyEncryptDek as typeof legacyEncryptDek)(...args)
    ),
    decryptDek: vi.fn((...args: Parameters<typeof decryptDek>) =>
      (crypto.decryptDek as typeof decryptDek)(...args)
    ),
    legacyDecryptDek: vi.fn((...args: Parameters<typeof legacyDecryptDek>) =>
      (crypto.legacyDecryptDek as typeof legacyDecryptDek)(...args)
    ),
  }
})

describe('tilig account client', () => {
  describe('parsers', () => {
    describe('EncryptedItem', () => {
      test('it should parse a v1 encrypted account', () =>
        expect(EncryptedItem.parse(v1EncryptedItem)).toMatchSnapshot())

      test('it should parse a v2 encrypted account', () => {
        const item = {
          ...v1EncryptedItem,
          template: 'login/1',
          encryption_version: 2,
          rsa_encrypted_dek: 'Non null dek',
          encrypted_overview: 'Non null overview',
          encrypted_details: 'Non null details',
        }

        expect(EncryptedItem.parse(item)).toMatchSnapshot()
      })

      test('it should throw on null new encryption values', () => {
        const item = {
          ...v1EncryptedItem,
          template: 'login/1',
          encryption_version: 2, // New encryption version
          rsa_encrypted_dek: null, // All null
          encrypted_overview: null, // All null
          encrypted_details: null, // All null
        }

        expect(() => EncryptedItem.parse(item)).toThrowErrorMatchingSnapshot()
      })

      test('it should throw on unknown template', () => {
        const item = {
          ...v1EncryptedItem,
          template: 'unknown/1',
        }

        expect(() => EncryptedItem.parse(item)).toThrowErrorMatchingSnapshot()
      })
    })

    describe('DecryptedOverview', () => {
      const overview = {
        name: 'My Account',
        info: 'e@mail.com',
        urls: [{ url: 'https://beer.com' }],
        android_app_ids: [],
      }

      test('it parses correctly', () => {
        expect(DecryptedOverview.parse(overview)).toMatchSnapshot()
      })

      test('it handles extra attributes', () => {
        expect(DecryptedOverview.parse({ ...overview, additionalField: true })).toMatchSnapshot()
      })

      test('it parses missing nullable overview values', () => {
        const overview = {
          urls: [{}],
          android_app_ids: [],
        }
        expect(DecryptedOverview.parse(overview)).toMatchSnapshot()
      })

      test('it parses null urls', () => {
        expect(
          DecryptedOverview.parse({ ...overview, urls: [{ url: null, name: null }] })
        ).toMatchSnapshot()
      })

      test('it parses malformed urls', () => {
        expect(
          DecryptedOverview.parse({ ...overview, urls: [{ url: '$invalid://' }] })
        ).toMatchSnapshot()
      })
    })

    describe('DecryptedDetails', () => {
      const details = {
        notes: 'My notes',
        main: [{ kind: 'password', value: 'My password' }],
        history: [],
        custom_fields: [],
      }

      test('it parses correctly', () => {
        expect(DecryptedDetails.parse(details)).toMatchSnapshot()
      })

      test('it handles extra attributes', () => {
        expect(
          DecryptedDetails.parse({ ...details, meta: { additionalField: true } })
        ).toMatchSnapshot()
      })

      test('it parses missing nullable details ', () => {
        const details = {
          main: [{ kind: 'password' }],
          history: [],
          custom_fields: [{ kind: 'custom', name: 'custom' }],
        }
        expect(DecryptedDetails.parse(details)).toMatchSnapshot()
      })
    })
  })

  describe('encryption', () => {
    const overview: DecryptedOverview = {
      name: 'New name',
      info: 'New info',
      android_app_ids: ['android://newapp'],
      urls: [{ url: 'https://new.website' }],
    }

    const details: DecryptedDetails = {
      main: [
        { kind: 'username', value: 'New username' },
        { kind: 'password', value: 'New password' },
        { kind: 'totp', value: 'New totp' },
      ],
      notes: 'New notes',
      history: [],
      custom_fields: [],
    }

    describe('encryptAccount', () => {
      let result: EncryptedItem
      beforeAll(async () => {
        vi.mocked(generateDek).mockClear()
        vi.mocked(encryptDek).mockClear()
        vi.mocked(legacyEncryptDek).mockClear()
        result = await encryptItem(v1EncryptedItem, overview, details, keyPair, rsaCryptoKeyPair)
      })

      test("it generates a dek if it's empty", () => {
        expect(vi.mocked(generateDek)).toHaveBeenCalledOnce()
        expect(vi.mocked(encryptDek)).toHaveBeenLastCalledWith(knownDek, keyPair)
        expect(vi.mocked(legacyEncryptDek)).toHaveBeenLastCalledWith(knownDek, rsaCryptoKeyPair)

        expect(result.encrypted_dek).toBeTruthy() // Non-deterministic
        expect(result.rsa_encrypted_dek).toBeTruthy() // Non-deterministic
      })

      test('it updates a v1 account to v2 account', () => {
        expect(result.template).toEqual('login/1')
        expect(result.encryption_version).toEqual(2)
        expect(result.encrypted_dek).not.toBeNull()
        expect(result.rsa_encrypted_dek).not.toBeNull()
        expect(result.encrypted_overview).not.toBeNull()
        expect(result.encrypted_details).not.toBeNull()
      })

      test('it updates legacy fields', () => {
        expect(result.android_app_id).toEqual('android://newapp')
        expect(result.username).not.toEqual(v1EncryptedItem.username)
        expect(result.password).not.toEqual(v1EncryptedItem.password)
        expect(result.otp).not.toEqual(v1EncryptedItem.otp)
        expect(result.notes).not.toEqual(v1EncryptedItem.notes)
      })

      test('it updates password history', () => {
        const parsedDetails = decryptDetails(result.encrypted_details ?? '', knownDek)
        expect(parsedDetails.history[0].value).toEqual('prnlhxfL3!')
      })

      describe('with legacy encryption disabled', () => {
        it('does not update the legacy fields', async () => {
          const result = await encryptItem(
            { ...v1EncryptedItem, legacy_encryption_disabled: true },
            overview,
            details,
            keyPair,
            rsaCryptoKeyPair
          )
          expect(result.android_app_id).toEqual(v1EncryptedItem.android_app_id)
          expect(result.username).toEqual(v1EncryptedItem.username)
          expect(result.password).toEqual(v1EncryptedItem.password)
          expect(result.otp).toEqual(v1EncryptedItem.otp)
          expect(result.notes).toEqual(v1EncryptedItem.notes)
        })
      })
    })

    describe('decryptAcount', () => {
      beforeEach(() => {
        vi.mocked(legacyDecryptDek).mockClear()
      })

      describe('v1 account', () => {
        let result: FullDecryption
        beforeAll(async () => {
          result = await decryptItem(v1EncryptedItem, keyPair, rsaCryptoKeyPair, true)
        })

        test("it ignores dek if it's empty", () => {
          expect(result.overview).toBeTruthy()
          expect(result.details).toBeTruthy()
          expect(vi.mocked(legacyDecryptDek)).not.toHaveBeenCalled()
        })

        test('it updates v2 overview fields from legacy fields', () => {
          expect(result.overview.name).toEqual('disneyplus.com')
          expect(result.overview.info).toEqual('mark@mdp.im')
          expect(result.overview.urls).toEqual([{ url: 'https://disneyplus.com' }])
          expect(result.overview.android_app_ids).toEqual([])
        })

        test('it updates v2 details fields from legacy fields', () => {
          expect(result.details.main).toContainEqual({ kind: 'password', value: 'prnlhxfL3!' })
          expect(result.details.main).toContainEqual({ kind: 'totp', value: null })
          expect(result.details.main).toContainEqual({ kind: 'username', value: 'mark@mdp.im' })
        })
      })

      describe('v2 account', () => {
        const v2Account: EncryptedItem = {
          ...v1EncryptedItem,
          template: Template.Login,
          website: 'old.website',
          encryption_version: 2,
          encrypted_dek:
            'Ym2zabNnQlRmrxwXhaFPuthutMFfRa0FVSiHhIuRdTwNc09TEGpweJSJZMtW3auSekwXJV6XbmGKW5eXEk8Y-vdbFZoNovhJ',
          rsa_encrypted_dek:
            'Xu3ofaDC7JbHSyEPk9CnNSKhH7cTs6030s+D2EV5bvXWEcFvpxYMNJwAgI0BQ4pz/z1L2ejk6+WfTGK+iCUMBTs03DJ0IBGiU/wh/78han7YZc2bPmAUNzjQnol3qDZQL2ucxulQ+Nei62JlkPrg7vto52L0q25Pf6ULcFT9Ug68y+2YNgolikuNRvn+BZ4yCIMbH1WzbcnaPtW1xqyhEYVgHTqqpwmDNR3IGHhTBbSAM4xNGRinhfhkVJCx+kKU5bGY9FveM01d4dYammo86bIqItSvMOID7Ur+7yNmF9XsJcoVPxMcGxO2i4mxjjxDiPRdnbWiHmDK2mWV0jsdJQ==',
          encrypted_overview:
            'NLtPPtqfcqNBx6GeKnLwKkUbTJWNxmRgoIsBzxBiH4mAf9hSPUxxAtKPmdRdMODJcDyo_RPCpKTiXmn2kMX9UrJJYJ3Oc7l3Iha1OWB_fGC9XACzKFdhodLDIundIKdeKhP8QhXqf_1Cxoh7rhLLZGL1dh7qVY38a7uwt8YV9cDz8q5Mzufn2YmNoKcIpFEQX9z5QN5kpTFv2Xk',
          encrypted_details:
            'IxAIjbG-k7oYkyyki-hAlKwrh-LWoy6jmFWxulX8tKG7dCHoLvmmMQTP6X2va-deOIsVjb4xdpkzuedVW962Y3m7nZvlEIxsIzKXTP1pyNDMSiifpCtSLf5J4-lHQWCdHZiDDZ7FSszv6w7Gb9Ku2EmCTss8EU5gWi80kvkEFHk2Owl16EHDqbJDvj5eZagv9pjiq86f5LZNQ87NElJus36YNdYQ7NKCztToJehyDOXoYboeBiQKgOaedbN9QjNHIHegLFeZEZlN4OX8EPmqIKEHX3OXNeKQg4fWo9o_xSq4K7LWZs_Zbd2cZHdQ0pM1qhxRauf6mx31-P6w5Zr2tHTxpW6W1k9J0DMxqRXRMazAoHzC-IMeH9bzhXCvegArZd8cgCQjpnuaNShkhMYKdg',
        }

        describe('DEK based decryption', () => {
          beforeEach(() => {
            vi.mocked(decrypt).mockClear()
            vi.mocked(decryptDek).mockClear()
            vi.mocked(legacyEncryptDek).mockClear()
          })

          test('it decrypts the dek using new keypair', async () => {
            await decryptItem(v2Account, keyPair, rsaCryptoKeyPair)
            expect(vi.mocked(decryptDek)).toHaveBeenLastCalledWith(v2Account.encrypted_dek, keyPair)
          })

          test('it decrypts the dek using legacy keypair', async () => {
            await decryptItem({ ...v2Account, encrypted_dek: null }, keyPair, rsaCryptoKeyPair)
            expect(vi.mocked(legacyDecryptDek)).toHaveBeenLastCalledWith(
              v2Account.rsa_encrypted_dek,
              rsaCryptoKeyPair
            )
          })

          test('it does not decrypt details by default', async () => {
            const result = await decryptItem(v2Account, keyPair, rsaCryptoKeyPair)
            expect('details' in result).toBe(false)
            expect(vi.mocked(decrypt)).toHaveBeenCalledOnce()
            expect(vi.mocked(decrypt)).toHaveBeenLastCalledWith(
              v2Account.encrypted_overview,
              knownDek
            )
          })

          test('it decrypts details if asked', async () => {
            const result = await decryptItem(v2Account, keyPair, rsaCryptoKeyPair, true)
            expect('details' in result).toBe(true)
            expect(vi.mocked(decrypt)).toHaveBeenCalledTimes(2)
            expect(vi.mocked(decrypt)).toHaveBeenNthCalledWith(
              1,
              v2Account.encrypted_overview,
              knownDek
            )
            expect(vi.mocked(decrypt)).toHaveBeenNthCalledWith(
              2,
              v2Account.encrypted_details,
              knownDek
            )
          })
        })

        // Currenly we don't update the legacy field 'website' on encryption
        // Therefore, with legacy decryption disabled we need to ensure
        // that we don't overwrite URL's with the website field
        describe('with legacy encryption enabled', () => {
          test('it does not override urls from website', async () => {
            const result = await decryptItem(
              { ...v2Account, legacy_encryption_disabled: false },
              keyPair,
              rsaCryptoKeyPair,
              true
            )
            expect(result.overview.urls).toEqual([{ url: 'https://new.website' }])
          })

          test('it does not override non login/1 items', async () => {
            const result = await decryptItem(
              {
                ...v2Account,
                name: 'different',
                template: Template.WiFi,
                legacy_encryption_disabled: false,
              },
              keyPair,
              rsaCryptoKeyPair,
              true
            )
            expect(result.overview.name).toEqual('New name')
          })
        })

        describe('with legacy encryption disabled', () => {
          test('it does not update v2 overview fields from legacy fields', async () => {
            const result = await decryptItem(
              { ...v2Account, legacy_encryption_disabled: true },
              keyPair,
              rsaCryptoKeyPair,
              true
            )
            expect(result.overview.name).toEqual('New name')
            expect(result.overview.info).toEqual('New info')
            expect(result.overview.urls).toEqual([{ url: 'https://new.website' }])
            expect(result.overview.android_app_ids).toEqual(['android://newapp'])
          })
        })

        describe('folder items', () => {
          beforeEach(() => {
            vi.mocked(decrypt).mockClear()
            vi.mocked(decryptDek).mockClear()
            vi.mocked(legacyEncryptDek).mockClear()
          })

          test('it decrypts using the folder dek', () => {
            const encryptedFolderDek =
              'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABHvCkWMGRKFF_pcm-1vj_JxuR7wO39Boyt20n6E3nw_wZreu4_YdIGX4G4ZQHqQNm'

            decryptItem(
              {
                ...v2Account,
                encrypted_folder_dek: encryptedFolderDek,
                folder: {
                  id: 'mock-folder',
                  name: null,
                  description: null,
                  single_item: false,
                  public_key: folderKeyPair.publicKey,
                  my_encrypted_private_key: encryptedFolderPrivateKey,
                  folder_memberships: [],
                  folder_invitations: [],
                  created_at: 'mock',
                  updated_at: 'mock',
                },
              },
              keyPair,
              rsaCryptoKeyPair
            )
            expect(vi.mocked(decryptDek)).toHaveBeenLastCalledWith(
              encryptedFolderDek,
              folderKeyPair
            )
            expect(vi.mocked(legacyDecryptDek)).not.toHaveBeenCalled()
          })
        })
      })
    })
  })
})
