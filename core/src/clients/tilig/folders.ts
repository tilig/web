import { useAuthenticatedAxios } from '../axios'
import { Folder, FolderMembership } from './item'

export interface PublicKeyResult {
  id: string
  email: string
  public_key: string
}

export interface TiligUserFolderMembershipAttributes {
  encrypted_private_key: string
  user_id: string
}
export interface NonTiligUserFolderMembershipAttributes {
  email: string
}
export type FolderMembershipAttributes =
  | TiligUserFolderMembershipAttributes
  | NonTiligUserFolderMembershipAttributes

export interface FolderAttributes {
  name: string
  public_key: string
  single_item?: boolean
}

export interface FolderCreationAttributes extends FolderAttributes {
  folder_memberships?: FolderMembershipAttributes[]
  items?: {
    encrypted_folder_dek: string
    id: string
    share_link?: {
      encrypted_master_key: string
    }
  }[]
}

export interface FolderMembershipAcceptanceAttributes {
  acceptance_token: string
  user_id: string
}

export const getFolderMemberPublicKey = (email: string) => {
  const axios = useAuthenticatedAxios()
  return axios.get<{ user: PublicKeyResult }>(`/api/v3/users/public_key?email=${email}`)
}

export const getFolders = () => {
  const axios = useAuthenticatedAxios()
  return axios.get<{ folders: Folder[] }>(`/api/v3/folders`)
}

export const createFolder = (folder: FolderCreationAttributes) => {
  const axios = useAuthenticatedAxios()
  return axios.post<{ folder: Folder }>(`/api/v3/folders`, { folder })
}

export const updateFolder = (id: string, folder: Partial<FolderAttributes>) => {
  const axios = useAuthenticatedAxios()
  return axios.patch<{ folder: Folder }>(`/api/v3/folders/${id}`, { folder })
}

export const deleteFolder = (id: string) => {
  const axios = useAuthenticatedAxios()
  return axios.delete<{ folder: Folder }>(`/api/v3/folders/${id}`)
}

export const createFolderMembership = (
  id: string,
  folder_membership: FolderMembershipAttributes
) => {
  const axios = useAuthenticatedAxios()
  return axios.post<{ folder_membership: FolderMembership }>(
    `/api/v3/folders/${id}/folder_memberships`,
    { folder_membership }
  )
}

export const acceptFolderMembership = (
  id: string,
  folderMembershipId: string,
  acceptance: FolderMembershipAcceptanceAttributes
) => {
  const axios = useAuthenticatedAxios()
  return axios.post<{ folder_membership: FolderMembership }>(
    `/api/v3/folders/${id}/folder_memberships/${folderMembershipId}/acceptance`,
    { acceptance }
  )
}

export const revokeFolderMembership = (id: string, folderMembershipId: string) => {
  const axios = useAuthenticatedAxios()
  return axios.delete<{ folder_membership: FolderMembership }>(
    `/api/v3/folders/${id}/folder_memberships/${folderMembershipId}`
  )
}

export const getFolderMembershipsWithMissingPrivateKeys = () => {
  const axios = useAuthenticatedAxios()
  return axios.get<{ pending_memberships: FolderMembership[] }>(`/api/v3/folders`)
}

export const updateFolderMemberPrivateKey = (
  id: string,
  folderMembershipId: string,
  encrypted_private_key: string
) => {
  const axios = useAuthenticatedAxios()
  return axios.put<{ folder_membership: FolderMembership }>(
    `/api/v3/folders/${id}/folder_memberships/${folderMembershipId}`,
    { folder_membership: { encrypted_private_key } }
  )
}
