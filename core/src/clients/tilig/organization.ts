import * as z from 'zod'

import { createLogger } from '@core/utils/logger'
import { useAuthenticatedAxios, useAxios } from '@core/clients/axios'
import { iso8601StrictSeparator } from '@core/clients/tilig/item'

const logger = createLogger('clients:tilig:organization')

const OrganizationMember = z.object({
  id: z.string().uuid(),
  email: z.string(),
  display_name: z.string(),
  picture: z.string().nullable(),
})

const Invitation = z.object({
  id: z.string(),
  email: z.string(),
  pending: z.boolean(),
  expired: z.boolean(),
  creator: OrganizationMember.nullable(),
  expired_at: z.string().regex(iso8601StrictSeparator),
  created_at: z.string().regex(iso8601StrictSeparator),
  accepted_at: z.string().regex(iso8601StrictSeparator).nullable(),
  updated_at: z.string().regex(iso8601StrictSeparator),
})

const Organization = z.object({
  id: z.string(),
  name: z.string(),
  created_at: z.string().regex(iso8601StrictSeparator),
  updated_at: z.string().regex(iso8601StrictSeparator),
  users: z.array(OrganizationMember),
  invitations: z.array(Invitation),
})

const InvitationWithOrganization = Invitation.extend({
  organization: z.object({
    id: z.string(),
    name: z.string(),
  }),
})

export type Organization = z.infer<typeof Organization>
export type Invitation = z.infer<typeof Invitation>
export type InvitationWithOrganization = z.infer<typeof InvitationWithOrganization>

export const createOrganization = (name: string) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Creating Organization')
  return axios.post<{ organization: Organization }>(`api/v3/organization`, {
    organization: { name },
  })
}

export const updateOrganization = (name: string) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Updating Organization')
  return axios.put<{ organization: Organization }>(`api/v3/organization`, {
    organization: { name },
  })
}

export const getOrganization = () => {
  const axios = useAuthenticatedAxios()
  logger.debug('Fetching Organization')
  return axios.get<{ organization: Organization }>(`api/v3/organization`)
}

export const sendInvitation = (emails: string[]) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Sending Invitation emails')
  return axios.post<{ invitations: Invitation[] }>('api/v3/organization/invitations', {
    invitations: { emails },
  })
}

export const getInvitation = (invitationId: string, viewToken: string) => {
  const axios = useAxios()
  logger.debug('Getting invitation')
  return axios.get<{ invitation: InvitationWithOrganization }>(
    `api/v3/organization/invitations/${invitationId}?invitation[view_token]=${viewToken}`
  )
}

export const revokeInvitation = (invitationId: string) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Revoking invitation')
  return axios.delete<{ invitation: Invitation }>(`api/v3/organization/invitations/${invitationId}`)
}

/**
 * Fetch pending invitations for the currently signed in user.
 * @returns An array of pending invitations
 */
export const getPendingInvitations = () => {
  const axios = useAuthenticatedAxios()
  logger.debug('Getting pending invitations')
  return axios.get<{ pending_invitations: InvitationWithOrganization[] }>(
    `api/v3/organization/my_invitations`
  )
}

/**
 * Accept an invitation that this user has received.
 * @param invitationId
 * @returns The accepted invitation
 */
export const acceptInvitation = (invitationId: string) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Accepting invitation')
  return axios.patch<{ invitation: InvitationWithOrganization }>(
    `api/v3/organization/my_invitations/${invitationId}`
  )
}

/**
 * Decline an invitation that this user has received.
 * @param invitationId
 * @returns The declined invitation
 */
export const declineInvitation = (invitationId: string) => {
  const axios = useAuthenticatedAxios()
  logger.debug('Declining invitation')
  return axios.delete<{ invitation: InvitationWithOrganization }>(
    `api/v3/organization/my_invitations/${invitationId}`
  )
}
