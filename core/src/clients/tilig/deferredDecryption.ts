import * as sodium from 'libsodium-wrappers'

import { decryptItem, EncryptedItem } from './item'
import { KeyPair } from './crypto'

/**
 * Decrypt an item non-blocking
 *
 * @param item Item to decrypt
 * @param keyPair Libsodium keypair
 * @param legacyKeyPair RSA CryptoKeyPair
 * @param includeDetails Also decrypt item details
 */
export const deferredDecryptItem = async <IncludeDetails extends boolean | undefined>(
  item: EncryptedItem,
  keyPair: KeyPair,
  legacyKeyPair: CryptoKeyPair,
  includeDetails: IncludeDetails = false as IncludeDetails
) => {
  // By awaiting `sodium.ready`, we ensure that:
  // 1. Sodium is ready to handle crypto operations;
  // 2. Decryption does not block.
  await sodium.ready
  return decryptItem<IncludeDetails>(item, keyPair, legacyKeyPair, includeDetails)
}
