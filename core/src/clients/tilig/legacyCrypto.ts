import * as sodium from 'libsodium-wrappers'
import { createLogger } from '@core/utils/logger'

const ENCRYPTION_PARAMS: RsaOaepParams = {
  name: 'RSA-OAEP',
} as const

const IMPORT_KEY_PARAMS: RsaHashedImportParams = {
  ...ENCRYPTION_PARAMS,
  hash: {
    name: 'SHA-1',
  },
} as const

const KEYGEN_PARAMS: RsaHashedKeyGenParams = {
  ...IMPORT_KEY_PARAMS,
  modulusLength: 2048,
  publicExponent: new Uint8Array([1, 0, 1]),
} as const

const PUBLIC_KEY_FORMAT = 'spki' as const
const PRIVE_KEY_FORMAT = 'pkcs8' as const

const logger = createLogger('clients:legacyCrypto')

const cleanBase64 = (str: string) => str.replaceAll(/[\s\r\n]/g, '')

/**
 * Prefixes to convert PKCS#1 into PKCS#8 encoded private keys
 *
 * iOS will create PKCS#1 encoded keys, so we need to convert it
 * into PKCS#8 before we can import it using `importKey`.
 */
const PKCS8_PRIVATE_KEY_PREFIX_1 = new Uint8Array([0x30, 0x82])
const PKCS8_PRIVATE_KEY_PREFIX_2 = new Uint8Array([
  0x02, 0x01, 0x00, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01,
  0x05, 0x00, 0x04, 0x82,
])

/**
 * Convert PKSC1 private key to PKCS8
 * @param privateKeyDer PKCS1 DER of private key
 */
const addPkcs8Header = (privateKeyDer: Uint8Array) => {
  /*
   * PKCS8 RSA private keys are relatively straightforward wrapped PKCS1
   * RSA private keys. The structure is as follows:
   * 1. ASN.1 header
   * 2. 2-byte length field of payload
   * 3. Information about the PKCS1 key (which is always the same for our private keys)
   * 4. 2-byte length field of private key DER
   * 5. private key DER (equal to PKCS1 DER)
   */
  const derLength = privateKeyDer.length
  const payloadLength = derLength + 22
  const totalLength = payloadLength + 4
  const pkcs8Der = new Uint8Array(totalLength)
  // Add ASN.1 start
  pkcs8Der.set(PKCS8_PRIVATE_KEY_PREFIX_1)
  // Add payload length field (2 bytes)
  pkcs8Der.set([payloadLength >> 8, payloadLength % 256], PKCS8_PRIVATE_KEY_PREFIX_1.length)
  // Add fixed version information
  pkcs8Der.set(PKCS8_PRIVATE_KEY_PREFIX_2, PKCS8_PRIVATE_KEY_PREFIX_1.length + 2)
  // Add private key length field (2 bytes)
  pkcs8Der.set(
    [derLength >> 8, derLength % 256],
    PKCS8_PRIVATE_KEY_PREFIX_1.length + PKCS8_PRIVATE_KEY_PREFIX_2.length + 2 // 24
  )
  // Add private
  pkcs8Der.set(
    privateKeyDer,
    PKCS8_PRIVATE_KEY_PREFIX_1.length + PKCS8_PRIVATE_KEY_PREFIX_2.length + 4 // 26
  )
  return pkcs8Der
}

/**
 * Prefix to convert OpenSSL encoded public keys into SPKI.
 *
 * iOS will create OpenSSL encoded keys, so we need to convert it
 * into SPKI before we can import it using `importKey`.
 */
const SPKI_PUBLIC_KEY_PREFIX = new Uint8Array([
  0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01,
  0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00,
])

/**
 * Convert PKSC1 public key to SPKI
 * @param privateKeyDer PKCS1 DER of private key
 */
const addSpkiHeader = (publicKeyDer: Uint8Array) => {
  /*
   * SPKI is rather simpler to construct than the PKCS8 private key. We just add
   * a fixed SPKI header.
   */
  const spkiDer = new Uint8Array(SPKI_PUBLIC_KEY_PREFIX.length + publicKeyDer.length)
  spkiDer.set(SPKI_PUBLIC_KEY_PREFIX)
  spkiDer.set(publicKeyDer, SPKI_PUBLIC_KEY_PREFIX.length)
  return spkiDer
}

/**
 * Export a key into string format
 * @param keyDer Key to export
 * @param type Type of the key
 */
const toPem = (keyDer: Uint8Array, type: 'PRIVATE' | 'PUBLIC') => {
  const exportedAsBase64 = sodium.to_base64(keyDer, sodium.base64_variants.ORIGINAL)
  return `-----BEGIN ${type} KEY-----\n${exportedAsBase64}\n-----END ${type} KEY-----`
}

const PEM_START_REGEX = /^-----BEGIN (RSA )?(PRIVATE|PUBLIC) KEY-----/
const PEM_END_REGEX = /-----END (RSA )?(PRIVATE|PUBLIC) KEY-----$/

/**
 * Convert a key from PEM format to DER
 * @param keyPem Key in pem format
 */
const fromPem = (keyPem: string) => {
  keyPem = keyPem.trim().replace(PEM_START_REGEX, '').replace(PEM_END_REGEX, '')

  return sodium.from_base64(cleanBase64(keyPem), sodium.base64_variants.ORIGINAL)
}

/**
 * Convert a CryptoKeypair to a serializable format
 */
const exportRsaCryptoKeyPair = async (rsaKeyPair: CryptoKeyPair) => {
  const [publicKeyBuffer, privateKeyBuffer] = await Promise.all([
    // https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/exportKey
    window.crypto.subtle.exportKey(PUBLIC_KEY_FORMAT, rsaKeyPair.publicKey),
    window.crypto.subtle.exportKey(PRIVE_KEY_FORMAT, rsaKeyPair.privateKey),
  ])

  return {
    publicKey: toPem(new Uint8Array(publicKeyBuffer), 'PUBLIC'),
    privateKey: toPem(new Uint8Array(privateKeyBuffer), 'PRIVATE'),
  }
}

/**
 * Generate a new RSA KeyPair
 */
export const generateRsaCryptoKeyPair = async () => {
  return window.crypto.subtle.generateKey(KEYGEN_PARAMS, true, ['decrypt', 'encrypt'])
}

export const generateLegacyKeyPair = async () => {
  return exportRsaCryptoKeyPair(await generateRsaCryptoKeyPair())
}

/**
 * Import RSA private key as they are stored in firebase
 *
 * @param privateKeyPem PEM encoded private key
 */
const importLegacyPrivateKey = async (privateKeyPem: string) => {
  let privateKeyDer = fromPem(privateKeyPem)

  if (privateKeyPem.trimStart().startsWith('-----BEGIN RSA')) {
    logger.debug('Converting private key to pkcs8')
    privateKeyDer = addPkcs8Header(privateKeyDer)
  }

  // No need to re-export the private key
  try {
    return await window.crypto.subtle.importKey(
      PRIVE_KEY_FORMAT,
      privateKeyDer,
      IMPORT_KEY_PARAMS,
      false,
      ['decrypt']
    )
  } catch (e) {
    logger.error('Error importing private key %O', e)
    throw e
  }
}

/**
 * Import RSA public key as stored in the API
 *
 * @param publicKeyPem PEM encoded public key
 */
const importLegacyPublicKey = async (publicKeyPem: string) => {
  let publicKeyDer = fromPem(publicKeyPem)

  if (publicKeyPem.trimStart().startsWith('-----BEGIN RSA')) {
    logger.debug('Converting public key to spki')
    publicKeyDer = addSpkiHeader(publicKeyDer)
  }

  // No need to re-export the public key
  try {
    return await window.crypto.subtle.importKey(
      PUBLIC_KEY_FORMAT,
      publicKeyDer,
      IMPORT_KEY_PARAMS,
      false,
      ['encrypt']
    )
  } catch (e) {
    logger.error('Error importing public key %O', e)
    throw e
  }
}

/**
 * Convert a serialized KeyPair to a CryptoKeyPair
 */
export const importLegacyKeyPair = async (keyPair: { privateKey: string; publicKey: string }) => {
  await sodium.ready
  const [privateKey, publicKey] = await Promise.all([
    importLegacyPrivateKey(keyPair.privateKey),
    importLegacyPublicKey(keyPair.publicKey),
  ])

  return {
    privateKey,
    publicKey,
  }
}

/**
 * Encrypts, and Base64-encodes, plain text using the given private (or public) key of a keypair
 * @param toEncrypt Original text to encrypt
 * @param publicKey Part of a KeyPair
 * @returns Base64 encoded, encrypted string
 */
export const encryptString = async (toEncrypt: string | Uint8Array, publicKey: CryptoKey) => {
  const plaintext = typeof toEncrypt === 'string' ? sodium.from_string(toEncrypt) : toEncrypt
  const ciphertext = await window.crypto.subtle.encrypt(ENCRYPTION_PARAMS, publicKey, plaintext)
  return sodium.to_base64(new Uint8Array(ciphertext), sodium.base64_variants.ORIGINAL)
}

type Encoding = 'utf8' | 'uint8array'

/**
 * Decrypts encrypted, Base64 encoded text using private (or public) key of a keypair
 *
 * @param toDecrypt Base64 encoded, encrypted text
 * @param privateKey Part of a keypair
 * @param encoding Output string or Uint8Array
 * @returns Plain text, decrypted
 */
export async function decryptString(toDecrypt: string, privateKey: CryptoKey): Promise<string>
export async function decryptString<E extends Encoding = 'utf8'>(
  toDecrypt: string,
  privateKey: CryptoKey,
  encoding: E
): Promise<E extends 'utf8' ? string : Uint8Array>
export async function decryptString(toDecrypt: string, privateKey: CryptoKey, encoding = 'utf8') {
  let ciphertext = sodium.from_base64(cleanBase64(toDecrypt), sodium.base64_variants.ORIGINAL)
  // Some implementations drop a leading zero in the ciphertext
  if (ciphertext.length < 256) {
    const fullSizeCiphertext = new Uint8Array(256)
    fullSizeCiphertext.set(ciphertext, 256 - ciphertext.length)
    ciphertext = fullSizeCiphertext
  }
  const plaintext = await window.crypto.subtle.decrypt(ENCRYPTION_PARAMS, privateKey, ciphertext)
  const buffer = new Uint8Array(plaintext)
  return encoding === 'utf8' ? sodium.to_string(buffer) : buffer
}

/**
 * Null-safe version of encryptString
 * @param toEncrypt Original text to encrypt
 * @param publicKey Part of a KeyPair
 * @returns Base64 encoded, encrypted string
 */
export const encryptStringOrNull = async (toEncrypt: string | null, publicKey: CryptoKey) => {
  if (!toEncrypt) {
    return null
  }
  return encryptString(toEncrypt, publicKey)
}

/**
 * Null-safe version of decryptString
 *
 * @param toDecrypt Base64 encoded, encrypted text
 * @param privateKey Part of a keypair
 * @returns Plain text, decrypted
 */
export const decryptStringOrNull = async (toDecrypt: string | null, privateKey: CryptoKey) => {
  if (!toDecrypt) {
    return null
  }
  return decryptString(toDecrypt, privateKey)
}
