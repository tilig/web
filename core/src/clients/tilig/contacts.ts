import { useAuthenticatedAxios } from '@core/clients/axios'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('clients:tilig:contact')

export interface Contact {
  display_name: string
  email: string
  id: string
  picture: string | null
  public_key: string
  organization: {
    id: string
    name: string
  } | null
  source: 'Contact' | 'Team'
}

export const getContacts = () => {
  const axios = useAuthenticatedAxios()
  logger.debug('Fetching Contacts')
  return axios.get<{ contacts: Contact[] }>(`api/v3/contacts`)
}
