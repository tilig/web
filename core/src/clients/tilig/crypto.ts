import * as sodium from 'libsodium-wrappers'
import { decryptString as legacyDecrypt, encryptString as legacyEncrypt } from './legacyCrypto'

import { createLogger } from '@core/utils/logger'

const logger = createLogger('clients:tilig:crypto')

export interface LegacyKeyPair {
  privateKey: string
  publicKey: string
}

export interface DecodedKeyPair {
  privateKey: Uint8Array
  publicKey: Uint8Array
  keyType: 'x25519'
}

export interface KeyPair {
  privateKey: string
  publicKey: string
  keyType: 'x25519'
}

export const cryptoReady = sodium.ready

/**
 * Generate a new unencoded random asymmetric keypair
 */
export const generateUnencodedKeypair = () => {
  return sodium.crypto_box_keypair() as DecodedKeyPair
}

/**
 * Generate a new random asymmetric keypair
 */
export const generateKeypair = () => {
  return encodeKeypair(generateUnencodedKeypair())
}

/**
 * Encode keypair into base64
 */
export const encodeKeypair = (keypair: DecodedKeyPair): KeyPair => {
  return {
    privateKey: sodium.to_base64(keypair.privateKey),
    publicKey: sodium.to_base64(keypair.publicKey),
    keyType: keypair.keyType,
  }
}

/**
 * Decode base64 encoded keypair
 */
export const decodeKeypair = (encodedKeypair: KeyPair): DecodedKeyPair => {
  return {
    privateKey: sodium.from_base64(encodedKeypair.privateKey),
    publicKey: sodium.from_base64(encodedKeypair.publicKey),
    keyType: encodedKeypair.keyType,
  }
}

/**
 * Generate a random nonce for asymmetric encryption
 */
export const generateAsymmetricNonce = () => {
  return sodium.randombytes_buf(sodium.crypto_box_NONCEBYTES)
}

/**
 * Generate a new random symmetric encryption key
 */
export const generateDek = () => {
  return sodium.crypto_secretbox_keygen()
}

/**
 * Generate a random nonce for symmetric encryption
 */
export const generateNonce = () => {
  return sodium.randombytes_buf(sodium.crypto_secretbox_NONCEBYTES)
}

class CryptoError implements Error {
  public name = 'CryptoError'
  constructor(public message: string) {}
}

class DecryptionError extends CryptoError {
  public name = 'DecryptionError'
}

class EncryptionError extends CryptoError {
  public name = 'EncryptionError'
}

/**
 * Strip possible newlines from the string
 *
 * Used to make sure wrong base64 is still parsed
 */
const cleanBase64 = (str: string) => str.replaceAll(/[\s\r\n]/g, '')

type Encoding = 'utf8' | 'uint8array'

/**
 * Decrypt a payload using symmetric encryption
 * @param encrypted_data Encrypted data
 * @param dek Data Encryption Key
 * @returns Decrypted data
 */
export function decrypt(encrypted_data: string, dek: Uint8Array): string
export function decrypt<E extends Encoding = 'utf8'>(
  encrypted_data: string,
  dek: Uint8Array,
  encoding: E
): E extends 'utf8' ? string : Uint8Array
export function decrypt(encrypted_data: string, dek: Uint8Array, encoding: Encoding = 'utf8') {
  if (dek.length !== sodium.crypto_secretbox_KEYBYTES) {
    logger.error('Wrong key length', sodium.crypto_secretbox_KEYBYTES, dek.length)
    throw new DecryptionError('Key has wrong length')
  }

  const payload_bytes = sodium.from_base64(cleanBase64(encrypted_data))

  if (
    payload_bytes.length <
    sodium.crypto_secretbox_NONCEBYTES + sodium.crypto_secretbox_MACBYTES
  ) {
    logger.error(
      'Wrong payload length',
      sodium.crypto_secretbox_NONCEBYTES + sodium.crypto_secretbox_MACBYTES,
      payload_bytes.length
    )
    throw new DecryptionError('Malformed payload')
  }

  const nonce = payload_bytes.slice(0, sodium.crypto_secretbox_NONCEBYTES)
  const ciphertext = payload_bytes.slice(sodium.crypto_secretbox_NONCEBYTES)

  if (encoding === 'utf8') {
    return sodium.crypto_secretbox_open_easy(ciphertext, nonce, dek, 'text')
  }
  return sodium.crypto_secretbox_open_easy(ciphertext, nonce, dek)
}

/**
 * Encrypt a payload using symmetric encryption
 * @param unencrypted_data Unencrypted data
 * @param dek Data Encryption Key
 * @returns Encrypted data
 */
export const encrypt = (unencrypted_data: string | Uint8Array, dek: Uint8Array) => {
  if (dek.length !== sodium.crypto_secretbox_KEYBYTES) {
    logger.error('Wrong key length', sodium.crypto_secretbox_KEYBYTES, dek.length)
    throw new EncryptionError('Key has wrong length')
  }

  const plaintext =
    typeof unencrypted_data === 'string' ? sodium.from_string(unencrypted_data) : unencrypted_data
  const nonce = generateNonce()
  const encrypted = Uint8Array.from([
    ...nonce,
    ...sodium.crypto_secretbox_easy(plaintext, nonce, dek),
  ])
  return sodium.to_base64(encrypted)
}

/**
 * Decrypt a DEK using asymmetric encryption
 * @param rsaEncryptedDek Encrypted Data Encryption Key
 * @param rsaKeyPair Assymetric Keypair (Key Encryption Key)
 * @returns Decrypted DEK
 */
export const legacyDecryptDek = (rsaEncryptedDek: string, rsaKeyPair: CryptoKeyPair) => {
  return legacyDecrypt(rsaEncryptedDek, rsaKeyPair.privateKey, 'uint8array')
}

/**
 * Encrypt a DEK using asymmetric encryption
 * @param unencryptedDek Unencrypted Data Encryption Key
 * @param rsaKeyPair Assymetric Keypair (Key Encryption Key)
 * @returns Encrypted DEK
 */
export const legacyEncryptDek = (unencryptedDek: Uint8Array, rsaKeyPair: CryptoKeyPair) => {
  return legacyEncrypt(unencryptedDek, rsaKeyPair.publicKey)
}

/**
 * Decrypt a DEK using asymmetric encryption
 * @param encrypted_dek Encrypted Data Encryption Key
 * @param keyPair Assymetric Keypair (Key Encryption Key)
 * @returns Decrypted DEK
 */
export const decryptDek = (encrypted_dek: string, keyPair: KeyPair) => {
  logger.debug('Decrypting DEK %O %O', encrypted_dek, keyPair)

  const decodedKeyPair = decodeKeypair(keyPair)
  const dek_bytes = sodium.from_base64(cleanBase64(encrypted_dek))

  if (dek_bytes.length < sodium.crypto_box_NONCEBYTES + sodium.crypto_box_MACBYTES) {
    logger.error(
      'Wrong payload length',
      sodium.crypto_box_NONCEBYTES + sodium.crypto_box_MACBYTES,
      dek_bytes.length
    )
    throw new DecryptionError('Malformed payload')
  }

  const nonce = dek_bytes.slice(0, sodium.crypto_box_NONCEBYTES)
  const ciphertext = dek_bytes.slice(sodium.crypto_box_NONCEBYTES)
  return sodium.crypto_box_open_easy(
    ciphertext,
    nonce,
    decodedKeyPair.publicKey,
    decodedKeyPair.privateKey
  )
}

/**
 * Encrypt a DEK using asymmetric encryption
 * @param unencrypted_dek Unencrypted Data Encryption Key
 * @param keyPair Assymetric Keypair (Key Encryption Key)
 * @returns Encrypted DEK
 */
export const encryptDek = (unencrypted_dek: Uint8Array, keyPair: KeyPair) => {
  const decodedKeyPair = decodeKeypair(keyPair)
  const nonce = generateAsymmetricNonce()
  const ciphertext = Uint8Array.from([
    ...nonce,
    ...sodium.crypto_box_easy(
      unencrypted_dek,
      nonce,
      decodedKeyPair.publicKey,
      decodedKeyPair.privateKey
    ),
  ])
  return sodium.to_base64(ciphertext)
}

/**
 * Anonymously encrypt dek
 */
export const anonymouslyEncryptPrivateKey = (privateKey: Uint8Array, publicKey: string) => {
  const decodedPublicKey = sodium.from_base64(publicKey)
  return sodium.crypto_box_seal(privateKey, decodedPublicKey, 'base64')
}

/**
 * Decrypt anonymously encrypted dek
 */
export const anonymouslyDecryptPrivateKey = (encrytedPrivateKey: string, keyPair: KeyPair) => {
  const decodedKeyPair = decodeKeypair(keyPair)
  const decodedDek = sodium.from_base64(encrytedPrivateKey)

  return sodium.crypto_box_seal_open(
    decodedDek,
    decodedKeyPair.publicKey,
    decodedKeyPair.privateKey,
    'uint8array'
  )
}

/*
 * Generate a new random sharing secret
 */
export const generateMasterSecret = () => {
  return sodium.crypto_kdf_keygen()
}

/**
 * Decrypt a sharing secret using asymmetric encryption
 * @param encrypted_master_secret Encrypted Master Secret
 * @param keyPair Asymmetric Keypair (Key Encryption Key)
 * @returns Decrypted Master Secret
 */
export const decryptMasterSecret = (encrypted_master_secret: string, keyPair: KeyPair) => {
  return sodium.to_base64(decryptDek(encrypted_master_secret, keyPair))
}
/**
 * Encrypt a sharing secret using asymmetric encryption
 * @param unencrypted_dek Unencrypted Master Secret
 * @param keyPair Assymetric Keypair (Key Encryption Key)
 * @returns Encrypted Master Secret
 */
export const encryptMasterSecret = encryptDek

/**
 * Derive all needed data from a sharing master secret
 * @param masterSecret Sharing secret
 */
export const deriveDataFromMasterSecret = (masterSecret: Uint8Array) => {
  const keyEncryptionKey = sodium.crypto_kdf_derive_from_key(32, 1, 'ShareKek', masterSecret)
  const uuid = sodium.crypto_kdf_derive_from_key(16, 2, 'ShareUid', masterSecret)
  const accessToken = sodium.crypto_kdf_derive_from_key(16, 3, 'ShareTkn', masterSecret)

  return {
    keyEncryptionKey,
    uuid: sodium.to_hex(uuid),
    accessToken: sodium.to_hex(accessToken),
  }
}
