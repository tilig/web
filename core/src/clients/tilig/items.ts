import { createLogger } from '@core/utils/logger'
import { useAuthenticatedAxios } from '../axios'
import { EncryptedItem, Share, ShareData } from './item'

const logger = createLogger('clients:tilig:items')

type SanitizedEncryptedItem = {
  template: string | null
  website: string | null
  name: string | null
  username: string | null
  notes: string | null
  password: string | null
  otp: string | null
  android_app_id: string | null
  encryption_version: 1 | 2
  encrypted_dek: string | null
  rsa_encrypted_dek: string | null
  encrypted_folder_dek: string | null
  encrypted_overview: string | null
  encrypted_details: string | null
  folder_id?: string | null
}

/**
 * Sanitize fields, to make sure we don't accidentally post data
 * we don't want to post.
 *
 * @param item the item object that needs to be sanitized
 */
const sanitize = (item: EncryptedItem) => {
  const payload: SanitizedEncryptedItem = {
    template: item.template,
    website: item.website,

    // Legacy Fields
    name: item.name ?? null,
    username: item.username ?? null,
    notes: item.notes ?? null,
    password: item.password ?? null,
    otp: item.otp ?? null,
    android_app_id: item.android_app_id ?? null,

    // v2 Encryption fields
    encryption_version: item.encryption_version,
    encrypted_dek: item.encrypted_dek,
    rsa_encrypted_dek: item.rsa_encrypted_dek,
    encrypted_folder_dek: item.encrypted_folder_dek,
    encrypted_overview: item.encrypted_overview,
    encrypted_details: item.encrypted_details,

    // Folder
    folder_id: item.folder?.id || null,
  }

  return payload
}

export const getItems = () => {
  logger.debug('Fetching items')
  const axios = useAuthenticatedAxios()
  return axios.get<{ items: EncryptedItem[] }>(`/api/v3/items`)
}

export const pollItems = (lastUpdate?: string | null) => {
  logger.debug('Polling items')
  const axios = useAuthenticatedAxios()
  return axios.get<{ updates: EncryptedItem[]; deletions: string[]; fetched_at: string }>(
    `/api/v3/polling/items?after=${lastUpdate}`
  )
}

export const getItem = (id: string) => {
  logger.debug('Fetching item', id)
  const axios = useAuthenticatedAxios()
  return axios.get<{ item: EncryptedItem }>(`/api/v3/items/${id}`)
}

export const createItem = (item: EncryptedItem) => {
  logger.debug('Creating item')
  const axios = useAuthenticatedAxios()
  return axios.post<{ item: EncryptedItem }>(`/api/v3/items/`, { item: sanitize(item) })
}

export const updateItem = (id: string, item: EncryptedItem) => {
  logger.debug('Updating item', id)
  const axios = useAuthenticatedAxios()
  return axios.put<{ item: EncryptedItem }>(`/api/v3/items/${id}`, { item: sanitize(item) })
}

export const deleteItem = (id: string) => {
  logger.debug('Deleting item', id)
  const axios = useAuthenticatedAxios()
  return axios.delete<{ item: EncryptedItem }>(`/api/v3/items/${id}`)
}

export const createShare = (id: string, shareData: ShareData) => {
  const axios = useAuthenticatedAxios()
  return axios.post<{ share_link: Share }>(`/api/v3/items/${id}/share_link`, {
    share_link: {
      encrypted_dek: shareData.encryptedDek,
      encrypted_master_key: shareData.encryptedMasterSecret,
      uuid: shareData.uuid,
      access_token: shareData.accessToken,
    },
  })
}

export const deleteShare = (id: string) => {
  const axios = useAuthenticatedAxios()
  return axios.delete<{ share_link: Share }>(`/api/v3/items/${id}/share_link`)
}
