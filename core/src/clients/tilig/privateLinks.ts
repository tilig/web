import { createLogger } from '@core/utils/logger'
import { useAxios } from '../axios'
import { deriveAccessDataFromMasterSecret, EncryptedShare } from './privateLink'

const logger = createLogger('clients:tilig:privateLinks')

export const fetchShare = async (masterSecret: string) => {
  logger.debug('Deriving data')
  const { uuid, accessToken } = await deriveAccessDataFromMasterSecret(masterSecret)
  const axios = useAxios()
  logger.debug('Fetching share')
  return axios.get<EncryptedShare>(`/api/shares/v1/private_links/${uuid}`, {
    headers: { 'Access-Token': accessToken },
  })
}
