import { useAuthenticatedAxios } from '../axios'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('clients:tilig:profile')

export type ProviderId = 'google.com' | 'apple.com' | 'microsoft.com'
export type Client = 'iOS' | 'Android' | 'Web' | 'WebMobile'

export enum AccountType {
  Personal = 'personal',
  Business = 'business',
}

export type OnboardingStep =
  | 'installExtension'
  | 'importPasswords'
  | 'installApp'
  | 'autoSave'
  | 'autoFill'

export interface EncryptedKeyPair {
  kid: string
  encrypted_private_key: string
  public_key: string
  key_type: string
}
export interface UserApplicationSettings {
  legacy_encryption_disabled?: boolean | null
  referral_link: string
  clients_signed_in?: Client[]
}

export type UserSettings = Partial<{
  accountType: AccountType
  onboarding: Partial<{
    stepsCompleted: OnboardingStep[]
    dismissed: boolean
    completed: boolean
  }>
  web: Partial<{
    squirrelCampaignEnabled: boolean
    squirrelCampaignDismissed: boolean
  }>
  extension: Partial<{
    autoSave: boolean
  }>
}>

export interface UserProfile {
  id: string
  email: string
  displayName: string | null
  // Given name (first name) is not always known due to Apple sign in
  givenName: string | null
  // Same for family name
  familyName: string | null
  locale: string
  photoURL: string | null
  providerId: ProviderId | null
  applicationSettings: UserApplicationSettings
  userSettings: UserSettings
}

interface UserProfilePayload {
  id: string
  email: string
  display_name: string | null
  given_name?: string | null
  family_name?: string | null
  locale: string
  picture: string | null
  provider_id: ProviderId | null
  application_settings: UserApplicationSettings
  public_key: string | null
  keypair: EncryptedKeyPair | null
  user_settings: UserSettings
}

export interface UserKeyPairData {
  rsaPublicKey: string | null
  encryptedKeyPair: EncryptedKeyPair | null
}

/**
 * Type to make sure we never overwrite public_key
 *
 * This is a type instead of an interface, so any additional
 * properties show up as an error (Excess Property Checking).
 */
type UpdateUserProfilePayload = Partial<
  Omit<
    UserProfilePayload,
    'id' | 'email' | 'application_settings' | 'public_key' | 'key_pair' | 'user_settings'
  >
>

const mapResponseToUserProfile = (data: UserProfilePayload): UserProfile => {
  return {
    id: data.id,
    email: data.email,
    displayName: data.display_name,
    givenName: data.given_name ?? null,
    familyName: data.family_name ?? null,
    locale: data.locale,
    photoURL: data.picture,
    providerId: data.provider_id ?? null,
    applicationSettings: data.application_settings,
    userSettings: data.user_settings,
  }
}

export const mapResponseToKeypairData = (data: UserProfilePayload): UserKeyPairData => {
  return {
    rsaPublicKey: data.public_key,
    encryptedKeyPair: data.keypair,
  }
}

const mapProfileToPayload = (
  profile: Omit<UserProfile, 'id' | 'applicationSettings' | 'userSettings'>
) => {
  const payload: UpdateUserProfilePayload = {
    display_name: profile.displayName,
    locale: profile.locale,
    // `photoURL` is `picture` on server
    picture: profile.photoURL,
    provider_id: profile.providerId,
  }

  // Only update if present
  if (profile.givenName) {
    payload.given_name = profile.givenName
  }
  if (profile.familyName) {
    payload.family_name = profile.familyName
  }

  return payload
}

export const getUserProfile = async () => {
  logger.debug('Fetching user profile')
  const axios = useAuthenticatedAxios()
  const { data } = await axios.get<UserProfilePayload>('/api/v3/profile')
  logger.debug('Got user profile')
  return { profile: mapResponseToUserProfile(data), keyPairData: mapResponseToKeypairData(data) }
}

export const updateUserProfile = async (
  profile: Omit<UserProfile, 'id' | 'applicationSettings' | 'userSettings'>
) => {
  const axios = useAuthenticatedAxios()
  const payload = mapProfileToPayload(profile)
  const { data } = await axios.put<UserProfilePayload>('/api/v3/profile', { user: payload })
  logger.debug('Updated user profile')
  return { profile: mapResponseToUserProfile(data), keyPairData: mapResponseToKeypairData(data) }
}

export const storeLegacyPublicKey = async (rsaPublicKey: string) => {
  const axios = useAuthenticatedAxios()
  const { data } = await axios.put<UserProfilePayload>('/api/v3/profile', {
    user: { public_key: rsaPublicKey },
  })
  return { profile: mapResponseToUserProfile(data), keyPairData: mapResponseToKeypairData(data) }
}

/**
 * Deletes the user's full profile on the server, including all passwords.
 * Be cautious!
 */
export const deleteUserProfile = () => {
  logger.debug('Deleting profile')
  const axios = useAuthenticatedAxios()
  return axios.delete<void>('/api/v3/profile')
}

export const updateUserSettings = async (userSettings: UserSettings) => {
  logger.debug(`Fetching user setings`)
  const axios = useAuthenticatedAxios()
  return axios.put<UserSettings>('/api/v3/profile/user_settings', {
    user_settings: userSettings,
  })
}
