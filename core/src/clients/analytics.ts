import axios, { AxiosInstance } from 'axios'

import { createLogger } from '@core/utils/logger'
import { getFirebaseConfig } from './firebase/config'

const logger = createLogger(`clients:google:analytics`)

const apiSecret = import.meta.env.VITE_GOOGLE_MEASUREMENT_API_KEY

let axiosInstance: AxiosInstance
let googleAnalyticsUserId: string | null
let googleAnalyticsClientId: string

const measurementId = getFirebaseConfig().measurementId

const getAxiosInstance = () => {
  if (!axiosInstance) {
    axiosInstance = axios.create({
      baseURL: 'https://www.google-analytics.com/',
    })
  }
  return axiosInstance
}

function getClientID() {
  if (!googleAnalyticsClientId) {
    googleAnalyticsClientId = crypto.randomUUID()
  }
  return googleAnalyticsClientId
}

export const setGoogleAnalyticsUserId = (userId: string) => {
  googleAnalyticsUserId = userId
}

export const sendGoogleAnalyticsEvent = async (eventAction: string, params = {}) => {
  if (!measurementId || !apiSecret) {
    logger.error('Unable to track events without a measurementId and api key')
    return
  }
  logger.debug(`Sending tracking event for ${eventAction}`)
  await getAxiosInstance().post(
    `mp/collect?api_secret=${encodeURIComponent(apiSecret)}&measurement_id=${encodeURIComponent(
      measurementId
    )}`,
    {
      user_id: googleAnalyticsUserId,
      client_id: getClientID(),
      events: [
        {
          name: eventAction,
          params,
        },
      ],
    }
  )
}
