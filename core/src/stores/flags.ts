import { watchEffect, ref, onUnmounted } from 'vue'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { UnleashClient } from 'unleash-proxy-client'

import { createLogger } from '@core/utils/logger'

import { useTrackingStore } from './tracking'
import { useAuthStore } from './auth'

const logger = createLogger('stores:flags')

interface UseFlagOptions {
  track?: boolean | string
}

type UpdateContext = Parameters<UnleashClient['updateContext']>[0]

export const useFlagsStore = defineStore('flags', () => {
  const trackingStore = useTrackingStore()
  const authStore = useAuthStore()

  const client = new UnleashClient({
    url: import.meta.env.VITE_UNLEASH_URL,
    clientKey: import.meta.env.VITE_UNLEASH_CLIENT_KEY,
    appName: import.meta.env.VITE_UNLEASH_APP_NAME,
  })

  /**
   * Client is initialized and received flag definitions
   */
  const ready = ref(false)
  /**
   * Error occured during flag fetching
   */
  const error = ref<Error | null>(null)

  client.on('ready', () => (ready.value = true))
  client.on('error', (e: Error) => (error.value = e))

  /**
   * Start the unleash client
   */
  const start = () => {
    logger.debug('Starting client')
    return client.start()
  }
  /**
   * Stop the unleash client
   */
  const stop = () => {
    logger.debug('Stopping client')
    return client.stop()
  }

  /**
   * Check if a given feature flag is enabled
   *
   * Flags can depend on the current user, and can change depending on
   * the current session identifier (randomly assigned at start), the
   * current user id (Tilig User UUID) and the current environment
   * (staging or production).
   *
   * Add `track: true` to options to report the value of the flag to mixpanel.
   *
   * This is an instanteneous (point-in-time) check. For a reactive flag, use
   * `useFlag`.
   *
   * @param name Flag to check
   * @param options Optional settings
   */
  const isEnabled = (name: string, options?: UseFlagOptions) => {
    const { track = true } = options ?? {}

    const flag = client.isEnabled(name)
    logger.debug('Got flag %s %o', name, flag)

    if (track) {
      const property = typeof track === 'string' ? track : name
      logger.debug('Tracking flag %s %o', property, flag ? 1 : 0)

      trackingStore
        .trackPerson({
          [property]: flag ? 1 : 0,
        })
        .catch((e) => logger.error('Error tracking flag %O', e))
    }

    return flag
  }

  /**
   * Use reactive flag value
   *
   * @param name Flag name
   */
  const useFlag = (name: string) => {
    const flag = ref(client.isEnabled(name))

    const onUpdate = () => {
      const enabled = client.isEnabled(name)
      if (flag.value !== enabled) {
        flag.value = enabled
      }
    }

    const onReady = () => {
      flag.value = client.isEnabled(name)
    }

    client.on('ready', onReady)
    client.on('update', onUpdate)

    onUnmounted(() => {
      client.off('ready', onReady)
      client.off('update', onUpdate)
    })

    return flag
  }

  /**
   * Update the Unleash context (with custom properties)
   */
  const updateContext = async (ctx: UpdateContext) => {
    await client.updateContext(ctx)
  }

  /**
   * Keep the userId synched to the Tilig User UUID
   */
  watchEffect(async () => {
    logger.debug('Setting userId', authStore.tiligUser?.id)

    await updateContext({
      userId: authStore.tiligUser?.id,
    })
  })

  return {
    isEnabled,
    useFlag,
    ready,
    error,
    start,
    stop,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useFlagsStore, import.meta.hot))
}
