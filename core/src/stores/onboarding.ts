import { acceptHMRUpdate, defineStore } from 'pinia'

import { getBrowserSpecificStore, os } from '@core/utils/onboardingUtils'
import { browser, platform } from '@core/utils/browserEnvironment'
import { createLogger } from '@core/utils/logger'
import { isDebugging } from '@core/utils/debug'
import { trackPerson, getUserProfile, OnboardingStep, AccountType } from '@core/clients/tilig'

import { useUserSettingsStore } from './userSettings'
import { useAuthStore } from './auth'

const logger = createLogger('stores:onboarding')

export const ONBOARDING_STEPS: Readonly<OnboardingStep[]> = [
  'installExtension',
  'importPasswords',
  'installApp',
  'autoSave',
  'autoFill',
]

interface BrowserWebStore {
  browserName: string
  webstoreName: string
  webstoreLink: string
}

export type OnboardingSettings = {
  stepsCompleted: OnboardingStep[]
  dismissed: boolean
  completed: boolean
}

export const initialOnboardingSettings: OnboardingSettings = {
  dismissed: false,
  stepsCompleted: [],
  completed: false,
}

export interface OnboardingState {
  ready: boolean
  completed: boolean
  accountType?: AccountType
  extensionInstalled: boolean
  dismissed: boolean
  browserSpecificWebstore: BrowserWebStore | null
  onboardingStepsCompleted: Set<OnboardingStep>
  os: string | undefined
  skippedMobileAppInstallationPrompt: boolean
}

export const useOnboardingStore = defineStore('onboarding', {
  state: (): OnboardingState => ({
    ready: false,
    accountType: undefined,
    extensionInstalled: false,
    completed: false,
    dismissed: false,
    onboardingStepsCompleted: new Set(),
    browserSpecificWebstore: getBrowserSpecificStore(),
    os: os.name,
    skippedMobileAppInstallationPrompt: false,
  }),
  getters: {
    hasCompletedAllSteps(state) {
      const onboardingStepsCompleted = Array.from(state.onboardingStepsCompleted)
      return onboardingStepsCompleted.length === ONBOARDING_STEPS.length
    },
    isOnboarding(state) {
      return !state.completed && !state.dismissed && state.browserSpecificWebstore
    },
  },
  actions: {
    async initialize() {
      const authStore = useAuthStore()

      if (!authStore.accessToken) return
      if (this.ready) return

      await this.loadUserSettings()
      // Order matters
      await this.checkIfExtensionIsInstalled()

      logger.debug('browser-specific-webstore', this.browserSpecificWebstore)

      if (!this.browserSpecificWebstore) {
        trackPerson({
          browserName: browser.name,
          platformType: platform.type,
        })
      }
      this.ready = true
    },
    async checkIfExtensionIsInstalled() {
      // Give the extension some time to set the meta tag flag.
      // We can only be sure if the browser extension is installed after that.
      await new Promise((resolve) => setTimeout(resolve, 1000))
      const installed = !!document.head.querySelector(
        "meta[name='tilig:extension'][content='installed']"
      )
      this.extensionInstalled = installed
      if (installed) {
        this.onboardingStepCompleted('installExtension')
      } else {
        this.onboardingStepsCompleted.delete('installExtension')
        this.updateUserSettings()
      }
      return installed
    },
    // A separate function for polling so that polling doesn't accidentally set other onboarding steps as uncompeted again
    async fetchAppInstalledState() {
      const { profile } = await getUserProfile()

      const clientsSignedIn = profile.applicationSettings.clients_signed_in ?? []
      if (clientsSignedIn.includes('iOS') || clientsSignedIn.includes('Android')) {
        this.onboardingStepCompleted('installApp')
      } else {
        this.onboardingStepsCompleted.delete('installApp')
        this.updateUserSettings()
      }
    },
    async loadUserSettings() {
      const { profile } = await getUserProfile()
      if (['business', 'personal'].includes(profile.userSettings?.accountType as string)) {
        this.accountType = profile.userSettings?.accountType as AccountType
        logger.debug(`Set account type based on user settings`, this.accountType)
      }

      const onboardingSettings = profile.userSettings?.onboarding || initialOnboardingSettings

      this.onboardingStepsCompleted = new Set(onboardingSettings.stepsCompleted)
      this.dismissed = onboardingSettings.dismissed ?? false
      this.completed = onboardingSettings.completed ?? false

      this.fetchAppInstalledState()
    },
    setAccountType(accountType: AccountType) {
      const userSettingsStore = useUserSettingsStore()
      this.accountType = accountType
      // No await on purpose here
      userSettingsStore.updateSettings({ accountType: accountType })
    },
    // Mark the setup process as completed, so that the user is not prompted to complete it again.
    async markCompleted() {
      this.completed = true
      this.updateUserSettings()
    },
    async updateUserSettings() {
      const userSettingsStore = useUserSettingsStore()

      await userSettingsStore.updateSettings({
        onboarding: {
          stepsCompleted: Array.from(this.onboardingStepsCompleted),
          dismissed: this.dismissed,
          completed: this.completed,
        },
      })
    },
    async updateImportSetupStatus() {
      this.onboardingStepCompleted('importPasswords')
    },
    onboardingStepCompleted(step: OnboardingStep) {
      this.onboardingStepsCompleted.add(step)
      this.updateUserSettings()
    },
    // Mark in app onboarding steps as completed
    async dismissOnboarding() {
      this.dismissed = true
      await this.updateUserSettings()
    },
    async updateAutoSaveSettings(autoSave: boolean) {
      const userSettingsStore = useUserSettingsStore()

      await userSettingsStore.updateSettings({
        extension: {
          autoSave,
        },
      })
    },
    /**
     * Debug mode only
     */
    async resetGettingStarted() {
      if (!isDebugging()) return

      this.onboardingStepsCompleted = new Set()
      this.dismissed = false
      this.completed = false

      await this.updateUserSettings()
    },
  },
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useOnboardingStore, import.meta.hot))
}
