import { computed } from 'vue'
import { acceptHMRUpdate, defineStore } from 'pinia'

import { createLogger } from '@core/utils/logger'
import { UserSettings, updateUserSettings } from '@core/clients/tilig'
import { useAuthStore } from '@core/stores/auth'

const logger = createLogger('stores:userSettings')

export const useUserSettingsStore = defineStore('userSettings', () => {
  const authStore = useAuthStore()

  const settings = computed(() => authStore.tiligUser?.userSettings ?? {})

  const updateSettings = async (updatedSettings: UserSettings) => {
    logger.debug('Updating user settings')
    await updateUserSettings(updatedSettings)
    logger.debug('Updated user settings')
    await authStore.refreshProfile()
  }

  return {
    settings,
    updateSettings,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useUserSettingsStore, import.meta.hot))
}
