import {
  createFolder as createFolderOnServer,
  createFolderMembership,
  deleteFolder as deleteFolderOnServer,
  encryptFolderPrivateKeyForMember,
  Folder,
  FolderCreationAttributes,
  FolderMembership,
  FolderMembershipAttributes,
  getFolderMemberPublicKey as getFolderMemberPublicKeyFromServer,
  getFolders as getFoldersOnServer,
  PublicKeyResult,
  revokeFolderMembership as revokeFolderMembershipOnServer,
  updateFolder as updateFolderOnserver,
  updateFolderMemberPrivateKey as updateFolderMemberPrivateKeyOnServer,
} from '@core/clients/tilig'
import {
  anonymouslyEncryptPrivateKey,
  encodeKeypair,
  generateUnencodedKeypair,
} from '@core/clients/tilig/crypto'
import { createLogger } from '@core/utils/logger'
import * as Sentry from '@sentry/vue'
import { sortBy } from 'lodash'
import { acceptHMRUpdate, defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useAuthStore } from './auth'

export type FolderMemberAttributes = PublicKeyResult | Pick<PublicKeyResult, 'email'>

const logger = createLogger('stores:folders')

type FolderMap = Map<string, Folder>

export const useFoldersStore = defineStore('folders', () => {
  const isLoaded = ref(false)
  const folderMap = ref<FolderMap>(new Map())
  const folders = computed(() =>
    sortBy(
      Array.from(folderMap.value.values()),
      ({ name }) => name?.toLocaleLowerCase() || 'zzzzzzzzzzzzzzzzzz'
    )
  )

  const publicKeyFetchError = ref<'own-email' | null>(null)
  const isAddingFolderMembership = ref(false)
  const isRevokingFolderMembership = ref(false)

  const $reset = () => {
    isLoaded.value = false
    folderMap.value = new Map()
    publicKeyFetchError.value = null
    isAddingFolderMembership.value = false
    isRevokingFolderMembership.value = false
  }

  const authStore = useAuthStore()
  authStore.onSignOut($reset)

  const fetchFolders = async () => {
    logger.debug('Getting folders')
    try {
      const newMap: FolderMap = new Map()
      const {
        data: { folders },
      } = await getFoldersOnServer()
      folders.forEach((folder) => newMap.set(folder.id, folder))
      folderMap.value = newMap
      return true
    } catch (e) {
      logger.error('Could not get folders %O', e)
      return false
    } finally {
      isLoaded.value = true
    }
  }

  const getFolderMemberPublicKey = async (email: string) => {
    logger.debug('Getting public key', email)
    email = email.toLocaleLowerCase().trim()
    if (!email) return null

    if (email === authStore.tiligUser?.email) {
      publicKeyFetchError.value = 'own-email'
      return null
    }

    try {
      const {
        data: { user },
      } = await getFolderMemberPublicKeyFromServer(email)
      logger.debug('Fetched public key', user)
      return user
    } catch (e) {
      logger.error('Could not fetch public key%O', e)
      return null
    }
  }

  const getFolderById = (id: string) => {
    return folderMap.value.get(id) ?? null
  }

  type PartialFolderCreationAttributes = Pick<FolderCreationAttributes, 'name'> &
    Partial<FolderCreationAttributes>
  const createFolder = async (folderAttributes: PartialFolderCreationAttributes) => {
    logger.debug('Creating folder %s', folderAttributes.name)
    let folder: FolderCreationAttributes

    if (!folderAttributes.public_key) {
      // Automatically generate a keypair and encrypt the private key for the current user
      if (!authStore.keyPair || !authStore.tiligUser) {
        logger.error('Cannot create a new folder without a user')
        return null
      }

      const decodedFolderKeyPair = generateUnencodedKeypair()
      const folderKeyPair = encodeKeypair(decodedFolderKeyPair)
      const encryptedPrivateKey = anonymouslyEncryptPrivateKey(
        decodedFolderKeyPair.privateKey,
        authStore.keyPair.publicKey
      )

      folder = {
        ...folderAttributes,
        public_key: folderKeyPair.publicKey,
        single_item: false,
        folder_memberships: [
          {
            user_id: authStore.tiligUser.id,
            encrypted_private_key: encryptedPrivateKey,
          },
        ],
      }
    } else {
      folder = folderAttributes as FolderCreationAttributes
    }

    // If this isn't explicitly a single item folder,
    // default to false.
    if (folder.single_item === undefined) {
      folder.single_item = false
    }

    try {
      const {
        data: { folder: newFolder },
      } = await createFolderOnServer(folder)

      // Don't add single item folder to the map
      if (!newFolder.single_item) {
        folderMap.value.set(newFolder.id, newFolder)
      }

      return newFolder
    } catch (e) {
      logger.error('Error creating folder %O', e)
      return null
    }
  }

  const updateFolder = async (folder: Folder) => {
    if (!folder.name) return false

    try {
      const {
        data: { folder: updatedFolder },
      } = await updateFolderOnserver(folder.id, { name: folder.name })
      folderMap.value.set(updatedFolder.id, updatedFolder)
      return true
    } catch (e) {
      logger.error('Error updating folder %O', e)
      return false
    }
  }

  const deleteFolder = async (folder: Folder) => {
    try {
      await deleteFolderOnServer(folder.id)
      folderMap.value.delete(folder.id)
      return true
    } catch (e) {
      logger.error('Error deleting folder %O', e)
      return false
    }
  }

  const updateFolderName = (id: string, name: string) => {
    const folder = folderMap.value.get(id)
    if (!folder) return
    folder.name = name
  }

  const addFolderMember = async (folder: Folder, folderMember: FolderMemberAttributes) => {
    if (!authStore.keyPair) return false

    isAddingFolderMembership.value = true

    let folderMembership: FolderMembershipAttributes

    if ('public_key' in folderMember) {
      const encryptedPrivateKey = encryptFolderPrivateKeyForMember(
        folder,
        authStore.keyPair,
        folderMember.public_key
      )

      folderMembership = {
        encrypted_private_key: encryptedPrivateKey,
        user_id: folderMember.id,
      }
    } else {
      folderMembership = {
        email: folderMember.email,
      }
    }

    try {
      await createFolderMembership(folder.id, folderMembership)
      return true
    } catch (e) {
      logger.error('Error creating folder membership %O', e)
      return false
    } finally {
      isAddingFolderMembership.value = false
    }
  }

  const revokeFolderMembership = async (folderId: string, memberId: string) => {
    logger.debug('Revoking folder membership', folderId, memberId)
    try {
      await revokeFolderMembershipOnServer(folderId, memberId)
      return true
    } catch (e) {
      logger.error('Error revoking member %O', e)
      return false
    }
  }

  const updateFolderMemberPrivateKey = async (
    folder: Folder,
    folderMembership: FolderMembership
  ) => {
    if (!authStore.keyPair) throw new Error('Keypair does not exist')
    if (!folderMembership.user.public_key) throw new Error('Public key is missing for user')

    const encryptedPrivateKey = encryptFolderPrivateKeyForMember(
      folder,
      authStore.keyPair,
      folderMembership.user.public_key
    )

    try {
      await updateFolderMemberPrivateKeyOnServer(
        folder.id,
        folderMembership.id,
        encryptedPrivateKey
      )
      return true
    } catch (e) {
      logger.error('Error updating folder member private key %O', e)
      return false
    }
  }

  /**
   * Confirm folder memberships for users that signed up after they got an invitation
   */
  const confirmPendingFolderInvitations = async (folder: Folder) => {
    const promises = folder.folder_invitations
      .filter(
        ({ creator, missing_private_key, user }) =>
          user.id && missing_private_key && creator?.id === authStore.tiligUser?.id
      )
      .map((folderMembership) =>
        updateFolderMemberPrivateKey(folder, folderMembership).catch((e) => {
          logger.error('Error confirming folder invitation %O', e)
          Sentry.captureException(e)
        })
      )

    await Promise.all(promises)
  }

  const confirmAllPendingFolderInvitations = async () => {
    await Promise.all(folders.value.map(confirmPendingFolderInvitations))
  }

  return {
    folderMap,
    folders,
    isLoaded,

    publicKeyFetchError,
    isAddingFolderMembership,
    isRevokingFolderMembership,

    getFolderById,

    fetchFolders,
    createFolder,
    updateFolder,
    deleteFolder,

    updateFolderName,
    getFolderMemberPublicKey,
    addFolderMember,
    revokeFolderMembership,

    confirmPendingFolderInvitations,
    confirmAllPendingFolderInvitations,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useFoldersStore, import.meta.hot))
}
