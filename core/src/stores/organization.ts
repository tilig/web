import { ref, computed } from 'vue'
import { isAxiosError } from '@core/clients/axios'
import { acceptHMRUpdate, defineStore } from 'pinia'

import { createLogger } from '@core/utils/logger'
import {
  createOrganization,
  updateOrganization,
  getOrganization,
  sendInvitation,
  getInvitation,
  acceptInvitation as acceptInvitationOnServer,
  declineInvitation as declineInvitationOnServer,
  revokeInvitation as revokeInvitationOnServer,
  Organization,
  InvitationWithOrganization,
  getPendingInvitations,
} from '@core/clients/tilig'

import { useAuthStore } from './auth'
import { AxiosError } from 'axios'

const logger = createLogger('stores:organization')

type SendInvitationError = {
  value: string
  errors: { email: string[] }
}

type SendInvitationsResponseError = {
  errors: SendInvitationError[]
}

export enum InvitationErrorType {
  AlreadyExists = 'AlreadyExists',
  AlreadyHasAnOrganization = 'AlreadyHasAnOrganization',
  Unknown = 'Unknown',
}

export type InvitationError = {
  email: string
  error: InvitationErrorType
}

/// Map a server invitation error message to our own type
const mapInvitationErrorStringToType = (errorMsg: string) => {
  if (errorMsg.includes('has already been taken')) {
    return InvitationErrorType.AlreadyExists
  } else if (errorMsg.includes('user already belongs to an organization')) {
    return InvitationErrorType.AlreadyHasAnOrganization
  } else {
    return InvitationErrorType.Unknown
  }
}

type InvitationStateMap = Map<string, InvitationWithOrganization>

export const useOrganizationStore = defineStore('organization', () => {
  const ready = ref(false)
  const isCreatingOrganization = ref(false)
  const isSendingOrganizationInvitation = ref(false)
  const isFetchingOrganizationInvitation = ref(false)
  const isAcceptingInvitation = ref(false)
  // True means the user has just accepted the invititation, but not seen the success screen yet
  const hasAcceptedInvitation = ref(false)
  const isFetchingOrganization = ref(false)
  const isRevokingInvitation = ref(false)

  const invitationErrors = ref<InvitationError[] | null>(null)
  const organization = ref<Organization | null>(null)
  const currentInvitation = ref<InvitationWithOrganization | null>(null)
  const pendingInvitations = ref<InvitationWithOrganization[] | null>(null)
  const invitationStateMap = ref<InvitationStateMap>(new Map())

  const authStore = useAuthStore()

  const $reset = () => {
    ready.value = false
    isAcceptingInvitation.value = false
    isCreatingOrganization.value = false
    isSendingOrganizationInvitation.value = false
    isFetchingOrganizationInvitation.value = false
    isAcceptingInvitation.value = false
    hasAcceptedInvitation.value = false
    isFetchingOrganization.value = false
    isRevokingInvitation.value = false
    organization.value = null
    currentInvitation.value = null
    pendingInvitations.value = []
  }

  authStore.onSignOut(() => {
    logger.debug('Resetting store')
    $reset()
  })

  /**
   * All invitations
   */
  const invitations = computed(() => Array.from(invitationStateMap.value.values()))

  const initialize = async () => {
    if (!authStore.accessToken) return

    if (!ready.value) {
      await fetchOrganization()
      await fetchPendingInvitations()
      ready.value = true
    }
  }

  const removeInvitation = (id: string) => {
    invitationStateMap.value.delete(id)
  }

  const fetchOrganization = async () => {
    if (!authStore.accessToken) return false

    isFetchingOrganization.value = true
    logger.info(`Fetching organization`)
    try {
      const { data } = await getOrganization()
      organization.value = data.organization

      // we want to store the invitations in a map so we can easily delete them
      const invitationMap = new Map()

      data.organization.invitations
        // filter out the invitations that have been accepted
        .filter((invitation) => !invitation.accepted_at)
        .forEach((invitation) => invitationMap.set(invitation.id, invitation))

      invitationStateMap.value = invitationMap
      logger.debug('Organization fetched %O', data.organization)
      return true
    } catch (error) {
      if (isAxiosError(error) && error.status === 404) {
        logger.debug('User has no organization')
      } else {
        logger.error('Error while fetching organization %O', error)
      }
      return false
    } finally {
      isFetchingOrganization.value = false
    }
  }

  /**
   * Fetch pending invitations for the current user.
   */
  const fetchPendingInvitations = async () => {
    if (!authStore.accessToken) return

    const {
      data: { pending_invitations },
    } = await getPendingInvitations()
    pendingInvitations.value = pending_invitations
  }

  const hasPendingInvitations = computed(() => !!pendingInvitations.value?.length)

  const organizationExists = async () => {
    await fetchOrganization()
    return !!organization.value
  }

  /**
   * Create a new team without triggering the fetchOrganization function
   * that changes the isFetchingOrganization loading state
   * @param teamName
   */
  const createNewOrganization = async (teamName: string) => {
    if (!teamName) {
      logger.error(`Cannot create an organization without a name`)
      return false
    }
    isCreatingOrganization.value = true

    try {
      const { data } = await createOrganization(teamName)
      if (data) return true
    } catch (error) {
      logger.error(`Failed to create organization: `, error)
      return false
    } finally {
      isCreatingOrganization.value = false
    }
  }

  /**
   * Check if an organization exists
   * If there is an organization, update the name
   * If not, create a new one
   * @param teamName
   */
  const setOrganizationName = async (teamName: string) => {
    if (!teamName) {
      logger.error(`Cannot create an organization without a name`)
      return false
    }
    isCreatingOrganization.value = true

    try {
      const exists = await organizationExists()

      if (exists) {
        const { data } = await updateOrganization(teamName)
        if (data) return true
      } else {
        const { data } = await createOrganization(teamName)
        if (data) return true
      }
    } catch (error) {
      logger.error(`Failed to set organization name: `, error)
      return false
    } finally {
      isCreatingOrganization.value = false
    }
  }

  /**
   * Send invitations to the given emailaddress.
   * Returns true on success
   * Returns false when something went wrong
   */
  const sendOrganizationInvitations = async (teamEmails: string[]) => {
    if (!teamEmails.length) {
      logger.error(`Cannot send an invitation without the emails`)
    }
    invitationErrors.value = null
    isSendingOrganizationInvitation.value = true

    try {
      await sendInvitation(teamEmails)
      // We want to refetch the organization so the invitation would be updated
      // The `/organization` page is watching `isFetchingOrganization`
      // so the spinner shows for a second here

      // Either we fetch the organization directly and set the state silently or we leave it as it is.
      // I think the loading state is not nice
      await fetchOrganization()
      return true
    } catch (error) {
      logger.error(`Could not send team invitations`, error)

      if (isAxiosError<SendInvitationsResponseError>(error)) {
        const errors = error.response?.data.errors
        if (errors?.length) {
          invitationErrors.value = errors.map((error) => {
            const email = error.value
            const errorMsg = error.errors.email[0]
            return {
              email: email,
              error: mapInvitationErrorStringToType(errorMsg),
            }
          })
          logger.error(`Got invitation errors %O`, invitationErrors.value)
          return false
        }
      }
      logger.error(`Error sending invitations %O`, error)
      return false
    } finally {
      isSendingOrganizationInvitation.value = false
    }
  }

  /**
   * Fetches and returns an invitation by the given id and token.
   * If an invitation is found, it'll be set as the `currentInvitation` in the store.
   */
  const fetchInvitation = async (invitationId: string, viewToken: string) => {
    if (!invitationId && !viewToken) {
      logger.error(`Cannot get invitation without the invitation ID and view token`)
    }
    isFetchingOrganizationInvitation.value = true
    try {
      const {
        data: { invitation },
      } = await getInvitation(invitationId, viewToken)
      logger.info(`Found invitation by invitationId`, invitation)
      currentInvitation.value = invitation
      return invitation
    } catch (error) {
      logger.error(
        `Could not get invitation for invitation ID %s and viewToken %d`,
        invitationId,
        viewToken,
        error
      )
      return null
    } finally {
      isFetchingOrganizationInvitation.value = false
    }
  }

  /**
   * If there is an invitation pending in this store, accept it.
   * If there is no pending invitation, do nothing.
   * Returns:
   *  On success: the accepted invitation
   *  On failure: false
   */
  const acceptCurrentInvitation = async () => {
    if (currentInvitation.value) {
      return await acceptInvitation(currentInvitation.value.id)
    }
    return null
  }

  /**
   * Returns the invitation on su
   */
  const acceptInvitation = async (invitationId: string) => {
    isAcceptingInvitation.value = true
    logger.info(`Accepting invitation for Invitation ID`, invitationId)

    try {
      const {
        data: { invitation },
      } = await acceptInvitationOnServer(invitationId)
      if (invitation) {
        hasAcceptedInvitation.value = true
      } else {
        hasAcceptedInvitation.value = false
      }
      return invitation
    } catch (error) {
      const { response } = error as AxiosError
      logger.error(`Failed to accept invitation`, error, response)
      return false
    } finally {
      isAcceptingInvitation.value = false
    }
  }

  const declineInvitation = async (invitationId: string) => {
    logger.info(`Rejecting invitation for Invitation ID`, invitationId)
    try {
      const {
        data: { invitation },
      } = await declineInvitationOnServer(invitationId)
      return invitation
    } catch (error) {
      const { response } = error as AxiosError
      logger.error(`Failed to reject invitation`, error, response)
      return false
    }
  }

  const revokeAllInvitations = async () => {
    const {
      data: { organization },
    } = await getOrganization()
    logger.debug(`Revoking ${organization.invitations.length} invitations`)
    if (organization.invitations.length) {
      organization.invitations.forEach((invitation) => {
        revokeInvitation(invitation.id)
      })
    }
  }

  const revokeInvitation = async (invitationId: string) => {
    isRevokingInvitation.value = true
    try {
      logger.debug(`Revoking invitation ${invitationId}`)
      await revokeInvitationOnServer(invitationId)
      logger.debug(`Invitation revoked`)

      removeInvitation(invitationId)
      return true
    } catch (error) {
      logger.error(`Failed to revoke invitation`, error)
      return false
    } finally {
      isRevokingInvitation.value = false
    }
  }

  const clearCurrentInvitation = () => {
    hasAcceptedInvitation.value = false
    currentInvitation.value = null
  }

  const clearInvitationErrors = () => {
    invitationErrors.value = null
  }

  return {
    ready,
    isCreatingOrganization,
    isSendingOrganizationInvitation,
    isFetchingOrganizationInvitation,
    isAcceptingInvitation,
    hasAcceptedInvitation,
    invitationErrors,
    isFetchingOrganization,
    isRevokingInvitation,
    hasPendingInvitations,

    invitations,
    currentInvitation,
    pendingInvitations,
    organization,

    fetchOrganization,
    initialize,
    setOrganizationName,
    sendOrganizationInvitations,
    createNewOrganization,

    fetchInvitation,
    fetchPendingInvitations,
    acceptInvitation,
    declineInvitation,
    acceptCurrentInvitation,

    revokeAllInvitations,
    revokeInvitation,
    clearCurrentInvitation,
    clearInvitationErrors,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useOrganizationStore, import.meta.hot))
}
