import { defineStore } from 'pinia'
import * as Sentry from '@sentry/vue'

import { TrackingEvent } from '@core/utils/trackingConstants'
import { os, browser, platform } from '@core/utils/browserEnvironment'
import { useAuthStore } from './auth'
import {
  TrackingProperties,
  trackAnonymousEvent,
  trackEvent,
  trackPerson,
} from '@core/clients/tilig/tracking'

const ANONYMOUS_EVENTS = [
  TrackingEvent.BROWSER_EXTENSION_INSTALLED,
  TrackingEvent.BROWSER_EXTENSION_UPDATED,
  TrackingEvent.BROWSER_EXTENSION_UNINSTALLED,
  TrackingEvent.WEBREQUEST_API_STATUS,
  TrackingEvent.UNINSTALL_SURVERY_ANSWERED,
  TrackingEvent.UNINSTALL_SURVERY_ANSWERED_FULL,
  TrackingEvent.SHARED_ITEM_VIEWED,
  TrackingEvent.SHARED_ITEM_USERNAME_COPIED,
  TrackingEvent.SHARED_ITEM_PASSWORD_COPIED,
  TrackingEvent.SHARED_ITEM_WEBSITE_OPENED,
  TrackingEvent.SHARED_ITEM_WEBSITE_COPIED,
  TrackingEvent.SHARED_ITEM_NOTES_COPIED,
  TrackingEvent.SHARED_ITEM_GETTING_STARTED_SELECTED,
  TrackingEvent.SHARED_ITEM_2FA_CODE_COPIED,
  TrackingEvent.REFERRAL_LINK_CLICKED,
]

const defaultEventProps = {
  browserName: browser.name,
  browserVersion: browser.version,
  osName: os.name,
  platformType: platform.type,
}

interface TrackingPayload {
  event: TrackingEvent
  properties?: TrackingProperties
}

export const useTrackingStore = defineStore('tracking', {
  actions: {
    async trackEvent({ event, properties }: TrackingPayload) {
      if (!event) {
        Sentry.captureException('Cannot track event without name')
        return
      }

      const authStore = useAuthStore()

      if (!authStore.isAuthenticated) {
        return this.trackAnonymousEvent({ event, properties })
      }

      const { data } = await trackEvent(event, {
        ...defaultEventProps,
        ...properties,
      })

      return data
    },
    async trackPerson(properties: TrackingProperties) {
      const { data } = await trackPerson(properties)
      return data
    },
    async trackAnonymousEvent({ event, properties }: TrackingPayload) {
      if (!ANONYMOUS_EVENTS.includes(event)) {
        Sentry.captureException(`${event} is not an anonymous event`)
        return
      }

      const { data } = await trackAnonymousEvent(event, {
        ...defaultEventProps,
        ...properties,
      })

      return data
    },
  },
})
