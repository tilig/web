import { acceptHMRUpdate, defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { cloneDeep, isEqual } from 'lodash'

import { createLogger } from '@core/utils/logger'
import { DecryptedItem } from '@core/clients/tilig'

import { useItemsStore } from './items'
import { useAuthStore } from './auth'
import { useFoldersStore } from './folders'

const logger = createLogger('stores:currentItem')

export const useCurrentItemStore = defineStore('currentItem', () => {
  /**
   * Current item loaded from the item store
   */
  const currentItem = ref<DecryptedItem | null>(null)
  const initialItem = ref<DecryptedItem | null>(null)
  const isDirty = computed(() => {
    if (!currentItem.value || !initialItem.value) return false
    return !isEqual(currentItem.value, initialItem.value)
  })

  const isLoading = ref(false)
  const hasLoadError = ref(false)

  const isUpdating = ref(false)
  const hasUpdateError = ref(false)

  const isDeleting = ref(false)
  const hasDeleteError = ref(false)
  /**
   * This item no longer exists on the server
   *
   * Changed happened outside of the store.
   */
  const isDeletedExternally = ref(false)

  const isCreatingShare = ref(false)
  const isDeletingShare = ref(false)

  const $reset = () => {
    currentItem.value = null
    initialItem.value = null

    isLoading.value = false
    hasLoadError.value = false
    isUpdating.value = false
    hasUpdateError.value = false
    isDeleting.value = false
    hasDeleteError.value = false
    isDeletedExternally.value = false
    isCreatingShare.value = false
    isDeletingShare.value = false
  }

  const authStore = useAuthStore()
  const itemsStore = useItemsStore()
  const foldersStore = useFoldersStore()

  authStore.onSignOut(() => {
    logger.debug('Resetting store')
    $reset()
  })

  const nextItemId = computed((): string | null => {
    if (!currentItem.value) return itemsStore.filteredItems[0]?.item.id ?? null

    const currentIndex = itemsStore.filteredItems.findIndex(
      (itemState) => itemState.item.id === currentItem.value?.item.id
    )

    const nextIndex =
      currentIndex === itemsStore.filteredItems.length - 1 ? currentIndex - 1 : currentIndex + 1
    return itemsStore.filteredItems[nextIndex]?.item.id ?? null
  })

  /**
   * Set the currentItem by ID
   * @param id ID of the item
   * @param refresh If we want to refresh the item from the server
   */
  const setCurrentItem = async (id: string, refresh = true) => {
    if (id !== currentItem.value?.item.id) {
      isLoading.value = true
    }
    hasLoadError.value = false

    if (!id) {
      currentItem.value = null
    } else {
      if (!itemsStore.isLoaded) {
        await itemsStore.fetchItems()
      } else if (refresh) {
        // Refresh item so changes from other clients are
        // pulled in.
        await itemsStore.refreshItem(id)
      }

      currentItem.value = await itemsStore.getItemById(id)
      logger.debug('Got new currentItem %O', currentItem.value)
      if (!currentItem.value) hasLoadError.value = true
    }

    initialItem.value = cloneDeep(currentItem.value)
    isLoading.value = false
    isDeletedExternally.value = false
    isCreatingShare.value = false
    isDeletingShare.value = false
  }

  /**
   * Refresh the current item from storage
   * @param refreshFromServer Fetch item details from the server
   */
  const refreshCurrentItem = async (refreshFromServer = false) => {
    if (!currentItem.value) return
    await setCurrentItem(currentItem.value.item.id, refreshFromServer)
  }

  const updateCurrentItem = async () => {
    if (!currentItem.value) return false
    const { item, overview, details } = currentItem.value

    logger.debug('Updating current item %O %O %O', item, overview, details)

    isUpdating.value = true
    hasUpdateError.value = false

    try {
      const success = await itemsStore.updateItem(item.id, overview, details, item.folder)
      hasUpdateError.value = !success
      return success
    } catch (e) {
      logger.error('Error updating item %O', e)
      hasUpdateError.value = true
      return false
    } finally {
      isUpdating.value = false
    }
  }

  const deleteCurrentItem = async () => {
    if (!currentItem.value) return

    isDeleting.value = true
    hasDeleteError.value = false

    const {
      item: { id },
    } = currentItem.value

    try {
      const success = await itemsStore.deleteItem(id)
      hasDeleteError.value = !success
    } catch (e) {
      logger.error('Error deleting item %O', e)
      hasDeleteError.value = true
    } finally {
      isDeleting.value = false
      currentItem.value = null
    }
  }

  const createShare = async () => {
    if (!currentItem.value) return
    isCreatingShare.value = true
    const { item } = currentItem.value

    try {
      const result = await itemsStore.createShare(item)

      if (result) {
        await refreshCurrentItem(true)
        return true
      }
    } catch (e) {
      logger.error('Error creating share %O', e)
      return false
    } finally {
      isCreatingShare.value = false
    }
  }

  const deleteShare = async () => {
    if (!currentItem.value?.item.share_link) return false
    isDeletingShare.value = true

    try {
      const success = await itemsStore.deleteShare(currentItem.value.item.id)

      if (success) {
        await refreshCurrentItem(true)
        return true
      }
    } catch (e) {
      logger.error('Error deleting share %O', e)
      return false
    } finally {
      isDeletingShare.value = false
    }
  }

  // const moveToFolder = async (folder: Folder) => {
  //   if (!currentItem.value) return false
  //   const { item, overview, details } = currentItem.value
  //   isUpdating.value = true
  //   hasUpdateError.value = false

  //   try {
  //     const success = await itemsStore.updateItem(item.id, overview, details, folder)
  //     hasUpdateError.value = !success
  //     return success
  //   } catch (e) {
  //     logger.error('Error updating item %O', e)
  //     hasUpdateError.value = true
  //     return false
  //   } finally {
  //     isUpdating.value = false
  //   }
  // }

  // TODO: There's a lot of duplicated logic and indirection here, maybe we
  // can clean this up and move it to the folder store.
  const addFolderMembership = async (email: string) => {
    if (!email) return false
    if (!currentItem.value) return false

    try {
      // If this call fails, then just return false
      const publicKeyResult = await foldersStore.getFolderMemberPublicKey(email)
      const folderMember = publicKeyResult ?? { email }

      // This is done in the items store, because we possibly need to convert
      // the item into a folder item.
      await itemsStore.addFolderMembership(currentItem.value.item.id, folderMember)
      await refreshCurrentItem(true)
      return true
    } catch (e) {
      logger.error('Error adding membership %O', e)
      return false
    }
  }

  const revokeFolderMembership = async (memberId: string) => {
    if (!memberId) return false
    if (!currentItem.value || !currentItem.value.item.folder) return false

    try {
      await foldersStore.revokeFolderMembership(currentItem.value.item.folder.id, memberId)
      await refreshCurrentItem(true)
      return true
    } catch (e) {
      logger.error('Error revoking folder membership %O', e)
      return false
    }
  }

  const confirmAllPendingFolderInvitations = async () => {
    if (!currentItem.value || !currentItem.value.item.folder) return false
    const folder = currentItem.value.item.folder

    if (!folder.folder_invitations.length) return true

    try {
      await foldersStore.confirmPendingFolderInvitations(folder)
      await refreshCurrentItem(true)
      return true
    } catch (e) {
      logger.error('Error confirming all pending folder memberships %O', e)
      return false
    }
  }

  // Handle updates outside of the current store
  itemsStore.onUpdate(({ updatedIds, deletedIds }) => {
    if (!currentItem.value) return

    logger.debug('Items externally updated')

    // Item was updated on the server and in the items store
    if (updatedIds.includes(currentItem.value.item.id)) {
      logger.debug('Current item was updated externally')
      refreshCurrentItem()
    }

    // Item no longer exist
    if (deletedIds.includes(currentItem.value.item.id)) {
      logger.debug('Current item was deleted externally')
      isDeletedExternally.value = true
    }
  })

  return {
    currentItem,
    nextItemId,

    isDirty,
    isLoading,
    hasLoadError,
    isUpdating,
    hasUpdateError,
    isDeleting,
    hasDeleteError,
    isCreatingShare,
    isDeletingShare,
    isDeletedExternally,

    setCurrentItem,
    refreshCurrentItem,
    updateCurrentItem,
    deleteCurrentItem,

    createShare,
    deleteShare,
    addFolderMembership,
    revokeFolderMembership,
    confirmAllPendingFolderInvitations,

    $reset,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCurrentItemStore, import.meta.hot))
}
