import { computed, ref } from 'vue'
import { createEventHook } from '@vueuse/core'
import { acceptHMRUpdate, defineStore } from 'pinia'

import { createLogger } from '@core/utils/logger'
import { useLoginForm } from '@core/composables/forms'
import { Brand, getBrand, searchBrands } from '@core/clients/tilig/brands'
import { initializeItem, Template } from '@core/clients/tilig'

import { useItemsStore } from './items'
import defaultWebsites from './defaultAddItemWebsites.json'

const logger = createLogger('stores:addItem')

export interface SelectedWebsite {
  name: string | null
  domain: string | null
  brand: Brand | null
}

export enum AddItemFlow {
  SelectWebsite = 'SelectWebsite',
  ShowForm = 'ShowForm',
}

const BRAND_FETCH_WAIT = 500

type BrandsMap = Map<string, Brand>

export const useAddItemStore = defineStore('addItem', () => {
  const selectedWebsite = ref<SelectedWebsite | null>(null)
  const websiteOptions = ref<SelectedWebsite[]>(defaultWebsites)
  const newItem = ref(initializeItem())
  const isCreating = ref(false)
  const isCreated = ref(false)
  const searchString = ref('')
  const currentStep = ref(AddItemFlow.SelectWebsite)
  const brandsMap = ref<BrandsMap>(new Map())
  const searchingForBrands = ref(false)
  const brandSearchRequestCounter = ref(0)
  const itemWebsiteRequestCounter = ref(0)

  const { website: newItemWebsite } = useLoginForm(newItem)

  const isSearching = computed(() => !!searchString.value)
  const brands = computed(() => Array.from(brandsMap.value.values()))

  const itemsStore = useItemsStore()

  const openAddItemModalHook = createEventHook<void>()
  const onOpenAddModal = openAddItemModalHook.on
  const unsubscribeOnOpenAddModal = openAddItemModalHook.off
  const openAddModal = openAddItemModalHook.trigger

  const $reset = () => {
    selectedWebsite.value = null
    newItem.value = initializeItem()
    isCreating.value = false
    isCreated.value = false
    searchString.value = ''
    currentStep.value = AddItemFlow.SelectWebsite
    brandsMap.value = new Map()
    searchingForBrands.value = false
  }

  const addItem = async () => {
    isCreating.value = true

    try {
      const item = await itemsStore.createItem(newItem.value)
      if (item) isCreated.value = true
      return item
    } finally {
      isCreating.value = false
    }
  }

  const getBrandMatches = async () => {
    logger.debug('Getting brand matches')
    const currentCounter = ++brandSearchRequestCounter.value

    searchingForBrands.value = true
    const { data: newBrands } = await searchBrands(searchString.value)

    if (brandSearchRequestCounter.value !== currentCounter) {
      // There is a new request that was fired after this one.
      // We just ignore there results from this request and
      // let the newer request handle things.
      logger.debug('Not storing changes from search request %d', currentCounter)
      return
    }

    const newBrandsMap: BrandsMap = new Map()
    newBrands.forEach((brand) => {
      newBrandsMap.set(brand.id, brand)
    })
    brandsMap.value = newBrandsMap

    if (newBrands.some((b) => !b.is_fetched)) {
      // This function is delayed by half a second
      // to allow enough time to pull the details from Brandfetch
      // https://gitlab.com/subshq/server/api/-/issues/174#note_1072096355
      setTimeout(getMissingBrandDetails, BRAND_FETCH_WAIT)
    }

    searchingForBrands.value = false
  }

  const getMissingBrandDetails = () => {
    logger.debug('Fetching missing brand details')
    brands.value
      .filter((brand) => !brand.is_fetched)
      .forEach(async (brand) => {
        try {
          const { data } = await getBrand(brand.domain)
          brandsMap.value.set(brand.id, data)
        } catch (e) {
          logger.error('Error re-fetching brand %s %O', brand.domain, e)
        }
      })
  }

  const updateNewItemBrand = async () => {
    if (newItem.value.item.template !== Template.Login) return
    if (!newItemWebsite.value) return

    logger.debug('Getting brand for new item')
    const currentCounter = ++itemWebsiteRequestCounter.value

    try {
      const { data: brand } = await getBrand(newItemWebsite.value)

      if (itemWebsiteRequestCounter.value !== currentCounter) {
        // There is a new request that was fired after this one.
        // We just ignore there results from this request and
        // let the newer request handle things.
        logger.debug('Not storing changes from brand search %d', currentCounter)
        return
      }

      if (selectedWebsite.value) {
        logger.debug('Updating brand')
        selectedWebsite.value.brand = brand
      } else {
        selectedWebsite.value = {
          name: brand.name,
          domain: brand.domain,
          brand,
        }
      }
    } catch (e) {
      logger.error('Error fetching item brand %O', e)
      if (selectedWebsite.value) selectedWebsite.value.brand = null
    }
  }

  /**
   * Prefill default website options
   */
  const getDefaultWebsiteOptions = async () => {
    websiteOptions.value.forEach(async (option, index) => {
      if (!option.domain) return

      try {
        const { data: brand } = await getBrand(option.domain)
        websiteOptions.value[index].brand = brand
      } catch (e) {
        logger.debug('Error fetching brand for default %O', option)
      }
    })
  }

  // Trigger update asynchronously in 1 second
  setTimeout(getDefaultWebsiteOptions, 1000)

  return {
    selectedWebsite,
    websiteOptions,
    newItem,
    isCreating,
    isCreated,
    searchString,
    isSearching,
    currentStep,
    brandsMap,
    searchingForBrands,
    brands,
    addItem,
    getBrandMatches,
    updateNewItemBrand,

    onOpenAddModal,
    unsubscribeOnOpenAddModal,
    openAddModal,

    $reset,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useAddItemStore, import.meta.hot))
}
