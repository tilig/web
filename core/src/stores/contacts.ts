import { ref } from 'vue'
import { acceptHMRUpdate, defineStore } from 'pinia'

import { createLogger } from '@core/utils/logger'
import { useAuthStore } from './auth'
import { Contact, getContacts } from '@core/clients/tilig'

const logger = createLogger('stores:contacts')

export const useContactsStore = defineStore('contacts', () => {
  const isFetchingContacts = ref(false)
  const contacts = ref<Contact[]>([])

  const authStore = useAuthStore()

  const $reset = () => {
    isFetchingContacts.value = false
    contacts.value = []
  }

  authStore.onSignOut(() => {
    logger.debug('Resetting store')
    $reset()
  })

  const fetchContacts = async () => {
    isFetchingContacts.value = true

    logger.debug('Fetching contacts')
    try {
      const {
        data: { contacts: userContacts },
      } = await getContacts()

      logger.debug('Contacts fetched', userContacts)

      contacts.value = userContacts
    } catch (error) {
      logger.error('Failed to fetch contacts', error)
    } finally {
      isFetchingContacts.value = false
    }
  }

  return {
    isFetchingContacts,
    contacts,

    fetchContacts,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useContactsStore, import.meta.hot))
}
