/// <reference types="pinia-plugin-persistedstate" />
import { computed, ref, shallowRef, watch } from 'vue'
import { defineStore, acceptHMRUpdate } from 'pinia'
import {
  getRedirectResult,
  getAdditionalUserInfo,
  AdditionalUserInfo,
  User,
  UserInfo,
  sendEmailVerification,
  UserCredential,
  updateCurrentUser,
} from 'firebase/auth'
import { UserImpl, AuthImpl } from '@firebase/auth/internal'
import { until, createEventHook, useBroadcastChannel } from '@vueuse/core'
import * as Sentry from '@sentry/vue'
import { FirebaseError } from '@firebase/util'

import { createLogger } from '@core/utils/logger'
import {
  deleteUser as deleteUserFromFirebase,
  getFirebaseClient,
  SignInMethod,
  signInWithApple,
  signInWithGoogle,
  signInWithMicrosoft,
} from '@core/clients/firebase'
import {
  AccessTokenTrigger,
  deleteUserProfile,
  getAccessToken as getApiAccessToken,
  getKeyPairs,
  getUserProfile,
  KeyPair,
  LegacyKeyPair,
  ProviderId,
  refreshAccessToken as refreshApiAccessToken,
  TiligAccessToken,
  updateUserProfile,
  UserProfile,
} from '@core/clients/tilig'
import { isAxiosError } from '@core/clients/axios'

import { singleCall } from '@core/utils/rateLimit'
import { supportsRedirectSignIn } from '@core/utils/browserEnvironment'
import { importLegacyKeyPair } from '@core/clients/tilig/legacyCrypto'

const logger = createLogger('stores:auth')

export enum AuthState {
  NONE = 'NONE',
  FIREBASE_SESSION = 'FIREBASE_SESSION',
  TILIG_SESSION = 'TILIG_SESSION',
  FULL_PROFILE = 'FULL_PROFILE',
  SIGNED_IN = 'SIGNED_IN',
}

interface AuthenticationError {
  severity: 'info' | 'warn' | 'error'
  message: string
  exception?: Error | FirebaseError | unknown
  exceptionExtras?: { [key: string]: string }
}

export interface HandoffProperties {
  userJson: string
  additionalUserInfo?: AdditionalUserInfo
}

export type AuthProvider = 'apple' | 'google' | 'microsoft'

/**
 * `watchEffect` but with specified sources
 *
 * @param source Source to watch
 * @param cb Effect
 * @param options Watch Options
 */
const watchImmediate = (...args: Parameters<typeof watch>) => {
  return watch(args[0], args[1], { immediate: true, ...args[2] })
}

const findInProviderData = (user: User | null, key: keyof UserInfo) => {
  return user?.providerData.map((d) => d[key]).find((v) => !!v) || null
}

const findInAdditionalUserInfo = (additionalUserInfo: AdditionalUserInfo | null, key: string) => {
  if (!additionalUserInfo?.profile) return null

  return (additionalUserInfo.profile[key] || null) as string | null
}

const extractUserProfile = (user: User, additionalUserInfo: AdditionalUserInfo) => {
  const displayName = user.displayName || findInProviderData(user, 'displayName')
  const photoURL = user.photoURL || findInProviderData(user, 'photoURL')
  // Note: don't use `user.providerId` here because that defaults to 'firebase'.
  // There's also a `providerId` value in the `additionalUserInfo`, but we're not using that.
  const providerId = findInProviderData(user, 'providerId')
  const givenName = findInAdditionalUserInfo(additionalUserInfo, 'given_name')
  const familyName = findInAdditionalUserInfo(additionalUserInfo, 'family_name')
  const locale = findInAdditionalUserInfo(additionalUserInfo, 'locale') || 'en'

  return {
    // email is always present, because we require Apple/Google sign in
    email: user.email as string,
    displayName,
    givenName,
    familyName,
    locale,
    photoURL,
    providerId: providerId as ProviderId | null,
  }
}

const isFirebaseError = (error: Error): error is FirebaseError => 'code' in error

/**
 * Firebase login error message
 * Use the error codes to handle custom error
 * https://firebase.google.com/docs/reference/js/v8/firebase.auth.Auth#error-codes_15
 */
const getFriendlyErrorMessage = (error: FirebaseError) => {
  switch (error.code) {
    case 'auth/account-exists-with-different-credential': {
      const {
        _tokenResponse: { verifiedProvider, email },
      } = error.customData as { _tokenResponse: { verifiedProvider: string[]; email: string } }

      return `You already signed up for a Tilig account with ${email}.\nPlease try signing in through ${verifiedProvider[0]}.`
    }
    case 'auth/popup-closed-by-user':
      return 'You closed the sign in popup. Please try again.'
    case 'auth/popup-blocked':
      return 'The sign in popup was blocked by your browser. Please try again.'
  }
}

const captureSignOutError = (error: AuthenticationError) => {
  Sentry.withScope((scope) => {
    scope.setTag('context', 'auth')
    scope.setExtra('message', error.message)
    scope.setExtra('severity', error.severity)
    if (error.exceptionExtras) {
      for (const key in error.exceptionExtras) {
        scope.setExtra(key, error.exceptionExtras[key])
      }
    }
    if (error.exception) {
      Sentry.captureException(error.exception)
    } else {
      Sentry.captureMessage(`${error.severity}: ${error.message}`)
    }
  })
}

export const useAuthStore = defineStore(
  'auth',
  () => {
    const firebaseClient = getFirebaseClient()
    /**
     * Whether firebase (auth) has been initialized
     */
    const firebaseInitialized = ref(false)
    /**
     * Whether there is a currently logged in firebase user
     */
    const hasFirebaseUser = ref(false)
    /**
     * Get current firebase auth user
     */
    const getFirebaseUser = () => firebaseClient.auth.currentUser
    /**
     * JSON serialized version of the firebase user
     *
     * For the extension to use in auth handoff.
     */
    const userJson = ref<string | null>(null)

    /**
     * The user as stored in our backend
     */
    const tiligUser = shallowRef<UserProfile | null>(null)

    /**
     * Provider specific additional info about user.
     * Persisted, so the profile store can access it.
     */
    const additionalUserInfo = shallowRef<AdditionalUserInfo | null>(null)
    /**
     * Last error received during auth flow
     */
    const authenticationError = shallowRef<AuthenticationError | null>(null)
    /**
     * Tilig SaaS access token. Persisted.
     */
    const accessToken = shallowRef<TiligAccessToken | null>(null)
    /**
     * What triggered fetching the accessToken
     */
    const accessTokenTrigger = ref<AccessTokenTrigger>(null)
    /**
     * The referral code used to sign up
     */
    const referralCode = ref<string | null>(null)
    /**
     * libsodium box key pair for encryption. Persisted.
     */
    const keyPair = shallowRef<KeyPair | null>(null)
    /**
     * Legacy RSA key pair for encryption. Persisted.
     */
    const legacyKeyPair = shallowRef<LegacyKeyPair | null>(null)
    const rsaCryptoKeyPair = shallowRef<CryptoKeyPair | null>(null)

    /**
     * Delete user on sign-in
     */
    const deleteAfterSignIn = ref(false)

    /**
     * Current state of auth process
     */
    const authState = computed(() => {
      if (!hasFirebaseUser.value) {
        return AuthState.NONE
      } else if (!accessToken.value) {
        return AuthState.FIREBASE_SESSION
      } else if (!tiligUser.value || !keyPair.value || !legacyKeyPair.value) {
        return AuthState.TILIG_SESSION
      } else {
        return AuthState.SIGNED_IN
      }
    })
    /**
     * True if we are not in the process of authenticating
     */
    const isSettled = computed(() => {
      return [AuthState.NONE, AuthState.SIGNED_IN].includes(authState.value)
    })
    /**
     * Set to true if we are in incognito mode
     */
    const isIncognito = ref(false)
    /**
     * Set to true if we are in the extension
     */
    const isExtension = ref(false)
    /**
     * True once the user is authenticated with both firebase and tilig
     */
    const isAuthenticated = computed(() => authState.value === AuthState.SIGNED_IN)
    /**
     * Whether any error occured during authentication
     */
    const hasAuthenticationError = computed(() => !!authenticationError.value)

    /**
     * Boolean to indicate if this store has been initialized
     */
    const initialized = ref(false)

    /**
     * A promise that can be awaited to make sure that authentication
     * has been successfully initialized.
     */
    const ready = until(initialized).toBe(true)

    // We use callbacks on the auth instance, so firebase changes are always
    // detected.
    firebaseClient.auth.onAuthStateChanged((authUser) => {
      logger.debug('Received new user', authUser?.uid)

      // onAuthStateChanged is guaranteed to be called, even if firebase is
      // already initialized.
      if (!firebaseInitialized.value) firebaseInitialized.value = true

      // There is no firebase auth session
      if (!authUser) {
        hasFirebaseUser.value = false
        userJson.value = null
        return
      }

      if (authUser.emailVerified) {
        hasFirebaseUser.value = true
        userJson.value = JSON.stringify(authUser.toJSON())
        authenticationError.value = null
      } else {
        sendEmailVerification(authUser, {
          url: new URL(
            `/login?provider=microsoft&email_hint=${authUser.email}`,
            window.location.origin
          ).toString(),
        })
          .then(() => logger.debug('Email verification sent'))
          .catch((e) => logger.error('Error during email verification %O', e))

        signOut({
          severity: 'info',
          message: `Before you can sign in, you'll need to verify your email address. Please check the inbox or the junk email folder of ${authUser.email} and follow the instructions.`,
        })
      }
    })

    /**
     * Handle firebase specific error
     * @param error The firebase error object
     */
    const handleFirebaseLoginError = (error: Error) => {
      if (isFirebaseError(error)) {
        logger.error('Firebase error during login %O', error)
        const message = getFriendlyErrorMessage(error)

        if (message) {
          signOut({
            severity: 'warn',
            message,
            exception: error,
          })
          return
        }
      }

      signOut({
        severity: 'error',
        message: `An unknown error occured on our end while signing you in. Please try again at a later time.`,
        exception: error,
      })
    }

    /**
     * Handle Firebase error caused by incognito mode
     * @param error Error from firebase
     * @returns whether this was a incognito error
     */
    const handleIncognitoError = (error: FirebaseError) => {
      if (error.code !== 'auth/web-storage-unsupported') {
        return false
      }

      hasFirebaseUser.value = false
      authenticationError.value = null
      isIncognito.value = true
      return true
    }

    /**
     * Check auth state for completeness, or sign out
     */
    const authStateSanityCheck = () => {
      logger.debug('Sanity checking auth state')
      if (!initialized.value || !firebaseInitialized.value) return

      const postFirebaseSessionValues = [
        accessToken.value,
        keyPair.value,
        legacyKeyPair.value,
        tiligUser.value,
      ]

      if (!hasFirebaseUser.value && postFirebaseSessionValues.some((v) => !!v)) {
        logger.error(
          'We have a session with the api, but not with firebase. This will result in errors. Signing out.'
        )
        signOut({
          severity: 'error',
          message: 'There was an issue with your session. Please try signing in again.',
        })
      }
    }

    /**
     * Initialize the auth store
     *
     * This checks whether a user has logged in through a redirect flow.
     */
    const initialize = async () => {
      logger.debug('Initializing')
      if (initialized.value) return

      // Allow firebase client to set persistance
      await firebaseClient.ready

      try {
        // NOTE: We handle the result from login through watchers.
        const result = await getRedirectResult(firebaseClient.auth)
        logger.debug('Redirect result handled %O', result)

        // It seems firebase does not provide a nice api to get additional user info directly
        // from the user. We need to set it here.
        additionalUserInfo.value = result && getAdditionalUserInfo(result)
      } catch (error) {
        logger.error('Error during redirect handling %O', error)
        if (handleIncognitoError(error as FirebaseError)) {
          return
        }
        handleFirebaseLoginError(error as FirebaseError)
      }

      initialized.value = true
      logger.debug('Auth store initialized')
      authStateSanityCheck()
    }

    /**
     * Handoff auth from popup
     * This happens in the extension, where we get handed back a variety
     * of user details which we need to pass to the auth store which sits
     * in a seperate context (The background.js / service worker)
     *
     */
    const handoff = async (handoffProps: HandoffProperties) => {
      logger.debug('Handoff', handoffProps)
      // Mark this authentication flow as a handoff
      accessTokenTrigger.value = 'handoff'

      try {
        await updateCurrentUser(
          firebaseClient.auth,
          UserImpl._fromJSON(firebaseClient.auth as AuthImpl, JSON.parse(handoffProps.userJson))
        )

        if (handoffProps.additionalUserInfo) {
          additionalUserInfo.value = handoffProps.additionalUserInfo
        }
        // Wait until we have completed the handoff and authenticated
        await until(isSettled).toBe(true)
        return isAuthenticated.value
      } catch (e) {
        logger.error('Error updating user from handoff %O', e)
        Sentry.captureException(e)
        return false
      } finally {
        initialized.value = true
        logger.debug('Auth store initialized from handoff')
      }
    }

    /**
     * Get a new access token for the API
     */
    const getAccessToken = singleCall(async () => {
      logger.debug('Getting an API access tokens')
      if (!hasFirebaseUser.value) {
        logger.debug('No user to get tilig access token for')
        return null
      }

      try {
        const { data } = await getApiAccessToken(accessTokenTrigger.value, referralCode.value)
        accessToken.value = data
        logger.debug('Received new access tokens')
        return accessToken.value
      } catch (e) {
        if (isAxiosError(e) && e.response?.status === 403) {
          // This is a new user account, do no track errors
          return null
        }
        logger.error("Couldn't get access token", e)
        Sentry.captureException(e)
        return null
      } finally {
        // Reset the trigger
        accessTokenTrigger.value = null
      }
    })

    /**
     * Reset Auth state that happens after firebase login
     */
    const restartApiSession = async () => {
      // Reset post-firebase auth state
      accessToken.value = null
      keyPair.value = null
      legacyKeyPair.value = null
      rsaCryptoKeyPair.value = null
      tiligUser.value = null
      additionalUserInfo.value = null

      // Reset all listening stores
      signOutEventHook.trigger()

      // Try to get a new access token
      accessTokenTrigger.value = 'refresh'
      const newAccessToken = await getAccessToken()
      if (!newAccessToken) {
        logger.error('Could not restart our API session')
        signOut({
          severity: 'error',
          message: 'There is something wrong with your session. Please try signin in again.',
        })
      }
      // If we have a new access token at this time, the watchers will resume
      // the rest of the auth flow.
      return newAccessToken
    }

    /**
     * Refresh accessToken for API access
     *
     * We only allow one concurrent accessToken refresh request.
     * Every subsequent call before the request is resolved will
     * instead receive a copy of current promise.
     */
    const refreshAccessToken = singleCall(async () => {
      logger.debug('Refreshing access tokens')
      if (!accessToken.value) {
        logger.debug('No accessToken to refresh')
        return null
      }

      try {
        const { data } = await refreshApiAccessToken(accessToken.value)
        accessToken.value = data
        logger.debug('Received new access tokens')
        return accessToken.value
      } catch (e) {
        logger.error("Couldn't refresh access token", e)
        Sentry.captureException(e)
        if (isAxiosError(e) && e.response?.status === 401) {
          // Our refresh token is invalid
          return restartApiSession()
        }
        return null
      }
    })

    const broadcast = useBroadcastChannel({ name: 'stores:auth' })

    if (broadcast.isSupported.value) {
      logger.debug('Listening on broadcastChannel auth')
      watch(broadcast.data, () => {
        logger.debug('Received data from other tab %O', broadcast.data.value)
        if (broadcast.data.value === 'signout') {
          if (isExtension.value) {
            logger.debug('Extension should never receive a `signout` broadcast')
            captureSignOutError({
              severity: 'error',
              message: 'Received `signout` broadcast in extension, which should never happen',
            })
          } else {
            logger.debug('Also signing out of this tab')
            signOut()
          }
        }

        if (broadcast.data.value === 'signin' && !hasFirebaseUser.value) {
          logger.debug('Signed in on other tab, reloading')
          window.location.reload()
        }
      })
    }

    const signOutEventHook = createEventHook<void>()
    const onSignOut = signOutEventHook.on
    const unsubscribeOnSignOut = signOutEventHook.off

    /**
     * Refresh the accessToken anytime auth changes.
     */
    watchImmediate([hasFirebaseUser], async () => {
      if (!hasFirebaseUser.value) {
        logger.debug('No user to fetch access token for')
        return
      }
      if (accessToken.value) {
        logger.debug('Already have access token')
        return
      }

      logger.debug('Fetching access token')
      const newToken = await getAccessToken()

      if (!newToken) {
        signOut({
          severity: 'error',
          message: `Tilig shuts down on the 30th of April, 2023. You cannot create a new account.`,
        })
      }
    })

    /**
     * Fetch keypair once user and accessToken are available
     */
    watchImmediate([hasFirebaseUser, accessToken], async () => {
      if (import.meta.env.MODE !== 'production' && deleteAfterSignIn.value) {
        await deleteUser()
        return
      }

      logger.debug('Check if we should fetch keypair')
      if (!hasFirebaseUser.value || !accessToken.value) {
        logger.debug('No user and/or access token')
        return
      }

      if (keyPair.value && legacyKeyPair.value) {
        logger.debug('Already obtained keypairs')
        if (!rsaCryptoKeyPair.value) {
          logger.debug('Importing legacy keypair')
          try {
            rsaCryptoKeyPair.value = await importLegacyKeyPair(legacyKeyPair.value)
          } catch (e) {
            logger.error('Error importing keypair %O', e)
            signOut({
              severity: 'error',
              message:
                'There was an error parsing your keypair. Please contact support. In the meantime you can use Tilig on Android or iOS.',
              exception: e,
            })
          }
        }
        return
      }

      logger.debug('Fetching profile and key pairs')

      try {
        const { profile, keyPairData } = await getUserProfile()
        const keyPairs = await getKeyPairs(keyPairData)
        tiligUser.value = profile
        keyPair.value = keyPairs.keyPair
        legacyKeyPair.value = keyPairs.legacyKeyPair
        rsaCryptoKeyPair.value = await importLegacyKeyPair(keyPairs.legacyKeyPair)

        if (broadcast.isSupported.value) {
          broadcast.post('signin')
        }
      } catch (e) {
        logger.error('Error fetching keypair %O', e)

        signOut({
          severity: 'error',
          message: `We couldn't obtain your account keys at this time. Please try again later.`,
          exception: e,
        })
      }
    })

    /**
     * Store user details on the server
     */
    watchImmediate([hasFirebaseUser, accessToken, additionalUserInfo], async () => {
      if (
        !hasFirebaseUser.value ||
        !firebaseClient.auth.currentUser ||
        !accessToken.value ||
        !additionalUserInfo.value
      ) {
        logger.debug(
          'Cannot update profile unless we have additionalUserInfo',
          !hasFirebaseUser.value,
          !firebaseClient.auth.currentUser,
          !accessToken.value,
          !additionalUserInfo.value
        )
        return
      }

      try {
        logger.debug('Extracting user profile')
        const updatedProfile = extractUserProfile(
          firebaseClient.auth.currentUser,
          additionalUserInfo.value
        )
        logger.debug('Updating user profile')
        const { profile } = await updateUserProfile(updatedProfile)
        tiligUser.value = profile
        logger.debug('Updated user profile')
      } catch (error) {
        logger.error('Error during profile updating %O', error)
        Sentry.captureException(error)
      }
    })

    watchImmediate(tiligUser, () => {
      if (tiligUser.value) {
        logger.debug('Set sentry user to', tiligUser.value.id, tiligUser.value.email)
        Sentry.setUser({ id: tiligUser.value.id, email: tiligUser.value.email })
      } else {
        logger.debug('Set sentry user to undefined')
        Sentry.setUser({ id: undefined, email: undefined })
      }
    })

    /**
     * True if sign in is currently attempted
     */
    const signingIn = ref(false)
    /**
     * Use AuthProvider to sign in
     */
    const signInWith = async (provider: AuthProvider) => {
      signingIn.value = true
      logger.debug(`Attempting to sign in with ${provider}`)
      const method =
        isIncognito.value || !supportsRedirectSignIn ? SignInMethod.PopUp : SignInMethod.Redirect

      try {
        let result: UserCredential

        switch (provider) {
          case 'apple':
            result = await signInWithApple(method)
            break
          case 'google':
            result = await signInWithGoogle(method)
            break
          case 'microsoft':
            result = await signInWithMicrosoft(method)
            break
          default:
            throw new Error(`Login failed. We don't recognize this login provider`)
        }

        additionalUserInfo.value = getAdditionalUserInfo(result)
        return true
      } catch (e) {
        logger.error('Error signing in %O', e)
        handleFirebaseLoginError(e as FirebaseError)
        return false
      } finally {
        signingIn.value = false
      }
    }

    const deleteUser = async () => {
      if (!hasFirebaseUser.value || !accessToken.value) return

      logger.debug('Deleting user from API and firebase')
      await deleteUserProfile()
      await deleteUserFromFirebase()

      signOut({
        severity: 'info',
        message: 'Deleted your account.',
      })
    }

    /**
     * True if sign in is currently attempted
     */
    const signingOut = ref(false)
    /**
     * Forcefully log the user out
     */
    const signOut = async (error?: AuthenticationError) => {
      logger.debug('Signing out %O', error)

      // Capture all events >= 'warn'
      if (error && error.severity != 'info') {
        captureSignOutError(error)
      }

      signingOut.value = true
      authenticationError.value = error ?? null

      try {
        logger.debug('Signing out of firebase')
        await firebaseClient.auth.signOut()
        logger.debug('Signed out of firebase')
      } catch (e) {
        logger.error('Error during firebase logout %O', e)
        Sentry.captureException(e)
      }

      // Reset user info
      logger.debug('Resetting user info')
      accessToken.value = null
      additionalUserInfo.value = null
      keyPair.value = null
      legacyKeyPair.value = null
      rsaCryptoKeyPair.value = null
      tiligUser.value = null

      if (broadcast.isSupported) {
        // Log out other tabs
        broadcast.post('signout')
      }

      // Trigger signOut event
      signOutEventHook.trigger()

      signingOut.value = false
    }

    /**
     * Refresh tilig user from server
     */
    const refreshProfile = async () => {
      const { profile } = await getUserProfile()
      tiligUser.value = profile
    }

    return {
      accessToken,
      additionalUserInfo,
      authenticationError,
      authState,
      deleteAfterSignIn,
      firebaseInitialized,
      hasAuthenticationError,
      getFirebaseUser,
      hasFirebaseUser,
      initialized,
      isAuthenticated,
      isIncognito,
      isExtension,
      isSettled,
      keyPair,
      legacyKeyPair,
      rsaCryptoKeyPair,
      signingIn,
      signingOut,
      tiligUser,
      userJson,
      referralCode,

      ready,

      initialize,
      handoff,
      deleteUser,
      refreshAccessToken,
      signInWith,
      signOut,
      onSignOut,
      unsubscribeOnSignOut,
      refreshProfile,
    }
  },
  {
    persist: {
      paths: ['accessToken', 'keyPair', 'legacyKeyPair', 'tiligUser', 'userJson'],
    },
  }
)

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useAuthStore, import.meta.hot))
}
