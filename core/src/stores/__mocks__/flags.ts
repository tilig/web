import { ref } from 'vue'
import { vi } from 'vitest'

const fakeFlagStore = {
  isEnabled: vi.fn(() => false),
  useFlag: vi.fn(() => ref(false)),
}

export const useFlagsStore = () => fakeFlagStore
