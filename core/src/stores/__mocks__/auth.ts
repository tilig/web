import { vi } from 'vitest'
import { reactive } from 'vue'
import { createEventHook } from '@vueuse/core'

import { KeyPair, LegacyKeyPair, UserProfile } from '@core/clients/tilig'
import { tiligUser, legacyKeyPair, keyPair } from '../__tests__/__fixtures__/auth.fixtures'

const signOutEventHook = createEventHook<void>()
const onSignOut = signOutEventHook.on
const unsubscribeOnSignOut = signOutEventHook.off

const signOut = vi.fn(() => {
  signOutEventHook.trigger()
})

const mockAuthStore = reactive({
  hasFirebaseUser: true,
  tiligUser: tiligUser as UserProfile | null,
  keyPair: keyPair as KeyPair | null,
  legacyKeyPair: legacyKeyPair as LegacyKeyPair | null,
  rsaCryptoKeyPair: null,
  signOut,
  onSignOut,
  unsubscribeOnSignOut,
})

export const useAuthStore = () => mockAuthStore
