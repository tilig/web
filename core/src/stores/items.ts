/// <reference types="pinia-plugin-persistedstate" />
import { computed, ref, watchEffect } from 'vue'
import { acceptHMRUpdate, defineStore, StateTree } from 'pinia'
import { cloneDeep, sortBy } from 'lodash'
import * as Sentry from '@sentry/vue'

import { createLogger } from '@core/utils/logger'

import {
  createItem as createItemOnServer,
  createShare as createShareOnServer,
  DecryptedDetails,
  DecryptedItem,
  DecryptedOverview,
  decryptItem,
  deferredDecryptItem,
  deleteItem as deleteItemOnServer,
  deleteShare as deleteShareOnServer,
  EncryptedItem,
  encryptItem,
  EncryptItemOptions,
  Folder,
  FolderCreationAttributes,
  generateFolderDataFromItem,
  generateShareData,
  getItem,
  getItems,
  initializeItem,
  MAX_LOGIN_VALUE_LENGTH,
  pollItems,
  Template,
  updateItem as updateItemOnServer,
} from '@core/clients/tilig'

import { useAuthStore } from './auth'
import { singleCall } from '@core/utils/rateLimit'
import { stripSchema } from '@core/utils/url'

import { computedWithControl, createEventHook } from '@vueuse/core'
import { FolderMemberAttributes, useFoldersStore } from './folders'

const logger = createLogger('stores:items')

export let CACHE_VERSION_KEY: string | null = null
const POLLING_INTERVAL = 30 * 1000 // Milliseconds between polling attempts

/**
 * We should invalidate the cache on any updates to the version
 * This is defined by each worspace vite.config.ts
 * If not defined, then caching is turned off
 */
CACHE_VERSION_KEY = typeof APP_VERSION === 'undefined' ? null : APP_VERSION

// Allow us to set this manually for tests
export const setCacheVersionKey = (version: string) => (CACHE_VERSION_KEY = version)

const serialize = (state: StateTree): string => {
  const itemStateMap = state.itemStateMap as ItemStateMap
  return JSON.stringify({
    version: CACHE_VERSION_KEY,
    itemState: Array.from(itemStateMap.entries()),
  })
}

const deserialize = (stateStr: string): StateTree => {
  if (!stateStr) return {}
  try {
    const state = JSON.parse(stateStr)
    if (
      CACHE_VERSION_KEY &&
      state['version'] &&
      state['version'] === CACHE_VERSION_KEY &&
      state['itemState']
    ) {
      const itemStateMap = new Map(state['itemState'])
      return {
        itemStateMap: itemStateMap,
      }
    }
    return {}
  } catch (err) {
    logger.error("Unable to deserialize 'items' state", err, stateStr)
    return {}
  }
}

export interface ItemState {
  item: EncryptedItem
  overview: DecryptedOverview | null
}

type ItemStateMap = Map<string, ItemState>

export enum ItemType {
  Logins = 'logins',
  CreditCards = 'cards',
  SecureNotes = 'notes',
  WiFi = 'wifi',
  Custom = 'custom',
}

const itemTypeTemplateMap = new Map<ItemType, Template>([
  [ItemType.Logins, Template.Login],
  [ItemType.SecureNotes, Template.SecureNote],
  [ItemType.CreditCards, Template.CreditCard],
  [ItemType.WiFi, Template.WiFi],
  [ItemType.Custom, Template.Custom],
])

/**
 * Return item template for the given item type
 * @param itemType Itemtype to get template for
 */
export const getTemplateByItemType = (itemType: ItemType) => itemTypeTemplateMap.get(itemType)

/**
 * Return all items for the given item type
 * @param items List of items to filter
 * @param itemType Itemtype to filter by. Passing null will include all supported types.
 */
export const filterItemsByItemType = (items: ItemState[], itemType: ItemType | null = null) => {
  if (!itemType) return items

  const template = itemTypeTemplateMap.get(itemType)
  return items.filter(({ item }) => item.template === template)
}

/**
 * Return all items for given folder
 * @param items List of items to filter
 * @param itemType Folder to filter by. Passing null will include all items.
 */
const filterItemsByFolder = (items: ItemState[], folder: Folder | null = null) => {
  if (!folder) return items
  return items.filter(({ item }) => item.folder?.id === folder.id)
}

/**
 * Create a filter for an overview field
 * @param field Overview field to filter
 */
const matchField =
  (field: keyof Pick<DecryptedOverview, 'info' | 'name'>) =>
  (itemState: ItemState, query: string) => {
    if (!itemState.overview) {
      return false
    }

    return itemState.overview[field]?.toLocaleLowerCase().includes(query)
  }

const matchesName = matchField('name')
const matchesInfo = matchField('info')

/**
 * Check if any of the items url match the query
 * @param itemState ItemState to check
 * @param query Query to match url
 */
const matchesAnyUrl = (itemState: ItemState, query: string) =>
  itemState.overview?.urls.some(
    (u) => u.url && stripSchema(u.url).toLocaleLowerCase().includes(query)
  )

interface ItemFilter {
  query?: string | null
  itemType?: ItemType | null
  folder?: Folder | null
}

/**
 * Filter a list of items based on name and info
 * @param query Query string
 * @param items List of items to filter
 * @param itemType Itemtype to filter by
 */
export const filterItems = (items: ItemState[], filter?: ItemFilter) => {
  const { query, itemType, folder } = filter ?? {}

  items = filterItemsByItemType(items, itemType)
  items = filterItemsByFolder(items, folder)

  if (!query) {
    return items
  }

  return items.filter(
    (itemState) =>
      matchesName(itemState, query) ||
      matchesInfo(itemState, query) ||
      matchesAnyUrl(itemState, query)
  )
}

/**
 * Sort a list of items
 * @param items List of items to sort
 */
const sortItems = (items: ItemState[]) => {
  return sortBy(
    items,
    (itemState) =>
      itemState.overview?.name?.toLocaleLowerCase() || 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'
  )
}

export const useItemsStore = defineStore(
  'items',
  () => {
    const authStore = useAuthStore()
    const foldersStore = useFoldersStore()

    /**
     * In memory map of items and decrypted overviews
     */
    const itemStateMap = ref<ItemStateMap>(new Map())
    /**
     * True if we are currently fetching items
     */
    const isLoading = ref(false)
    /**
     * True if we have already fetched items
     */
    const isLoaded = ref(false)
    /**
     * True if we are currenctly decrypting overviews
     */
    const isDecrypting = ref(false)

    /**
     * Current search query
     */
    const query = ref('')

    /**
     * List of all decrypted items sorted by name
     */
    const itemStates = computed(() =>
      sortItems(Array.from(itemStateMap.value.values()).filter(({ overview }) => !!overview))
    )
    /**
     * Total number of items
     */
    const numberOfItems = computed(() => itemStateMap.value.size)
    /**
     * Items for active item type
     */
    const itemsForActiveItemType = computed(() =>
      filterItemsByItemType(itemStates.value, activeItemType.value)
    )
    /**
     * Total number of items for active item type
     */
    const numberOfItemsForActiveItemType = computed(() => itemsForActiveItemType.value.length)
    /**
     * Items for active item type
     */
    const itemsForFolder = computed(() => filterItemsByFolder(itemStates.value, activeFolder.value))
    /**
     * Total number of items for active item type
     */
    const numberOfItemsForActiveFolder = computed(() => itemsForFolder.value.length)

    /**
     * Lowercased and stripped version of the query string
     */
    const cleanedQuery = computed(() => query.value.toLocaleLowerCase().trim())

    /**
     * active item type
     */
    const activeItemType = ref<ItemType | null>(null)
    /**
     * active folder
     */
    const activeFolderId = ref<string | null>(null)
    /**
     * active folder
     */
    const activeFolder = computedWithControl(
      () => [activeFolderId.value, foldersStore.folders],
      () => (activeFolderId.value ? foldersStore.getFolderById(activeFolderId.value) : null)
    )

    /**
     * Items filtered on the current query filter
     */
    const filteredItems = computed(() =>
      filterItems(itemStates.value, {
        query: cleanedQuery.value,
        itemType: activeItemType.value,
        folder: activeFolder.value,
      })
    )
    /**
     * Returns true if the user has any custom items
     */
    const hasCustomItems = computed(
      () => !!filterItemsByItemType(itemStates.value, ItemType.Custom).length
    )
    /**
     * Allow values without length limit on login/1 items
     *
     * Enabled when the user has legacy_encryption_disabled.
     */
    const allowUnlimitedLoginValues = ref(false)
    /**
     * Maximum length of the login values
     *
     * Either 190 or null when the limit is disabled.
     */
    const maxLoginValueLength = computed(() =>
      allowUnlimitedLoginValues.value ? null : MAX_LOGIN_VALUE_LENGTH
    )

    /**
     * Disable automatically fetching new items
     */
    const disablePolling = ref(false)
    const lastPollingUpdate = ref<string | null>(null)
    const pollingIntervalHandle = ref<ReturnType<typeof setInterval> | null>(null)

    const $reset = () => {
      itemStateMap.value = new Map()
      isLoading.value = false
      isLoaded.value = false
      isDecrypting.value = false
      query.value = ''
      activeItemType.value = null
      activeFolderId.value = null
      allowUnlimitedLoginValues.value = false
      disablePolling.value = false
      lastPollingUpdate.value = null
    }

    // Clear data on sign out
    authStore.onSignOut(() => {
      logger.debug('Resetting store')
      $reset()
    })

    // Remove limit from login items depending on user
    watchEffect(() => {
      if (authStore.tiligUser) {
        allowUnlimitedLoginValues.value =
          !!authStore.tiligUser?.applicationSettings?.legacy_encryption_disabled
      } else {
        allowUnlimitedLoginValues.value = false
      }
    })

    /**
     * Update encrypted item states
     *
     * Only updates state, and removes removed items. Decryption needs to be handled separately.
     * @param items Items to add to state
     * @param deletedIds Items to delete from state
     * @param inPlace Whether to perform an update in-place or a complete update
     */
    const updateEncryptedItems = async (
      items: EncryptedItem[],
      deletedIds: string[] = [],
      inPlace = false
    ) => {
      const itemMap: ItemStateMap = inPlace ? itemStateMap.value : new Map()

      deletedIds.forEach((id) => itemMap.delete(id))

      items.forEach((item) => {
        const parseResult = EncryptedItem.safeParse(item)

        if (!parseResult.success) {
          logger.error('Could not parse item %s %O', item.id, parseResult.error)
          Sentry.captureMessage('Parsing item failed', {
            extra: { item_id: item.id, zod_errors: parseResult.error.format() },
          })
          return
        }

        const parsedItem = parseResult.data
        // TODO: Remove once all items have a template
        if (!parsedItem.template) parsedItem.template = Template.Login

        // If the item exists and the updated_at timestamps are the same,
        // skip updating.
        const existingItemState = itemStateMap.value.get(parsedItem.id)
        if (existingItemState?.item.updated_at === parsedItem.updated_at) {
          itemMap.set(existingItemState.item.id, { ...existingItemState })
        } else {
          itemMap.set(parsedItem.id, {
            item: parsedItem,
            overview: null,
          })
        }
      })

      // Replace current itemStates
      itemStateMap.value = itemMap
    }

    /**
     * Decrypt the overview of all items
     */
    const decryptItemOverviews = async () => {
      logger.debug('Decrypting overviews')

      isDecrypting.value = true

      const promises: Promise<void>[] = []

      itemStateMap.value.forEach((itemState) => {
        if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) return
        // Don't decrypt already decrypted items
        if (itemState.overview !== null) return

        promises.push(
          deferredDecryptItem(itemState.item, authStore.keyPair, authStore.rsaCryptoKeyPair)
            .then(({ overview }) => {
              itemState.overview = overview
            })
            .catch((e) => {
              logger.error("Couldn't decrypt overview for %s %O", itemState.item.id, e)
              Sentry.captureException(e, { extra: { item_id: itemState.item.id } })
              itemStateMap.value.delete(itemState.item.id)
            })
        )
      })

      await Promise.all(promises)

      isDecrypting.value = false
      logger.debug('Done decrypting overviews')
    }

    const migrateLegacyItems = () => {
      // This was written with promise chaining, because await can block at unexpected times
      logger.debug('Migrating legacy items')
      const legacyItems = itemStates.value.filter(
        ({ item }) =>
          item.encryption_version === 1 || (!item.encrypted_dek && !item.encrypted_folder_dek)
      )
      logger.debug('Migrating legacy items, total: %o', legacyItems.length)

      const promises = legacyItems.map(({ item }) => {
        if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) return
        logger.debug('Migrating item: %o', item.id)

        return deferredDecryptItem(item, authStore.keyPair, authStore.rsaCryptoKeyPair, true)
          .then((decrypted) => {
            if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) {
              throw new Error('No keypair')
            }
            return encryptItem(
              item,
              decrypted.overview,
              decrypted.details,
              authStore.keyPair,
              authStore.rsaCryptoKeyPair
            )
          })
          .then((reencryptedItem) => {
            logger.debug('Reencrypted item %O', reencryptedItem)
            return updateItemOnServer(item.id, reencryptedItem)
          })
          .then(({ data: { item: newItem } }) => {
            if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) {
              throw new Error('No keypair')
            }

            return deferredDecryptItem(newItem, authStore.keyPair, authStore.rsaCryptoKeyPair).then(
              (newlyDecrypted) => {
                itemStateMap.value.set(newItem.id, {
                  item: newItem,
                  overview: newlyDecrypted.overview,
                })
              }
            )
          })
          .catch((e) => {
            logger.error('Could not migrate %o %O', item.id, e)
            Sentry.captureException(e, { extra: { item_id: item.id } })
          })
      })

      Promise.all(promises).then(() => logger.debug('Migrated legacy accounts'))
    }

    const updateItemEventHook = createEventHook<{
      updatedIds: string[]
      deletedIds: string[]
    }>()
    const onUpdate = updateItemEventHook.on
    const unsubscribeOnUpdate = updateItemEventHook.off

    /**
     * Fetch all items from the server and decrypt the overviews
     *
     * There are multiple components and places that may call fetchItems
     * at the same time. To prefent double executions, we wrap the function
     * with the `singleCall` helper.
     */
    const fetchItems = singleCall(async () => {
      logger.debug('Loading items')
      isLoading.value = true

      try {
        logger.debug('Fetching items from server')
        lastPollingUpdate.value = new Date().toISOString()
        const {
          data: { items },
        } = await getItems()
        logger.debug('Done fetching items from server')
        await updateEncryptedItems(items)
        await decryptItemOverviews()
        // Allow some time before scheduling migrations so UI can do everything it needs to do
        setTimeout(migrateLegacyItems, 2000)
      } finally {
        logger.debug('Done loading items')
        isLoading.value = false
        isLoaded.value = true
      }
    })

    /**
     * Incrementally update items modified on the server
     */
    const updateItemsFromPolling = async () => {
      logger.debug('Polling for updates')
      try {
        // Also refresh folders
        await foldersStore.fetchFolders()
        const { data } = await pollItems(lastPollingUpdate.value)
        lastPollingUpdate.value = data.fetched_at

        if (data.updates.length || data.deletions.length) {
          // Update
          await updateEncryptedItems(data.updates, data.deletions, true)
          await decryptItemOverviews()

          // Trigger downstream changes
          updateItemEventHook.trigger({
            updatedIds: data.updates.map((item) => item.id),
            deletedIds: data.deletions,
          })
        }

        logger.debug(
          'Done polling (%d updates, %d deletions, at %s)',
          data.updates.length,
          data.deletions.length,
          data.fetched_at
        )
      } catch (e) {
        logger.error('Error during polling %O', e)
      }
    }

    // Poll for changes every POLLING_INTERVAL
    watchEffect(() => {
      if (authStore.isAuthenticated && !disablePolling.value && !pollingIntervalHandle.value) {
        logger.debug('Enabling polling')
        pollingIntervalHandle.value = setInterval(updateItemsFromPolling, POLLING_INTERVAL)
      } else if (
        (!authStore.isAuthenticated || disablePolling.value) &&
        pollingIntervalHandle.value
      ) {
        logger.debug('Disabling polling')
        clearInterval(pollingIntervalHandle.value)
      }
    })

    /**
     * Get an unencrypted item, including details
     * @param id UUID of item
     */
    const getItemById = async (id: string) => {
      if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) {
        logger.debug('Unathenticated - Unable to get item')
        return null
      }

      const itemState = itemStateMap.value.get(id)
      if (!itemState) {
        logger.debug('Item not found: ', id)
        return null
      }

      try {
        const { overview, details, share } = await deferredDecryptItem(
          itemState.item,
          authStore.keyPair,
          authStore.rsaCryptoKeyPair,
          true
        )

        const fullItem: DecryptedItem = {
          item: { ...itemState.item },
          overview,
          details,
          share,
        }

        return fullItem
      } catch (e) {
        logger.error('Error decrypting item %O', e)
        Sentry.captureException(e, { extra: { item_id: itemState.item.id } })
        return null
      }
    }

    const createItem = async (initial?: DecryptedItem) => {
      if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) return null

      const {
        item: initialItem,
        overview: initialOverview,
        details: initialDetails,
      } = initial || initializeItem()

      // TODO: Clean up legacy stuff
      initialItem.legacy_encryption_disabled =
        authStore.tiligUser?.applicationSettings?.legacy_encryption_disabled || false

      const encryptedItem = await encryptItem(
        initialItem,
        initialOverview,
        initialDetails,
        authStore.keyPair,
        authStore.rsaCryptoKeyPair
      )

      const {
        data: { item },
      } = await createItemOnServer(encryptedItem)
      const { overview } = await decryptItem(item, authStore.keyPair, authStore.rsaCryptoKeyPair)

      const itemState = {
        item,
        overview,
      }

      itemStateMap.value.set(item.id, itemState)

      return itemState
    }

    const refreshItem = async (id: string) => {
      if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) return false

      const {
        data: { item },
      } = await getItem(id)
      if (!item) {
        // TODO: Maybe we should not delete client side
        itemStateMap.value.delete(id)
        return false
      }

      const { overview } = await decryptItem(item, authStore.keyPair, authStore.rsaCryptoKeyPair)

      itemStateMap.value.set(item.id, {
        item,
        overview,
      })

      return true
    }

    const updateItem = async (
      id: string,
      overview: DecryptedOverview,
      details: DecryptedDetails,
      folder?: Folder | null
    ) => {
      if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) return false
      await refreshItem(id)
      const itemState = itemStateMap.value.get(id)
      if (!itemState) return false

      logger.debug('Updated overview %O', overview)
      logger.debug('Updated details %O', details)
      logger.debug('Updated folder', folder)

      const options: EncryptItemOptions = {}

      let item = itemState.item

      if (folder !== undefined && folder !== item.folder) {
        logger.debug('Folder changed')
        // We need to replace the folder and rotate the DEK
        options.originalItem = item
        options.rotateDek = true
        // Create copy of original item we can modify
        item = cloneDeep(item)
        item.folder = folder
      }

      const updatedItem = await encryptItem(
        item,
        overview,
        details,
        authStore.keyPair,
        authStore.rsaCryptoKeyPair,
        options
      )

      logger.debug('Updated item %O', updatedItem)

      try {
        const {
          data: { item },
        } = await updateItemOnServer(id, updatedItem)
        const { overview } = await decryptItem(item, authStore.keyPair, authStore.rsaCryptoKeyPair)

        itemStateMap.value.set(item.id, {
          item,
          overview,
        })
      } catch (e) {
        logger.error('Error updating item %O', e)
        return false
      }

      return true
    }

    const deleteItem = async (id: string) => {
      try {
        await deleteItemOnServer(id)
        itemStateMap.value.delete(id)
        return true
      } catch (e) {
        logger.error('Could not delete item: %O', e)
        return false
      }
    }

    const createShare = async (item: EncryptedItem) => {
      if (!authStore.keyPair || !authStore.rsaCryptoKeyPair) return null

      logger.debug('Creating share')

      if (!item.encrypted_folder_dek && !item.encrypted_dek) {
        logger.debug('No encrypted_dek, migrating item')
        // Force the item to be a v2 item with an encrypted_dek
        const { overview, details } = await decryptItem(
          item,
          authStore.keyPair,
          authStore.rsaCryptoKeyPair,
          true
        )
        await updateItem(item.id, overview, details)
        const newItem = itemStateMap.value.get(item.id)
        if (newItem) {
          item = newItem.item
        }
      }

      const shareData = generateShareData(item, authStore.keyPair)

      try {
        const result = await createShareOnServer(item.id, shareData)
        logger.debug('Created share')
        return result.data
      } catch (e) {
        logger.error('Error creating share %O', e)
        return null
      }
    }

    const deleteShare = async (id: string) => {
      logger.debug('Deleting share')
      try {
        await deleteShareOnServer(id)
        logger.debug('Deleted share')
        return true
      } catch (e) {
        logger.error('Could not delete share %O', e)
        return false
      }
    }

    const convertToFolder = async (item: ItemState, folderMember: FolderMemberAttributes) => {
      logger.debug('Converting item to folder', item.item, folderMember)

      if (!item) throw new Error('Item does not exist')
      if (!authStore.keyPair) throw new Error('Keypair does not exist')
      if (!authStore.tiligUser) throw new Error('Tilig user is missing')

      // get folder data
      logger.debug('Generating folder data from item', item.item.id, folderMember)

      const folderMemberPublicKey = 'public_key' in folderMember ? folderMember.public_key : null

      const folderData = generateFolderDataFromItem(
        item.item,
        authStore.keyPair,
        folderMemberPublicKey
      )

      const shareLinkData = folderData.encryptedMasterSecret
        ? { share_link: { encrypted_master_key: folderData.encryptedMasterSecret } }
        : null

      const folder: Required<FolderCreationAttributes> = {
        name: item.overview?.name || 'Folder',
        public_key: folderData.folderPublicKey,
        single_item: true,
        folder_memberships: [
          {
            encrypted_private_key: folderData.ownEncryptedPrivateKey,
            user_id: authStore.tiligUser.id,
          },
        ],
        items: [
          {
            encrypted_folder_dek: folderData.encryptedFolderDek,
            id: item.item.id,
            ...shareLinkData,
          },
        ],
      }

      if ('public_key' in folderMember && folderData.folderMemberEncryptedPrivateKey) {
        folder.folder_memberships.push({
          user_id: folderMember.id,
          encrypted_private_key: folderData.folderMemberEncryptedPrivateKey,
        })
      } else {
        folder.folder_memberships.push({
          email: folderMember.email,
        })
      }

      try {
        await foldersStore.createFolder(folder)
        return true
      } catch (e) {
        logger.error('Error creating folder %O', e)
        return false
      }
    }

    const addFolderMembership = async (itemId: string, folderMember: FolderMemberAttributes) => {
      // We want to check if the item has been upgraded to a folder
      // If it's not a folder, then we call the upgrade to folder function
      const item = itemStateMap.value.get(itemId)
      if (!item) throw new Error('Item does not exist')

      const folder = item.item.folder

      if (folder) {
        logger.debug('Adding member to folder', itemId, folderMember)
        return foldersStore.addFolderMember(folder, folderMember)
      }

      logger.debug('Converting to folder', itemId, folderMember)
      return convertToFolder(item, folderMember)
    }

    return {
      query,
      activeItemType,
      activeFolderId,
      activeFolder,

      disablePolling,

      isLoaded,
      isLoading,
      isDecrypting,

      // Note: This ref is only exposed so pinia-plugin-persistedstate can hydrate it
      // Do not use it in your code.
      itemStateMap,

      itemStates,
      numberOfItems,
      numberOfItemsForActiveItemType,
      numberOfItemsForActiveFolder,
      filteredItems,
      hasCustomItems,

      // Length migration fields
      allowUnlimitedLoginValues,
      maxLoginValueLength,

      // Subscribe to events
      onUpdate,
      unsubscribeOnUpdate,

      fetchItems,
      updateItemsFromPolling,
      getItemById,
      refreshItem,
      createItem,
      updateItem,
      deleteItem,

      createShare,
      deleteShare,

      // Folder helpers
      addFolderMembership,

      $reset,
    }
  },
  {
    persist: {
      serializer: {
        serialize,
        deserialize,
      },
    },
  }
)

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useItemsStore, import.meta.hot))
}
