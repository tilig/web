import { ref, computed } from 'vue'
import { acceptHMRUpdate, defineStore } from 'pinia'

import { getReferralLeaderboard, LeaderBoard } from '@core/clients/tilig'
import { createLogger } from '@core/utils/logger'

const logger = createLogger('stores:referral')

export const useReferralStore = defineStore('referral', () => {
  const isFetchingLeaderBoard = ref(false)
  const leaderboard = ref<LeaderBoard | null>(null)

  const leaderboardWithMe = computed(() => {
    const topList = (leaderboard.value?.top_list ?? []).slice(0, 3)
    const me = leaderboard.value?.me

    const inTopList = topList.some(
      (ranking) => ranking.email_short === leaderboard.value?.me.email_short
    )

    if (!inTopList) {
      const myRank = topList.length + 1
      me && topList.push({ ...me, rank: myRank })
    }

    return topList
  })

  const fetchReferralLeaderboard = async () => {
    logger.info('Fetching referral campaign leaderboard')
    isFetchingLeaderBoard.value = true
    try {
      const {
        data: { referral_leaderboard },
      } = await getReferralLeaderboard()

      logger.info('Fetched referral campaign leaderboard', referral_leaderboard)

      leaderboard.value = referral_leaderboard
    } catch (error) {
      logger.error('Failed to fetch referral campaign leaderboard. ', error)
    } finally {
      isFetchingLeaderBoard.value = false
    }
  }

  return {
    isFetchingLeaderBoard,
    leaderboardWithMe,
    leaderboard,

    fetchReferralLeaderboard,
  }
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useReferralStore, import.meta.hot))
}
