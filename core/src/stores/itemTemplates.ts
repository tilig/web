// import { computed, ref } from 'vue'
// import { acceptHMRUpdate, defineStore } from 'pinia'

// import {
//   createItemTemplate as createItemTemplateOnServer,
//   deleteItemTemplate as deleteItemTemplateOnServer,
//   getItemTemplates as getItemTemplatesFromServer,
//   ItemTemplate,
// } from '@core/clients/tilig'
// import { useAuthStore } from './auth'
// import { createLogger } from '@core/utils/logger'
// import { sortBy } from 'lodash'

// const logger = createLogger('stores:itemTemplates')

// export const useItemTemplatesStore = defineStore('itemTemplates', () => {
//   const authStore = useAuthStore()
//   const isFetched = ref(false)
//   const itemTemplatesMap = ref<Map<string, ItemTemplate>>(new Map())
//   const itemTemplates = computed(() =>
//     sortBy(Array.from(itemTemplatesMap.value.values()), ({ name }) => name)
//   )

//   const fetchItemTemplates = async () => {
//     if (!authStore.isAuthenticated) return

//     try {
//       const {
//         data: { item_templates: newItemTemplates },
//       } = await getItemTemplatesFromServer()
//       const newItemTemplatesMap = new Map<string, ItemTemplate>()

//       newItemTemplates.forEach((itemTemplate) => {
//         newItemTemplatesMap.set(itemTemplate.id, itemTemplate)
//       })

//       itemTemplatesMap.value = newItemTemplatesMap
//       isFetched.value = true
//       return true
//     } catch (e) {
//       logger.error('Error fetching item templates %O', e)
//       return false
//     }
//   }

//   const createItemTemplate = async (itemTemplate: Partial<ItemTemplate>) => {
//     try {
//       const {
//         data: { item_template: newItemTemplate },
//       } = await createItemTemplateOnServer(itemTemplate)
//       itemTemplatesMap.value.set(newItemTemplate.id, newItemTemplate)
//       return true
//     } catch (e) {
//       logger.error('Error creating itemTemplate %O', e)
//       return false
//     }
//   }

//   const deleteItemTemplate = async (id: string) => {
//     try {
//       await deleteItemTemplateOnServer(id)
//       itemTemplatesMap.value.delete(id)
//       return true
//     } catch (e) {
//       logger.error('Error deleting itemTemplate %O', e)
//       return false
//     }
//   }

//   return {
//     isFetched,
//     itemTemplates,
//     fetchItemTemplates,
//     createItemTemplate,
//     deleteItemTemplate,
//   }
// })

// if (import.meta.hot) {
//   import.meta.hot.accept(acceptHMRUpdate(useItemTemplatesStore, import.meta.hot))
// }

export {}
