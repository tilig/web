import { createApp, reactive } from 'vue'
import { beforeAll, beforeEach, describe, expect, it, vi } from 'vitest'
import { createPinia } from 'pinia'

import { useFlagsStore } from '../flags'
import { UserProfile } from '@firebase/auth'
import { flushPromises } from '@core/utils/tests'

vi.mock('unleash-proxy-client', () => ({
  UnleashClient: vi.fn(() => mockClient),
}))

vi.mock('../tracking', () => ({
  useTrackingStore: vi.fn(() => mockTrackingStore),
}))

vi.mock('../auth', () => ({
  useAuthStore: vi.fn(() => mockAuthStore),
}))

const mockClient = {
  on: vi.fn(),
  stop: vi.fn(),
  start: vi.fn(),
  isEnabled: vi.fn(),
  updateContext: vi.fn(),
}

const mockTrackingStore = {
  trackPerson: vi.fn(() => Promise.resolve(null)),
}

const mockAuthStore = reactive({
  tiligUser: null as UserProfile | null,
})

describe('Flags store', () => {
  beforeAll(() => {
    const app = createApp({})
    const pinia = createPinia()
    app.use(pinia)
  })

  describe('flags checking', () => {
    it('correcly reflects the value of a disabled flag', () => {
      const flag = 'my-flag'
      mockClient.isEnabled.mockImplementationOnce(() => false)

      const flagsStore = useFlagsStore()
      expect(flagsStore.isEnabled(flag)).toBe(false)
    })

    it('correcly reflects the value of an enabled flag', () => {
      const flag = 'my-flag'
      mockClient.isEnabled.mockImplementationOnce(() => true)

      const flagsStore = useFlagsStore()
      expect(flagsStore.isEnabled(flag)).toBe(true)
    })
  })

  describe('tracking', () => {
    beforeEach(() => {
      mockTrackingStore.trackPerson.mockClear()
    })

    it('tracks flag settings by default', () => {
      const flagsStore = useFlagsStore()
      flagsStore.isEnabled('a_flag')
      expect(mockTrackingStore.trackPerson).toHaveBeenCalled()
    })

    it('tracks the flag setting if asked', () => {
      mockClient.isEnabled.mockImplementationOnce(() => true)
      const flagsStore = useFlagsStore()
      flagsStore.isEnabled('a_flag', { track: true })
      expect(mockTrackingStore.trackPerson).toHaveBeenCalledWith({ a_flag: 1 })
    })

    it('does not track the flag setting if disabled', () => {
      mockClient.isEnabled.mockImplementationOnce(() => true)
      const flagsStore = useFlagsStore()
      flagsStore.isEnabled('a_flag', { track: false })
      expect(mockTrackingStore.trackPerson).not.toHaveBeenCalled()
    })

    it('track with a different event', () => {
      mockClient.isEnabled.mockImplementationOnce(() => false)
      const flagsStore = useFlagsStore()
      flagsStore.isEnabled('a_flag', { track: 'other_name' })
      expect(mockTrackingStore.trackPerson).toHaveBeenCalledWith({ other_name: 0 })
    })
  })

  describe('context', () => {
    beforeEach(() => {
      mockClient.updateContext.mockClear()
    })

    it('updates the context when the tilig user changes', async () => {
      useFlagsStore()
      expect(mockClient.updateContext).not.toHaveBeenCalled()
      mockAuthStore.tiligUser = { id: 'my-id' }
      await flushPromises()
      expect(mockClient.updateContext).toHaveBeenCalledWith({ userId: 'my-id' })
    })
  })
})
