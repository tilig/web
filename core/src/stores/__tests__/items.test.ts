/* eslint-disable vue/one-component-per-file */
import { afterAll, beforeAll, beforeEach, describe, expect, it, vi } from 'vitest'
import { cloneDeep } from 'lodash'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

import { encryptedItems } from './__fixtures__/items.fixtures'
import { useItemsStore, setCacheVersionKey, ItemState } from '../items'
import localStorageFixture from './__fixtures__/items_persisted_state.json'

import {
  EncryptedItem,
  getItems,
  deferredDecryptItem,
  encryptItem,
  Template,
  DecryptedOverview,
  DecryptedDetails,
} from '@core/clients/tilig'
import { flushPromises } from '@core/utils/tests'
import { useAuthStore } from '../auth'
import { importLegacyKeyPair } from '@core/clients/tilig/legacyCrypto'

const mockEncryptedItems = () => cloneDeep(encryptedItems) as EncryptedItem[]

vi.mock('../auth')

vi.mock('@core/clients/tilig', async () => {
  const client = await vi.importActual<Record<string, unknown>>('@core/clients/tilig')
  return {
    ...client,
    getItems: vi.fn(() => {
      const items = mockEncryptedItems()
      return Promise.resolve({ data: { items } })
    }),
    pollItems: vi.fn(() => {
      const updates = mockEncryptedItems()
      return Promise.resolve({
        data: {
          updates,
          deletions: [],
          fetched_at: new Date().toISOString(),
        },
      })
    }),
    createItem: vi.fn(() => {
      const items = mockEncryptedItems()
      return Promise.resolve({ data: { item: items[0] } })
    }),
    encryptItem: vi.fn(() => {
      const items = mockEncryptedItems()
      return Promise.resolve(items[0])
    }),
    decryptItem: vi.fn(() => {
      return Promise.resolve({
        overview: {
          name: 'Mock item',
          info: null,
          urls: [],
          android_app_ids: [],
        },
      })
    }),
    deferredDecryptItem: vi.fn(() => {
      return Promise.resolve({
        overview: {
          name: 'Mock item',
          info: null,
          urls: [],
          android_app_ids: [],
        },
      })
    }),
  }
})

describe('Items store', () => {
  beforeAll(async () => {
    const authStore = useAuthStore()
    // Populate RSA CryptoKeyPair
    if (authStore.legacyKeyPair) {
      authStore.rsaCryptoKeyPair = await importLegacyKeyPair(authStore.legacyKeyPair)
    }
  })

  beforeEach(() => {
    const app = createApp({})
    const pinia = createPinia()
    app.use(pinia)
    vi.mocked(getItems).mockClear()
    vi.mocked(deferredDecryptItem).mockClear()
  })

  describe('fetching items', () => {
    it('fetching the items for a user', async () => {
      const itemsStore = useItemsStore()

      await itemsStore.fetchItems()
      expect(vi.mocked(getItems)).toHaveBeenCalledOnce()
      expect(itemsStore.itemStates.length).toEqual(7)
      // Ensure that we decrypted all the expected overviews
      expect(deferredDecryptItem).toHaveBeenCalledTimes(7)
    })

    it('reuses already decrypted items', async () => {
      const itemsStore = useItemsStore()
      await itemsStore.fetchItems()
      expect(deferredDecryptItem).toHaveBeenCalledTimes(7)
      await itemsStore.fetchItems()
      expect(deferredDecryptItem).toHaveBeenCalledTimes(7)
    })

    it('decrypts overviews only on changes to encrypted item', async () => {
      const itemsStore = useItemsStore()
      await itemsStore.fetchItems()
      expect(deferredDecryptItem).toHaveBeenCalledTimes(7)

      //Return a new set of encrypted items with one updated
      const updatedEncryptedItems = mockEncryptedItems()
      updatedEncryptedItems[0].updated_at = new Date().toISOString()
      vi.mocked(getItems).mockResolvedValueOnce({
        data: { items: updatedEncryptedItems },
      } as any)
      await itemsStore.fetchItems()
      expect(deferredDecryptItem).toHaveBeenCalledTimes(8)
    })

    it('removes deleted items from the state', async () => {
      const itemsStore = useItemsStore()
      itemsStore.itemStateMap = new Map([
        [
          'NON_EXISTING',
          { item: { mocked: true }, overview: { mocked: true } } as unknown as ItemState,
        ],
      ])
      expect(itemsStore.itemStates.length).toEqual(1)
      await itemsStore.fetchItems()
      expect(itemsStore.itemStates.length).toEqual(7)
    })
  })

  describe('polling updates', () => {
    it('updates the items in place', async () => {
      const itemsStore = useItemsStore()
      itemsStore.itemStateMap = new Map([
        [
          'ALREADY_DECRYPTED',
          { item: { mocked: true }, overview: { mocked: true } } as unknown as ItemState,
        ],
      ])
      expect(itemsStore.itemStates.length).toEqual(1)
      await itemsStore.updateItemsFromPolling()
      expect(itemsStore.itemStates.length).toEqual(8)
      expect(deferredDecryptItem).toHaveBeenCalledTimes(7)
    })
  })

  describe('persistance', () => {
    const CACHE_VERSION_KEY = 'v1.2.3'

    beforeEach(async () => {
      const app = createApp({})
      const pinia = createPinia()
      app.use(pinia)
      pinia.use(piniaPluginPersistedstate)
      await flushPromises()
      setCacheVersionKey(CACHE_VERSION_KEY)
    })

    it('items should rehydrate from localstorage', async () => {
      const serializedItems = cloneDeep(localStorageFixture) as any
      serializedItems['version'] = CACHE_VERSION_KEY
      localStorage.setItem('items', JSON.stringify(serializedItems))

      const itemsStore = useItemsStore()
      expect(itemsStore.itemStates.length).toEqual(7)
      await itemsStore.fetchItems()
      expect(deferredDecryptItem).toHaveBeenCalledTimes(0)
    })

    it('items should rehydrate from localstorage and refetch should only decrypt updated items', async () => {
      const serializedItems = cloneDeep(localStorageFixture) as any
      serializedItems['version'] = CACHE_VERSION_KEY
      serializedItems['itemState'][0][1]['item'].updated_at = new Date(1234567890).toISOString()

      localStorage.setItem('items', JSON.stringify(serializedItems))

      const itemsStore = useItemsStore()
      expect(itemsStore.itemStates.length).toEqual(7)
      await itemsStore.fetchItems()
      expect(deferredDecryptItem).toHaveBeenCalledTimes(1)
    })

    it('items should not rehydrate from localstorage is the version changes', async () => {
      const serializedItems = cloneDeep(localStorageFixture) as any
      serializedItems['version'] = 'v999.999.999'
      localStorage.setItem('items', JSON.stringify(serializedItems))

      const itemsStore = useItemsStore()
      expect(itemsStore.itemStates.length).toEqual(0)
    })

    it('clears items on sign out', async () => {
      const serializedItems = cloneDeep(localStorageFixture) as any
      serializedItems['version'] = CACHE_VERSION_KEY
      localStorage.setItem('items', JSON.stringify(serializedItems))

      const itemsStore = useItemsStore()
      const authStore = useAuthStore()
      expect(itemsStore.itemStates.length).toEqual(7)

      authStore.signOut()
      expect(itemsStore.itemStates.length).toEqual(0)
      await flushPromises()
      expect(JSON.parse(localStorage.getItem('items') || '{}')['itemState']).toEqual([])
    })
  })

  describe('item creation', () => {
    beforeAll(() => {
      vi.mocked(encryptItem).mockClear()
      vi.setSystemTime(new Date('2023-01-01T00:00:00.000Z'))
    })

    afterAll(() => {
      vi.useRealTimers()
    })

    const emptyItem: EncryptedItem = {
      android_app_id: null,
      brand: null,
      created_at: '2023-01-01T00:00:00.000Z',
      encrypted_dek: null,
      encrypted_details: '',
      encrypted_folder_dek: null,
      encrypted_overview: '',
      encryption_version: 2,
      folder: null,
      id: '',
      legacy_encryption_disabled: true,
      name: null,
      notes: null,
      otp: null,
      password: null,
      rsa_encrypted_dek: null,
      share_link: null,
      template: 'login/1' as Template,
      updated_at: '2023-01-01T00:00:00.000Z',
      username: null,
      website: null,
    }

    const emptyOverview: DecryptedOverview = {
      android_app_ids: [],
      info: null,
      name: null,
      urls: [],
    }

    const emptyDetails: DecryptedDetails = {
      custom_fields: [],
      history: [],
      main: [],
      notes: null,
    }

    it.each([true, false])(
      'copies the legacy setting %s from user to a new item',
      async (setting) => {
        const authStore = useAuthStore()
        const itemsStore = useItemsStore()

        if (!authStore.tiligUser) {
          throw new Error('No user')
        }

        const originalLegacySetting =
          authStore.tiligUser.applicationSettings.legacy_encryption_disabled
        authStore.tiligUser.applicationSettings.legacy_encryption_disabled = setting

        await itemsStore.createItem()
        expect(vi.mocked(encryptItem)).toHaveBeenCalledWith(
          { ...emptyItem, legacy_encryption_disabled: setting },
          emptyOverview,
          emptyDetails,
          authStore.keyPair,
          authStore.rsaCryptoKeyPair
        )

        authStore.tiligUser.applicationSettings.legacy_encryption_disabled = originalLegacySetting
      }
    )
  })
})
