import { describe, expect, test } from 'vitest'

import { filterItems, ItemState } from '../../items'
import itemsMock from './itemsMock.json'

const items = [...itemsMock] as unknown as ItemState[]

describe('Filter Items', () => {
  test(`It returns the full list if the query is empty`, () => {
    const filteredItems = filterItems(items, { query: '' })

    expect(filteredItems.length).toBe(items.length)
  })

  test('It matches item urls without taking the schema into consideration', () => {
    const filteredItems = filterItems(items, { query: 's' })

    expect(filteredItems.length).toBe(1)
  })

  test('It includes notes items in the search results', () => {
    const filteredItems = filterItems(items, { query: 'note' })

    expect(filteredItems.length).toBe(2)
  })
})
