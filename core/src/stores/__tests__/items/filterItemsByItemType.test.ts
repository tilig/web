import { Template } from '@core/clients/tilig/item'
import { describe, expect, test } from 'vitest'

import { filterItemsByItemType, ItemState, ItemType } from '../../items'

import itemsMock from './itemsMock.json'

const items = [...itemsMock] as unknown as ItemState[]

describe('Filter Items', () => {
  test(`It returns only the type of items passed`, () => {
    const filteredItems = filterItemsByItemType(items, ItemType.Logins)

    const allItemsAreLogins = filteredItems.every(
      (itemState) => itemState.item.template === Template.Login
    )

    expect(allItemsAreLogins).toBeTruthy()
  })

  test('It returns all items if the item type passed is null', () => {
    const filteredItems = filterItemsByItemType(items, null)

    expect(filteredItems.length).toBe(items.length)
  })
})
