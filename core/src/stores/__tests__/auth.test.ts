import {
  NextOrObserver,
  User,
  getRedirectResult,
  getAdditionalUserInfo,
  AdditionalUserInfo,
  UserCredential,
  sendEmailVerification,
  updateCurrentUser,
} from 'firebase/auth'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import { beforeEach, describe, expect, it, vi } from 'vitest'

import {
  getAccessToken,
  getKeyPairs,
  refreshAccessToken,
  updateUserProfile,
} from '@core/clients/tilig'
import { flushPromises } from '@core/utils/tests'
import { keyPair, legacyKeyPair } from './__fixtures__/auth.fixtures'

import { AuthState, useAuthStore } from '../auth'
import { FirebaseTiligClient, getFirebaseClient } from '@core/clients/firebase'
import { AxiosError } from 'axios'

const mockFirebaseClient = {
  auth: {
    currentUser: null,
    onAuthStateChanged: vi.fn(),
    signOut: vi.fn(),
  },
}

vi.mock('@core/clients/firebase', () => {
  return {
    getFirebaseClient: vi.fn(() => mockFirebaseClient),
  }
})

vi.mock('@core/clients/tilig', () => {
  return {
    getAccessToken: vi.fn(() => Promise.resolve({ data: null })),
    refreshAccessToken: vi.fn(() => Promise.resolve({ data: null })),
    getKeyPairs: vi.fn(() => Promise.resolve({ keyPair, legacyKeyPair })),
    updateUserProfile: vi.fn(() => Promise.resolve({ profile: null, keyPairData: null })),
    getUserProfile: vi.fn(() => Promise.resolve({ profile: null, keyPairData: null })),
  }
})

vi.mock('firebase/auth', () => {
  return {
    getRedirectResult: vi.fn(() => Promise.resolve(null)),
    getAdditionalUserInfo: vi.fn(() => {
      return {} as AdditionalUserInfo
    }),
    sendEmailVerification: vi.fn(() => Promise.resolve(null)),
    updateCurrentUser: vi.fn(() => Promise.resolve(null)),
  }
})

vi.mock('@firebase/auth/internal', () => {
  return {
    UserImpl: {
      _fromJSON: vi.fn(() => null),
    },
  }
})

const settleBeforeAndAfter = async (cb: () => void) => {
  await flushPromises()
  cb()
  await flushPromises()
}

describe('Auth store', () => {
  beforeEach(() => {
    const app = createApp({})
    const pinia = createPinia()
    pinia.use(piniaPluginPersistedstate)
    app.use(pinia)
  })

  it('set up event handlers on the auth instance', () => {
    useAuthStore()
    expect(vi.mocked(mockFirebaseClient.auth.onAuthStateChanged)).toHaveBeenCalledOnce()
  })

  it('resolves the ready promise if it is initialized', async () => {
    const authStore = useAuthStore()
    expect(authStore.initialized).toBeFalsy()

    let ready = false

    authStore.ready.then(() => (ready = true))
    authStore.initialized = true

    await flushPromises()
    expect(ready).toBe(true)
  })

  describe('auth callbacks', () => {
    let userCallback: NextOrObserver<User | null> | undefined

    beforeEach(() => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      vi.mocked(mockFirebaseClient.auth.onAuthStateChanged).mockImplementation((u: any) => {
        userCallback = u
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        return () => {}
      })

      userCallback = undefined
      vi.clearAllMocks()
    })

    it('registeres the user and error callbacks', () => {
      useAuthStore()
      expect(userCallback).toBeTypeOf('function')
    })

    it('sets the user through the auth callbacks', () => {
      const authStore = useAuthStore()
      const fakeUser = { displayName: 'User', emailVerified: true, toJSON: () => 'FAKE' }

      if (typeof userCallback === 'function') {
        userCallback(fakeUser as unknown as User)
      } else {
        throw new Error('User callback not registered')
      }

      expect(authStore.hasFirebaseUser).toBe(true)
      expect(authStore.authenticationError).toBeNull()
    })

    it('should throw an error when email is unverified', () => {
      const authStore = useAuthStore()
      expect(userCallback).toBeTypeOf('function')

      const fakeUser = {
        displayName: 'User',
        emailVerified: false,
        email: 'johndoe@example.com',
        toJSON: () => 'FAKE',
      }

      if (typeof userCallback === 'function') {
        userCallback(fakeUser as unknown as User)
      }

      expect(authStore.hasFirebaseUser).toBe(false)
      expect(authStore.authenticationError?.message).toBe(
        `Before you can sign in, you'll need to verify your email address. Please check the inbox or the junk email folder of ${fakeUser.email} and follow the instructions.`
      )
    })

    it('should send a verification email to the user when email is unverified', () => {
      const authStore = useAuthStore()

      expect(userCallback).toBeTypeOf('function')

      const fakeUser = { displayName: 'User', emailVerified: false, toJSON: () => 'FAKE' }

      if (typeof userCallback === 'function') {
        userCallback(fakeUser as unknown as User)
      }

      expect(authStore.hasFirebaseUser).toBe(false)
      expect(sendEmailVerification).toHaveBeenCalledOnce()
    })

    it('should send not send a verification email to the user when email is verified', () => {
      const authStore = useAuthStore()

      expect(userCallback).toBeTypeOf('function')

      const fakeUser = { displayName: 'User', emailVerified: true, toJSON: () => 'FAKE' }

      if (typeof userCallback === 'function') {
        userCallback(fakeUser as unknown as User)
      }
      expect(authStore.hasFirebaseUser).toBe(true)
      expect(sendEmailVerification).toHaveBeenCalledTimes(0)
    })
  })

  describe('accessToken handling', () => {
    beforeEach(() => {
      vi.mocked(getAccessToken).mockClear()
      vi.mocked(refreshAccessToken).mockClear()
    })

    it('does not try to fetch accessTokens without a user', () => {
      const mock = vi.mocked(getAccessToken)
      const authStore = useAuthStore()
      expect(authStore.hasFirebaseUser).toBe(false)
      expect(mock).not.toHaveBeenCalled()
    })

    it('tries to fetch the accessTokens if the user is set', async () => {
      const mock = vi.mocked(getAccessToken)
      const authStore = useAuthStore()
      await settleBeforeAndAfter(() => (authStore.hasFirebaseUser = true))
      expect(mock).toHaveBeenCalledOnce()
    })

    it("does not try to fetch accessToken if it's already in localStorage", async () => {
      localStorage.setItem(
        'auth',
        JSON.stringify({
          accessToken: { accessToken: 'localStorageTest', refreshToken: 'localStorageTest' },
        })
      )

      const mock = vi.mocked(getAccessToken)
      const authStore = useAuthStore()

      await settleBeforeAndAfter(() => (authStore.hasFirebaseUser = true))

      expect(mock).not.toHaveBeenCalled()
      expect(authStore.accessToken).toEqual({
        accessToken: 'localStorageTest',
        refreshToken: 'localStorageTest',
      })
    })

    it('does not refresh access tokens multiple times before resolving', async () => {
      const authStore = useAuthStore()

      authStore.$patch({
        hasFirebaseUser: true,
        accessToken: {
          accessToken: 'old-token',
          refreshToken: 'old-token',
        },
      })

      authStore.refreshAccessToken()
      authStore.refreshAccessToken()
      authStore.refreshAccessToken()
      await flushPromises()

      expect(vi.mocked(refreshAccessToken)).toHaveBeenCalledOnce()
    })

    it('refreshes access tokens again after resolving', async () => {
      const authStore = useAuthStore()

      authStore.$patch({
        hasFirebaseUser: true,
        accessToken: {
          accessToken: 'old-token',
          refreshToken: 'old-token',
        },
      })

      vi.mocked(refreshAccessToken).mockResolvedValue({
        data: {
          accessToken: 'new-token',
          refreshToken: 'new-token',
        },
      } as any)

      authStore.refreshAccessToken()
      await flushPromises()
      authStore.refreshAccessToken()
      await flushPromises()

      expect(vi.mocked(refreshAccessToken)).toHaveBeenCalledTimes(2)
    })

    describe('refresh failure', () => {
      it('restarts the API session on a refresh failure', async () => {
        vi.mocked(refreshAccessToken).mockRejectedValue({
          isAxiosError: true,
          response: { status: 401 },
        } as unknown as AxiosError)

        const authStore = useAuthStore()

        authStore.$patch({
          hasFirebaseUser: true,
          accessToken: {
            accessToken: 'old-token',
            refreshToken: 'old-token',
          },
        })

        let reset = false
        const { off } = authStore.onSignOut(() => (reset = true))
        const accessToken = await authStore.refreshAccessToken()
        await flushPromises()
        off()
        expect(reset).toBe(true)
        expect(accessToken).toBeNull()
      })

      it('does not restarts the API session on any other failure', async () => {
        vi.mocked(refreshAccessToken).mockRejectedValue({
          isAxiosError: true,
          response: { status: 500 },
        } as unknown as AxiosError)

        const authStore = useAuthStore()

        authStore.$patch({
          hasFirebaseUser: true,
          accessToken: {
            accessToken: 'old-token',
            refreshToken: 'old-token',
          },
        })

        let reset = false
        const { off } = authStore.onSignOut(() => (reset = true))
        const accessToken = await authStore.refreshAccessToken()
        await flushPromises()
        off()
        expect(reset).toBe(false)
        expect(accessToken).toBeNull()
      })
    })
  })

  describe('keyPair handling', () => {
    beforeEach(() => {
      vi.mocked(getKeyPairs).mockClear()
      localStorage.clear()
    })

    it('does not try to fetch keyPair if there is no user', async () => {
      const mock = vi.mocked(getKeyPairs)
      const authStore = useAuthStore()
      authStore.$patch({
        initialized: true,
        accessToken: {
          accessToken: '1234',
          refreshToken: '1234',
        },
      })
      await flushPromises()
      expect(mock).not.toHaveBeenCalled()
    })

    it('does not try to fetch keyPair if there is no accessToken', async () => {
      const mock = vi.mocked(getKeyPairs)
      const authStore = useAuthStore()
      authStore.$patch({
        initialized: true,
        hasFirebaseUser: true,
      })
      await flushPromises()
      expect(mock).not.toHaveBeenCalled()
    })

    it('tries to fetch keyPair', async () => {
      const mock = vi.mocked(getKeyPairs)
      const authStore = useAuthStore()
      authStore.$patch({
        initialized: true,
        hasFirebaseUser: true,
        accessToken: {
          accessToken: '1234',
          refreshToken: '1234',
        },
      })
      await flushPromises()
      expect(mock).toHaveBeenCalledOnce()
    })

    it("does not try to fetch keyPair if it's already in localStorage", async () => {
      localStorage.setItem(
        'auth',
        JSON.stringify({
          accessToken: {
            accessToken: '1234',
            refreshToken: '1234',
          },
          keyPair,
          legacyKeyPair,
        })
      )

      const mock = vi.mocked(getKeyPairs)
      const authStore = useAuthStore()

      authStore.hasFirebaseUser = true
      await flushPromises()

      expect(authStore.keyPair).toEqual(keyPair)
      expect(authStore.legacyKeyPair).toEqual(legacyKeyPair)
      expect(authStore.rsaCryptoKeyPair).not.toBeNull()
      expect(mock).not.toHaveBeenCalled()
    })
  })

  describe('User credential handoff from web to extension', () => {
    it('should sign-in the user and await until complete', async () => {
      const authStore = useAuthStore()
      vi.mocked(updateCurrentUser).mockImplementation((_auth, _user) => {
        // Eventually handoff will get to this state
        authStore.$patch({
          initialized: true,
          firebaseInitialized: true,
          tiligUser: { id: '1', email: 'user@tilig.com' },
          accessToken: { accessToken: '12345', refreshToken: '12345' },
          hasFirebaseUser: true,
        })
        return Promise.resolve()
      })
      expect(authStore.isAuthenticated).toBe(false)

      await authStore.handoff({
        userJson: JSON.stringify({
          id: '12345',
          email: 'fdurst@gmail.com',
        }),
        additionalUserInfo: {
          isNewUser: true,
          profile: {
            given_name: 'Fred',
            family_name: 'Durst',
          },
          providerId: 'google',
        },
      })
      await flushPromises()
      expect(authStore.isAuthenticated).toBe(true)
    })
  })

  describe('legacy state', () => {
    it('signs out users that on init have a keypair and accesstoken persisted, but no user data', async () => {
      const authStore = useAuthStore()
      authStore.$patch({
        initialized: false,
        firebaseInitialized: true,
        keyPair,
        legacyKeyPair,
        accessToken: { accessToken: '12345' },
      })
      authStore.initialize()
      await flushPromises()
      expect(authStore.initialized).toEqual(true)
      expect(authStore.legacyKeyPair).toEqual(null)
      expect(authStore.accessToken).toEqual(null)
      expect(authStore.authState).toEqual(AuthState.NONE)
    })
  })

  describe('initialization', () => {
    it('sets initialized to true', async () => {
      const authStore = useAuthStore()
      authStore.$patch({
        initialized: false,
      })
      authStore.initialize()
      await flushPromises()
      expect(authStore.initialized).toEqual(true)
    })

    it('calls getRedirectResult', async () => {
      const mock = vi.mocked(getRedirectResult)
      const authStore = useAuthStore()
      authStore.$patch({
        initialized: false,
      })
      authStore.initialize()
      await flushPromises()
      expect(mock).toHaveBeenCalled()
    })
  })

  describe('additionalUserInfo handling', () => {
    describe('when there is no redirect result', () => {
      beforeEach(() => {
        vi.mocked(getRedirectResult).mockImplementation((_auth, _resolver) => {
          return Promise.resolve(null)
        })
        vi.mocked(getAdditionalUserInfo).mockClear()
      })

      it('calls getRedirectResult', async () => {
        const mock = vi.mocked(getRedirectResult)
        const authStore = useAuthStore()
        authStore.$patch({
          initialized: false,
        })
        authStore.initialize()
        await flushPromises()
        expect(mock).toHaveBeenCalled()
        expect(authStore.additionalUserInfo).toBeNull()
      })

      it('shows a friendly message when account user tries to login with an existing account with a different provider', async () => {
        const email = 'johndoe@example.com'
        const verifiedProvider = ['microsoft.com']
        const error = {
          code: 'auth/account-exists-with-different-credential',
          customData: {
            _tokenResponse: {
              verifiedProvider,
              email,
            },
          },
        }
        vi.mocked(getRedirectResult).mockImplementation(() => Promise.reject(error))

        const authStore = useAuthStore()
        authStore.$patch({
          initialized: false,
        })
        authStore.initialize()
        await flushPromises()

        expect(authStore.hasAuthenticationError).toBeTruthy()
        expect(authStore.authenticationError?.message).toContain(
          `You already signed up for a Tilig account with ${email}`
        )
      })
    })

    describe('when there is a redirect result', () => {
      beforeEach(() => {
        vi.mocked(getRedirectResult).mockImplementation((_auth, _resolver) => {
          // `getRedirectResult` can be mocked by simply returning true. However,
          // TypeScript doesn't like that because the return type is not a bool.
          // So we'll return a UserCredential object that contains a User instead.
          const user: User = {
            emailVerified: true,
            isAnonymous: false,
            providerData: [],
            refreshToken: 'asdads',
            tenantId: '1',
            metadata: {},
            displayName: 'Henk Broekjes',
            email: 'henk@broekjes.com',
            phoneNumber: null,
            photoURL: 'https://some.photo/of-henk-broekjes',
            providerId: 'google.com',
            uid: '123',
            delete: function () {
              throw new Error('Function not implemented.')
            },
            getIdToken: function () {
              throw new Error('Function not implemented.')
            },
            getIdTokenResult: function () {
              throw new Error('Function not implemented.')
            },
            reload: function () {
              throw new Error('Function not implemented.')
            },
            toJSON: function () {
              return { mocked: true }
            },
          }
          const userCredential: UserCredential | null = {
            providerId: 'google.com',
            user: user,
            operationType: 'signIn',
          }
          return Promise.resolve(userCredential)
        })
        vi.mocked(getAdditionalUserInfo).mockImplementation((_userCredential) => {
          return {
            isNewUser: true,
            profile: {
              given_name: 'Fred',
              family_name: 'Durst',
            },
            providerId: 'google',
          }
        })
      })

      it('handles getAdditionalUserInfo', async () => {
        const mock = vi.mocked(getAdditionalUserInfo)
        const authStore = useAuthStore()
        authStore.$patch({
          initialized: false,
        })
        authStore.initialize()
        await flushPromises()
        expect(mock).toHaveBeenCalled()
        expect(authStore.additionalUserInfo).toEqual({
          isNewUser: true,
          profile: {
            given_name: 'Fred',
            family_name: 'Durst',
          },
          providerId: 'google',
        })
      })
    })
  })

  describe('user profile updating', () => {
    it('does not call the update profile API if there is no additionalUserInfo', async () => {
      const authStore = useAuthStore()
      authStore.$patch({
        initialized: true,
        tiligUser: null,
        accessToken: { accessToken: '12345', refreshToken: '12345' },
        hasFirebaseUser: true,
        additionalUserInfo: null,
      })
      await flushPromises()
      expect(updateUserProfile).not.toHaveBeenCalled()
    })

    it('calls the update profile API if firebase user data and accessToken are present', async () => {
      const firebaseClientMock = vi.mocked(getFirebaseClient)
      firebaseClientMock.mockImplementation(
        () =>
          ({
            auth: {
              currentUser: {
                emailVerified: true,
                email: 'fred@durst.com',
                // this value shouldnt't be used
                providerId: 'firebase',
                // this value shouldnt't be used
                displayName: null,
                // this value shouldnt't be used
                photoURL: null,
                providerData: [
                  {
                    // this value should be used
                    providerId: 'google.com',
                    // this value should be used
                    displayName: 'Fred Durst',
                    // this value should be used
                    photoURL: 'https://www.freddurst.com/photo',
                    email: 'fred@durst.com',
                    uid: '1234',
                    phoneNumber: null,
                  },
                ],
              },
              onAuthStateChanged: vi.fn(),
              signOut: vi.fn(),
            },
          } as unknown as FirebaseTiligClient)
      )

      const authStore = useAuthStore()

      authStore.$patch({
        initialized: true,
        firebaseInitialized: true,
        tiligUser: null,
        accessToken: { accessToken: '12345', refreshToken: '12345' },
        hasFirebaseUser: true,
        additionalUserInfo: {
          isNewUser: true,
          profile: {
            given_name: 'Fred',
            family_name: 'Durst',
            locale: 'de',
          },
          providerId: 'google',
        },
      })

      await flushPromises()

      expect(updateUserProfile).toHaveBeenCalledWith({
        displayName: 'Fred Durst',
        email: 'fred@durst.com',
        familyName: 'Durst',
        givenName: 'Fred',
        locale: 'de',
        photoURL: 'https://www.freddurst.com/photo',
        providerId: 'google.com',
      })
    })
  })

  describe('getters', () => {
    it('isAuthenticated only if there is a full account state', () => {
      const authStore = useAuthStore()
      expect(authStore.isAuthenticated).toBe(false)

      authStore.$patch({
        hasFirebaseUser: true,
        accessToken: { accessToken: '1234', refreshToken: '1234' },
        keyPair,
        legacyKeyPair,
        tiligUser: { id: '1', email: 'user@tilig.com' },
      })
      expect(authStore.isAuthenticated).toBe(true)
    })

    it('isSettled only if all auth values are null or filled', () => {
      const authStore = useAuthStore()

      expect(authStore.isSettled).toBe(true)

      const fullState = {
        hasFirebaseUser: true,
        accessToken: { accessToken: '1234', refreshToken: '1234' },
        keyPair,
        legacyKeyPair,
        tiligUser: { id: '1', email: 'user@tilig.com' },
      }

      authStore.$patch(fullState)
      expect(authStore.isSettled).toBe(true)

      authStore.$patch({ ...fullState, keyPair: null })
      expect(authStore.isSettled).toBe(false)

      authStore.$patch({ ...fullState, keyPair: null, tiligUser: null })
      expect(authStore.isSettled).toBe(false)

      authStore.$patch({ ...fullState, accessToken: null, keyPair: null })
      expect(authStore.isSettled).toBe(false)
    })
  })

  describe('logging out', () => {
    let signedOutCalled = false

    beforeEach(() => {
      vi.mocked(getFirebaseClient)
        .mockImplementation(
          () =>
            ({
              auth: {
                currentUser: null,
                onAuthStateChanged: vi.fn(),
                signOut: vi.fn(() => {
                  signedOutCalled = true
                  return Promise.resolve()
                }),
              },
            } as unknown as FirebaseTiligClient)
        )
        .mockClear()
    })

    it('calls firebase logic', async () => {
      const authStore = useAuthStore()
      await flushPromises()
      await authStore.signOut()
      expect(signedOutCalled).toBe(true)
    })

    it('resets all user info', async () => {
      const authStore = useAuthStore()
      authStore.$patch({
        hasFirebaseUser: true,
        accessToken: { accessToken: '1234', refreshToken: '1234' },
        keyPair,
        legacyKeyPair,
        tiligUser: { id: '1', email: 'user@tilig.com' },
      })
      await flushPromises()
      expect(authStore.accessToken).toBeTruthy()
      expect(authStore.keyPair).toBeTruthy()
      expect(authStore.legacyKeyPair).toBeTruthy()
      expect(authStore.tiligUser).toBeTruthy()
      expect(authStore.rsaCryptoKeyPair).toBeTruthy()

      await authStore.signOut()
      expect(authStore.accessToken).toBeNull()
      expect(authStore.keyPair).toBeNull()
      expect(authStore.legacyKeyPair).toBeNull()
      expect(authStore.tiligUser).toBeNull()
      expect(authStore.rsaCryptoKeyPair).toBeNull()
    })
  })
})
