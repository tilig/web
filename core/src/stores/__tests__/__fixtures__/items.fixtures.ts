import { EncryptedItem, Template } from '@core/clients/tilig'

export const encryptedItems: EncryptedItem[] = [
  {
    id: 'aca798b2-a5f9-49b2-aba3-9b3e6ba34d40',
    android_app_id: null,
    created_at: '2022-10-03T18:15:26.735Z',
    encrypted_dek: null,
    rsa_encrypted_dek: null,
    encrypted_details: null,
    encrypted_overview: null,
    encryption_version: 1,
    name: 'CNN',
    notes: null,
    otp: null,
    password:
      'q/TbburXQLSpy+G+9PZg/7zfWxhN5bN6+gDJgIGj2FZs4jkDjMghcjQIurTRswrSDhP3i69k358CRI9TOWlqAtKp27DKxxOsTshGXkoEWvnCWdiMaJDmzDcRuSDBu9jpd9CDQhvCjDJ4G2R3UJCFmzczMT48GrKdfqkd2ctcDcnPzxQ6TKxpj16FALsAblRGyxOSRLUrqi6VqZbXko/aW80RqeqC9lUV4mmnioPr7A9V8tTZUBUV/SjT64wAqFDglhpUX1J5cZ/BVHAN67eFuHOcVkX4gefwFjI/f8nNP8B0egWvyPJ5SCmg2sgwF5+MIFW4rdhmkiZHwcoAN1DTsQ==',
    template: null,
    updated_at: '2022-10-03T18:15:26.735Z',
    username:
      'pH7IMxAY11W464Dk8wiJbUVwJB+sCH4BPgpoLCQwYQ2NW7oXpHV2a8OSADJX6gfPi3vy5BL8hpvRN9wQl71GCKYjxJOrK+hp8fyzXEZW1+KXufog291OA8tHeWpr69OMpZs7eQ91B0Vo1sncjiluIHVW3OgCkRmrGnxw5PP8nFuTOj4H1dIPXp1/DJzrdURB10BPJntQF1q7JD9Ik7OoyXPCqJYdD1xEzMB/WRJnutvj0Hhu7IhMQq4wn9/BqvL0Z+RgazxV9HlGNSbWJE0OThFINZI15CrabZ39Dq1YgrSDxv4t7BbWgnT8yxuZQ2FoaEDpMt9qI8My7Po7bYPxww==',
    website: 'cnn.com',
    brand: {
      id: '6d323445-2e67-486f-b30c-a98a16ed698d',
      name: 'CNN',
      domain: 'cnn.com',
      public_suffix_domain: 'cnn.com',
      totp: false,
      main_color_hex: '#cc0000',
      main_color_brightness: '43',
      logo_source: 'https://asset.brandfetch.io/idhidc5593/idsiqVrXz6.png',
      logo_icon_source: 'https://asset.brandfetch.io/idhidc5593/idv0JKZqrw.png',
      is_fetched: true,
    },
    share_link: null,
    folder: null,
    encrypted_folder_dek: null,
  },
  {
    id: '09a6aa07-7b69-4cbe-a5c9-4ee77c438cb6',
    android_app_id: null,
    created_at: '2022-10-03T18:13:06.335Z',
    encrypted_dek: null,
    rsa_encrypted_dek:
      'cztD6xB8x/ghkZRoBy7oc+9oB8o5cYIiRXe4PQw5khJ1EQQcQUSQJC88j/1GzqpgjE9A/Mz1lNmOUW1zCXiX+LQsbK1iDG+b6j31mCilpcdUejgHQzvCbPGCPJP+aJJAu62y7orDVOL9I/dME0vlBxdwJiga9+GX4PWPp0XxD5NgDVaFd054qClojXih+tYdK4dM31a4iqxPdccUJycXzydkBk71zlGMK+03cRmg/X3FY4f0mUSf0/JVs4HWrxkvLTq/svcr4nsRW+a3d+wQMDG6eZ4y/gVEmkvFraHtvotEcgdAcXvFUmyoAugHy3hIU5R1ZK4Rp6DhbZTzCyVUxg==',
    encrypted_details:
      'Y6h91gXd205lwZMbXnEgRPqlnwVGUdF5LLlS2mEHOJtzZ_3vqOVhUDQR9f0agcEc1UVWUqnh7dTF3P1RJAi9iO_eRVCAjJb0-ARF6sPo477nxOm7_NgZmX30cVXv9fkMbT7r9p6vHYiUh17yDgW0l9O2yYfbet9odYmkc4cVgVYfqOiRLgnqa_ZId-HCteK5DVi_a_NKQ3Md4tPqRWnBwGfpbqEyVScYfY2xf4d4UKfPaqw',
    encrypted_overview:
      'AoQUL_OfkUVae07mphBT4QMQgjZjF7cN4T0sJqJjdk4yVcEBGkQuKUiz-34JYnJtFmtxqyssbBUCgcI7MUNuKM4XDYb54gVoSrFOk7Qub7hhLDDfLDws6A7v0813H8J1GI0UD0q3X4e2wClFrTbMs40LNnUA1Guauw',
    encryption_version: 2,
    name: 'CNN1',
    notes: null,
    otp: null,
    password:
      'c7SUiKfJnNZ3GFDPVeBTJYhzzrlyu5CCtAi3MHxDGUmjvfPzss5sEn0qHZSCWpqkIDR8xSehiHZrwN4hu1C0Grik0NcaBi57vGzq7mlBbiKmdFFpRQlUId4iWzBk1thKhvClzjcgjtvY8klMSEHbHx1gWNRrIuNdAN52YRhHjtsY4UDEOnJyrKpQAvRDVTGieRCdpMOBdTTwWRxSBGl9LATl3Ncy/co1cWkHxEoZlB3106+8mEG30pSo6UtsR2iFs50s9NGfybl3jl8alJvp/CfecQTQiHSJjTZrN6YjdZV5Sz+v34nWir4+vcz0hfgikSJNqvqDrXKT7hRgEUD34Q==',
    template: Template.Login,
    updated_at: '2022-10-06T01:38:59.859Z',
    username:
      'UBlmQ0z80k/7DRqiQiCxVRqcDC/i5iwyKVRRwLm+AngPavLkurDym0nFoTiFHxqDkwFnOc+HLSedlkic3562qtGz/tv2ekeGItNk9ywAy1OmTVNIGUNd+dTQ/DCXN8fc8Wvp73/fNqppNsjZVBJwPc1Lw9UprdNhBeAnUgLHiIK158ndhVZQg00V7DEcVxxc+Rll0/OBfkfMWeIqOH/Lthn54Vm6semd2Ud2T8yROEsUXHDPItcw18+BlJsyI7/V8MOkAcyDuBuM+/ublQlIR07WM/tygfcMgzgPki74dlO8UZt1Qc/iT7ojIykkOOg+zP7pltHV1IHn+TuAg8u46Q==',
    share_link: null,
    website: 'cnn.com',
    brand: {
      id: '6d323445-2e67-486f-b30c-a98a16ed698d',
      name: 'CNN',
      domain: 'cnn.com',
      public_suffix_domain: 'cnn.com',
      totp: false,
      main_color_hex: '#cc0000',
      main_color_brightness: '43',
      logo_source: 'https://asset.brandfetch.io/idhidc5593/idsiqVrXz6.png',
      logo_icon_source: 'https://asset.brandfetch.io/idhidc5593/idv0JKZqrw.png',
      is_fetched: true,
    },
    folder: null,
    encrypted_folder_dek: null,
  },
  {
    id: 'fc7b6dd0-49f2-4a64-9f0a-112530638bff',
    android_app_id: null,
    created_at: '2022-10-03T18:45:38.184Z',
    rsa_encrypted_dek: null,
    encrypted_dek: null,
    encrypted_details: null,
    encrypted_overview: null,
    encryption_version: 1,
    name: 'disneyplus.com',
    notes: null,
    otp: null,
    password:
      'vOtgiYidnF2CE2HMlkANg5JZITUAo8hYKixFgJ7gcNxbpdRlx2tGKUB0SMjkYVXRmyv2D9jEi0JM389GQgFEN8dprMfNqYoTg6DiXLw1baomSgncAI3j/EthFot/IH6RfCv+Yb4qfZeoQTpcw12lPJiUMBUod/0+kmvETo5IAJ6s0vWJytl5Wxumc0CqV0W/aqZLpXdCzHZfCKEf0X3WbXoS8J8hHBX6/h3BaIN0uWyQh1J6aAN3QjfI0feRSh5ack9UjdbQXgm68ToT3WIkiHVekrc61yA0ME3CAWzMzDy65CcLWu89maJrutiEKqPaLgdBDYl5yCwSi4sZt0BpaA==',
    template: null,
    updated_at: '2022-10-03T18:59:02.517Z',
    username:
      'jboEV/c+BZhLC27BGafcTymKhQmc7ctBj3J0W3vgkJoBiUdY4hGA+mKefcNBdcY5cKV+GOkhsoXL3QA07I9IwmTgQoolnhRlpC91AV00QEMuniZ7xAvhGQ43jrU59cnjRYL7lnGpa0YeKPNGhPuBuA0Wm+1EIMKGYC9iCne3aA2SaZ/rdFuKcFGFoDuDvXDzpBRJLvtZ7tz7xJHevsroHFYkkAoEj11snNZDVwc5hvy2u8+o+Trd9qy7gDkNE5cT8p421GVp/G1KuurEcYwst15K0cHUk9pZuOxS/vAxmeGenHqUSzj8sEBWptc3ROKA3sA9oTtovCcd0laECPdS/w==',
    share_link: null,
    website: 'disneyplus.com',
    brand: {
      id: '3ed01477-1364-4134-aa00-2b9691395350',
      name: 'Disney+',
      domain: 'disneyplus.com',
      public_suffix_domain: 'disneyplus.com',
      totp: false,
      main_color_hex: '#0063e5',
      main_color_brightness: '87',
      logo_source: 'https://asset.brandfetch.io/idhQlYRiX2/idXlvTZp9i.png',
      logo_icon_source: 'https://asset.brandfetch.io/idhQlYRiX2/idRXC5mxqs.png',
      is_fetched: true,
    },
    folder: null,
    encrypted_folder_dek: null,
  },
  {
    id: 'cbd745fd-6c4e-4e8f-be0e-612d29f13796',
    android_app_id: null,
    created_at: '2022-10-03T18:15:37.837Z',
    encrypted_dek: null,
    rsa_encrypted_dek:
      'Y3jnPmmsGn3L4q9XBL34Gpl5SASPpCGUo8wOjvfPNHLwyKaUi11oKhqCpdfkeRB1yREPZjcLlwaVKg1sBHJWru80pUlKEvIF6GG8jSWkh/l2ySPVq8eY/LxR6uKm5cii78OhLwNtXmSGjGUvfld3DzAYGcMUKvOsGBzZxCAZQyb1ItUq8X/syZWu17sdfPic9uFJwbyQtRc7OMjNp+WK7vFQUu4tKxKvOPofpai19MyET75KDgkf2cLtkUCsSnWMBP2VQYI9A4noslvtQjrLyJQMCjQXRLp8cZc9CIs6m1eZKD3QjUBOjdMsWTRZOZRjuCzWI47FEjCHM/Azd5N7QQ==',
    encrypted_details:
      '8R_IfG3rAZsZ_hR8BvAl8-qs74yk6IdmraixJ51cojqL6n3FSmELkwCCic-VKKCxXPxuOZ2L8LxHIqGgtDdz1zOlKgAO0V-nPnUq2ELY5jH3-8_5eKh2k4KWUz9_J3MnQl6fUtpnUkpJgLTO8GpfCmyW95c3A7p3R-Hg0t53MfegII08FDhVp4gpNMxqmvg2FCZMhXgUKGVi8k9RzbELT4__p-QhpIHb',
    encrypted_overview:
      'XOACiR-ffvFBYX16EqVuw6j2MYccK-WSlwvUhCgtM0gqkGYiyBhW2NNkB7n1R8aGL371_xs7LnK_4r05y0luCvziGXCpV77zQ5Aq6qoHZNB1lafqI5tbI_W7Lj1lu9vG-NP6UpEjd5Nj0rIKQCCQ9kPVfwqKoaFHzogO',
    encryption_version: 2,
    name: 'eBay1',
    notes: null,
    otp: null,
    password:
      'VFQQihHW5rLfCe5hB8PlAc/NZ1L0e7iin+9ydCoJsAR2530Zvh6FG/ARjQhfe1Lwb/TfFT/G9F+BcCiQX4SWH551DRp2BTTaEQNJcnZunt1KERBkwza6Tes2ko2dk+qLzsnnLoz7T57s6SMx+gpx7hLPcWFIf1juRIqKTi13eQpMoHfysCaYT1j690gkmud9MSGbolX3vRoN2QnlEpZOD/1MrXzWe5MH3mtH7z1CidXBKte/yG6Z9Awamqay/jAHe/6e+Cc1rm01OLGSxWwq/h7GUgOe+TDnWFWc2WRwSdQgiz/xLwQUmgQsBbTdOwH3DgSrmVtINu75T9o9Qms7Mw==',
    template: Template.Login,
    updated_at: '2022-10-05T18:53:29.690Z',
    username:
      'hFZ78TP29457MgJZXCIfJyoULiqegnR7X8spIWRpn6nHYPE4CQWc5GqQdaAxCCjIzhcqMdgoLfOhKfJuIgdz7bFsphhog+eVezrI6scsb2N0KoWYMdFfKCMAwkYsRQibsdJRMwQPGN8jOMpH8u/Eh9qjl5lPXL98toT1FXHxWoUR6hzeH/cKwhIxCNpV+zIrbsq05WI8eo3sZDkDRyft/QBGGPRBTe9TyBVTiYkYCug3AruE14mKDymeFXXOw42V7clZfuxFXnSi4AZnETpsvE9hQt/9KzxFL6WCG0ABrrrbB2jfEKm8TaM211c1klzS3FS7ZOjc+kBilSCFEeWj8w==',
    share_link: null,
    website: 'ebay.com',
    brand: {
      id: '9fdee511-648c-49d0-af4b-eabdb30a6d87',
      name: 'eBay',
      domain: 'ebay.com',
      public_suffix_domain: 'ebay.com',
      totp: false,
      main_color_hex: '#e53238',
      main_color_brightness: '88',
      logo_source: 'https://asset.brandfetch.io/idkxsB7mZ4/id0RfRY-VT.png',
      logo_icon_source: 'https://asset.brandfetch.io/idkxsB7mZ4/idF1p3hfel.png',
      is_fetched: true,
    },
    folder: null,
    encrypted_folder_dek: null,
  },
  {
    id: '641c578f-ded4-4563-b71c-654b8567aea3',
    android_app_id: null,
    created_at: '2022-10-03T18:15:54.677Z',
    encrypted_dek: null,
    rsa_encrypted_dek: null,
    encrypted_details: null,
    encrypted_overview: null,
    encryption_version: 1,
    name: 'Hulu',
    notes: null,
    otp: null,
    password:
      'BFu6pnvx00SoZzkmAjWYf3rLunmxEgWa6qa6xCOj3R2nKugcZhDoqkz3aPMVTloP5jvao6DorPtJSYRTxRIyue4U6EO9cvz2x5YLuH274MkWEubpGyE2aCvWkim6EjgijF29Hz6seMRfBL2brjgKKFDarFfb2Knm3ZBsvp6OQS9mn51Pkn+aNHIo8AFwFYp9P1Da+E3MOncgZcDg9tbOV7qjFUmpmcNFEmR5Z3T/lYPnj/wX09r2vLlmG2YddeXPJhQsiC/wu2nUgfeudHYrUv6IUMtQPvdxUGgG7BitH7WTD9vVGpVtOrjXPZE3dcXD2lu4otKOjQ/3kSc1QXrQUQ==',
    template: null,
    updated_at: '2022-10-03T18:15:54.677Z',
    username:
      'cKEQNAYe6VCa0yc2zQuJpVsD2qBDKCKiCnv37DvyA10YXl2fT/Y2G4390dPKC5no+t7zJBaKup9kYq8e4Bxx2jt8v/4QVXvUSlilfu9x8LzL3N1DcFQfyktM7qQ8Y/IXi0JEPgK5COGYP0IyplhQme952JfXIVVmTqjrZ7oriKI4S2YbOytqzeHfY/bzDsqqLs0+Z8EHOen9UofTymo31rWIU30XBx/t4oei4SaLFspCz4XqGJqev2JutDvRmFSOfWAWjk/c7YKOwCD+HSNkMVd73v1Xy+taV33uBg9XAb9uKhRMaxYz5RgLLJVyIvyz8g+Che/7Po/DP1jwq5Qxlg==',
    share_link: null,
    website: 'hulu.com',
    brand: {
      id: '1b34488e-386b-4ee5-a584-8cba11ce9c26',
      name: 'Hulu',
      domain: 'hulu.com',
      public_suffix_domain: 'hulu.com',
      totp: false,
      main_color_hex: '#1ce783',
      main_color_brightness: '181',
      logo_source: 'https://asset.brandfetch.io/id4KRloCCr/idjIkUyeHR.png',
      logo_icon_source: 'https://asset.brandfetch.io/id4KRloCCr/idd-YJ2MMm.jpeg',
      is_fetched: true,
    },
    folder: null,
    encrypted_folder_dek: null,
  },
  {
    id: 'c398948c-0ce4-4fcb-a723-a7bdd1df4258',
    android_app_id: null,
    created_at: '2022-10-03T18:16:44.823Z',
    encrypted_dek: null,
    rsa_encrypted_dek: null,
    encrypted_details: null,
    encrypted_overview: null,
    encryption_version: 1,
    name: 'Yahoo',
    notes: null,
    otp: null,
    password:
      'H5WQYZJRGKITKE0C4JaMQ3CSvax1HYkOKzxktK2ax1mjF1DQx0vmwb7jXu7Zsqxgem3NHFwlMq+zYXZz5WghhHD7rRwsy38hIgNgm0oFj6GQRM4mkl28x7BhOVTCNOSFzcOnTcUwO73iXyz5WhjaN2UfSyx1FOwOtgcLtzMtygGyRt/jdX+WFSOAf3jc3nVpl1E0qKg2n3kmgmanQou4jPupuir3LHW5LP7++rAa6a8SCt9kU2w5UI1vFxX0ExzF8qu/4RerKNqOlbcbcK5b/zKa4kHfP9kMGhbqZu0NT3vcMiM5c914PFSVjPtmrMKWeFsrZvVOM2LFXNt+rU9FOw==',
    template: null,
    updated_at: '2022-10-03T18:16:44.823Z',
    username:
      'LvgNnNWBgMyTrQD09pFLo/rzM0AzSPDu8pJq2m3sppcJj1OrSXYCsPAo/8D2ciU6RYIqEd5Fms9ZgGMlsCcFUUL1GepKQaOM6npF6Q4SxZN16Qw46D1quntzRU0Kr96xx1KQBpzsKnGY9VYqH9OMBWlNvDWEJzSCbsxIeJA41ahwFfYjuRAk/YsGqcZcfgj/6pb5UeBUBdA4Y+yyLsZ1jQGvxlebN04MRhgliPrEvZU5vQBYZsDoUw4bNvfdokDBac4dPsgK254kg1u4qCMjj4nkqqNnzF9i73JKw8olvj2NJOWdns6jkPx91Vn9kmjXo7GDQciDViGWSeryK6OSNA==',
    share_link: null,
    website: 'yahoo.com',
    brand: {
      id: '650182f2-6a1c-4859-93cc-51d02c3bb60b',
      name: 'Yahoo!',
      domain: 'yahoo.com',
      public_suffix_domain: 'yahoo.com',
      totp: false,
      main_color_hex: '#4d00ae',
      main_color_brightness: '29',
      logo_source: 'https://asset.brandfetch.io/id6AVuB8Pi/id7ql4PMXb.png',
      logo_icon_source: 'https://asset.brandfetch.io/id6AVuB8Pi/idlC53ugW2.png',
      is_fetched: true,
    },
    folder: null,
    encrypted_folder_dek: null,
  },
  {
    id: 'd30fd6b3-5ce8-42a6-8677-aae7c218e88e',
    android_app_id: null,
    created_at: '2022-10-03T18:16:22.873Z',
    rsa_encrypted_dek: null,
    encrypted_dek: null,
    encrypted_details: null,
    encrypted_overview: null,
    encryption_version: 1,
    name: 'ycombinator.com',
    notes: null,
    otp: null,
    password:
      'WI4nkD7IuXNExQgRm/WNLt0LIUXj048eiTS8nd2ISEBu3gs1C7cwKfP+32gdkUkIDT/6iVhfnRJRMTY4gqzFHY2jnGyHs2Wvcp1wsri/BzxIr+Wsp7FALJp/Yljw/a8Iuw787ZA/G/n+UbRvk5KA60peG4bgP2kBgYVVNQxe/CplUEyG17HB1n4AN5K7Dt9gViNhsspX33bEQ/bD4qoLVTkPwRYHTfVVynrom4KYqhREcpRNCr/S3S0FX7LrM94YDcN+HqrvpDOhkpIyYTUpnX5pTk6GPuAxpDUanpRWm/Uh1MAGsrlPB27FX1lbT/PUdy0Z/rYlZyLkP0s44f0ubg==',
    template: null,
    updated_at: '2022-10-03T18:16:22.873Z',
    username:
      'n0ECfXmGP1ks59hrNfLe3Sw8YWVc0gL/NJo++jwAV58DSzEFZFoOZIXOLRicLMTYa5nYR+b0R1PfLCjjOhCM88jlTnnNejnvjVYH5xzEPAtNaoG6RZgP0G1H7EdVYH8S3ZX26IjEmHJWJtaHcywFFcBxg9OWrnZJ5cp2aHayWp4EDJ7BJsmD5xdVuEJ4Bbcm7H5LPiS6dhEH3Fb0KuOCJ2S9/YT+kcH5JqpYeEK0k4jCJQM8jWxfjNJAoz7ce+4QnHgz7szfoen4hok4+ZSvhztnBwnRLUS8z689GLj3e+n5QQet+0jOEDZ/scegb1Tu9naxy5rLZrO0N1Iksa8ZSg==',
    share_link: null,
    website: 'ycombinator.com',
    brand: {
      id: 'e3905aa6-15ec-4c6e-8754-ba15e6ca0637',
      name: 'Y Combinator',
      domain: 'ycombinator.com',
      public_suffix_domain: 'ycombinator.com',
      totp: false,
      main_color_hex: '#f26522',
      main_color_brightness: '126',
      logo_source: 'https://asset.brandfetch.io/idKhWTXUYD/idDeo_Ni08.png',
      logo_icon_source: 'https://logo.clearbit.com/ycombinator.com',
      is_fetched: true,
    },
    folder: null,
    encrypted_folder_dek: null,
  },
]
