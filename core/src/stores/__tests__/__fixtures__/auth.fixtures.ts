import { KeyPair, LegacyKeyPair } from '@core/clients/tilig'
import { ProviderId, UserProfile } from '@core/clients/tilig/profile'

const authFixture = {
  additionalUserInfo: {
    isNewUser: false,
    providerId: 'google.com',
    profile: {
      name: 'Mark Percival',
      granted_scopes:
        'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email openid',
      id: '110490352083005400967',
      verified_email: true,
      given_name: 'Mark',
      locale: 'en',
      hd: 'tilig.com',
      family_name: 'Percival',
      email: 'mark@tilig.com',
      picture:
        'https://lh3.googleusercontent.com/a/ALm5wu3aBtBsGZtTPsCKw7-jnhCc-rbaLBfyEA4ldtLc=s96-c',
    },
  },
  tiligUser: {
    id: '2045b850-f7e5-4e5d-b0fc-ea5fd652a3a2',
    email: 'mark@tilig.com',
    displayName: 'Mark',
    givenName: 'Mark',
    familyName: 'Percival',
    locale: 'en',
    photoURL:
      'https://lh3.googleusercontent.com/a/AATXAJwZqZDyEIJvzr5AzJsaMkM0JiR3UER0Fz0_zj-S=s96-c',
    providerId: 'google.com' as ProviderId,
    applicationSettings: {
      referral_link: 'squirrel-qfoHKje4XT',
    },
    userSettings: {},
  },
  keyPair: {
    publicKey: 'OiCFvLEeKCHex0YNnQtc5o4CeY2e-Scut9IE5gvKaWY',
    privateKey: 'LLTsyfKDM7h2V9W3d8Bi5TNwab25XOsr4rh5jGEhYOw',
    keyType: 'x25519' as const,
  },
  legacyKeyPair: {
    privateKey:
      '-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDK51eC6tNbStVm4PUhoirdzHy+0t+qWoQRdOJU4UO95obrd/A7fcj4rB0czXGhdR0cFb5Tb07eduSIMbIwURhDacTqf4dt1FuYy0Pa//d/wiov99ZuzTZLxWZCfzAiC6+8SKwfoHuEllGzMTZOXyJfZYPn4FfQ0QUICoEh4nmvc7GQKu9lT1B2iX/VJ9A3TtG4+o02QlBuJU+dQCshqva/RkBYjarHDwpRoH0v124tbukDtrFu5T/DE2aVCptuL6kFxy41mywCJoF/1D+p4Irt9Ga6hJlHS94rCiqpuRGfrdppMwyF3G7eLylxRA8PW38J3s32nQYnDYenPykWt12ZAgMBAAECggEAFKIm74zCtodrIR2iP+vWURzQ2LToAtJWmNHajEgmE+Mj+EEHoH0fmU4jXcD0m7cloSwC4Ugx7ISJo+Ht3WT5wy031aLnWj9cmzKccWJTQF413Q/mcfkTWuw1hHCbq/KlSc/YFRVESkzg/Qh1nc2l2K6hlcxmEbmXueQbkp8Nwc4jMAqo0tpRKElnKqB+P4VyButbmWTt6YnMEIxGXRNS8H+H9focDs+8FtQ+04dc1QCE54yeVQTT2FBhoYvkX4TgoVvWqpDQOVLDbVKNiH5nVuj9+gtJwg6+zNgbz//1g8aNtWmMyywWFTKBpKQNMJoX2HcDN7BWHBDJJqcygNjdAQKBgQDsFMpC0Tq1LqQHc6+beam8t6bNqaMbA4Bu0MK5T13Mss2cX447aiEPJAEIsSZ9Kw6o4C1SZuWK+KUflaVHvjwrfK2zBsKGd38fDc4HlYsFJCTE/NLaSrx25ZJZwSG6glXThV+rnveNuYkpG9/0yJ5ypF+lXOJbOigDZ4+8zdZN2QKBgQDcBe/1N0osIV3HqbehPH6hGcf26UWPtNRo3Jw3Zt+rPJptBfWUs/YISysd6/yUun4jmIBx4vGiHelfS9rfSBzRBBj2LvZY+vQG7KNBPTAsQYewh0XaXIOxbFhP0bmuNLcZMd6zyZvxjbjs8uTZPPOdiSsMJfiYqfxr2/P7txb1wQKBgE7J4XbLPTyahEO4aDMLN4q0AAdDRhwN3x8crOALjNJ0GgrGGUHa+Y2EBpRQCvhbFlll97o4fQMUWIdh+fcnlg0tXwsQhns6BkLm6Iu/bNYaaesUNYqExsEnfOBXhFoqhVpCeNteAmBaO1xs4SFgkwDPutwFU8X+crwwI9hvGcaBAoGAb/JxagHsslFzxsAal2YRQ+S7Mmz0IK8wF/6PWTz1dsnKBMiwcC20c/nTAVFt3TqaLYBNciS3LvYrJHqS7WJijxKXpMK2QwXEnbVPR0VbuljJMk9Rgk3qD58Bu/MIl5Noyd/u4OkmeBvUyHK5b7KsfS4qge7b8skijrqGBD/e5cECgYEA1CBovkirtByRhorxQMQ0EqnrfPHtYqBL+p8OhyoNC8cJkRXW/1xSeUXIa1+w1a6uGjXsElYAgq0uNs6wubgXasNzE3Y+PmbHAaF2QHpS4hPHgfR24zEgj9rDfAelGRnOQi2D2z91w7OFb1GujsdarBZtpuNSORRsJhZOXCZE3sw=\n-----END PRIVATE KEY-----',
    publicKey:
      '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyudXgurTW0rVZuD1IaIq3cx8vtLfqlqEEXTiVOFDveaG63fwO33I+KwdHM1xoXUdHBW+U29O3nbkiDGyMFEYQ2nE6n+HbdRbmMtD2v/3f8IqL/fWbs02S8VmQn8wIguvvEisH6B7hJZRszE2Tl8iX2WD5+BX0NEFCAqBIeJ5r3OxkCrvZU9Qdol/1SfQN07RuPqNNkJQbiVPnUArIar2v0ZAWI2qxw8KUaB9L9duLW7pA7axbuU/wxNmlQqbbi+pBccuNZssAiaBf9Q/qeCK7fRmuoSZR0veKwoqqbkRn63aaTMMhdxu3i8pcUQPD1t/Cd7N9p0GJw2Hpz8pFrddmQIDAQAB\n-----END PUBLIC KEY-----',
  },
  accessToken: {
    accessToken:
      'eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwYjJlMmU2MC1jZDUxLTQ3NWMtYTg5Yi02MTc0ZGUxNGU4MjciLCJ1aWQiOiIyMDQ1Yjg1MC1mN2U1LTRlNWQtYjBmYy1lYTVmZDY1MmEzYTIiLCJleHAiOjE2NjUxMDgyODIsImlzcyI6Imh0dHBzOi8vYXBwLnRpbGlnLmNvbSIsImlhdCI6MTY2NTAyMTg4Mn0.v8_6AavXGk7hadIYa9NLu2G7xs_iFlnabOZPdalKIYg',
    refreshToken:
      'eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIzNDQ2YjM3NC00NzgzLTRjMDctYjI3ZS01MDM0NTY5YWU3NDIiLCJ1aWQiOiIyMDQ1Yjg1MC1mN2U1LTRlNWQtYjBmYy1lYTVmZDY1MmEzYTIiLCJleHAiOjE2OTY1NTc4ODIsImlzcyI6Imh0dHBzOi8vYXBwLnRpbGlnLmNvbSIsImlhdCI6MTY2NTAyMTg4Mn0.JjnyjydPqM22hVakzWJT0NtYzQsesCQ9WJyNehY8eo8',
  },
}

export const tiligUser: UserProfile = authFixture.tiligUser
export const keyPair: KeyPair = authFixture.keyPair
export const legacyKeyPair: LegacyKeyPair = authFixture.legacyKeyPair
