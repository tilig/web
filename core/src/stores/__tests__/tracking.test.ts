import { beforeEach, describe, expect, test, vi } from 'vitest'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

import { useTrackingStore } from '@core/stores/tracking'
import { useAuthStore } from '@core/stores/auth'
import { TrackingEvent } from '@core/utils/trackingConstants'
import { trackAnonymousEvent, trackEvent } from '@core/clients/tilig/tracking'
import { flushPromises } from '@core/utils/tests'

const mockFirebaseClient = {
  auth: {
    currentUser: null,
    onAuthStateChanged: vi.fn(),
    signOut: vi.fn(),
  },
  signInWithApple: vi.fn(),
  signInWithGoogle: vi.fn(),
}

vi.mock('@core/clients/firebase', () => {
  return {
    getFirebaseClient: vi.fn(() => mockFirebaseClient),
  }
})

vi.mock('@core/clients/tilig/tracking', () => {
  return {
    trackEvent: vi.fn(() =>
      Promise.resolve({
        data: null,
      })
    ),
    trackAnonymousEvent: vi.fn(() =>
      Promise.resolve({
        data: null,
      })
    ),
  }
})

describe('Tracking Store', () => {
  beforeEach(() => {
    const app = createApp({})
    const pinia = createPinia()
    pinia.use(piniaPluginPersistedstate)
    app.use(pinia)
    vi.mocked(trackAnonymousEvent).mockClear()
  })

  test('calls the track anonymous event function when the user is unauthenticated', async () => {
    const trackingStore = useTrackingStore()
    trackingStore.trackEvent({ event: TrackingEvent.BROWSER_EXTENSION_INSTALLED })

    await flushPromises()
    expect(vi.mocked(trackAnonymousEvent)).toHaveBeenCalledOnce()
  })

  test('calls the track event function when the user is authenticated', async () => {
    const store = useAuthStore()
    store.$patch({
      accessToken: { accessToken: 'test ' },
      keyPair: { privateKey: '1235' },
      legacyKeyPair: { privateKey: '1234' },
      hasFirebaseUser: true,
      tiligUser: {
        displayName: 'Fred Durst',
      },
    })
    const trackingStore = useTrackingStore()
    trackingStore.trackEvent({ event: TrackingEvent.BROWSER_EXTENSION_INSTALLED })

    await flushPromises()
    expect(vi.mocked(trackEvent)).toHaveBeenCalledOnce()
  })

  test('does not call the track anonymous event when the event passed is not in the list of anonymous events', async () => {
    const trackingStore = useTrackingStore()
    trackingStore.trackAnonymousEvent({
      event: TrackingEvent.ACCOUNT_LIST_VIEWED,
    })

    await flushPromises()
    expect(vi.mocked(trackAnonymousEvent)).toHaveBeenCalledTimes(0)
  })
})
